
CREATE TABLE `tb_enter_car` (
  `id` VARCHAR(64) NOT NULL COMMENT '车辆入场ID',
  `version` VARCHAR(64) COMMENT '版本号',
  `appid` VARCHAR(128)   COMMENT '应用ID',
  `method` VARCHAR(64)  COMMENT '接口方法',
  `rand` VARCHAR(64)   COMMENT '随机字符串',
  `sign` VARCHAR(128)  COMMENT '签名字符串',
  `park_key` VARCHAR(128)   COMMENT '停车场key',
  `car_no` VARCHAR(64)  COMMENT '车牌号',
  `order_no` VARCHAR(128)   COMMENT '停车订单号',
  `enter_time` DATETIME  COMMENT '入场时间',
  `car_type` VARCHAR(64)   COMMENT '车辆类型编码',
  `gate_name` VARCHAR(128)  COMMENT '入口车道名称',
  `operator_name` VARCHAR(64)   COMMENT '入口操作员名称',
  `reserve_order_no` VARCHAR(128)  COMMENT '预约车位订单号',
  `img_url` VARCHAR(1028)  COMMENT '入口车辆图片',
  PRIMARY KEY (`id`),
  UNIQUE INDEX (`order_no`)
) ENGINE=INNODB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='上传车辆入场信息表';

SELECT * FROM tb_enter_car


CREATE TABLE `tb_out_car` (
  `id` VARCHAR(64) NOT NULL COMMENT '车辆入场ID',
  `version` VARCHAR(64) COMMENT '版本号',
  `appid` VARCHAR(128)   COMMENT '应用ID',
  `method` VARCHAR(64)  COMMENT '接口方法',
  `rand` VARCHAR(64)   COMMENT '随机字符串',
  `sign` VARCHAR(128)  COMMENT '签名字符串',
  `park_key` VARCHAR(128)   COMMENT '停车场key',
  `car_no` VARCHAR(64)  COMMENT '车牌号',
  `order_no` VARCHAR(128)   COMMENT '停车订单号',
  `out_time` DATETIME  COMMENT '出场时间',
  `car_type` VARCHAR(64)   COMMENT '车辆类型编码',
  `gate_name` VARCHAR(128)  COMMENT '入口车道名称',
  `operator_name` VARCHAR(64)   COMMENT '入口操作员名称',
  `total_amount` DECIMAL(18,4)  COMMENT '订单总金额',
  `coupon_key` VARCHAR(128)  COMMENT '优惠券GUID',
  `img_url` VARCHAR(1028)  COMMENT '出口车辆图片',
  `coupon_money` DECIMAL(18,4)  COMMENT '优惠券金额',
  `wallet_pay_money` DECIMAL(18,4)  COMMENT '钱包付款金额',
  `free_reason` VARCHAR(128)  COMMENT '免费原因',
  PRIMARY KEY (`id`),
  UNIQUE INDEX (`order_no`)
) ENGINE=INNODB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='上传车辆出场信息表';

SELECT * FROM tb_out_car


CREATE TABLE `tb_enter_car_img` (
  `id` VARCHAR(64) NOT NULL COMMENT '车辆入场ID',
  `version` VARCHAR(64) COMMENT '版本号',
  `appid` VARCHAR(128)   COMMENT '应用ID',
  `method` VARCHAR(64)  COMMENT '接口方法',
  `rand` VARCHAR(64)   COMMENT '随机字符串',
  `sign` VARCHAR(128)  COMMENT '签名字符串',
  `park_key` VARCHAR(128)   COMMENT '停车场key',
  `car_no` VARCHAR(64)  COMMENT '车牌号',
  `order_no` VARCHAR(128)   COMMENT '停车订单号',
  `img_url` VARCHAR(1028)  COMMENT '入口车辆图片',
  PRIMARY KEY (`id`),
  UNIQUE INDEX (`order_no`)
) ENGINE=INNODB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='上传车辆入场图片信息表';

SELECT * FROM tb_enter_car_img

CREATE TABLE `tb_out_car_img` (
  `id` VARCHAR(64) NOT NULL COMMENT '车辆入场ID',
  `version` VARCHAR(64) COMMENT '版本号',
  `appid` VARCHAR(128)   COMMENT '应用ID',
  `method` VARCHAR(64)  COMMENT '接口方法',
  `rand` VARCHAR(64)   COMMENT '随机字符串',
  `sign` VARCHAR(128)  COMMENT '签名字符串',
  `park_key` VARCHAR(128)   COMMENT '停车场key',
  `car_no` VARCHAR(64)  COMMENT '车牌号',
  `order_no` VARCHAR(128)   COMMENT '停车订单号',
  `img_url` VARCHAR(1028)  COMMENT '入口车辆图片',
  PRIMARY KEY (`id`),
  UNIQUE INDEX (`order_no`)
) ENGINE=INNODB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='上传车辆出场图片信息表';

SELECT * FROM tb_out_car_img

CREATE TABLE `tb_MthCar_people` (
  `id` VARCHAR(64) NOT NULL COMMENT 'ID',
  `appid` VARCHAR(128)   COMMENT 'appID',
  `version` VARCHAR(128)   COMMENT '版本号',
  `rand` VARCHAR(128)   COMMENT '随机值',
  `sign` VARCHAR(128)   COMMENT '签名',
  `park_key` VARCHAR(128) COMMENT '车场唯一编号',
  `user_name` VARCHAR(128)   COMMENT '车主姓名',
  `user_no` VARCHAR(128)   COMMENT '人员编号',
  `car_type_no` VARCHAR(128)  COMMENT '车辆类型编码',
  `home_address` VARCHAR(1028)   COMMENT '车位联系人住址',
  `mob_number` VARCHAR(128)  COMMENT '车位负责人联系方式',
  `park_no` VARCHAR(128)   COMMENT '车场车位编号',
  `machine_no` VARCHAR(128)  COMMENT '机号',
  PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='月租车用户信息表';

SELECT * FROM tb_mthcar_people


CREATE TABLE `tb_photograph` (
  `id` VARCHAR(64) NOT NULL COMMENT 'ID',
  `version` VARCHAR(255)   COMMENT '推送方协议版本',
  `cmd` VARCHAR(255)   COMMENT '命令',
  `sequence_no` INT   COMMENT '序号',
  `device_no` VARCHAR(255)   COMMENT '设备（相机）编号',
  `device_sn` VARCHAR(255) COMMENT '设备（相机）序列号',
  `addr_no` VARCHAR(255)   COMMENT '点位编号',
  `addr_name` VARCHAR(255)   COMMENT '点位名称',
  `cap_time` VARCHAR(255)  COMMENT '抓拍时间2017/12/18 16:45:30.003（需要与推送过来的一致，不要重新格式化，一个标点符号都不能错）',
  `is_realtime` INT  COMMENT '实时抓拍标志',
  `match_failed_reson` INT   COMMENT '不通行原因:限制人员：-2,人员过期：-3,不在调度时间：-4,节假日：-5,温度过高：-6,口罩：-7,未带安全帽：-8,卡号未注册：-9,人证不匹配：-10,未授权：-11',
  `match_result` INT  COMMENT '对比结果',

  `sex` VARCHAR(255)   COMMENT '性别（male表示男，female表示女,空字符串表示无此信息）',
  `age` INT   COMMENT '年龄（0表示无此信息）',
  `hat` VARCHAR(255)   COMMENT '是否佩戴安全帽:none：未佩戴安全帽。white：白色安全帽。blue：蓝色安全帽。orange：橙色安全帽。red：红色安全帽。yellow：黄色安全帽',
  `temperatur` DOUBLE(10,2)   COMMENT '体温',
  `has_mask` TINYINT(1) COMMENT '是否佩戴口罩',
  `face_quality` INT   COMMENT '人像质量。（0~100）',
  `turn_angle` INT   COMMENT '扭转角度。头部旋转平面与图片不处于同一平面。取值范围-90~90',
  `rotate_angle` INT  COMMENT '平面旋转角度。头部旋转平面与图像处于同一平面。取值范围-90~90',
  `wg_card_id` INT  COMMENT '32bit韦根协议门禁卡号',
  `long_card_id` BIGINT(20)  COMMENT '64bit韦根协议门禁卡号，wg_card_id字段不存在时，使用此字段作为门禁卡号',

  `number` VARCHAR(255)   COMMENT '身份证编号',
  `name` VARCHAR(255) COMMENT '姓名',
  `birthday` VARCHAR(255)   COMMENT '生日',
  `c_sex` VARCHAR(255)   COMMENT '性别（male：男 female：女）',
  `national` VARCHAR(255)   COMMENT '民族',
  `residence_address` VARCHAR(1024) COMMENT '地址',
  `organ_issue` VARCHAR(255)   COMMENT '签发机关',
  `valid_date_start` VARCHAR(255)   COMMENT '有效期起始时间',
  `valid_date_end` VARCHAR(255)   COMMENT '有效期截止时间',

  `is_encryption` TINYINT(1)  COMMENT '是否加密(加密后姓名、ID字段为AES加密后的二进制数据转base64,其余字段未进行加密)',
  `person_id` VARCHAR(255)  COMMENT '人员ID',
  `person_name` VARCHAR(255)  COMMENT '人员姓名',
  `person_role` INT COMMENT '人员角色',
  `format` VARCHAR(255)  COMMENT '注册图像（模板图片）格式',
  `image` TEXT  COMMENT '注册图像（模板图片）数据（二进制数据对应base64）',
  `origin` VARCHAR(255)  COMMENT '人员注册来源:none：无此信息。software：注册软件注册。cluster：无感录入注册。 sync：云同步注册。unknown：未知',
  `person_attr` VARCHAR(255)  COMMENT '人员名单有效属性,none：无此信息permanent_list：永久有效名单temporary_list：临时有效名单invalid_list：永久无效名单 unknown：未知',
  `customer_text` VARCHAR(1024)  COMMENT '用户自定义文本内容,可用于相机TTS播放、LCD显示，也可作为用户平台自定义',

  `overall_pic_flag` TINYINT(1)  COMMENT '全景图像标志（是否包含全景图像）',
  `o_format` VARCHAR(255)   COMMENT '图像格式',
  `o_data` TEXT COMMENT '图像数据（二进制数据对应base64）',
  `o_face_x` INT   COMMENT '人脸位置位于全景图X坐标',
  `o_face_y` INT   COMMENT '人脸位置位于全景图Y坐标',
  `o_face_width` INT  COMMENT '人脸在全景图中宽度',
  `o_face_height` INT  COMMENT '人脸在全景图中高度',

  `closeup_pic_flag` TINYINT(1)  COMMENT '特写图像标志（是否包含特写图像）',
  `c_format` VARCHAR(255)   COMMENT '图像格式',
  `c_data` TEXT COMMENT '图像数据（二进制数据对应base64）',
  `c_face_x` INT   COMMENT '人脸位置位于特写图X坐标',
  `c_face_y` INT   COMMENT '人脸位置位于特写图Y坐标',
  `c_face_width` INT  COMMENT '人脸在特写图中宽度',
  `c_face_height` INT  COMMENT '人脸在特写图中高度',

  `video_flag` TINYINT(1)  COMMENT '视频标志（是否包含视频）',
  `v_start_time` VARCHAR(255)  COMMENT '视频起始时间2017/12/18 16:45:30.003（需要与推送过来的一致，不要重新格式化，一个标点符号都不能错）',
  `v_end_time` VARCHAR(255)  COMMENT '视频结束时间2017/12/18 16:45:30.003（需要与推送过来的一致，不要重新格式化，一个标点符号都不能错）',
  `v_format` VARCHAR(255)   COMMENT '视频格式(avi/mp4)',
  `v_data` TEXT   COMMENT '视频数据（二进制数据对应base64）',
  PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='考勤抓拍信息表';

SELECT * FROM tb_photograph


CREATE TABLE `tb_park_space` (
  `id` VARCHAR(64) NOT NULL COMMENT 'ID',
  `appid` VARCHAR(128)   COMMENT 'appID',
  `version` VARCHAR(128)   COMMENT '版本号',
  `method` VARCHAR(128)   COMMENT '接口方法',
  `rand` VARCHAR(128)   COMMENT '随机值',
  `sign` VARCHAR(128)   COMMENT '签名',
  `park_key` VARCHAR(128) COMMENT '车场唯一编号',
  `remainder_spaces` VARCHAR(128)   COMMENT '空闲车位数',
  `total_spaces` VARCHAR(128)   COMMENT '总车位数',
  PRIMARY KEY (`id`),
  UNIQUE INDEX (`rand`),
  UNIQUE INDEX (`park_key`)
) ENGINE=INNODB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='车场车位数信息表';

SELECT * FROM tb_park_space