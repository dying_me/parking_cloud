package com.cygsunri.common;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * 配置常量类
 *
 */
@Data
@Accessors(chain = true)
@Configuration
@ConfigurationProperties(prefix = "constant")
@PropertySource("classpath:configConstant.properties")
public class ConfigConstant {
    //空气传感器
    private String airSensor;

    //安科瑞电表
    private String ankerui;

    //网关
    private String tuyagateway;

    //涂鸦-人体感应传感器
    private String tuyahumanbodydetection;

    //门窗传感器
    private String tuyamcs;

    //4位情景开关
    private String tuyasceneswitch4;

    //6位情景开关
    private String tuyasceneswitch6;

    //智能插座
    private String tuyasmartsocket;

    //温湿度传感器
    private String tuyaTHTB;

    //涂鸦-通断器-zigbee
    private String tuyazigbee;

    //雅达M1电表
    private String yadaM1;

    //雅达M23电表
    private String yadaM23;

    //雅达水表
    private String yadawater;

    //亚禾AC360空调控制器
    private String yaheac360;
}
