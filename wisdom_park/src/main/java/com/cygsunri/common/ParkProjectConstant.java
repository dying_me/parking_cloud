package com.cygsunri.common;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * 停车场常量类
 *
 */
@Data
@Accessors(chain = true)
@Configuration
@ConfigurationProperties(prefix = "parkproject")
@PropertySource("classpath:configConstant.properties")
public class ParkProjectConstant {

    //全部停车场
    private String all;

    //路内停车场
    private String in;

    //路外停车场
    private String outside;

    //一类路内停车场
    private String onein;

    //二类路内停车场
    private String twoin;

    //三类路内停车场
    private String threein;
}
