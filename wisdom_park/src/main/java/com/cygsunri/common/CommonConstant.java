package com.cygsunri.common;

/**
 * 公共常量类（非业务关联）
 */
public class CommonConstant {
    public final static String DATEFORMAT_24HOURS = "yyyy-MM-dd HH:mm:ss";
    public final static String DATEFORMAT_ONLYDATE = "yyyy-MM-dd";
    public static final String USER_KEY_CACHE_PREFIX = "user:key:";
    public static final String APP_ID = "ym2fa27c03057f63fa"; //智慧停车appId
    public static final String APP_SECRET = "6b06b431c0354110a7d812004b8b5006"; //智慧停车appSecret
    public static final String APP_VERSION = "v1.0"; //智慧停车appVersion
}
