package com.cygsunri.common;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * 涂鸦配置常量类
 *
 */
@Data
@Accessors(chain = true)
@Configuration
@ConfigurationProperties(prefix = "tuya")
@PropertySource("classpath:configConstant.properties")
public class TuYaConstant {

    private String accessId;
    private String accessSecret;
}
