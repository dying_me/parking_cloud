package com.cygsunri.common;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * 亚禾配置常量类
 *
 */
@Data
@Accessors(chain = true)
@Configuration
@ConfigurationProperties(prefix = "yahe")
@PropertySource("classpath:configConstant.properties")
public class YaHeConstant {

//    //亚禾：批次号
//    private String sq;
    //亚禾：账号
    private String mobile;
    //亚禾：密码
    private String password;
    //亚禾：服务器地址
    private String url;
    //亚禾：根据账号，查询出当前账号下的所有设备
    private String apiDeviceList;
    //亚禾：控制单台空调控制器发送红外
    private String infrared;
    //亚禾：查询单台控制器的设备数据
    private String apiDeviceInfo1;
    //亚禾：查询账号下某个类型设备的所有数据
    private String apiDeviceInfo2;
    //亚禾：查询单台AC360类型设备某个月份的用电时长和用电量
    private String apiGetPowerData1;
    //亚禾：查询账号下AC360类型设备某个月份的用电时长和用电量
    private String apiGetPowerData2;
    //亚禾：查询账号下 各个分组的总用电量和用电时长
    private String apiGetPowerData3;
    //亚禾：单台AC360设备的参数设置
    private String apiControl;
}
