package com.cygsunri.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.experimental.Accessors;
import org.springframework.context.annotation.Configuration;

@Data
@Accessors(chain = true)
@Configuration
public class EnumConstant {

    /**
     * 错误码和错误信息定义类
     * 错误码定义规则为5为数字:前两位表示业务场景，最后三位表示错误码。例如：10001。10:通用 000:系统未知异常
     * @param
     * @return
     */
    @Getter
    @AllArgsConstructor
    public enum BizCodeEnume {
        UNKNOW_EXCEPTION(10000,"系统未知异常"),
        VAILD_EXCEPTION(10001,"参数格式校验失败");

        private final int value;
        private final String name;
    }

    /**
     *车辆类型字典值枚举
     */
    @Getter
    public enum StatusEnum {
        DISABLE_A("3651", "临时车A"),
        DISABLE_B("3650", "临时车B"),
        DISABLE_C("3649", "临时车C"),
        DISABLE_D("3648", "临时车D"),
        DISABLE_E("3652", "月租车A"),
        DISABLE_F("3653", "月租车B"),
        DISABLE_G("3654", "月租车C"),
        DISABLE_H("3655", "月租车D"),
        DISABLE_I("3661", "月租车E"),
        DISABLE_J("3662", "月租车F"),
        DISABLE_K("3663", "月租车G"),
        DISABLE_L("3664", "月租车H"),
        DISABLE_M("3656", "免费车"),
        DISABLE_N("3657", "储值车"),
        DISABLE_O("3660", "超时收费");

        private String code;
        private String desc;

        StatusEnum(String code, String desc) {
            this.code = code;
            this.desc = desc;
        }

        public static String getDescFromCode(String code) {
            EnumConstant.StatusEnum[] values = EnumConstant.StatusEnum.values();
            for (EnumConstant.StatusEnum value : values) {
                if (value.getCode().equals(code)) {
                    return value.getDesc();
                }
            }
            return null;
        }
    }

    /**
     * 返回结果
     * <p>Company:cygsunri</p>
     * @author gaoyang
     * @date 2021-06-07
     */
    @Getter
    @AllArgsConstructor
    public static enum CallBackResult {
        SUCCESS("1","成功"),
        FAILURE("0","失败");

        private final String value;
        private final String name;
    }

    /**
     * 公共类型
     * <p>Company:cygsunri</p>
     * @author gaoyang
     * @date 2020-12-8
     */
    @Getter
    @AllArgsConstructor
    public static enum CommonConstant {
        SELECT_ALL("all","无条件限制全部查找");

        private final String value;
        private final String name;
    }

    /**
     * 区域类别
     * <p>Company:cygsunri</p>
     * @author gaoyang
     * @date 2021-01-25
     */
    @Getter
    @AllArgsConstructor
    public static enum ParkingFeeClass {
        NO_CLASS(0,"无类别"),
        ONE_CLASS(1,"一类"),
        TWO_CLASS(2,"二类"),
        THREE_CLASS(3,"三类"),
        NOUSE(-1,"不使用区域类别");

        private final Integer value;
        private final String name;
    }

    /**
     * 车场类型
     */
    @Getter
    @AllArgsConstructor
    public enum type {
        /**
         * 虚拟
         */
        virtual(0,"虚拟"),
        /**
         * 路内
         */
        in(1,"路内"),
        /**
         * 路外
         */
        outside(2,"路外");

        private final Integer value;
        private final String name;
    }


    /**
     * 订单支付状态
     */
    @Getter
    @AllArgsConstructor
    public enum tradeStatus {
        /**
         * 已支付
         */
        havepaid(0,"已支付"),
        /**
         * 未支付
         */
        notpaid(1,"未支付");

        private final Integer value;
        private final String name;
    }

    /**
     * 支付类型
     */
    @Getter
    @AllArgsConstructor
    public enum payType {
        /**
         * 现金
         */
        cash(0,"现金"),
        /**
         * 微信
         */
        wxpay(1,"微信"),
        /**
         * 支付宝
         */
        alipay(2,"支付宝");

        private final Integer value;
        private final String name;
    }


    /**
     * 订单来源
     */
    @Getter
    @AllArgsConstructor
    public enum sourceType {
        /**
         * 扫码
         */
        qrcode(0,"扫码"),
        /**
         * APP
         */
        app(1,"APP"),
        /**
         * 小程序
         */
        miniapp(2,"小程序");

        private final Integer value;
        private final String name;
    }
    /**
     * 泊位状态
     */
    @Getter
    @AllArgsConstructor
    public enum spotStatus {
        /**
         * 空闲
         */
        IDLE_MODE(0,"空闲"),
        /**
         * 占用
         */
        OCCUPY_MODE(1,"占用");

        private final Integer value;
        private final String name;
    }


    /**
     * 涂鸦消息队列协议号
     */
    @Getter
    @AllArgsConstructor
    public enum tuyaProtocol {
        /**
         * 设备数据上报事件
         */
        DATA_REPORTED(4,"设备数据上报事件"),
        /**
         * 设备变更
         */
        EQUIPMENT_CHANGES(20,"设备变更"),
        /**
         * 户外告警事件
         */
        WARNNING(40,"户外告警事件");

        private final Integer value;
        private final String name;
    }


    /**
     * ctwing电信平台消息类型
     */
    @Getter
    @AllArgsConstructor
    public enum ctwingMessageType {
        /**
         * 设备数据变化
         */
        dataReport("dataReport","设备数据变化"),
        /**
         * 设备命令响应
         */
        commandResponse("commandResponse","设备命令响应"),
        /**
         * 设备命令响应
         */
        eventReport("eventReport","设备命令响应"),
        /**
         * 设备上下线
         */
        deviceOnlineOfflineReport("deviceOnlineOfflineReport","设备上下线"),
        /**
         * TUP合并数据变化
         */
        dataReportTupUnion("dataReportTupUnion","TUP合并数据变化");

        private final String value;
        private final String name;
    }

    /**
     * 保存数据厂家
     */
    @Getter
    @AllArgsConstructor
    public enum saveSource {
        /**
         * 涂鸦
         */
        tuya("tuya","涂鸦");

        private final String value;
        private final String name;
    }

    /**
     * 涂鸦人体存在传感器存在状态
     */
    @Getter
    @AllArgsConstructor
    public enum tuya_humanbody_detection_presence_state {
        /**
         * none
         */
        none("none",1),
        /**
         * presence
         */
        presence("presence",2),
        /**
         * peaceful
         */
        peaceful("peaceful",3),
        /**
         * small_move
         */
        small_move("small_move",4),
        /**
         * large_move
         */
        large_move("large_move",5),
        /**
         * Exist
         */
        Exist("Exist",6),
        /**
         * fallow
         */
        fallow("fallow",7),
        /**
         * active
         */
        active("active",8);

        private final String name;
        private final Integer value;
    }



    /**
     * 涂鸦字段类型
     */
    @Getter
    @AllArgsConstructor
    public enum tuyaDataType {
        /**
         * none
         */
        Enum("Enum",true),
        /**
         * presence
         */
        Integer("Integer",false),
        /**
         * Boolean
         */
        Boolean("Boolean",true),
        /**
         * Raw
         */
        Raw("Raw",true),
        /**
         * String
         */
        String("String",true);

        private final String name;
        //是否需要处理
        private final Boolean value;
    }


    /**
     * 涂鸦智能通断器-上电状态
     */
    @Getter
    @AllArgsConstructor
    public enum tuya_zigbee_relay_status {
        /**
         * power_off
         */
        power_off("power_off",1),
        /**
         * power_on
         */
        power_on("power_on",2),
        /**
         * last
         */
        last("last",3);

        private final String name;
        private final Integer value;
    }

    /**
     * 涂鸦6位场景开关-场景 1
     */
    @Getter
    @AllArgsConstructor
    public enum tuya_scene_switch6_scene_1 {
        /**
         * scene
         */
        scene("scene",1);

        private final String name;
        private final Integer value;
    }

    /**
     * 涂鸦6位场景开关-场景 2
     */
    @Getter
    @AllArgsConstructor
    public enum tuya_scene_switch6_scene_2 {
        /**
         * scene
         */
        scene("scene",1);

        private final String name;
        private final Integer value;
    }

    /**
     * 涂鸦6位场景开关-场景 3
     */
    @Getter
    @AllArgsConstructor
    public enum tuya_scene_switch6_scene_3 {
        /**
         * scene
         */
        scene("scene",1);

        private final String name;
        private final Integer value;
    }

    /**
     * 涂鸦6位场景开关-场景 4
     */
    @Getter
    @AllArgsConstructor
    public enum tuya_scene_switch6_scene_4 {
        /**
         * scene
         */
        scene("scene",1);

        private final String name;
        private final Integer value;
    }

    /**
     * 涂鸦6位场景开关-场景 5
     */
    @Getter
    @AllArgsConstructor
    public enum tuya_scene_switch6_scene_5 {
        /**
         * scene
         */
        scene("scene",1);

        private final String name;
        private final Integer value;
    }

    /**
     * 涂鸦6位场景开关-场景 6
     */
    @Getter
    @AllArgsConstructor
    public enum tuya_scene_switch6_scene_6 {
        /**
         * scene
         */
        scene("scene",1);

        private final String name;
        private final Integer value;
    }

    /**
     * 涂鸦4位场景开关-场景 1
     */
    @Getter
    @AllArgsConstructor
    public enum tuya_scene_switch4_scene_1 {
        /**
         * scene
         */
        scene("scene",1);

        private final String name;
        private final Integer value;
    }

    /**
     * 涂鸦4位场景开关-场景 2
     */
    @Getter
    @AllArgsConstructor
    public enum tuya_scene_switch4_scene_2 {
        /**
         * scene
         */
        scene("scene",1);

        private final String name;
        private final Integer value;
    }

    /**
     * 涂鸦4位场景开关-场景 3
     */
    @Getter
    @AllArgsConstructor
    public enum tuya_scene_switch4_scene_3 {
        /**
         * scene
         */
        scene("scene",1);

        private final String name;
        private final Integer value;
    }

    /**
     * 涂鸦4位场景开关-场景 4
     */
    @Getter
    @AllArgsConstructor
    public enum tuya_scene_switch4_scene_4 {
        /**
         * scene
         */
        scene("scene",1);

        private final String name;
        private final Integer value;
    }

    /**
     * 涂鸦网关-主机状态
     */
    @Getter
    @AllArgsConstructor
    public enum tuya_gateway_master_state {
        /**
         * normal
         */
        normal("normal",1),
        /**
         * alarm
         */
        alarm("alarm",1);

        private final String name;
        private final Integer value;
    }

}
