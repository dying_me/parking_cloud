package com.cygsunri.request;

import lombok.Data;

import java.io.Serializable;

/**
 * @author hanyuan
 * @date 2021/8/02 10:53
 * @description 分页查询入参
 */
@Data
public class BasePageQuery implements Serializable {
    private static final long serialVersionUID = 3218110991633277589L;

    /**
     * 当前页
     */
    protected int pageNumber;
    /**
     * 每页个数
     */
    protected int pageSize;
}
