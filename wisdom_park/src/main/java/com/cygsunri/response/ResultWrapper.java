package com.cygsunri.response;


import org.apache.commons.lang.StringUtils;

import java.io.Serializable;

/**
 * @param <T> 泛型
 * @author lhy
 * @date 2020-06-28 15:16
 * @description 统一返回
 */
public class ResultWrapper<T> implements Serializable {
    private static final long serialVersionUID = -553694757969021783L;
    private static final int SUCCESS_CODE = ResultCodeEnum.SYS_SUCCESS.getCode();
    private static final String SUCCESS_MESSAGE = ResultCodeEnum.SYS_SUCCESS.getDesc();
    private static final int ERROR_CODE = ResultCodeEnum.SYS_ERROR.getCode();
    private static final String ERROR_MESSAGE = ResultCodeEnum.SYS_ERROR.getDesc();

    private int code;
    private String message;
    private T data;

    private ResultWrapper() {
    }

    private ResultWrapper(int code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    private ResultWrapper(int code, String message) {
        this.code = code;
        this.message = message;
    }

    private ResultWrapper(int code, T data) {
        this.code = code;
        this.data = data;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    /**
     * 操作成功 - 无数据返回
     *
     * @return ResultWrapper
     */
    public static <T> ResultWrapper<T> ok() {
        return new ResultWrapper<>(ResultWrapper.SUCCESS_CODE, ResultWrapper.SUCCESS_MESSAGE);
    }

    /**
     * 操作成功 - 自定义提示信息
     *
     * @param message 提示信息
     * @return ResultWrapper
     */
    public static <T> ResultWrapper<T> ok(String message) {
        return new ResultWrapper<>(ResultWrapper.SUCCESS_CODE, message);
    }


    /**
     * 操作成功
     *
     * @param message 提示信息
     * @param data    数据
     * @param <T> t
     * @return ResultWrapper
     */
    public static <T> ResultWrapper<T> ok(String message, T data) {
        String msg = StringUtils.isBlank(message) ? ResultWrapper.SUCCESS_MESSAGE : message;
        return new ResultWrapper<>(ResultWrapper.SUCCESS_CODE, msg, data);
    }

    /**
     * 操作失败 - 自定义提示信息
     *
     * @param message 失败信息
     * @return ResultWrapper
     */
    public static <T> ResultWrapper<T> error(String message) {
        return new ResultWrapper<>(ResultWrapper.ERROR_CODE, StringUtils.isBlank(message) ? ResultWrapper.ERROR_MESSAGE : message);
    }

    /**
     * 操作失败 - 自定义异常
     *
     * @param e 异常
     * @return ResultWrapper
     */
    public static <T> ResultWrapper<T> error(Exception e) {
        return new ResultWrapper<>(ResultWrapper.ERROR_CODE, e.getMessage());
    }

    /**
     * 操作失败 - 自定义code与提示信息
     *
     * @param code    异常码
     * @param message 失败信息
     * @return ResultWrapper
     */
    public static <T> ResultWrapper<T> error(int code, String message) {
        return new ResultWrapper<>(code, message);
    }

    /**
     * 操作失败 - 自定义code与提示信息和错误数据
     *
     * @param message 提示信息
     * @param data    数据
     * @param <T> t
     * @return ResultWrapper
     */
    public static <T> ResultWrapper<T> error(int code,String message, T data) {
        return new ResultWrapper<>(code, message, data);
    }
}
