package com.cygsunri.response;

/**
 * @author lhy
 * @date 2020-06-28 15:08
 * 统一状态码
 * <p>
 * 1****:请求异常码
 * 2****:成功码
 * 3****:业务异常码
 * 4****:接口异常
 * 5****:系统异常码
 */
public enum ResultCodeEnum {
    /**
     * 1****请求异常
     */
    PARAM_NOT_EXIST(10001, "参数不存在,请核实"),
    PARAM_VALIDATE_ERROR(10002, "参数验证失败,请核实"),


    /**
     * 2****操作成功
     */
    SYS_SUCCESS(20000, "操作成功"),

    /**
     * 3****业务异常
     */
    USER_LOGIN_FAILED(30000, "用户登录失败"),
    SIGN_IN_NO_ACCESS(30001, "用户无权限访问"),
    USER_IDENTIFY_FAILED(30002, "用户认证失败"),
    USER_OPERATE_FAILED(30003, "用户操作失败"),
    USER_IDENTIFY_NAME_FAILED(30004, "用户校验失败"),

    /**
     * 4****接口异常
     */
    BUSINESS_ERROR(40000, "系统内部错误"),
    DATA_EXIST(40001, "数据已存在,请核实"),
    DATA_ERROR(40002,"数据错数,请核实!"),
    DATA_NOT_EXIST(40003,"数据不存在,请核实!"),
    SIGN_NOT_PASS(40004,"不合法签名,请核实!"),
    IDEMPOTENT_NOT_PASS(40005,"不符合幂等性,请核实!"),
    /**
     * 5****系统异常
     */
    SYS_ERROR(50000, "操作失败");

    private Integer code;
    private String desc;

    ResultCodeEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public Integer getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}
