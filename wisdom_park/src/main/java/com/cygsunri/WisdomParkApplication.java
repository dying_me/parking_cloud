package com.cygsunri;

import com.cygsunri.common.ConfigConstant;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableDiscoveryClient
@MapperScan({"com.cygsunri.wisdompark.*.mapper"})
@EnableConfigurationProperties({ConfigConstant.class})
@EnableScheduling
public class WisdomParkApplication {
    public static void main(String[] args)
    {
        SpringApplication.run(WisdomParkApplication.class, args);
    }

}
