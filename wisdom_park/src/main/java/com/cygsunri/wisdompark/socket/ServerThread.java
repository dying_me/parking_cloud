package com.cygsunri.wisdompark.socket;

import com.cygsunri.wisdompark.util.Byte16Util;
import com.cygsunri.wisdompark.util.HexAsciiUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;

import java.io.*;
import java.net.Socket;

/*
 * 服务器线程处理类
 */
@Slf4j
@PropertySource("classpath:configConstant.properties")
public class ServerThread extends Thread {
    // 和本线程相关的Socket
    Socket socket = null;
    DataInputStream din;
    DataOutputStream dout;
    boolean assciiToHex;
    public ServerThread(Socket socket,boolean assciiToHex) {
        this.socket = socket;
        this.assciiToHex = assciiToHex;
    }

    //线程执行的操作，响应客户端的请求
    public void run() {
        InputStream is = null;
        InputStreamReader isr = null;
        BufferedReader br = null;
        OutputStream os = null;
        PrintWriter pw = null;

        DataInputStream din = null;
        try {
            //获取输入流，并读取客户端信息
            is = socket.getInputStream();
            isr = new InputStreamReader(is);
            br = new BufferedReader(isr);
            String info ="";
            int num = 0;
            char ch;
//            while ((info = br.readLine())!=null) {//循环读取客户端的信息
//            while ((num = br.read()) != -1 ){
//                ch = (char) num;
////                System.out.println(br.ready());
//                info += ch;
////                System.out.println("client：" + ch);
//                boolean a = br.ready();
//                if(!a){
//                    if(assciiToHex){
//                        info = HexAsciiUtil.hexToAscii(info);
//                    }
//                    log.info(socket.getInetAddress().getHostAddress()+":"+info);
//                    info = "";
//                }
//            }
            byte[] bytes = new byte[1024];
            int length = 0;
            while ((length = is.read(bytes))!=-1){
                byte[] b1 = new byte[length];
                System.arraycopy(bytes, 0, b1, 0, length);
                String a = Byte16Util.toHexString(b1);
                log.error("socket service accept to {}:{} ", socket.getInetAddress().getHostAddress(),a);
            }
//            byte []b =new byte[1024];//采用byte数组 按字节进行数据的接收  避免readline()方法的阻塞机制！！ 弊端是接受字节长度受限，但是在这个项目中足够了
//            int x= is.read(b, 0, b.length);//相较于readline（）读取数据  字节读取慢  不适合大量数据的通信
//            String info= new String(b, 0, x);
//            if(info != null) {//循环读取客户端的信息
//                System.out.println("client：" + info);
//            }
            socket.shutdownInput();//关闭输入流
//            //获取输出流，响应客户端的请求
//            os = socket.getOutputStream();
//            pw = new PrintWriter(os);
//            pw.write("ok\n");
//            pw.flush();//调用flush()方法将缓冲输出
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            //关闭资源
            try {
                if (pw != null)
                    pw.close();
                if (os != null)
                    os.close();
                if (br != null)
                    br.close();
                if (isr != null)
                    isr.close();
                if (is != null)
                    is.close();
                if (socket != null)
                    socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
}