package com.cygsunri.wisdompark.socket;

import com.cygsunri.wisdompark.util.Byte16Util;
import com.cygsunri.wisdompark.util.HexAsciiUtil;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;

@Slf4j
public class SendMessThread extends Thread {
    // 和本线程相关的Socket
    String message = null;
    Socket socket = null;
    OutputStream out = null;
    public SendMessThread(Socket socket,String message) {
        this.socket = socket;
        this.message = message;
    }

    @Override
    public void run() {
        super.run();
        try {
            if (socket != null) {
                out = socket.getOutputStream();
                byte[] bytes = Byte16Util.toByteArray(message);
                out.write(bytes);
                out.flush();// 清空缓存区的内容
                log.error("socket service send to {}:{}",socket.getInetAddress().getHostAddress()+":"+socket.getPort(),message);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
