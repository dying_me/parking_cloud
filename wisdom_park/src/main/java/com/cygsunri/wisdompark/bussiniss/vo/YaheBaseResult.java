package com.cygsunri.wisdompark.bussiniss.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * <p>
 * 亚禾回调类响应结果基础Bean
 * </p>
 *
 * @author gaoyang
 * @since  2021-08-02 16:45
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class YaheBaseResult {

    /**
     * ERR-失败、OK-成功
     */
    private String result;

    /**
     * 批次号
     */
    private String sq;

}
