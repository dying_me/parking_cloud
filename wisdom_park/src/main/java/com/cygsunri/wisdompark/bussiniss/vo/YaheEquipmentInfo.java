package com.cygsunri.wisdompark.bussiniss.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * <p>
 * 亚禾设备信息Bean
 * </p>
 *
 * @author cygsunri
 * @since 2021-08-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class YaheEquipmentInfo {

    /**
     * 设备名称
     */
    private String name;

    /**
     * 设备类型
     */
    private String model;
    /**
     * 设备标识
     */
    private String airid;

}
