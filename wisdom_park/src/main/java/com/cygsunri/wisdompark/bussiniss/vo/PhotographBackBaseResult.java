package com.cygsunri.wisdompark.bussiniss.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;



/**
 * <p>
 * 抓拍数据回调推送响应结果基础Bean
 * </p>
 *
 * @author cygsunri
 * @since 2021-06-04
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class PhotographBackBaseResult {

    /**
     * 自定义msg
     */
    private String reply;
    /**
     * 指令
     */
    private String cmd;
    /**
     * 应答码
     */
    private Integer code;
    /**
     * 序号（需要与推送过来的一致）
     */
    private Integer sequence_no;
    /**
     * 抓拍时间（需要与推送过来的一致，不要重新格式化，一个标点符号都不能错）
     */
    private String cap_time;
    /**
     * 闸机控制（可选），在线模式下使用
     */
    //private Gateway_ctrl gateway_ctrl;
    /**
     * 需要播放的语音（可选）
     */
    //private Tts tts;
    /**
     * 需要在屏幕上显示的文字（可选）
     */
    //private Text_display text_display;

}
