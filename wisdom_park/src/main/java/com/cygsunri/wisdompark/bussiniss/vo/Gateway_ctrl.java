package com.cygsunri.wisdompark.bussiniss.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 闸机控制（可选），在线模式下使用
 * </p>
 *
 * @author cygsunri
 * @since 2021/8/24
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class Gateway_ctrl {

    /**
     * 设备类型，gpio：io设备 wiegand：韦根设备
     */
    private String device_type;
    /**
     * 设备类型为gpio时，此字段表示io编号
     * 设备类型为wiegand时，此字段表示韦根卡号，
     */
    private Integer device_no;
    /**
     * 韦根协议门禁卡号（长整型），wg36、wg44、wg66协议时使用此字段
     * 使用此字段时，device_no须赋为0
     */
    private Long long_card_id;
    /**
     * 控制模式，test: 测试模式 force: 强制模式 scene: 场景模式
     * 测试模式：用于测试，不产生开闸信息记录；
     * 强制方式：输出信号将强制输出到设备；
     * 场景模式：是否输出信号到设备取决于设备是否使能；
     */
    private String ctrl_mode;
    /**
     * 人员id，仅在线模式使用，用于产生开闸纪录
     */
    private String person_id;
}
