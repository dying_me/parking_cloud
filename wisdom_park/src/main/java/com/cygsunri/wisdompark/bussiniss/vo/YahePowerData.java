package com.cygsunri.wisdompark.bussiniss.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * <p>
 * 亚禾设备用电时长和用电量
 * </p>
 *
 * @author cygsunri
 * @since 2021-08-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class YahePowerData {

    /**
     * 设备名称
     */
    private String name;

    /**
     * 用电量
     */
    private String usedele;
    /**
     * 用电时长
     */
    private String usedtime;

}
