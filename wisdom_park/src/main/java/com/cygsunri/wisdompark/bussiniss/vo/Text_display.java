package com.cygsunri.wisdompark.bussiniss.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 需要在屏幕上显示的文字（可选）
 * </p>
 *
 * @author cygsunri
 * @since 2021/8/24
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class Text_display {

    /**
     * 文字显示位置
     */
    private Position position;
    /**
     * 文字存在时间 单位ms
     */
    private Integer alive_time;
    /**
     * 字体大小
     */
    private Integer font_size;
    /**
     * 文字间隔
     */
    private Integer font_spacing;
    /**
     * 文字颜色 格式ARGB
     */
    private String font_color;
    /**
     * 文字内容
     */
    private String text;
}
