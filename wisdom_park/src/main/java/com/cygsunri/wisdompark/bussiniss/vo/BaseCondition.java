package com.cygsunri.wisdompark.bussiniss.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;


/**
 * <p>
 * 回调类响应结果基础Bean
 * </p>
 *
 * @author cygsunri
 * @since 2021-06-04
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class BaseCondition {

    /**
     * 应用ID
     */
    @NotBlank(message = "应用ID不能为空")
    private String appid;

    /**
     * 版本号
     */
    @NotBlank(message = "版本号不能为空")
    private String version;

    /**
     * 接口方法
     */
    @NotBlank(message = "接口方法不能为空")
    private String method;

    /**
     * 随机字符串
     */
    @NotBlank(message = "随机字符串不能为空")
    private String rand;

    /**
     * 签名字符串
     */
    @NotBlank(message = "签名字符串不能为空")
    private String sign;

}
