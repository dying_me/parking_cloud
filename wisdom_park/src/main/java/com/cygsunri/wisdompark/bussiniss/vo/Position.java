package com.cygsunri.wisdompark.bussiniss.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 文字显示位置
 * </p>
 *
 * @author cygsunri
 * @since 2021/8/24
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class Position {
    private int x;
    private int y;
}
