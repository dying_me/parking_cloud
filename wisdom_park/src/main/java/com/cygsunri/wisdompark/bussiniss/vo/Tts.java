package com.cygsunri.wisdompark.bussiniss.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 需要播放的语音（可选）
 * </p>
 *
 * @author cygsunri
 * @since 2021/8/24
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class Tts {

    /**
     * 语音的文字（必填，UTF8编码）,长度不超过64字节
     */
    private String text;
}
