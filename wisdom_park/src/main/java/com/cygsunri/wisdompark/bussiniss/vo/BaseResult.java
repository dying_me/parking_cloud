package com.cygsunri.wisdompark.bussiniss.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * <p>
 * 回调类响应结果基础Bean
 * </p>
 *
 * @author cygsunri
 * @since 2021-06-04
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class BaseResult {

    /**
     * 返回状态值：1-表示成功，其他表示失败
     */
    private String code;

    /**
     * 错误描述
     */
    private String msg;

    /**
     * 随机字符串
     */
    private String rand;

    /**
     * 签名字符串
     */
    private String sign;

}
