package com.cygsunri.wisdompark.bussiniss.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author hanyuanLiu
 * @site 回调类响应结果基础Bean
 * @create 2021-06-07 14:16
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class BaseDataResult extends BaseResult{

    private Object data;
}
