package com.cygsunri.wisdompark.bussiniss.vo;

import com.cygsunri.common.CommonConstant;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;


/**
 * <p>
 * 通知类响应结果基础Bean
 * </p>
 *
 * @author cygsunri
 * @since 2021-06-04
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class BaseQueryNoticeCondition {

    /**
     * 应用ID
     */
    private String appid = CommonConstant.APP_ID;

    /**
     * 版本号
     */
    private String version = CommonConstant.APP_VERSION;

    /**
     * 随机字符串
     */
    private String rand;

    /**
     * 签名字符串
     */
    private String sign;

}
