package com.cygsunri.wisdompark.util;

import java.math.BigInteger;
import java.text.NumberFormat;

public class NumberUtil {


    /**
     * 16进制转10进制
     * @throws Exception
     */
    public static int hex16To10(String strHex){
        BigInteger ingNum = new BigInteger(strHex,16);
        return  ingNum.intValue();
    }

    /**
     * 10进制转16进制
     * @throws Exception
     */
    public static String hex10To16(Integer numb){
        String hex= Integer.toHexString(numb);
        return hex;
    }

    /**
     * 16进制转10进制浮点型
     * @throws Exception
     */
    public static Float hex16To10Float(String strHex){
        Float value = Float.intBitsToFloat(Integer.valueOf(strHex.trim(), 16));
        return  value;
    }

    /**
     * 10进制浮点型转16进制
     * @throws Exception
     */
    public static String hex10FloatTo16(Float numb){
        return Integer.toHexString(Float.floatToIntBits(numb));
    }




    /**
     * i除以10的size方求小数
     * @throws Exception
     */
    public static String intdDivide(int i,int size){
        double divisor = Math.pow(10,size);
        float num= (float)i/(float) divisor;
        NumberFormat nt = NumberFormat.getInstance();
        // 设置精确到小数点后size位
        nt.setMinimumFractionDigits(size);
        nt.setMaximumFractionDigits(size);
        return nt.format((float)i/(float) divisor);
    }

}
