package com.cygsunri.wisdompark.util;

import com.cygsunri.common.CommonConstant;
import org.apache.commons.lang3.StringUtils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class TransformerUtil {

    /**
     * 将java.util.Date 格式转换为固定格式字符串格式<br>
     * 如'yyyy-MM-dd HH:mm:ss'(24小时制)
     * @param time Date 日期
     * @param formatType String 格式
     * @return String 字符串
     */
    public static String dateToStringWithFormat(Date time, String formatType){
        String type = CommonConstant.DATEFORMAT_24HOURS;
        if (StringUtils.isNotBlank(formatType)) {
            type = formatType;
        }
        SimpleDateFormat formatter;
        formatter = new SimpleDateFormat (type);
        String ctime = formatter.format(time);
        return ctime;
    }
    /**
     *取系统当前时间:返回只值为如下形式
     * @param formatType String 格式
     * @return String
     */
    public static String nowDateStr(String formatType){
        String type = CommonConstant.DATEFORMAT_24HOURS;
        if (StringUtils.isNotBlank(formatType)) {
            type = formatType;
        }
        return dateToStringWithFormat(new Date(), type);
    }
    /**
     * 将yyyyMMddHHmmss-->yyyy-MM-dd HH:mm:ss<br>
     * @param changeTime
     * @return String 字符串
     */
    public static String strDateFormatTransform(String changeTime){
        changeTime = changeTime.replaceAll("(\\d{4})(\\d{2})(\\d{2})(\\d{2})(\\d{2})(\\d{2})", "$1-$2-$3 $4:$5:$6");
        return changeTime;
    }

    /**
     * 将yyyy-MM-dd HH:mm:ss-->yyyyMMddHHmmss<br>
     * @param changeTime
     * @return String 字符串
     */
    public static String strDateFormatTransformOnlyNum(String changeTime){
        changeTime = changeTime.replaceAll("-", "").replace(" ","").replace(":","");
        return changeTime;
    }

    /**
     * 获取昨天
     * @return
     */
    public static String getYesterday()
    {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        SimpleDateFormat formatter = new SimpleDateFormat (CommonConstant.DATEFORMAT_ONLYDATE);
        String yesterday = formatter.format(cal.getTime());
        return yesterday;
    }

    /**
     * 获取今天
     * @return
     */
    public static String getToday()
    {
        Calendar cal = Calendar.getInstance();
//        cal.add(Calendar.DATE, -1);
        SimpleDateFormat formatter = new SimpleDateFormat (CommonConstant.DATEFORMAT_ONLYDATE);
        String yesterday = formatter.format(cal.getTime());
        return yesterday;
    }

    /**
     * 获取一周前
     * @return
     */
    public static String getLastWeek()
    {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -7);
        SimpleDateFormat formatter = new SimpleDateFormat (CommonConstant.DATEFORMAT_ONLYDATE);
        String yesterday = formatter.format(cal.getTime());
        return yesterday;
    }
}
