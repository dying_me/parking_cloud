package com.cygsunri.wisdompark.util.id;

/**
 * Id 生成接口
 *
 * @author lhy
 * @date 2020/09/22
 **/
public interface IdGenerator {

    /**
     * 获取ID
     *
     * @return id值
     */
    long nextId();
}
