package com.cygsunri.wisdompark.util;

import com.cygsunri.common.EnumConstant;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import java.lang.reflect.Method;
import java.math.BigInteger;
import java.util.Base64;

public class EnumUtil {

    public static Object getValueByClassName(String className, String enumName,String name) throws Exception {
        //根据反射获取常量类
        Class c2 = Class.forName(className);
        //获取常量类中的所有内部类
        Class innerClazz[] = c2.getDeclaredClasses();
        //遍历内部内
        for (Class class1 : innerClazz) {
            //判断类是不是枚举类
            if (class1.isEnum()) {
                //反射获取枚举类
                Class<Enum> clazz = (Class<Enum>) Class.forName(className+"$"+enumName);
                //根据方法名获取方法
                Method getValue = clazz.getMethod("getValue");
                //获取所有枚举实例
                Enum[] enumConstants = clazz.getEnumConstants();
                for (Enum enum1 : enumConstants) {
                    //得到枚举实例名
                    String name2 = enum1.name();
                    if(name2.equals(name)){
                        //执行枚举方法获得枚举实例对应的值
                        Object invoke = getValue.invoke(enum1);
                        return invoke;
                    }
                }
            }
        }
        return null;
    }
}
