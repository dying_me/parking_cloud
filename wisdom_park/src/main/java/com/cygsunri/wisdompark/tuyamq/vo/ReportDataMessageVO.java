package com.cygsunri.wisdompark.tuyamq.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 数据上报事件消息
 * @date: 2019/3/26 下午5:36
 */
@Data
public class ReportDataMessageVO implements Serializable {


    private String devId;
    private String productKey;
    private String dataId;
    private List<ReportDataStatuDataVO> status;

}