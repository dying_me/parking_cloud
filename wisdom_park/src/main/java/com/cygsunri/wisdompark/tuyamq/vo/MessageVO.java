package com.cygsunri.wisdompark.tuyamq.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * @author: bilahepan
 * @date: 2019/3/26 下午5:36
 */
@Data
public class MessageVO implements Serializable {


    private String data;
    private Integer protocol;
    private String pv;
    private String sign;
    private Long t;

}