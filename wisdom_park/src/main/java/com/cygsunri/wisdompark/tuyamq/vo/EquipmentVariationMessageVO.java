package com.cygsunri.wisdompark.tuyamq.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.Map;

/**
 * 设备变更事件消息
 * @date: 2019/3/26 下午5:36
 */
@Data
public class EquipmentVariationMessageVO implements Serializable {


    private String devId;
    private String productKey;
    private String bizCode;
    private Map bizData;
    private long ts;

}