package com.cygsunri.wisdompark.tuyamq.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * 数据上报事件消息
 * @date: 2019/3/26 下午5:36
 */
@Data
public class ReportDataStatuDataVO implements Serializable {


    private String code;
    private Long t;
    private String value;

}