package com.cygsunri.wisdompark.callback.vo;

import com.cygsunri.wisdompark.bussiniss.vo.BaseCondition;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author hanyuanLiu
 * @site 创建无牌车入场订单vo
 * @create 2021-06-07 14:37
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class UnVehicleEnterConditionVO extends BaseCondition {

    /**
     * 车场唯一编号
     */
    private String parkKey;

    /**
     * 车道控制机道
     */
    private String ctrlNo;

    /**
     * 虚拟车牌号
     */
    private String carNo;
}
