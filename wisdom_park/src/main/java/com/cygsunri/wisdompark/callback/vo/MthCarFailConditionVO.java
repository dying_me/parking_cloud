package com.cygsunri.wisdompark.callback.vo;

import com.cygsunri.wisdompark.bussiniss.vo.BaseCondition;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * <p>
 * 回调类月租车注销信息Bean
 * </p>
 *
 * @author cygsunri
 * @since 2021-06-04
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class MthCarFailConditionVO extends BaseCondition {

    /**
     * 月租车注销信息
     */
    private MthCarFailInfo data;

}
