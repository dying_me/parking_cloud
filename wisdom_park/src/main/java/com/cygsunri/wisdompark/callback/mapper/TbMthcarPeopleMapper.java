package com.cygsunri.wisdompark.callback.mapper;

import com.cygsunri.wisdompark.callback.entity.TbMthcarPeople;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cygsunri.wisdompark.callback.vo.MthcarPeopleQueryDTO;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 月租车用户信息表 Mapper 接口
 * </p>
 *
 * @author hanyuan
 * @since 2021-08-18
 */
@Repository
public interface TbMthcarPeopleMapper extends BaseMapper<TbMthcarPeople> {

    List<TbMthcarPeople> getPageMthCarPersons(MthcarPeopleQueryDTO mthcarPeopleQueryDTO);
}
