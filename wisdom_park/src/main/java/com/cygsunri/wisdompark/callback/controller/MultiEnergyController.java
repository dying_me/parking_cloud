package com.cygsunri.wisdompark.callback.controller;

import com.cygsunri.wisdompark.callback.service.DeviceTemplateDatasService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 运维API
 *
 *
 */

@RestController
@RequestMapping("/api/v2")
public class MultiEnergyController {

    @Autowired
    private DeviceTemplateDatasService deviceTemplateDataService;
//    @Autowired
//    private DeviceTemplateService deviceTemplateDataService;
//
//    /**
//     * 获取设备实时数据
//     * @param deviceId
//     * @return
//     */
//    @GetMapping(value = "/measurement/get/values")
//    public ResponseEntity<Map<String, List<RealTimeDataBean>>> getValuesByDevice(@RequestParam("deviceId") String deviceId) {
//
//        Map<String, List<RealTimeDataBean>> map = new HashMap<>();
//
//        DeviceTemplate deviceTemplate = psrTemplateMappingService.getDeviceTemplateByMapping(deviceId);
//        if (deviceTemplate == null) {
//            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
//        }
//
//        List<DeviceTemplateData> deviceTemplateDatas = deviceTemplateDataService.getDeviceTemplateDataListByDeviceTemplateId(deviceTemplate.getId());
//
//        List<DeviceTemplateData> yxList = deviceTemplate.getDeviceTemplateByType(deviceTemplateDatas, 1);
//        List<DeviceTemplateData> ycList = deviceTemplate.getDeviceTemplateByType(deviceTemplateDatas, 2);
//        List<DeviceTemplateData> ymList = deviceTemplate.getDeviceTemplateByType(deviceTemplateDatas, 3);
//
//        List<RealTimeDataBean> yxData = getRealTimeDataBean(yxList, "yx", deviceId);
//        List<RealTimeDataBean> ycData = getRealTimeDataBean(ycList, "yc", deviceId);
//        List<RealTimeDataBean> ymData = getRealTimeDataBean(ymList, "ym", deviceId);
//
//        map.put("yx", yxData);
//        map.put("yc", ycData);
//        map.put("ym", ymData);
//        return new ResponseEntity<>(map, HttpStatus.OK);
//
//    }
}
