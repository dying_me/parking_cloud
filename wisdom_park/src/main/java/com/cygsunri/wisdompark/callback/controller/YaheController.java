package com.cygsunri.wisdompark.callback.controller;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.cygsunri.common.YaHeConstant;
import com.cygsunri.wisdompark.bussiniss.vo.YaheBaseDataResult;
import com.cygsunri.wisdompark.callback.entity.PSRTemplateMapping;
import com.cygsunri.wisdompark.callback.service.PSRTemplateService;
import com.cygsunri.wisdompark.callback.vo.*;
import com.cygsunri.wisdompark.util.DateUtil;
import com.cygsunri.wisdompark.util.WebUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 亚禾平台接口调用
 * 前端控制器
 * </p>
 *
 * @author cygsunri
 * @since 2021-06-04
 */
@Slf4j
@Controller
@RequestMapping("/api/v2/airCondition/")
public class YaheController {

    @Autowired
    private YaHeConstant yaHeConstant;

    @Autowired
    private PSRTemplateService psrTemplateMappingService;

    /**
     * 根据账号，查询出当前账号下的所有设备
     *
     * @param
     * @return
     */
    @RequestMapping(value = "/apiDeviceList", method = RequestMethod.GET)
    public ResponseEntity<String> apiDeviceList() {
        try {
            String sq = String.valueOf(System.currentTimeMillis());
            Map paramMap = new HashMap();
            paramMap.put("sq",sq);
            paramMap.put("mobile",yaHeConstant.getMobile());
            paramMap.put("password",yaHeConstant.getPassword());
            String result = WebUtils.get(yaHeConstant.getUrl()+yaHeConstant.getApiDeviceList(),paramMap);
            log.info(result);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(e.getMessage(), HttpStatus.OK);
        }
    }


    /**
     * 控制单台空调控制器发送红外
     *
     * @param
     * @return
     */
    @RequestMapping(value = "/infrared", method = RequestMethod.GET)
    public ResponseEntity<String> infrared() {
        try {
            String sq = String.valueOf(System.currentTimeMillis());
            Map paramMap = new HashMap();
            paramMap.put("sq",sq);
            paramMap.put("airid","11217");
            paramMap.put("conoff",0);
            paramMap.put("cmode",0);
            paramMap.put("ctemp",16);
            paramMap.put("cwind",0);
            paramMap.put("cwinddir",0);
            String result = WebUtils.get(yaHeConstant.getUrl()+yaHeConstant.getInfrared(),paramMap);
            log.info(result);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(e.getMessage(), HttpStatus.OK);
        }
    }

    /**
     * 查询单台控制器的设备数据
     *
     * @param
     * @return
     */
    @RequestMapping(value = "/apiDeviceInfo1", method = RequestMethod.GET)
    public ResponseEntity<String> apiDeviceInfo1() {
        try {
            String sq = String.valueOf(System.currentTimeMillis());
            Map paramMap = new HashMap();
            paramMap.put("sq",sq);
            paramMap.put("mobile",yaHeConstant.getMobile());
            paramMap.put("airid","11217");
            String result = WebUtils.get(yaHeConstant.getUrl()+yaHeConstant.getApiDeviceInfo1(),paramMap);
            log.info(result);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(e.getMessage(), HttpStatus.OK);
        }
    }

    /**
     * 查询账号下某个类型设备的所有数据
     *
     * @param
     * @return
     */
    @RequestMapping(value = "/apiDeviceInfo2", method = RequestMethod.GET)
    public ResponseEntity<String> apiDeviceInfo2() {
        try {
            String sq = String.valueOf(System.currentTimeMillis());
            Map paramMap = new HashMap();
            paramMap.put("sq",sq);
            paramMap.put("model","AC360");
            paramMap.put("mobile",yaHeConstant.getMobile());
            paramMap.put("password",yaHeConstant.getPassword());
            String result = WebUtils.get(yaHeConstant.getUrl()+yaHeConstant.getApiDeviceInfo2(),paramMap);
            log.info(result);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(e.getMessage(), HttpStatus.OK);
        }
    }

    /**
     * 查询单台AC360类型设备某个月份的用电时长和用电量
     *
     * @param
     * @return
     */
    @RequestMapping(value = "/apiGetPowerData1", method = RequestMethod.GET)
    public ResponseEntity<String> apiGetPowerData1() {
        try {
            String sq = String.valueOf(System.currentTimeMillis());
            Map paramMap = new HashMap();
            paramMap.put("sq",sq);
            paramMap.put("date","202101");
            paramMap.put("airid","11217");
            paramMap.put("mode",2);
            String result = WebUtils.get(yaHeConstant.getUrl()+yaHeConstant.getApiGetPowerData1(),paramMap);
            log.info(result);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(e.getMessage(), HttpStatus.OK);
        }
    }

    /**
     * 查询账号下AC360类型设备某个月份的用电时长和用电量
     *
     * @param
     * @return
     */
    @RequestMapping(value = "/apiGetPowerData2", method = RequestMethod.GET)
    public ResponseEntity<String> apiGetPowerData2() {
        try {
            String sq = String.valueOf(System.currentTimeMillis());
            Map paramMap = new HashMap();
            paramMap.put("sq",sq);
            paramMap.put("mobile",yaHeConstant.getMobile());
            paramMap.put("password",yaHeConstant.getPassword());
            paramMap.put("date","202101");
            paramMap.put("mode",2);
            String result = WebUtils.get(yaHeConstant.getUrl()+yaHeConstant.getApiGetPowerData2(),paramMap);
            log.info(result);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(e.getMessage(), HttpStatus.OK);
        }
    }


    /**
     * 查询账号下 各个分组的总用电量和用电时长
     *
     * @param
     * @return
     */
    @RequestMapping(value = "/apiGetPowerData3", method = RequestMethod.GET)
    public ResponseEntity<String> apiGetPowerData3() {
        try {
            String sq = String.valueOf(System.currentTimeMillis());
            Map paramMap = new HashMap();
            paramMap.put("sq",sq);
            paramMap.put("mobile",yaHeConstant.getMobile());
            paramMap.put("password",yaHeConstant.getPassword());
            String result = WebUtils.get(yaHeConstant.getUrl()+yaHeConstant.getApiGetPowerData3(),paramMap);
            log.info(result);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(e.getMessage(), HttpStatus.OK);
        }
    }

    /**
     * 单台AC360设备的参数设置
     *
     * @param
     * @return
     */
    @RequestMapping(value = "/apiControl", method = RequestMethod.GET)
    public ResponseEntity<String> apiControl() {
        try {
            String sq = String.valueOf(System.currentTimeMillis());
            Map paramMap = new HashMap();
            paramMap.put("sq",sq);
            paramMap.put("airid","11217");
            paramMap.put("model","AC360");
            String result = WebUtils.get(yaHeConstant.getUrl()+yaHeConstant.getApiControl(),paramMap);
            log.info(result);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(e.getMessage(), HttpStatus.OK);
        }
    }

    private List<YaheRealTimeDataVO> getRealTimeData(YaheSingleAC360InfoVO yaheSingleAC360InfoVO) {
        List<YaheRealTimeDataVO> yaheRealTimeDataVOList = new ArrayList<>();
        DecimalFormat df = new DecimalFormat("0.00");
        YaheRealTimeDataVO tempData = new YaheRealTimeDataVO();
        tempData.setName("temperature");
        tempData.setDescInfo("温度");
        tempData.setValue(Float.parseFloat(yaheSingleAC360InfoVO.getCtemp()));
        tempData.setUnit("℃");
        YaheRealTimeDataVO humData = new YaheRealTimeDataVO();
        humData.setName("humidity");
        humData.setDescInfo("湿度");
        humData.setValue(Float.parseFloat(yaheSingleAC360InfoVO.getHumidity()));
        humData.setUnit("%");
        YaheRealTimeDataVO illumData = new YaheRealTimeDataVO();
        illumData.setName("Illuminance");
        illumData.setDescInfo("光照度");
        illumData.setValue(Float.parseFloat(yaheSingleAC360InfoVO.getPhotositive()));
        illumData.setUnit("%");
        YaheRealTimeDataVO avaData = new YaheRealTimeDataVO();
        avaData.setName("availableKwh");
        avaData.setDescInfo("可用电量");
        avaData.setValue(Float.parseFloat(yaheSingleAC360InfoVO.getUsablele()));
        avaData.setUnit("度");
        YaheRealTimeDataVO volData = new YaheRealTimeDataVO();
        volData.setName("voltage");
        volData.setDescInfo("电压");
        volData.setValue(Float.parseFloat(yaheSingleAC360InfoVO.getAvol()));
        volData.setUnit("V");
        YaheRealTimeDataVO aeleData = new YaheRealTimeDataVO();
        aeleData.setName("voltage");
        aeleData.setDescInfo("电流");
        aeleData.setValue(Float.parseFloat(yaheSingleAC360InfoVO.getAele()));
        aeleData.setUnit("V");
        YaheRealTimeDataVO powerData = new YaheRealTimeDataVO();
        powerData.setName("power");
        powerData.setDescInfo("功率");
        powerData.setValue(Float.parseFloat(yaheSingleAC360InfoVO.getTotalpow()));
        powerData.setUnit("W");
        YaheRealTimeDataVO accData = new YaheRealTimeDataVO();
        accData.setName("accumulateKwh");
        accData.setDescInfo("累计用电");
        accData.setValue(Float.parseFloat(yaheSingleAC360InfoVO.getUsedele()));
        accData.setUnit("kWh");
        YaheRealTimeDataVO eleData = new YaheRealTimeDataVO();
        eleData.setName("accumulateKwh");
        eleData.setDescInfo("用电时长");
        eleData.setValue(Float.parseFloat(yaheSingleAC360InfoVO.getUsedtime()));

        return yaheRealTimeDataVOList;
    }

}
