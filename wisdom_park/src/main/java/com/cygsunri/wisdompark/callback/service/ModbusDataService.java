package com.cygsunri.wisdompark.callback.service;

import com.cygsunri.wisdompark.callback.entity.*;

import java.util.List;

/**
 * <p>
 * 寄存器信息 服务类
 * </p>
 */
public interface ModbusDataService {

    /**
     * 通过制造厂商，设备型号获取寄存器信息
     *
     * @param manufacturer  制造厂商
     * @param model 设备型号
     * @return ModbusData
     */
    List<ModbusData> findDataByModel(String manufacturer,String model);
}
