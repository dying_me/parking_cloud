package com.cygsunri.wisdompark.callback.vo;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;


/**
 * <p>
 * 车辆入出场图片信息Data Bean
 * </p>
 *
 * @author cygsunri
 * @since 2021-06-07
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class EnterOutImgInfo {

    /**
     * 停车场key
     */
    @NotBlank(message = "停车场key不能为空")
    @JSONField(ordinal = 1,name="key")
    public String key;

    /**
     * 车牌号
     */
    @NotBlank(message = "车牌号不能为空")
    @JSONField(ordinal = 2,name="carNo")
    public String carNo;

    /**
     * 停车订单号
     */
    @NotBlank(message = "停车订单号不能为空")
    @JSONField(ordinal = 3,name="orderNo")
    public String orderNo;

    /**
     * 入口\出口车辆图片
     */
    @NotBlank(message = "入口-出口车辆图片不能为空")
    @JSONField(ordinal = 4,name="imgUrl")
    public String imgUrl;

}
