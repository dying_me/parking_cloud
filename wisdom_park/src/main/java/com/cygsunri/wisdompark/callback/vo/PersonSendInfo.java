package com.cygsunri.wisdompark.callback.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author hanyuanLiu
 * @site 上传新增人事信息
 * @create 2021-06-07 11:21
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class PersonSendInfo {
    /**
     * 停车场key
     */
    public String key;

    /**
     * 车牌号
     */
    public String carNo;

    /**
     * 用户编号
     */
    public String userNo;

    /**
     * 用户名称
     */
    public String userName;

    /**
     * 性别
     */
    public String sex;

    /**
     * 家庭住址
     */
    public String homeAddress;

    /**
     * 手机号码
     */
    public String mobNumber;

    /**
     * 车辆个数
     */
    public String carSpalcesNum;

}
