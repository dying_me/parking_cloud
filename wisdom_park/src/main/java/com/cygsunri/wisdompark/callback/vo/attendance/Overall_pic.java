package com.cygsunri.wisdompark.callback.vo.attendance;

import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * <p>
 * 全景图像（全景图像标志为true）
 * </p>
 *
 * @author cygsunri
 * @since 2021/8/23
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class Overall_pic {
    /**
     * 图像格式
     */
    private String format;
    /**
     * 图像数据（二进制数据对应base64）
     */
    private String data;
    /**
     * 人脸位置位于全景图X坐标
     */
    private Integer face_x;
    /**
     * 人脸位置位于全景图Y坐标
     */
    private Integer face_y;
    /**
     * 人脸在全景图中宽度
     */
    private Integer face_width;
    /**
     * 人脸在全景图中高度
     */
    private Integer face_height;
}
