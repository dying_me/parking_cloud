package com.cygsunri.wisdompark.callback.service;

import com.cygsunri.wisdompark.callback.entity.DeviceInfo;
import com.cygsunri.wisdompark.callback.entity.DeviceTemplateData;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 */
public interface DeviceTemplateDatasService {

    /**
     * 通过设备ID查找模板信息
     *
     * @param psrID  设备ID
     * @param
     * @return DeviceTemplateData
     */
    List<DeviceTemplateData> findDataByPsrID(String psrID);

    /**
     * 通过设备code查找设备信息
     *
     * @param devCode  设备ID
     * @param
     * @return DeviceTemplateData
     */
    DeviceInfo findDeviceByDevCode(String devCode);
}
