package com.cygsunri.wisdompark.callback.vo.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author hanyuanLiu
 * 车辆进出场记录大数据导出
 */
@Data
public class ExportInoutRecordsModel implements Serializable {
    private static final long serialVersionUID = -7019332980553047752L;

    @Excel(name = "序列", width=12.5)
    private Integer id ;

    @Excel(name = "车牌号", width=20, orderNum = "1")
    private String carNo;

    private String carType;

    @Excel(name = "车辆类型", width=20, orderNum = "2")
    private String carTypeStatus;

    @Excel(name = "订单号", width=20, orderNum = "3")
    private String orderNo;

    @Excel(name = "入场时间", width=20, orderNum = "4")
    private String enterTime;

    @Excel(name = "出场时间", width=20, orderNum = "5")
    private String outTime;

    @Excel(name = "订单金额", width=20, orderNum = "6")
    private BigDecimal totalAmount;

    @Excel(name = "免费原因", width=20, orderNum = "7")
    private String freeReason;

    @Excel(name = "操作人员", width=20, orderNum = "8")
    private String operatorName;

    @Excel(name = "停车时长", width=20, orderNum = "9")
    private String dateDistance ;


}
