package com.cygsunri.wisdompark.callback.entity;

import lombok.Data;

import java.util.Map;

/**
 * <p>
 * 空气传感器实体类
 * </p>
 *
 * @author cygsunri
 * @since 2021/6/29
 */
@Data
public class AirSensor {
    /**
     * id
     */
    private String devCode;
    /**
     * uuid
     */
    private Integer devType;
    /**
     * 部门名称
     */
    private String devAddr;
    /**
     * 部门联系电话
     */
    private Map data;

}
