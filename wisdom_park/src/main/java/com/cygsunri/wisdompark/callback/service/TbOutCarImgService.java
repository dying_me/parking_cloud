package com.cygsunri.wisdompark.callback.service;

import com.cygsunri.wisdompark.callback.entity.TbOutCarImg;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 上传车辆出场图片信息表 服务类
 * </p>
 *
 * @author hanyuan
 * @since 2021-07-30
 */
public interface TbOutCarImgService extends IService<TbOutCarImg> {

    void deleteBatch(String[] ids);
}
