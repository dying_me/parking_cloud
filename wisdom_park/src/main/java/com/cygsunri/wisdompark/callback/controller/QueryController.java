package com.cygsunri.wisdompark.callback.controller;

import com.cygsunri.response.ResultWrapper;
import com.cygsunri.wisdompark.bussiniss.vo.BaseDataResult;
import com.cygsunri.wisdompark.callback.service.QueryService;
import com.cygsunri.wisdompark.callback.vo.notice.MthCarInfoQueryDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;



/**
 * <p>
 * 智慧停车开放平台通知类接口对接
 * </p>
 *
 * @author cygsunri
 * @since 2021/8/10
 */
@Slf4j
@RestController
@RequestMapping("/v2/query")
public class QueryController {

    @Autowired
    private QueryService queryService;



    /**
     * 获取月租车信息
     * @param mthCarInfoQueryDTO
     * @return
     */
    @PostMapping("/getMthCarInfo")
    public ResultWrapper<Object> getMthCarInfo(@Validated @RequestBody MthCarInfoQueryDTO mthCarInfoQueryDTO) {
        BaseDataResult data = queryService.getMthCarInfo(mthCarInfoQueryDTO);
        return ResultWrapper.ok("获取月租车信息成功",data);
    }





}
