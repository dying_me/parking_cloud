package com.cygsunri.wisdompark.callback.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cygsunri.wisdompark.callback.entity.DayData;
import com.cygsunri.wisdompark.callback.entity.DeviceInfo;
import com.cygsunri.wisdompark.callback.entity.DeviceTemplateData;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author
 * @since 2020-12-21
 */
public interface DayDataMapper extends BaseMapper<DayData> {

    /**
     * 通过时间和名称和相差天数查找值
     *
     * @param time  时间
     * @param name 名称
     * @param diff 相差天数
     * @return DeviceTemplateData
     */
    List<DayData> finDiffDayDataByTimeAndName(String time, String name, int diff);
}
