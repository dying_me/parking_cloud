package com.cygsunri.wisdompark.callback.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.List;

/**
 * <p>
 * 部门树数据封装
 * </p>
 *
 * @author cygsunri
 * @since 2021/6/30
 */
@Data
public class Departree {
    /**
     * 部门id
     */
    private String id;
    /**
     * 部门名称
     */
    private String departname;
    /**
     * 公司编号
     */
    private String org_code;

    /**
     * 下属部门
     */
    @JsonInclude(JsonInclude.Include.NON_EMPTY)//表示当只有该字段不为空时才会返回该属性
    @TableField(exist=false)
    private List<Departree> children;

}
