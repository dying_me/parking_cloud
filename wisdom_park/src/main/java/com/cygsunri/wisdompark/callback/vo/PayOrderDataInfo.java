package com.cygsunri.wisdompark.callback.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * <p>
 * 上传临停车缴费记录	data
 * </p>
 *
 * @author cygsunri
 * @since 2021-06-04
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class PayOrderDataInfo {

    /**
     * 停车场key
     */
    public String key;

    /**
     * 	设备名称
     */
    public String deviceName;

    /**
     * 支付订单号
     */
    public String payOrderNo;

    /**
     * 	停车订单号
     */
    public String orderNo;

    /**
     * 车牌号
     */
    public String carNo;

    /**
     * 车辆类型
     */
    public String carType;

    /**
     * 支付时间
     */
    public String payTime;

    /**
     * 应收金额
     */
    public String payMoney;

    /**
     * 实收金额
     */
    public String payedMoney;

    /**
     * 入场时间
     */
    public String enterTime;

    /**
     * 优惠券GUID
     */
    public String couponKey;

    /**
     * 优惠券金额
     */
    public String couponMoney;


}
