package com.cygsunri.wisdompark.callback.vo.notice;

import com.cygsunri.wisdompark.bussiniss.vo.BaseQueryNoticeCondition;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;

/**
 * <p>
 * 远程开闸封装类
 * </p>
 *
 * @author cygsunri
 * @since 2021/8/10
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class OpenGateDTO extends BaseQueryNoticeCondition {

    /**
     * 车场唯一编号
     */
    @NotBlank(message = "车场唯一编号不能为空")
    private String parkKey;

    /**
     * 控制机号
     */
    @NotBlank(message = "控制机号不能为空")
    private String vlCtrNo;


}
