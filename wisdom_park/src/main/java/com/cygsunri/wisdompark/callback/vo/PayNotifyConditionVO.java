package com.cygsunri.wisdompark.callback.vo;

import com.cygsunri.wisdompark.bussiniss.vo.BaseCondition;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author hanyuanLiu
 * @site 订单支付回调
 * @create 2021-06-07 14:33
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class PayNotifyConditionVO extends BaseCondition {

    /**
     * 车场唯一编号
     */
    private String parkKey;

    /**
     * 支付订单号
     */
    private String payOrderNo;

    /**
     * 支付宝/微信交易单号
     */
    private String payedSN;

    /**
     * 支付金额 单位:元
     */
    private String payedMoney;
}
