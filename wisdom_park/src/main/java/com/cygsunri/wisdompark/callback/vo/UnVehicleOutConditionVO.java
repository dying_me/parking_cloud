package com.cygsunri.wisdompark.callback.vo;

import com.cygsunri.wisdompark.bussiniss.vo.BaseCondition;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author hanyuanLiu
 * @site 出口缴费开闸vo
 * @create 2021-06-07 14:40
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class UnVehicleOutConditionVO extends BaseCondition {

    /**
     * 车场唯一编号
     */
    private String parkKey;

    /**
     * 车道控制机道
     */
    private String ctrlNo;

    /**
     * 支付订单号
     */
    private String payOrderNo;

    /**
     * 实际支付金额 单位:元
     */
    private String payedMoney;

    /**
     * 支付宝/微信交易单号
     */
    private String payedSN;
}
