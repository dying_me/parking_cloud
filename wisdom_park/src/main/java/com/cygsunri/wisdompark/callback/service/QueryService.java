package com.cygsunri.wisdompark.callback.service;

import com.cygsunri.wisdompark.bussiniss.vo.BaseDataResult;
import com.cygsunri.wisdompark.callback.vo.notice.MthCarInfoQueryDTO;

public interface QueryService {
    BaseDataResult getMthCarInfo(MthCarInfoQueryDTO mthCarInfoQueryDTO);
}
