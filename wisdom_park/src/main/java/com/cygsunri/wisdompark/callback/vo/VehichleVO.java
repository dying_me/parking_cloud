package com.cygsunri.wisdompark.callback.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * <p>
 * 上传岗亭车道信息 车道信息
 * </p>
 *
 * @author cygsunri
 * @since 2021-06-04
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class VehichleVO {

    /**
     * 车道编号
     */
    public String vehicleLane_No;

    /**
     * 大小车场
     */
    public String bigPark;

    /**
     * 车道类型
     */
    public String vlType;

    /**
     * 车道名称
     */
    public String vlName;

    /**
     * 控制机机号
     */
    public String vlctrlNo;

    /**
     * 相机Ip
     */
    public String vlvideoIp;

}
