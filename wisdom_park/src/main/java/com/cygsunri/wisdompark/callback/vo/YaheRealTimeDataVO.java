package com.cygsunri.wisdompark.callback.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * <p>
 * 前端用单台AC360的设备数据信息
 * </p>
 *
 * @author cygsunri
 * @since 2021-08-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class YaheRealTimeDataVO extends YaheCommonDataVO{

    private float value;

    private String unit;

}
