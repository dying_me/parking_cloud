package com.cygsunri.wisdompark.callback.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cygsunri.wisdompark.callback.entity.TbOutCar;
import com.cygsunri.wisdompark.callback.mapper.TbOutCarMapper;
import com.cygsunri.wisdompark.callback.service.TbOutCarService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 上传车辆出场信息表 服务实现类
 * </p>
 *
 * @author hanyuan
 * @since 2021-07-30
 */
@Service
public class TbOutCarServiceImpl extends ServiceImpl<TbOutCarMapper, TbOutCar> implements TbOutCarService {



    @Override
    public void deleteBatch(String[] ids) {
        baseMapper.deleteBatch(ids);
    }
}
