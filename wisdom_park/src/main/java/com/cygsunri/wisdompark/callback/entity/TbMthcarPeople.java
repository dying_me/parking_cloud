package com.cygsunri.wisdompark.callback.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 月租车用户信息表
 * </p>
 *
 * @author hanyuan
 * @since 2021-08-18
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class TbMthcarPeople implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * ID
     */
    @TableId(value = "id", type = IdType.INPUT)
    private String id;

    /**
     * appID
     */
    private String appid;

    /**
     * 版本号
     */
    private String version;

    /**
     * 随机值
     */
    private String rand;

    /**
     * 签名
     */
    private String sign;

    /**
     * 车场唯一编号
     */
    @TableField("park_key")
    private String parkKey;

    /**
     * 车主姓名
     */
    @TableField("user_name")
    private String userName;

    /**
     * 人员编号
     */
    @TableField("user_no")
    private String userNo;

    /**
     * 车辆类型编码
     */
    @TableField("car_type_no")
    private String carTypeNo;

    /**
     * 车辆类型编码字典值释义
     */
    @TableField(exist=false)
    private String carTypeStatus;

    /**
     * 车位联系人住址
     */
    @TableField("home_address")
    private String homeAddress;

    /**
     * 车位负责人联系方式
     */
    @TableField("mob_number")
    private String mobNumber;

    /**
     * 车场车位编号
     */
    @TableField("park_no")
    private String parkNo;

    /**
     * 机号
     */
    @TableField("machine_no")
    private String machineNo;


}
