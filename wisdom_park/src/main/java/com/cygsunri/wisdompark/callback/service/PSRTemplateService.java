package com.cygsunri.wisdompark.callback.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cygsunri.wisdompark.callback.entity.DeviceInfo;
import com.cygsunri.wisdompark.callback.entity.PSRTemplateMapping;
import com.cygsunri.wisdompark.callback.vo.YaheCommonDataVO;

import java.util.List;

public interface PSRTemplateService extends IService<PSRTemplateMapping> {


    /**
     * 查询设备唯一标识
     * @param belong
     * @param deviceTemplate_id
     * @return List<String>
     */
    public List<DeviceInfo> getAirIdList(String belong, String deviceTemplate_id);

    /**
     * 查询设备名称
     * @param belong
     * @param deviceTemplate_id
     * @return String
     */
    public String getDeviceName(String belong, String deviceTemplate_id);

    /**
     * 查询楼层
     * @param belong
     * @param deviceTemplate_id
     * @return List<YaheCommonDataVO>
     */
    public List<YaheCommonDataVO> getFloorInfo(String belong, String deviceTemplate_id);
    /**
     * 查询区域
     * @param belong
     * @param deviceTemplate_id
     * @return List<YaheCommonDataVO>
     */
    public List<YaheCommonDataVO> getAreaInfo(String belong, String deviceTemplate_id);
    /**
     * 条件查询空调设备id列表
     * @param areaList
     * @param deviceTemplate_id
     * @return List<String>
     */
    public List<String> getAirConditionIdList(List<String> areaList, String deviceTemplate_id);
}
