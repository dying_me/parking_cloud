package com.cygsunri.wisdompark.callback.vo.notice;

import com.cygsunri.wisdompark.bussiniss.vo.BaseQueryNoticeCondition;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;

/**
 * <p>
 * 访客车辆新增封装类
 * </p>
 *
 * @author cygsunri
 * @since 2021/8/10
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class AddVisitorCarDTO extends BaseQueryNoticeCondition {

    /**
     * 车场唯一编号
     */
    @NotBlank(message = "车场唯一编号不能为空")
    private String parkKey;

    /**
     * 商家编码
     */
    @NotBlank(message = "商家编码不能为空")
    private String businessNo;

    /**
     * 车牌号码
     */
    @NotBlank(message = "车牌号码不能为空")
    private String carNo;

    /**
     * 开始时间 yyyy-MM-dd HH:mm:ss
     */
    @NotBlank(message = "开始时间不能为空")
    private String beginTime;

    /**
     * 结束时间 yyyy-MM-dd HH:mm:ss
     */
    @NotBlank(message = "结束时间不能为空")
    private String endTime;

}
