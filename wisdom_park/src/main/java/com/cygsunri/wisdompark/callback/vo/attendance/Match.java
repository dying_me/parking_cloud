package com.cygsunri.wisdompark.callback.vo.attendance;

import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * <p>
 * 匹配人员信息
 * </p>
 *
 * @author cygsunri
 * @since 2021/8/23
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class Match {
    /**
     * 是否加密(加密后姓名、ID字段为AES加密后的二进制数据转base64,其余字段未进行加密)
     * 1（true）， 0（false）
     */
    private Boolean is_encryption;
    /**
     * 人员ID
     */
    private String person_id;
    /**
     *  人员姓名
     */
    private String person_name;
    /**
     * 人员角色
     */
    private Integer person_role;
    /**
     * 注册图像（模板图片）格式
     */
    private String format;
    /**
     * 注册图像（模板图片）数据（二进制数据对应base64）
     */
    private String image;
    /**
     * 人员注册来源
     * none：无此信息。software：注册软件注册。
     * cluster：无感录入注册。 sync：云同步注册。unknown：未知
     */
    private String origin;
    /**
     * 人员名单有效属性
     * none：无此信息permanent_list：永久有效名单
     * temporary_list：临时有效名单invalid_list：永久无效名单 unknown：未知
     */
    private String person_attr;
    /**
     * 用户自定义文本内容
     * 可用于相机TTS播放、LCD显示，也可作为用户平台自定义
     */
    private String customer_text;
}
