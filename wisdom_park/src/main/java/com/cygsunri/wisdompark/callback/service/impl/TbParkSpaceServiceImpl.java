package com.cygsunri.wisdompark.callback.service.impl;

import com.cygsunri.wisdompark.callback.entity.TbParkSpace;
import com.cygsunri.wisdompark.callback.mapper.TbParkSpaceMapper;
import com.cygsunri.wisdompark.callback.service.TbParkSpaceService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 车场车位数信息表 服务实现类
 * </p>
 *
 * @author hanyuan
 * @since 2021-08-30
 */
@Service
public class TbParkSpaceServiceImpl extends ServiceImpl<TbParkSpaceMapper, TbParkSpace> implements TbParkSpaceService {

}
