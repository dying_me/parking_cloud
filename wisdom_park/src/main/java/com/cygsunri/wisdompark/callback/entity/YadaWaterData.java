package com.cygsunri.wisdompark.callback.entity;

import lombok.Data;

/**
 * <p>
 *  influxdb 雅达水表数据-WaterMeterDataInfo：上报数据
 * </p>
 *
 * @author cygsunri
 * @since 2021/6/29
 */
@Data
public class YadaWaterData {
    /**
     * id
     */
//    private String id;
    /**
     * uuid
     */
    private long time;
    /**
     * 程序版本
     */
//    private String Version;
    /**
     * 阀门状态
     */
//    private String Valve;
    /**
     * 服务Ip地址
     */
//    private String ServerIp1;
    /**
     * 信号
     */
    private String RSSI;
    /**
     * 温度
     */
    private String MeterTemperature;
    /**
     * 表状态解析
     */
//    private String MeterStateStr;
    /**
     * 表状态hex
     */
//    private String MeterStateHex;
    /**
     * 表编号
     */
//    private String MeterCode;
    /**
     * 表端时间
     */
//    private String MeterClock;
    /**
     * 电池
     */
    private String MeterBattery;
    /**
     * 瞬时流量
     */
    private String InstantFlow;
    /**
     *
     */
//    private String ICCID;
    /**
     * Gis地址
     */
//    private String GisLocation;
    /**
     * （固定01，暂无使用）
     */
//    private String DeviceAddress;
    /**
     * 累计流量
     */
    private String CumulativeFlow;
    /**
     * 控制码
     */
//    private String CtrlCode;

}
