package com.cygsunri.wisdompark.callback.vo.attendance;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 二维码数据，检测到二维码时有此字段，否则无此字段
 * </p>
 *
 * @author cygsunri
 * @since 2021/8/23
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class Qr_code {
    /**
     * 二维码类型
     */
    private String qr_type;
    /**
     * 二维码数据
     */
    private String qr_data;
}
