package com.cygsunri.wisdompark.callback.mapper;

import com.cygsunri.wisdompark.callback.entity.TbEnterCar;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cygsunri.wisdompark.callback.vo.EnterOutCarQueryDTO;
import com.cygsunri.wisdompark.callback.vo.OutEnterCarVO;
import com.cygsunri.wisdompark.callback.vo.excel.ExportInoutRecordsModel;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 上传车辆入场信息表 Mapper 接口
 * </p>
 *
 * @author hanyuan
 * @since 2021-07-30
 */
@Repository
public interface TbEnterCarMapper extends BaseMapper<TbEnterCar> {

    List<OutEnterCarVO> getInEnterCarPageList(EnterOutCarQueryDTO enterOutCarQueryDTO);

    List<ExportInoutRecordsModel> getExportInoutRecords(EnterOutCarQueryDTO enterOutCarQueryDTO);
}
