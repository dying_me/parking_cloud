package com.cygsunri.wisdompark.callback.vo;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;


/**
 * <p>
 * 车辆入场信息Data Bean
 * </p>
 *
 * @author cygsunri
 * @since 2021-06-04
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class EnterCarInfo {

    /**
     * 停车场key
     */
    @NotBlank(message = "停车场key不能为空")
    @JSONField(ordinal = 1,name="key")
    public String key;

    /**
     * 车牌号
     */
    @NotBlank(message = "车牌号不能为空")
    @JSONField(ordinal = 2,name="carNo")
    public String carNo;

    /**
     * 停车订单号
     */
    @NotBlank(message = "停车订单号不能为空")
    @JSONField(ordinal = 3,name="orderNo")
    public String orderNo;

    /**
     * 入场时间
     */
    //@NotBlank(message = "车牌号不能为空")
    @JSONField(ordinal = 4,name="enterTime")
    public String enterTime;

    /**
     * 车辆类型编码
     */
    //@NotBlank(message = "车辆类型编码不能为空")
    @JSONField(ordinal = 5,name="carType")
    public String carType;

    /**
     * 入口车道名称
     */
    //@NotBlank(message = "入口车道名称不能为空")
    @JSONField(ordinal = 6,name="gateName")
    public String gateName;

    /**
     * 入口操作员名称
     */
    //@NotBlank(message = "入口操作员名称不能为空")
    @JSONField(ordinal = 7,name="operatorName")
    public String operatorName;

    /**
     * 预约车位订单号
     */
    //@NotBlank(message = "预约车位订单号不能为空")
    @JSONField(ordinal = 8,name="reserveOrderNo")
    public String reserveOrderNo;

    /**
     * 入口车辆图片
     */
    //@NotBlank(message = "入口车辆图片不能为空")
    @JSONField(ordinal = 9,name="imgUrl")
    public String imgUrl;

}
