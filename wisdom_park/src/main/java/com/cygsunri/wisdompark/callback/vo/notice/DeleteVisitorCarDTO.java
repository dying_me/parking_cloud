package com.cygsunri.wisdompark.callback.vo.notice;

import com.cygsunri.wisdompark.bussiniss.vo.BaseQueryNoticeCondition;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;

/**
 * <p>
 * 访客车辆删除封装类
 * </p>
 *
 * @author cygsunri
 * @since 2021/8/10
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class DeleteVisitorCarDTO extends BaseQueryNoticeCondition {

    /**
     * 车场唯一编号
     */
    @NotBlank(message = "车场唯一编号不能为空")
    private String parkKey;

    /**
     * 商家编码
     */
    @NotBlank(message = "商家编码不能为空")
    private String businessNo;

    /**
     * 车牌号码
     */
    @NotBlank(message = "车牌号码不能为空")
    private String carNo;


}
