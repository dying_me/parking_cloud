package com.cygsunri.wisdompark.callback.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cygsunri.wisdompark.callback.entity.DeviceInfo;
import com.cygsunri.wisdompark.callback.entity.PSRTemplateMapping;
import com.cygsunri.wisdompark.callback.vo.YaheCommonDataVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author gaoyang
 * @since 2021-08-16
 */
@Component
public interface PSRTemplateMapper extends BaseMapper<PSRTemplateMapping> {
    /**
     * 查询设备唯一标识
     * @param belong
     * @param deviceTemplate_id
     * @return List<String>
     */
    public List<DeviceInfo> getAirIdList(@Param("belong")String belong, @Param("deviceTemplate_id")String deviceTemplate_id);
    /**
     * 查询设备名称
     * @param belong
     * @param deviceTemplate_id
     * @return String
     */
    public String getDeviceName(@Param("belong")String belong, @Param("deviceTemplate_id")String deviceTemplate_id);
    /**
     * 查询楼层
     * @param belong
     * @param deviceTemplate_id
     * @return String
     */
    public List<YaheCommonDataVO> getFloorInfo(@Param("belong")String belong, @Param("deviceTemplate_id")String deviceTemplate_id);
    /**
     * 查询区域
     * @param belong
     * @param deviceTemplate_id
     * @return String
     */
    public List<YaheCommonDataVO> getAreaInfo(@Param("belong")String belong, @Param("deviceTemplate_id")String deviceTemplate_id);
    /**
     * 条件查询空调设备id列表
     * @param areaList
     * @param deviceTemplate_id
     * @return List<String>
     */
    public List<String> getAirConditionIdList(@Param("areaList")List<String> areaList, @Param("deviceTemplate_id")String deviceTemplate_id);
}
