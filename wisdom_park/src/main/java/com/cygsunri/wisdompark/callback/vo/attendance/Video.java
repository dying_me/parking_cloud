package com.cygsunri.wisdompark.callback.vo.attendance;

import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * <p>
 * 视频（视频标志为true）
 * </p>
 *
 * @author cygsunri
 * @since 2021/8/23
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class Video {

    /**
     *  视频起始时间
     */
    private String start_time;
    /**
     * 视频结束时间（秒）
     */
    private String end_time;
    /**
     * 视频格式(avi/mp4)
     */
    private String format;
    /**
     * 视频数据（二进制数据对应base64）
     */
    private String data;
}
