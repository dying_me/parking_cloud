package com.cygsunri.wisdompark.callback.service;


import com.cygsunri.wisdompark.bussiniss.vo.BaseResult;
import com.cygsunri.wisdompark.callback.entity.TbMthcarPeople;
import com.cygsunri.wisdompark.callback.vo.*;
import com.github.pagehelper.PageInfo;
import org.springframework.ui.ModelMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface CallbackService {


    BaseResult carEnterCar(EnterCarConditionVO enterCarConditionVO);

    BaseResult carOutCar(OutCarConditionVO outCarConditionVO);

    BaseResult carEnterCarImg(EnterOutImgConditionVO enterOutImgConditionVO);

    BaseResult carOutCarImg(EnterOutImgConditionVO enterOutImgConditionVO);

    PageInfo<OutEnterCarVO> pageInOutRecordListQuey(EnterOutCarQueryDTO enterOutCarQueryDTO);

    void deleteById(String id);

    void deleteBatchByIds(String[] ids);

    void exportedInoutRecordList(EnterOutCarQueryDTO enterOutCarQueryDTO, HttpServletRequest request, HttpServletResponse response, ModelMap modelMap);

    PageInfo<TbMthcarPeople> getPageMthCarPersons(MthcarPeopleQueryDTO mthcarPeopleQueryDTO);

    void deleteMthCarPerBatch(String[] ids);

    BaseResult sendParkSpace(SendParkSpaceReq sendParkSpaceReq);
}
