package com.cygsunri.wisdompark.callback.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * modbus寄存器信息表
 * </p>
 */
@Data
@Accessors(chain = true)
@TableName("tb_modbusdata")
public class ModbusData implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键,UUID
     */
    @TableId(value = "ID", type = IdType.ASSIGN_UUID)
    private String id;

    /**
     * 寄存器地址
     */
    @TableField("registerAddress")
    private String registerAddress;

    /**
     * 定义说明
     */
    @TableField("name")
    private String name;

    /**
     * 单位
     */
    @TableField("unit")
    private String unit;

    /**
     * 数据类型
     */
    @TableField("dataType")
    private String dataType;

    /**
     * 占用位数
     */
    @TableField("dataSize")
    private Integer dataSize;

    /**
     * 设备型号
     */
    @TableField("model")
    private String model;

    /**
     * 制造厂家
     */
    @TableField("manufacturer")
    private String manufacturer;

}
