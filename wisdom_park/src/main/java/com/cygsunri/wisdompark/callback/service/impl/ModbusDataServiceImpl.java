package com.cygsunri.wisdompark.callback.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cygsunri.wisdompark.callback.entity.ModbusData;
import com.cygsunri.wisdompark.callback.mapper.ModbusDataMapper;
import com.cygsunri.wisdompark.callback.service.ModbusDataService;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 寄存器信息 服务类
 * </p>
 */
@Service
public class ModbusDataServiceImpl  extends ServiceImpl<ModbusDataMapper, ModbusData> implements ModbusDataService {

    /**
     * 通过制造厂商，设备型号获取寄存器信息
     *
     * @param manufacturer  制造厂商
     * @param model 设备型号
     * @return ModbusData
     */
    @Override
    public List<ModbusData> findDataByModel(String manufacturer, String model) {
        LambdaQueryWrapper<ModbusData> queryWrapper = new LambdaQueryWrapper<>();
        if(StringUtils.isNotEmpty(manufacturer)){
            queryWrapper.eq(ModbusData::getManufacturer,manufacturer);
        }
        if(StringUtils.isNotEmpty(model)){
            queryWrapper.eq(ModbusData::getModel,model);
        }
        return this.list(queryWrapper);
    }
}
