package com.cygsunri.wisdompark.callback.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 考勤抓拍信息表
 * </p>
 *
 * @author hanyuan
 * @since 2021-08-23
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class TbPhotograph implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * ID
     */
    @TableId(value = "id", type = IdType.INPUT)
    private String id;

    /**
     * 推送方协议版本
     */
    private String version;

    /**
     * 命令
     */
    private String cmd;

    /**
     * 序号
     */
    private Integer sequence_no;

    /**
     * 设备（相机）编号
     */
    private String device_no;

    /**
     * 设备（相机）序列号
     */
    private String device_sn;

    /**
     * 点位编号
     */
    private String addr_no;

    /**
     * 点位名称
     */
    private String addr_name;

    /**
     * 抓拍时间
     */
    private String cap_time;

    /**
     * 实时抓拍标志
     */
    private Integer is_realtime;

    /**
     * 不通行原因:限制人员：-2,人员过期：-3,不在调度时间：-4,节假日：-5,温度过高：-6,口罩：-7,未带安全帽：-8,卡号未注册：-9,人证不匹配：-10,未授权：-11
     */
    private Integer match_failed_reson;

    /**
     * 对比结果
     */
    private Integer match_result;

    /**
     * 性别（male表示男，female表示女,空字符串表示无此信息）
     */
    private String sex;

    /**
     * 年龄（0表示无此信息）
     */
    private Integer age;

    /**
     * 是否佩戴安全帽:none：未佩戴安全帽。white：白色安全帽。blue：蓝色安全帽。orange：橙色安全帽。red：红色安全帽。yellow：黄色安全帽
     */
    private String hat;

    /**
     * 体温
     */
    private Double temperatur;

    /**
     * 是否佩戴口罩
     */
    private Boolean has_mask;

    /**
     * 人像质量。（0~100）
     */
    private Integer face_quality;

    /**
     * 扭转角度。头部旋转平面与图片不处于同一平面。取值范围-90~90
     */
    private Integer turn_angle;

    /**
     * 平面旋转角度。头部旋转平面与图像处于同一平面。取值范围-90~90
     */
    private Integer rotate_angle;

    /**
     * 32bit韦根协议门禁卡号
     */
    private Integer wg_card_id;

    /**
     * 64bit韦根协议门禁卡号，wg_card_id字段不存在时，使用此字段作为门禁卡号
     */
    private Long long_card_id;

    /**
     * 身份证编号
     */
    private String number;

    /**
     * 姓名
     */
    private String name;

    /**
     * 生日
     */
    private String birthday;

    /**
     * 性别（male：男 female：女）
     */
    private String c_sex;

    /**
     * 民族
     */
    private String national;

    /**
     * 地址
     */
    private String residence_address;

    /**
     * 签发机关
     */
    private String organ_issue;

    /**
     * 有效期起始时间
     */
    private String valid_date_start;

    /**
     * 有效期截止时间
     */
    private String valid_date_end;

    /**
     * 是否加密(加密后姓名、ID字段为AES加密后的二进制数据转base64,其余字段未进行加密)
     */
    private Boolean is_encryption;

    /**
     * 人员ID
     */
    private String person_id;

    /**
     * 人员姓名
     */
    private String person_name;

    /**
     * 人员角色
     */
    private Integer person_role;

    /**
     * 注册图像（模板图片）格式
     */
    private String format;

    /**
     * 注册图像（模板图片）数据（二进制数据对应base64）
     */
    private String image;

    /**
     * 人员注册来源:none：无此信息。software：注册软件注册。cluster：无感录入注册。 sync：云同步注册。unknown：未知
     */
    private String origin;

    /**
     * 人员名单有效属性,none：无此信息permanent_list：永久有效名单temporary_list：临时有效名单invalid_list：永久无效名单 unknown：未知
     */
    private String person_attr;

    /**
     * 用户自定义文本内容,可用于相机TTS播放、LCD显示，也可作为用户平台自定义
     */
    private String customer_text;

    /**
     * 全景图像标志（是否包含全景图像）
     */
    private Boolean overall_pic_flag;

    /**
     * 图像格式
     */
    private String o_format;

    /**
     * 图像数据（二进制数据对应base64）
     */
    private String o_data;

    /**
     * 人脸位置位于全景图X坐标
     */
    private Integer o_face_x;

    /**
     * 人脸位置位于全景图Y坐标
     */
    private Integer o_face_y;

    /**
     * 人脸在全景图中宽度
     */
    private Integer o_face_width;

    /**
     * 人脸在全景图中高度
     */
    private Integer o_face_height;

    /**
     * 特写图像标志（是否包含特写图像）
     */
    private Boolean closeup_pic_flag;

    /**
     * 图像格式
     */
    private String c_format;

    /**
     * 图像数据（二进制数据对应base64）
     */
    private String c_data;

    /**
     * 人脸位置位于特写图X坐标
     */
    private Integer c_face_x;

    /**
     * 人脸位置位于特写图Y坐标
     */
    private Integer c_face_y;

    /**
     * 人脸在特写图中宽度
     */
    private Integer c_face_width;

    /**
     * 人脸在特写图中高度
     */
    private Integer c_face_height;

    /**
     * 视频标志（是否包含视频）
     */
    private Boolean video_flag;

    /**
     * 视频起始时间
     */
    private String v_start_time;

    /**
     * 视频结束时间
     */
    private String v_end_time;

    /**
     * 视频格式(avi/mp4)
     */
    private String v_format;

    /**
     * 视频数据（二进制数据对应base64）
     */
    private String v_data;


}
