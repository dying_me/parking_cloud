package com.cygsunri.wisdompark.callback.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * <p>
 * 上传停车场设备状态data
 * </p>
 *
 * @author cygsunri
 * @since 2021-06-04
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class AddDeviceMonitorDataInfo {

    /**
     * 停车场key
     */
    public String key;

    /**
     * 设备名称
     */
    public String deviceName;

    /**
     * 	设备ip
     */
    public String deviceIp;

    /**
     * 	设备状态
     */
    public String status;


}
