package com.cygsunri.wisdompark.callback.service;

import cn.afterturn.easypoi.handler.inter.IExcelExportServer;

/**
 * @author hanyuanLiu
 * @site USER_M
 * @create 2020-12-01 16:57
 */
public interface ExcelInoutRecordsExportService extends IExcelExportServer {

}
