package com.cygsunri.wisdompark.callback.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;


/**
 * <p>
 * 上传车场车位数data
 * </p>
 *
 * @author cygsunri
 * @since 2021-06-04
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class SendParkSpaceDataInfo {

    /**
     * 停车场key
     */
    @NotBlank(message = "停车场key不能为空")
    public String key;

    /**
     * 空闲车位数
     */
    @NotBlank(message = "空闲车位数不能为空")
    public String remainder_spaces;

    /**
     * 总车位数
     */
    @NotBlank(message = "总车位数不能为空")
    public String total_spaces;


}
