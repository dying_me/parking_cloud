package com.cygsunri.wisdompark.callback.service;

import com.cygsunri.wisdompark.bussiniss.vo.BaseDataResult;
import com.cygsunri.wisdompark.callback.vo.notice.*;

import java.util.List;

public interface NoticeService {

    BaseDataResult addMthCarPerson(MthCarPeopleDTO mthCarPeopleDTO);

    BaseDataResult addMthCar(MthCarDTO mthCarDTO);

    BaseDataResult openGate(OpenGateDTO openGateDTO);

    BaseDataResult closeGate(CloseGateDTO closeGateDTO);

    BaseDataResult addVisitorCar(AddVisitorCarDTO addVisitorCarDTO);

    BaseDataResult deleteVisitorCar(DeleteVisitorCarDTO deleteVisitorCarDTO);

    BaseDataResult setCarLockStatus(SetCarLockStatusDTO setCarLockStatusDTO);

    BaseDataResult failMonthCar(FailMonthCarDTO failMonthCarDTO);

    void failMonthCars(List<FailMonthCarDTO> failMonthCars);
}
