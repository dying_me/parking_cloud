package com.cygsunri.wisdompark.callback.controller;

import com.cygsunri.common.CommonConstant;
import com.cygsunri.wisdompark.callback.entity.*;
import com.cygsunri.wisdompark.callback.service.AttrecordService;
import com.cygsunri.response.ResultWrapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 共享版考勤协议抓拍接口对接
 * </p>
 *
 * @author cygsunri
 * @since 2021/6/29
 */
@Slf4j
@RestController
@RequestMapping("/attrecord")
public class AttrecordController {

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private AttrecordService attrecordService;

    /**
     * 登录
     * @return
     */
    @GetMapping("/checkUser")
    public ResultWrapper<Object> checkUser() {
        Map<String, String> query = new HashMap<>();
        query.put("username", "13340922682");
        query.put("password", "CYGhysyb123");
        UserBaseInfo userInfo = attrecordService.checkUser(query);
        if(userInfo!=null){
            return ResultWrapper.ok("获取第三方登录认证成功",userInfo);
        }else{
            return ResultWrapper.error("获取第三方登录认证失败,请重试");
        }

    }

    /**
     * 查询人员信息
     * @return
     */
    @GetMapping("/faceWhitelist")
    public ResultWrapper<Object> faceWhitelist() {
        UserBaseInfo userInfo = (UserBaseInfo) redisTemplate.opsForValue().get(CommonConstant.USER_KEY_CACHE_PREFIX + "13340922682");
        if(userInfo==null){
            //登录超时,重新登录
            Map<String, String> query = new HashMap<>();
            query.put("username", "13340922682");
            query.put("password", "CYGhysyb123");
            userInfo = attrecordService.checkUser(query);
            if(userInfo==null){
                return ResultWrapper.error("获取第三方登录认证失败,请重试");
            }
        }
        Map<String, String> query = new HashMap<>();
        query.put("username", userInfo.getUsername());
        query.put("token", userInfo.getToken());
        query.put("unitno", userInfo.getOrgcode());
        List<PeopleInfo> peoplesInfo = attrecordService.faceWhitelist(query);
        return ResultWrapper.ok("查询人员信息成功!",peoplesInfo);
    }

    /**
     * 获取部门信息
     * @return
     */
    @GetMapping("/departmentList")
    public ResultWrapper<Object> departmentList() {
        UserBaseInfo userInfo = (UserBaseInfo) redisTemplate.opsForValue().get(CommonConstant.USER_KEY_CACHE_PREFIX + "13340922682");
        if(userInfo==null){
            //登录超时,重新登录
            Map<String, String> query = new HashMap<>();
            query.put("username", "13340922682");
            query.put("password", "CYGhysyb123");
            userInfo = attrecordService.checkUser(query);
            if(userInfo==null){
                return ResultWrapper.error("获取第三方登录认证失败,请重试");
            }
        }
        Map<String, String> query = new HashMap<>();
        query.put("username", userInfo.getUsername());
        query.put("token", userInfo.getToken());
        query.put("unitno", userInfo.getOrgcode());
        List<Departree> departmentsInfo = attrecordService.departmentList(query);
        return ResultWrapper.ok("获取部门信息成功!",departmentsInfo);
    }

    /**
     * 远程开门
     * @return
     */
    @GetMapping("/openThegate")
    public ResultWrapper<Object> openThegate() {
        UserBaseInfo userInfo = (UserBaseInfo) redisTemplate.opsForValue().get(CommonConstant.USER_KEY_CACHE_PREFIX + "13340922682");
        if(userInfo==null){
            //登录超时,重新登录
            Map<String, String> query = new HashMap<>();
            query.put("username", "13340922682");
            query.put("password", "CYGhysyb123");
            userInfo = attrecordService.checkUser(query);
            if(userInfo==null){
                return ResultWrapper.error("获取第三方登录认证失败,请重试");
            }
        }
        Map<String, String> query = new HashMap<>();
        query.put("username", userInfo.getUsername());
        query.put("token", userInfo.getToken());
        query.put("unitno", userInfo.getOrgcode());
        query.put("deviceid", "xxx");//设备编号
        boolean flag = attrecordService.openThegate(query);
        if(flag){
            return ResultWrapper.ok("远程开门成功!");
        }else{
            return ResultWrapper.error("远程开门失败,请重试!");
        }
    }

    /**
     * 生成开门二维码
     * @return
     */
    @GetMapping("/getQRcode")
    public ResultWrapper<Object> getQRcode() {
        UserBaseInfo userInfo = (UserBaseInfo) redisTemplate.opsForValue().get(CommonConstant.USER_KEY_CACHE_PREFIX + "13340922682");
        if(userInfo==null){
            //登录超时,重新登录
            Map<String, String> query = new HashMap<>();
            query.put("username", "13340922682");
            query.put("password", "CYGhysyb123");
            userInfo = attrecordService.checkUser(query);
            if(userInfo==null){
                return ResultWrapper.error("获取第三方登录认证失败,请重试");
            }
        }
        Map<String, String> query = new HashMap<>();
        query.put("username", userInfo.getUsername());
        query.put("token", userInfo.getToken());
        query.put("unitno", userInfo.getOrgcode());
        query.put("times", "10");//使用次数
        query.put("enddate", "2021-08-27 09:00:00");//到期时间
        String imgUrl = attrecordService.getQRcode(query);
        return ResultWrapper.ok("开门二维码生成成功!",imgUrl);
    }

    /**
     * 开门记录
     * @return
     */
    @GetMapping("/openRecordPage")
    public ResultWrapper<Object> openRecordPage() {
        UserBaseInfo userInfo = (UserBaseInfo) redisTemplate.opsForValue().get(CommonConstant.USER_KEY_CACHE_PREFIX + "13340922682");
        if(userInfo==null){
            //登录超时,重新登录
            Map<String, String> query = new HashMap<>();
            query.put("username", "13340922682");
            query.put("password", "CYGhysyb123");
            userInfo = attrecordService.checkUser(query);
            if(userInfo==null){
                return ResultWrapper.error("获取第三方登录认证失败,请重试");
            }
        }
        Map<String, String> query = new HashMap<>();
        query.put("username", userInfo.getUsername());
        query.put("token", userInfo.getToken());
        query.put("unitno", userInfo.getOrgcode());
        query.put("capturetime_begin", "2021-05-27 09:00:00");//抓拍时间
        query.put("page", "1");//页码
        query.put("rows", "10");//也大小
        List<OpenRecords> OpenRecordsInfo = attrecordService.openRecordPage(query);
        return ResultWrapper.ok("获取开门记录信息成功!",OpenRecordsInfo);
    }

    /**
     * 查询访客信息
     * @return
     */
    @GetMapping("/visitorList")
    public ResultWrapper<Object> visitorList() {
        UserBaseInfo userInfo = (UserBaseInfo) redisTemplate.opsForValue().get(CommonConstant.USER_KEY_CACHE_PREFIX + "13340922682");
        if(userInfo==null){
            //登录超时,重新登录
            Map<String, String> query = new HashMap<>();
            query.put("username", "13340922682");
            query.put("password", "CYGhysyb123");
            userInfo = attrecordService.checkUser(query);
            if(userInfo==null){
                return ResultWrapper.error("获取第三方登录认证失败,请重试");
            }
        }
        Map<String, String> query = new HashMap<>();
        query.put("username", userInfo.getUsername());
        query.put("token", userInfo.getToken());
        query.put("unitno", userInfo.getOrgcode());
        List<VisitorInfo> visitorInfos = attrecordService.visitorList(query);
        return ResultWrapper.ok("获取访客信息成功!",visitorInfos);
    }


}
