package com.cygsunri.wisdompark.callback.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * <p>
 * 单台AC360的设备数据信息
 * </p>
 *
 * @author cygsunri
 * @since 2021-08-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class YaheStatisticInfoVO {

    private int totalNumber;

    private int commSuccessNumber;

    private int commFailureNumber;

    private int runningNumber;

    private int shutdownNumber;

    private int errorNumber;

}
