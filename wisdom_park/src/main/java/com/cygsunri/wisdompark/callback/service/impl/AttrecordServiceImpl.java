package com.cygsunri.wisdompark.callback.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.cygsunri.common.CommonConstant;
import com.cygsunri.wisdompark.callback.entity.*;
import com.cygsunri.wisdompark.callback.service.AttrecordService;
import com.cygsunri.wisdompark.util.CommonUtil;
import com.cygsunri.wisdompark.util.HttpUtils;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 *
 * </p>
 *
 * @author cygsunri
 * @since 2021/6/29
 */
@Service
public class AttrecordServiceImpl implements AttrecordService {

    @Autowired
    RedisTemplate redisTemplate;

    @Override
    public UserBaseInfo checkUser(Map<String, String> query) {
        UserBaseInfo userInfo = null;
        try {
            //发送post请求
            HttpResponse response = HttpUtils.doPost("http://kq.lsyat.com", "/attrecordController.do?checkuser", "post", new HashMap<String, String>(),new HashMap<String, String>(),query);
            if (response.getStatusLine().getStatusCode() == 200) {
                String json = EntityUtils.toString(response.getEntity());
                JSONObject object = JSON.parseObject(json);
                userInfo = object.getObject("data",new TypeReference<UserBaseInfo>(){});
                // 缓存登录信息,模拟登录超时时间为30分钟
                redisTemplate.opsForValue().set(CommonConstant.USER_KEY_CACHE_PREFIX + "13340922682", userInfo , 30, TimeUnit.MINUTES);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return userInfo;
    }

    @Override
    public List<PeopleInfo> faceWhitelist(Map<String, String> query) {
        List<PeopleInfo> peoplesInfo = new ArrayList<>();
        try {
            //发送post请求
            HttpResponse response = HttpUtils.doPost("http://kq.lsyat.com", "/attrecordController.do?FaceWhitelist", "post", new HashMap<String, String>(),new HashMap<String, String>(),query);
            if (response.getStatusLine().getStatusCode() == 200) {
                String json = EntityUtils.toString(response.getEntity(), Charset.forName("UTF-8"));
                JSONObject object = JSON.parseObject(json);
                JSONArray objects = object.getJSONArray("data");
                objects.forEach(o -> {
                    String dataJson = JSON.toJSONString(o);
                    PeopleInfo peopleInfo = JSON.parseObject(dataJson, new TypeReference<PeopleInfo>() {});
                    peoplesInfo.add(peopleInfo);
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return peoplesInfo;
    }

    @Override
    public List<Departree> departmentList(Map<String, String> query) {
        List<Departree> departrees = new ArrayList<>();
        try {
            //发送post请求
            HttpResponse response = HttpUtils.doPost("http://kq.lsyat.com", "/attrecordController.do?listAll", "post", new HashMap<String, String>(),new HashMap<String, String>(),query);
            if (response.getStatusLine().getStatusCode() == 200) {
                String json = EntityUtils.toString(response.getEntity(), Charset.forName("UTF-8"));
                //读取测试数据
                String path = AttrecordServiceImpl.class.getClassLoader().getResource("test.json").getPath();
                String s = CommonUtil.readJsonFile(path);
                JSONObject object = JSON.parseObject(s).getJSONObject("data");
                JSONArray objects = object.getJSONArray("departlist");
                objects.forEach(o -> {
                    String dataJson = JSON.toJSONString(o);
                    Departree departree = JSON.parseObject(dataJson, new TypeReference<Departree>() {});
                    departrees.add(departree);
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return departrees;
    }

    @Override
    public boolean openThegate(Map<String, String> query) {
        boolean flag = false;
        try {
            //发送post请求
            HttpResponse response = HttpUtils.doPost("http://kq.lsyat.com", "/attrecordController.do?Openthegate", "post", new HashMap<String, String>(),new HashMap<String, String>(),query);
            if (response.getStatusLine().getStatusCode() == 200) {
                String json = EntityUtils.toString(response.getEntity(), Charset.forName("UTF-8"));
                String code =  JSON.parseObject(json).getString("code");
                if("0".equals(code)){
                    flag = true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return flag;
    }

    @Override
    public List<OpenRecords> openRecordPage(Map<String, String> query) {
        List<OpenRecords> openRecords = new ArrayList<>();
        try {
            //发送post请求
            HttpResponse response = HttpUtils.doPost("http://kq.lsyat.com", "/attrecordController.do?openrecordpage", "post", new HashMap<String, String>(),new HashMap<String, String>(),query);
            if (response.getStatusLine().getStatusCode() == 200) {
                String json = EntityUtils.toString(response.getEntity(), Charset.forName("UTF-8"));
                JSONObject object = JSON.parseObject(json).getJSONObject("data");
                JSONArray objects = object.getJSONArray("results");
                objects.forEach(o -> {
                    String dataJson = JSON.toJSONString(o);
                    OpenRecords departree = JSON.parseObject(dataJson, new TypeReference<OpenRecords>() {});
                    openRecords.add(departree);
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return openRecords;
    }

    @Override
    public String getQRcode(Map<String, String> query) {
        String imgUrl = "";
        try {
            //发送post请求
            HttpResponse response = HttpUtils.doPost("http://kq.lsyat.com", "/attrecordController.do?GetQRcode", "post", new HashMap<String, String>(),new HashMap<String, String>(),query);
            if (response.getStatusLine().getStatusCode() == 200) {
                String json = EntityUtils.toString(response.getEntity(), Charset.forName("UTF-8"));
                imgUrl = JSON.parseObject(json).getString("data");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return imgUrl;
    }

    @Override
    public List<VisitorInfo> visitorList(Map<String, String> query) {
        List<VisitorInfo> visitorInfos = new ArrayList<>();
        try {
            //发送post请求
            HttpResponse response = HttpUtils.doPost("http://kq.lsyat.com", "/attrecordController.do?visitorlist", "post", new HashMap<String, String>(),new HashMap<String, String>(),query);
            if (response.getStatusLine().getStatusCode() == 200) {
                String json = EntityUtils.toString(response.getEntity(), Charset.forName("UTF-8"));
                JSONArray objects = JSON.parseObject(json).getJSONArray("data");
                objects.forEach(o -> {
                    String dataJson = JSON.toJSONString(o);
                    VisitorInfo visitorInfo = JSON.parseObject(dataJson, new TypeReference<VisitorInfo>() {});
                    visitorInfos.add(visitorInfo);
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return visitorInfos;

    }
}
