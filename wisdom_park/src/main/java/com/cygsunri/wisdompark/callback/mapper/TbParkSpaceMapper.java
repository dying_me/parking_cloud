package com.cygsunri.wisdompark.callback.mapper;

import com.cygsunri.wisdompark.callback.entity.TbParkSpace;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 车场车位数信息表 Mapper 接口
 * </p>
 *
 * @author hanyuan
 * @since 2021-08-30
 */
public interface TbParkSpaceMapper extends BaseMapper<TbParkSpace> {

}
