package com.cygsunri.wisdompark.callback.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * <p>
 * 单台AC360的设备数据信息
 * </p>
 *
 * @author cygsunri
 * @since 2021-08-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class YaheSingleAC360InfoVO {

    private String id;

    private String name;

    private String code;

    private String mac;

    private String model;

    private String gateway_id;

    private String gateway_name;

    private String address;

    private String product_id;

    private String product_name;

    private String status;

    private String state;

    private String create_date;

    private String create_user_id;

    private String create_user_name;

    private String update_date;

    private String update_user_id;

    private String update_user_name;

    private String corp_code;

    private String corp_name;

    private String dept_id;

    private String dept_code;

    private String dept_name;

    private String timing;

    private String ctemp;

    private String conoff;

    private String cmode;

    private String cwind;

    private String cwinddir;

    private String par;

    private String person;

    private String avol;

    private String bvol;

    private String cvol;

    private String aele;

    private String bele;

    private String cele;

    private String totalpow;

    private String nullpow;

    private String intemp;

    private String outtemp;

    private String venttemp;

    private String intaketemp;

    private String coretemp;

    private String humidity;

    private String inpcon;

    private String usablele;

    private String usedele;

    private String usedtime;

    private String elecon;

    private String tempcon;

    private String season;

    private String relay;

    private String buzzer;

    private String openma;

    private String fault;

    private String temptop;

    private String templow;

    private String networking;

    private String version;

    private String peaktimestart;

    private String peaktimeend;

    private String valleystart;

    private String valleyend;

    private String elepolice;

    private String photositive;

    private String timeSpace;

    private String inframan;

    private String threshold;

    private String colltime;

    private String remotepsw;

    private String onstate;

    private String intemp_value;

    private String coretemp_value;

    private String tuyeretemp_value;

    private String autotemp_value;

    private String indoor_error;

    private String outdoor_error_1;

    private String outdoor_error_2;

}
