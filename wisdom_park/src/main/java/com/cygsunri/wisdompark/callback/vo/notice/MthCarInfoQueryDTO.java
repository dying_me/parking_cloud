package com.cygsunri.wisdompark.callback.vo.notice;

import com.cygsunri.wisdompark.bussiniss.vo.BaseQueryNoticeCondition;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;

/**
 * <p>
 * 新增月租车封装类
 * </p>
 *
 * @author cygsunri
 * @since 2021/8/10
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class MthCarInfoQueryDTO extends BaseQueryNoticeCondition {

    /**
     * 车场唯一编号
     */
    @NotBlank(message = "车场唯一编号不能为空")
    private String parkKey;

    /**
     * 车牌号码
     */
    private String carNo;

    /**
     * 查询第几页数据，默认0为第一页，1为第二页，以此类推
     */
    private String indexId;

    /**
     * 返回查询记录条数
     */
    private String size;

}
