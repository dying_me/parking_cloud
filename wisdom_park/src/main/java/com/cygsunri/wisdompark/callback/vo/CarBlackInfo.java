package com.cygsunri.wisdompark.callback.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author hanyuanLiu
 * @site 上传车辆黑名单
 * @create 2021-06-07 11:27
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class CarBlackInfo {
    /**
     * 停车场key
     */
    public String key;

    /**
     * 车牌号
     */
    public String carNo;

    /**
     * 用户名称
     */
    public String userName;

    /**
     * 手机号码
     */
    public String phone;

    /**
     * 开始时间
     */
    public String beginTime;

    /**
     * 结束时间
     */
    public String endTime;

    /**
     * 发行时间
     */
    public String iusseTime;

    /**
     * 备注
     */
    public String remark;

    /**
     * 状态 0代表删除，1代表新增（当第三方系统中存在此车辆时，应执行修改操作）
     */
    public String status;
}
