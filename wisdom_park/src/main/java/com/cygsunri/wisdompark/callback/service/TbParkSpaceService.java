package com.cygsunri.wisdompark.callback.service;

import com.cygsunri.wisdompark.callback.entity.TbParkSpace;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 车场车位数信息表 服务类
 * </p>
 *
 * @author hanyuan
 * @since 2021-08-30
 */
public interface TbParkSpaceService extends IService<TbParkSpace> {

}
