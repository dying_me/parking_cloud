package com.cygsunri.wisdompark.callback.vo.attendance;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 位置信息,只有外接了gps设备才有这个信息
 * </p>
 *
 * @author cygsunri
 * @since 2021/8/23
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class Gps {
    /**
     * 经度(字符串)
     */
    private String n;
    /**
     * 纬度(字符串)
     */
    private String e;
    /**
     * 参考卫星数量
     */
    private int strength;
}
