package com.cygsunri.wisdompark.callback.service.impl;

import cn.afterturn.easypoi.entity.vo.BigExcelConstants;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import cn.afterturn.easypoi.excel.entity.enmus.ExcelType;
import cn.afterturn.easypoi.view.PoiBaseView;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cygsunri.common.CommonConstant;
import com.cygsunri.common.EnumConstant;
import com.cygsunri.exception.BusinessException;
import com.cygsunri.response.ResultCodeEnum;
import com.cygsunri.wisdompark.bussiniss.vo.BaseResult;
import com.cygsunri.wisdompark.callback.entity.*;
import com.cygsunri.wisdompark.callback.mapper.TbEnterCarMapper;
import com.cygsunri.wisdompark.callback.mapper.TbMthcarPeopleMapper;
import com.cygsunri.wisdompark.callback.service.*;
import com.cygsunri.wisdompark.callback.vo.*;
import com.cygsunri.wisdompark.callback.vo.excel.ExportInoutRecordsModel;
import com.cygsunri.wisdompark.util.CommonUtil;
import com.cygsunri.wisdompark.util.MD5Util;
import com.cygsunri.wisdompark.util.RedisKeys;
import com.cygsunri.wisdompark.util.id.SnowFlakeIdWorker;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * <p>
 * 智慧停车开放平台回调类接口Service
 * </p>
 *
 * @author cygsunri
 * @since 2021/7/29
 */
@Slf4j
@Service
public class CallbackServiceImpl implements CallbackService {

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private TbEnterCarService tbEnterCarService;

    @Autowired
    private TbOutCarService tbOutCarService;

    @Autowired
    private TbEnterCarImgService tbEnterCarImgService;

    @Autowired
    private TbOutCarImgService tbOutCarImgService;

    @Autowired
    private TbEnterCarMapper tbEnterCarMapper;

    @Value("${zhihuipark.snapshotDir}")
    private String snapshotDir;

    @Autowired
    private ExcelInoutRecordsExportService excelInoutRecordsExportService;

    @Autowired
    private TbMthcarPeopleMapper tbMthcarPeopleMapper;

    @Autowired
    private TbParkSpaceService tbParkSpaceService;


    @Override
    @Transactional(rollbackFor = Exception.class)
    public BaseResult carEnterCar(EnterCarConditionVO enterCarConditionVO) {
        log.info("CallbackServiceImpl carEnterCar ==>> "+JSON.toJSONString(enterCarConditionVO));
        BaseResult result = new BaseResult();
        BeanUtils.copyProperties(enterCarConditionVO,result);
        String paramRand = enterCarConditionVO.getRand();
        String paramSign = enterCarConditionVO.getSign();
        try {
            Map<String, Object> stringObjectMap = CommonUtil.objectToMap(enterCarConditionVO);
            EnterCarInfo enterCarInfo = (EnterCarInfo)stringObjectMap.get("data");
            stringObjectMap.put("data",JSON.toJSONString(enterCarInfo));
            stringObjectMap.remove("sign");
            Map<String, Object> strObjectMap = CommonUtil.sortMapByKey(stringObjectMap);
            String urlParamsByMap = CommonUtil.getUrlParamsByMap(strObjectMap);
            String stringSignTemp=urlParamsByMap+"&"+CommonConstant.APP_SECRET;
            String strSign = MD5Util.encrypt(stringSignTemp).toUpperCase();//注：MD5签名方式
            log.info("签名串 ==>> "+stringSignTemp + " Sign签名 ==>> "+strSign);
//            if(paramSign.equals(strSign)){
                String rand = (String)redisTemplate.opsForValue().get(RedisKeys.getSysConfigKey("enrand"));
                if(StringUtils.isNotBlank(rand)){
                    if(!rand.equals(paramRand)){
                        TbEnterCar tbEnterCar = new TbEnterCar();
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        BeanUtils.copyProperties(enterCarConditionVO,tbEnterCar);
                        EnterCarInfo data = enterCarConditionVO.getData();
                        BeanUtils.copyProperties(data,tbEnterCar);
                        tbEnterCar.setId(SnowFlakeIdWorker.generateNextIdStr());
                        tbEnterCar.setParkKey(data.getKey());
                        tbEnterCar.setEnterTime(sdf.parse(data.getEnterTime()));
                        log.info("tbEntterCar ==>> "+JSON.toJSONString(tbEnterCar));
                        boolean flag = tbEnterCarService.save(tbEnterCar);
                        if(!flag){
                            throw new BusinessException(ResultCodeEnum.SYS_ERROR.getCode(),"插入上传车辆入场信息表失败,请核实!");
                        }
                        redisTemplate.opsForValue().set(RedisKeys.getSysConfigKey("enrand"), paramRand);
                        result.setCode("1");
                        result.setMsg("成功");
                    }
                }else{
                    redisTemplate.opsForValue().set(RedisKeys.getSysConfigKey("enrand"), paramRand);
                }
//            }else{
//                throw new BusinessException(ResultCodeEnum.SIGN_NOT_PASS.getCode(),ResultCodeEnum.SIGN_NOT_PASS.getDesc());
//            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new BusinessException(ResultCodeEnum.SYS_ERROR.getCode(),e.getMessage());
        }
        return result;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BaseResult carOutCar(OutCarConditionVO outCarConditionVO) {
        log.info("CallbackServiceImpl carOutCar ==>> "+JSON.toJSONString(outCarConditionVO));
        BaseResult result = new BaseResult();
        BeanUtils.copyProperties(outCarConditionVO,result);
        String paramRand = outCarConditionVO.getRand();
        String paramSign = outCarConditionVO.getSign();
        try {
            Map<String, Object> stringObjectMap = CommonUtil.objectToMap(outCarConditionVO);
            OutCarInfo outCarInfo = (OutCarInfo)stringObjectMap.get("data");
            stringObjectMap.put("data",JSON.toJSONString(outCarInfo));
            stringObjectMap.remove("sign");
            Map<String, Object> strObjectMap = CommonUtil.sortMapByKey(stringObjectMap);
            String urlParamsByMap = CommonUtil.getUrlParamsByMap(strObjectMap);
            String stringSignTemp=urlParamsByMap+"&"+CommonConstant.APP_SECRET;
            String strSign = MD5Util.encrypt(stringSignTemp).toUpperCase();//注：MD5签名方式
            log.info("签名串 ======>> "+stringSignTemp + " Sign签名 ======>> "+strSign);
//            if(paramSign.equals(strSign)){
                String rand = (String)redisTemplate.opsForValue().get(RedisKeys.getSysConfigKey("ourand"));
                if(StringUtils.isNotBlank(rand)){
                    if(!rand.equals(paramRand)){
                        TbOutCar tbOutCar = new TbOutCar();
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        BeanUtils.copyProperties(outCarConditionVO,tbOutCar);
                        OutCarInfo data = outCarConditionVO.getData();
                        BeanUtils.copyProperties(data,tbOutCar);
                        tbOutCar.setId(SnowFlakeIdWorker.generateNextIdStr());
                        tbOutCar.setParkKey(data.getKey());
                        tbOutCar.setOutTime(sdf.parse(data.getOutTime()));
                        tbOutCar.setTotalAmount(new BigDecimal(data.getTotalAmount()));
                        tbOutCar.setCouponMoney(new BigDecimal(data.getCouponMoney()));
                        tbOutCar.setWalletPayMoney(new BigDecimal(data.getWalletPayMoney()));
                        log.info("tbOutCar ==>> "+JSON.toJSONString(tbOutCar));
                        boolean flag = tbOutCarService.save(tbOutCar);
                        if(!flag){
                            throw new BusinessException(ResultCodeEnum.SYS_ERROR.getCode(),"插入上传车辆出场信息表失败,请核实!");
                        }
                        redisTemplate.opsForValue().set(RedisKeys.getSysConfigKey("ourand"), paramRand);
                        result.setCode("1");
                        result.setMsg("成功");
                    }
                }else{
                    redisTemplate.opsForValue().set(RedisKeys.getSysConfigKey("ourand"), paramRand);
                }
//            }else{
//                throw new BusinessException(ResultCodeEnum.SIGN_NOT_PASS.getCode(),ResultCodeEnum.SIGN_NOT_PASS.getDesc());
//            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new BusinessException(ResultCodeEnum.SYS_ERROR.getCode(),e.getMessage());
        }
        return result;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BaseResult carEnterCarImg(EnterOutImgConditionVO enterOutImgConditionVO) {
        log.info("CallbackServiceImpl carEnterCarImg ==>> "+JSON.toJSONString(enterOutImgConditionVO));
        BaseResult result = new BaseResult();
        BeanUtils.copyProperties(enterOutImgConditionVO,result);
        String paramRand = enterOutImgConditionVO.getRand();
        String paramSign = enterOutImgConditionVO.getSign();
        try {
            Map<String, Object> stringObjectMap = CommonUtil.objectToMap(enterOutImgConditionVO);
            EnterOutImgInfo enterOutImgInfo = (EnterOutImgInfo)stringObjectMap.get("data");
            stringObjectMap.put("data",JSON.toJSONString(enterOutImgInfo));
            stringObjectMap.remove("sign");
            Map<String, Object> strObjectMap = CommonUtil.sortMapByKey(stringObjectMap);
            String urlParamsByMap = CommonUtil.getUrlParamsByMap(strObjectMap);
            String stringSignTemp=urlParamsByMap+"&"+CommonConstant.APP_SECRET;
            String strSign = MD5Util.encrypt(stringSignTemp).toUpperCase();//注：MD5签名方式
            log.info("签名串 ==>> "+stringSignTemp + " Sign签名 ==>> "+strSign);
//            if(paramSign.equals(strSign)){
                String rand = (String)redisTemplate.opsForValue().get(RedisKeys.getSysConfigKey("enirand"));
                if(StringUtils.isNotBlank(rand)){
                    if(!rand.equals(paramRand)){
                        TbEnterCarImg tbEnterCarImg = new TbEnterCarImg();
                        BeanUtils.copyProperties(enterOutImgConditionVO,tbEnterCarImg);
                        EnterOutImgInfo data = enterOutImgConditionVO.getData();
                        BeanUtils.copyProperties(data,tbEnterCarImg);
                        tbEnterCarImg.setId(SnowFlakeIdWorker.generateNextIdStr());
                        tbEnterCarImg.setParkKey(data.getKey());
                        //http协议get方法获取图片并保存到本地
                        String imgUrl = tbEnterCarImg.getImgUrl();
                        String format = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
                        String dir = format + "/";
                        String filePath = snapshotDir + dir + SnowFlakeIdWorker.generateNextIdStr()+".jpg";
                        log.info("照片文件目录 ==>> " + filePath);
                        CommonUtil.changeFolderPermission(filePath);
                        CommonUtil.saveImageToDisk(imgUrl,filePath);
                        tbEnterCarImg.setImgUrl(filePath);
                        boolean flag = tbEnterCarImgService.save(tbEnterCarImg);
                        if(!flag){
                            throw new BusinessException(ResultCodeEnum.SYS_ERROR.getCode(),"插入上传车辆入场图片信息表失败,请核实!");
                        }
                        redisTemplate.opsForValue().set(RedisKeys.getSysConfigKey("enirand"), paramRand);
                        result.setCode("1");
                        result.setMsg("成功");
                    }
                }else{
                    redisTemplate.opsForValue().set(RedisKeys.getSysConfigKey("enirand"), paramRand);
                }
//            }else{
//                throw new BusinessException(ResultCodeEnum.SIGN_NOT_PASS.getCode(),ResultCodeEnum.SIGN_NOT_PASS.getDesc());
//            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new BusinessException(ResultCodeEnum.SYS_ERROR.getCode(),e.getMessage());
        }
        return result;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BaseResult carOutCarImg(EnterOutImgConditionVO enterOutImgConditionVO) {
        log.info("CallbackServiceImpl carOutCarImg ==>> "+JSON.toJSONString(enterOutImgConditionVO));
        BaseResult result = new BaseResult();
        BeanUtils.copyProperties(enterOutImgConditionVO,result);
        String paramRand = enterOutImgConditionVO.getRand();
        String paramSign = enterOutImgConditionVO.getSign();
        try {
            Map<String, Object> stringObjectMap = CommonUtil.objectToMap(enterOutImgConditionVO);
            EnterOutImgInfo enterOutImgInfo = (EnterOutImgInfo)stringObjectMap.get("data");
            stringObjectMap.put("data",JSON.toJSONString(enterOutImgInfo));
            stringObjectMap.remove("sign");
            Map<String, Object> strObjectMap = CommonUtil.sortMapByKey(stringObjectMap);
            String urlParamsByMap = CommonUtil.getUrlParamsByMap(strObjectMap);
            String stringSignTemp=urlParamsByMap+"&"+CommonConstant.APP_SECRET;
            String strSign = MD5Util.encrypt(stringSignTemp).toUpperCase();//注：MD5签名方式
            log.info("签名串 ==>> "+stringSignTemp + " Sign签名 ==>> "+strSign);
//            if(paramSign.equals(strSign)){
                String rand = (String)redisTemplate.opsForValue().get(RedisKeys.getSysConfigKey("ouirand"));
                if(StringUtils.isNotBlank(rand)){
                    if(!rand.equals(paramRand)){
                        TbOutCarImg tbOutCarImg = new TbOutCarImg();
                        BeanUtils.copyProperties(enterOutImgConditionVO,tbOutCarImg);
                        EnterOutImgInfo data = enterOutImgConditionVO.getData();
                        BeanUtils.copyProperties(data,tbOutCarImg);
                        tbOutCarImg.setId(SnowFlakeIdWorker.generateNextIdStr());
                        tbOutCarImg.setParkKey(data.getKey());
                        //http协议get方法获取图片并保存到本地
                        String imgUrl = tbOutCarImg.getImgUrl();
                        String format = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
                        String dir = format + "/";
                        String filePath = snapshotDir + dir + SnowFlakeIdWorker.generateNextIdStr()+".jpg";
                        log.info("照片文件目录 ==>> " + filePath);
                        CommonUtil.changeFolderPermission(filePath);
                        CommonUtil.saveImageToDisk(imgUrl,filePath);
                        tbOutCarImg.setImgUrl(filePath);
                        boolean flag = tbOutCarImgService.save(tbOutCarImg);
                        if(!flag){
                            throw new BusinessException(ResultCodeEnum.SYS_ERROR.getCode(),"插入上传车辆出场图片信息表失败,请核实!");
                        }
                        redisTemplate.opsForValue().set(RedisKeys.getSysConfigKey("ouirand"), paramRand);
                        result.setCode("1");
                        result.setMsg("成功");
                    }
                }else{
                    redisTemplate.opsForValue().set(RedisKeys.getSysConfigKey("ouirand"), paramRand);
                }
//            }else{
//                throw new BusinessException(ResultCodeEnum.SIGN_NOT_PASS.getCode(),ResultCodeEnum.SIGN_NOT_PASS.getDesc());
//            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new BusinessException(ResultCodeEnum.SYS_ERROR.getCode(),e.getMessage());
        }
        return result;
    }

    @Override
    public PageInfo<OutEnterCarVO> pageInOutRecordListQuey(EnterOutCarQueryDTO enterOutCarQueryDTO) {
        PageHelper.startPage(enterOutCarQueryDTO.getPageNumber(),enterOutCarQueryDTO.getPageSize());
        List<OutEnterCarVO> list = tbEnterCarMapper.getInEnterCarPageList(enterOutCarQueryDTO);
        list.forEach((item) ->{
            item.setCarTypeStatus(EnumConstant.StatusEnum.getDescFromCode(item.getCarType()));
        });
        return new PageInfo<>(list);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteById(String id) {
        QueryWrapper<TbEnterCar> enterWrapper = new QueryWrapper<>();
        enterWrapper.eq("park_key",id);
        TbEnterCar enterCar = tbEnterCarService.getById(id);
        if(enterCar!=null){
            String parkKey = enterCar.getParkKey();
            String carNo = enterCar.getCarNo();
            String orderNo = enterCar.getOrderNo();
            boolean temp = tbEnterCarService.removeById(enterCar.getId());
            if(!temp){
                throw new BusinessException(ResultCodeEnum.SYS_ERROR.getCode(),"删除车辆入场信息失败,请核实!");
            }
            QueryWrapper<TbEnterCarImg> enterImgWarpper = new QueryWrapper<>();
            if(StringUtils.isNotBlank(parkKey)){
                enterImgWarpper.eq("park_key",parkKey);
            }
            if(StringUtils.isNotBlank(carNo)){
                enterImgWarpper.eq("car_no",carNo);
            }
            if(StringUtils.isNotBlank(orderNo)){
                enterImgWarpper.eq("order_no",orderNo);
            }
            TbEnterCarImg tbEnterCarImg = tbEnterCarImgService.getOne(enterImgWarpper);
            if(tbEnterCarImg!=null){
                boolean flag = tbEnterCarImgService.removeById(tbEnterCarImg.getId());
                if(!flag){
                    throw new BusinessException(ResultCodeEnum.SYS_ERROR.getCode(),"删除车辆入场照片信息失败,请核实!");
                }
            }
            QueryWrapper<TbOutCar> outWarpper = new QueryWrapper<>();
            if(StringUtils.isNotBlank(parkKey)){
                outWarpper.eq("park_key",parkKey);
            }
            if(StringUtils.isNotBlank(carNo)){
                outWarpper.eq("car_no",carNo);
            }
            if(StringUtils.isNotBlank(orderNo)){
                outWarpper.eq("order_no",orderNo);
            }
            TbOutCar tbOutCar = tbOutCarService.getOne(outWarpper);
            if(tbOutCar!=null){
                boolean flag = tbOutCarService.removeById(tbOutCar.getId());
                if(!flag){
                    throw new BusinessException(ResultCodeEnum.SYS_ERROR.getCode(),"删除车辆出场信息失败,请核实!");
                }
            }
            QueryWrapper<TbOutCarImg> outImgWarpper = new QueryWrapper<>();
            if(StringUtils.isNotBlank(parkKey)){
                outImgWarpper.eq("park_key",parkKey);
            }
            if(StringUtils.isNotBlank(carNo)){
                outImgWarpper.eq("car_no",carNo);
            }
            if(StringUtils.isNotBlank(orderNo)){
                outImgWarpper.eq("order_no",orderNo);
            }
            TbOutCarImg tbOutCarImg = tbOutCarImgService.getOne(outImgWarpper);
            if(tbOutCarImg!=null){
                boolean flag = tbOutCarImgService.removeById(tbOutCarImg.getId());
                if(!flag){
                    throw new BusinessException(ResultCodeEnum.SYS_ERROR.getCode(),"删除车辆出场照片信息失败,请核实!");
                }
            }
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteBatchByIds(String[] ids) {
        //删除车辆入场照片信息表
        tbEnterCarImgService.deleteBatch(ids);
        //删除车辆出场信息表
        tbOutCarService.deleteBatch(ids);
        //删除车辆出场照片信息表
        tbOutCarImgService.deleteBatch(ids);
        //删除车辆入场信息表
        tbEnterCarService.removeByIds(Arrays.asList(ids));
    }

    @Override
    public void exportedInoutRecordList(EnterOutCarQueryDTO enterOutCarQueryDTO, HttpServletRequest request, HttpServletResponse response, ModelMap modelMap) {
        ExportParams params = new ExportParams("车辆进出场记录数据导出", "车辆进出场记录数据", ExcelType.XSSF);
        modelMap.put(BigExcelConstants.CLASS, ExportInoutRecordsModel.class);
        modelMap.put(BigExcelConstants.PARAMS, params);
        Map<String,Object> dataParams = new HashMap<>();
        dataParams.put("parkingLotId",enterOutCarQueryDTO.getParkingLotId());
        dataParams.put("carNumber",enterOutCarQueryDTO.getCarNumber());
        dataParams.put("beginTime",enterOutCarQueryDTO.getBeginTime());
        dataParams.put("endTime",enterOutCarQueryDTO.getEndTime());
        dataParams.put("flag",enterOutCarQueryDTO.getFlag());
        modelMap.put(BigExcelConstants.DATA_PARAMS, dataParams);
        modelMap.put(BigExcelConstants.DATA_INTER,excelInoutRecordsExportService);
        modelMap.put(BigExcelConstants.FILE_NAME,"车辆进出场记录数据导出");
        PoiBaseView.render(modelMap, request, response, BigExcelConstants.EASYPOI_BIG_EXCEL_VIEW);
    }

    @Override
    public PageInfo<TbMthcarPeople> getPageMthCarPersons(MthcarPeopleQueryDTO mthcarPeopleQueryDTO) {
        PageHelper.startPage(mthcarPeopleQueryDTO.getPageNumber(),mthcarPeopleQueryDTO.getPageSize());
        List<TbMthcarPeople> list = tbMthcarPeopleMapper.getPageMthCarPersons(mthcarPeopleQueryDTO);
        list.forEach((item) ->{
            item.setCarTypeStatus(EnumConstant.StatusEnum.getDescFromCode(item.getCarTypeNo()));
        });
        return new PageInfo<>(list);
    }

    @Override
    public void deleteMthCarPerBatch(String[] ids) {
        int flag = tbMthcarPeopleMapper.deleteBatchIds(Arrays.asList(ids));
        if(flag == 0){
            throw new BusinessException(ResultCodeEnum.SYS_ERROR.getCode(),"批量删除月租车用户信息失败,请核实!");
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BaseResult sendParkSpace(SendParkSpaceReq sendParkSpaceReq) {
        log.info("CallbackServiceImpl sendParkSpace ==>> "+JSON.toJSONString(sendParkSpaceReq));
        BaseResult result = new BaseResult();
        BeanUtils.copyProperties(sendParkSpaceReq,result);
        String paramRand = sendParkSpaceReq.getRand();
        String paramSign = sendParkSpaceReq.getSign();
        try {
            Map<String, Object> stringObjectMap = CommonUtil.objectToMap(sendParkSpaceReq);
            SendParkSpaceDataInfo sendParkSpaceDataInfo = (SendParkSpaceDataInfo)stringObjectMap.get("data");
            stringObjectMap.put("data",JSON.toJSONString(sendParkSpaceDataInfo));
            stringObjectMap.remove("sign");
            Map<String, Object> strObjectMap = CommonUtil.sortMapByKey(stringObjectMap);
            String urlParamsByMap = CommonUtil.getUrlParamsByMap(strObjectMap);
            String stringSignTemp=urlParamsByMap+"&"+CommonConstant.APP_SECRET;
            String strSign = MD5Util.encrypt(stringSignTemp).toUpperCase();//注：MD5签名方式
            log.info("签名串 ==>> "+stringSignTemp + " Sign签名 ==>> "+strSign);
//            if(paramSign.equals(strSign)){
            String rand = (String)redisTemplate.opsForValue().get(RedisKeys.getSysConfigKey("parspace"));
            if(StringUtils.isNotBlank(rand)){
                if(!rand.equals(paramRand)){
                    TbParkSpace tbParkSpace = new TbParkSpace();
                    BeanUtils.copyProperties(sendParkSpaceReq,tbParkSpace);
                    tbParkSpace.setId(SnowFlakeIdWorker.generateNextIdStr());
                    tbParkSpace.setParkKey(sendParkSpaceReq.getData().getKey());
                    tbParkSpace.setRemainderSpaces(sendParkSpaceReq.getData().getRemainder_spaces());
                    tbParkSpace.setTotalSpaces(sendParkSpaceReq.getData().getTotal_spaces());
                    log.info("TbParkSpace ==>> "+JSON.toJSONString(tbParkSpace));
                    QueryWrapper<TbParkSpace> wrapper = new QueryWrapper();
                    wrapper.eq("park_key",sendParkSpaceReq.getData().getKey());
                    TbParkSpace space = tbParkSpaceService.getOne(wrapper);
                    if(space!=null){
                        tbParkSpace.setId(space.getId());
                        boolean flag = tbParkSpaceService.updateById(tbParkSpace);
                        if(!flag){
                            throw new BusinessException(ResultCodeEnum.SYS_ERROR.getCode(),"修改车场车位信息表失败,请核实!");
                        }
                    }else{
                        boolean flag = tbParkSpaceService.save(tbParkSpace);
                        if(!flag){
                            throw new BusinessException(ResultCodeEnum.SYS_ERROR.getCode(),"插入车场车位信息表失败,请核实!");
                        }
                    }
                    redisTemplate.opsForValue().set(RedisKeys.getSysConfigKey("parspace"), paramRand);
                    result.setCode("1");
                    result.setMsg("成功");
                }
            }else{
                redisTemplate.opsForValue().set(RedisKeys.getSysConfigKey("parspace"), paramRand);
            }
//            }else{
//                throw new BusinessException(ResultCodeEnum.SIGN_NOT_PASS.getCode(),ResultCodeEnum.SIGN_NOT_PASS.getDesc());
//            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new BusinessException(ResultCodeEnum.SYS_ERROR.getCode(),e.getMessage());
        }
        return result;
    }
}
