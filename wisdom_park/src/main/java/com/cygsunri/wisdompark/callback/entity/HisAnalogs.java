package com.cygsunri.wisdompark.callback.entity;

import lombok.Data;

/**
 * <p>
 *  influxdb
 * </p>
 *
 * @author cygsunri
 * @since 2021/6/29
 */
@Data
public class HisAnalogs {
    /**
     * id
     */
    private String code;
    /**
     * uuid
     */
    private String time;
    /**
     * 部门名称
     */
    private String data;

}
