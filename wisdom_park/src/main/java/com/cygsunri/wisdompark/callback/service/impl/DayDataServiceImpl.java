package com.cygsunri.wisdompark.callback.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cygsunri.wisdompark.callback.entity.DayData;
import com.cygsunri.wisdompark.callback.entity.TbEnterCar;
import com.cygsunri.wisdompark.callback.mapper.DayDataMapper;
import com.cygsunri.wisdompark.callback.mapper.DeviceTemplateDatasMapper;
import com.cygsunri.wisdompark.callback.service.DayDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Wrapper;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hanyuan
 * @since 2021-07-30
 */
@Service
public class DayDataServiceImpl extends ServiceImpl<DayDataMapper, DayData> implements DayDataService {

    @Autowired
    private DayDataMapper dayDataMapper;

    /**
     * 通过时间和名称和相差天数查找值
     *
     * @param time  时间
     * @param name 名称
     * @param diff 相差天数
     * @return DeviceTemplateData
     */
    @Override
    public List<DayData> finDiffDayDataByTimeAndName(String time, String name, int diff) {
        return dayDataMapper.finDiffDayDataByTimeAndName(time,name,diff);
    }
    /**
     * 通过时间和名称查找值
     *
     * @param time  时间
     * @param name 名称
     * @return DeviceTemplateData
     */
    @Override
    public List<DayData> finDayDataByTimeAndName(String time, String name) {
        LambdaQueryWrapper<DayData> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(DayData::getTime,time);
        wrapper.eq(DayData::getName,name);
        return dayDataMapper.selectList(wrapper);
    }
}
