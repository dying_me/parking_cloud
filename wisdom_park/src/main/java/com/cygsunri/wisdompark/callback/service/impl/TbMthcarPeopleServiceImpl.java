package com.cygsunri.wisdompark.callback.service.impl;

import com.cygsunri.wisdompark.callback.entity.TbMthcarPeople;
import com.cygsunri.wisdompark.callback.mapper.TbMthcarPeopleMapper;
import com.cygsunri.wisdompark.callback.service.TbMthcarPeopleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 月租车用户信息表 服务实现类
 * </p>
 *
 * @author hanyuan
 * @since 2021-08-18
 */
@Service
public class TbMthcarPeopleServiceImpl extends ServiceImpl<TbMthcarPeopleMapper, TbMthcarPeople> implements TbMthcarPeopleService {

}
