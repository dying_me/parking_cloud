package com.cygsunri.wisdompark.callback.vo.attendance;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 人员属性信息
 * </p>
 *
 * @author cygsunri
 * @since 2021/8/23
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class Person {
    /**
     * 性别（male表示男，female表示女,空字符串表示无此信息）
     */
    private String sex;
    /**
     * 年龄（0表示无此信息）
     */
    private Integer age;
    /**
     * 是否佩戴安全帽
     * none：未佩戴安全帽。white：白色安全帽。blue：蓝色安全帽。
     * orange：橙色安全帽。red：红色安全帽。yellow：黄色安全帽。
     */
    private String hat;
    /**
     * 体温
     */
    private Double temperatur;
    /**
     * 是否佩戴口罩
     *  1（true）， 0（false）
     */
    private Boolean has_mask;
    /**
     * 人像质量。（0~100）
     */
    private Integer face_quality;
    /**
     * 扭转角度。头部旋转平面与图片不处于同一平面。取值范围-90~90
     */
    private Integer turn_angle;
    /**
     * 平面旋转角度。头部旋转平面与图像处于同一平面。取值范围-90~90。
     */
    private Integer rotate_angle;
    /**
     * 32bit韦根协议门禁卡号
     */
    private Integer wg_card_id;
    /**
     * 64bit韦根协议门禁卡号，wg_card_id字段不存在时，使用此字段作为门禁卡号
     */
    private Long long_card_id;
}
