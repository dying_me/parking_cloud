package com.cygsunri.wisdompark.callback.vo;

import com.cygsunri.wisdompark.bussiniss.vo.BaseCondition;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * <p>
 * 回调类进场车辆车牌修正信息Bean
 * </p>
 *
 * @author cygsunri
 * @since 2021-06-04
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class UpdateCarConditionVO extends BaseCondition {

    /**
     * 进场车辆车牌修正
     */
    private UpdateCarInfo data;

}
