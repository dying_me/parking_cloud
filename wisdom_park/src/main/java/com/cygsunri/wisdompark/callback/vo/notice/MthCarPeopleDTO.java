package com.cygsunri.wisdompark.callback.vo.notice;

import com.cygsunri.wisdompark.bussiniss.vo.BaseQueryNoticeCondition;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;

/**
 * <p>
 * 新增月租车用户封装类
 * </p>
 *
 * @author cygsunri
 * @since 2021/8/10
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class MthCarPeopleDTO extends BaseQueryNoticeCondition {

    /**
     * 车场唯一编号
     */
    @NotBlank(message = "车场唯一编号不能为空")
    private String parkKey;

    /**
     * 车主姓名
     */
    @NotBlank(message = "车主姓名不能为空")
    private String userName;

    /**
     * 车辆类型编码
     */
    @NotBlank(message = "车辆类型编码不能为空")
    private String carTypeNo;

    /**
     * 车位联系人地址
     */
    @NotBlank(message = "车位联系人地址不能为空")
    private String homeAddress;

    /**
     * 车位负责人联系方式
     */
    @NotBlank(message = "车位负责人联系方式不能为空")
    private String mobNumber;

    /**
     * 车场车位编号
     */
    private String parkNo;

    /**
     * 机号
     */
    private String machineNo;
}
