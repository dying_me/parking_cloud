package com.cygsunri.wisdompark.callback.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * modbus寄存器信息表
 * </p>
 */
@Data
@Accessors(chain = true)
@TableName("tb_deviceTemplate")
public class DeviceTemplate implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键,UUID
     */
    @TableId(value = "ID", type = IdType.ASSIGN_UUID)
    private String id;

    /**
     * 寄存器地址
     */
    @TableField("comments")
    private String comments;

    /**
     * 定义说明
     */
    @TableField("name")
    private String name;


}
