package com.cygsunri.wisdompark.callback.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cygsunri.wisdompark.callback.entity.DeviceInfo;
import com.cygsunri.wisdompark.callback.entity.DeviceTemplateData;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author
 * @since 2020-12-21
 */
public interface DeviceTemplateDatasMapper extends BaseMapper<DeviceTemplateData> {

    /**
     * 通过设备ID查找模板信息
     *
     * @param psrID  设备ID
     * @param
     * @return DeviceTemplateData
     */
    List<DeviceTemplateData> findDataByPsrID(String psrID);

    /**
     * 通过设备code查找设备信息
     *
     * @param devCode  设备ID
     * @param
     * @return DeviceTemplateData
     */
    DeviceInfo findDeviceByDevCode(String devCode);
}
