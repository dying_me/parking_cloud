package com.cygsunri.wisdompark.callback.entity;

import lombok.Data;

/**
 * <p>
 * 用户基本信息
 * </p>
 *
 * @author cygsunri
 * @since 2021/6/29
 */
@Data
public class UserBaseInfo {

    /**
     * 用户名
     */
    private String username;

    /**
     * token
     */
    private String token;

    /**
     * 公司编号
     */
    private String orgcode;

    /**
     * 所属部门
     */
    private String departname;

    /**
     * 是否已经绑定设备
     */
    private boolean havedevice;

}
