package com.cygsunri.wisdompark.callback.controller;


import com.cygsunri.wisdompark.bussiniss.vo.PhotographBackBaseResult;
import com.cygsunri.wisdompark.callback.service.TbPhotographService;
import com.cygsunri.wisdompark.callback.vo.attendance.PhotographDataDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 共享考勤开放平台回调类 前端控制器
 * </p>
 *
 * @author hanyuan
 * @since 2021-08-23
 */
@Slf4j
@RestController
@RequestMapping("/photographback/")
public class PhotographBackController {

    @Autowired
    private TbPhotographService tbPhotographService;

    /**
     * 抓拍数据回调推送
     * @param photographDataDTO
     * @return ResponseEntity
     */
    @RequestMapping(value = "/getPhotographData", method = RequestMethod.POST)
    public ResponseEntity<PhotographBackBaseResult> getPhotographData(@RequestBody PhotographDataDTO photographDataDTO)  {
        PhotographBackBaseResult data = tbPhotographService.getPhotographData(photographDataDTO);
        return new ResponseEntity<>(data, HttpStatus.OK);
    }

}

