package com.cygsunri.wisdompark.callback.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cygsunri.wisdompark.callback.entity.TbEnterCar;
import com.cygsunri.wisdompark.callback.mapper.TbEnterCarMapper;
import com.cygsunri.wisdompark.callback.service.TbEnterCarService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 上传车辆入场信息表 服务实现类
 * </p>
 *
 * @author hanyuan
 * @since 2021-07-30
 */
@Service
public class TbEnterCarServiceImpl extends ServiceImpl<TbEnterCarMapper, TbEnterCar> implements TbEnterCarService {




}
