package com.cygsunri.wisdompark.callback.vo;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;


/**
 * <p>
 * 车辆出场信息Data Bean
 * </p>
 *
 * @author cygsunri
 * @since 2021-06-04
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class OutCarInfo {

    /**
     * 停车场key
     */
    @NotBlank(message = "停车场key不能为空")
    @JSONField(ordinal = 1,name="key")
    public String key;

    /**
     * 车牌号
     */
    @NotBlank(message = "车牌号不能为空")
    @JSONField(ordinal = 2,name="carNo")
    public String carNo;

    /**
     * 停车订单号
     */
    @NotBlank(message = "停车订单号不能为空")
    @JSONField(ordinal = 3,name="orderNo")
    public String orderNo;

    /**
     * 出场时间
     */
    //@NotBlank(message = "出场时间不能为空")
    @JSONField(ordinal = 4,name="outTime")
    public String outTime;

    /**
     * 车辆类型编码
     */
    //@NotBlank(message = "车辆类型编码不能为空")
    @JSONField(ordinal = 5,name="carType")
    public String carType;

    /**
     * 出口车道名称
     */
    //@NotBlank(message = "出口车道名称不能为空")
    @JSONField(ordinal = 6,name="gateName")
    public String gateName;

    /**
     * 出口操作员名称
     */
    //@NotBlank(message = "出口操作员名称不能为空")
    @JSONField(ordinal = 7,name="operatorName")
    public String operatorName;

    /**
     * 订单总金额
     */
    //@NotBlank(message = "订单总金额不能为空")
    @JSONField(ordinal = 8,name="totalAmount")
    public String totalAmount;

    /**
     * 优惠券GUID
     */
    //@NotBlank(message = "优惠券GUID不能为空")
    @JSONField(ordinal = 9,name="couponKey")
    public String couponKey;

    /**
     * 出口车辆图片
     */
    //@NotBlank(message = "出口车辆图片不能为空")
    @JSONField(ordinal = 10,name="imgUrl")
    public String imgUrl;

    /**
     * 优惠券金额
     */
    //@NotBlank(message = "优惠券金额不能为空")
    @JSONField(ordinal = 11,name="couponMoney")
    public String couponMoney;

    /**
     * 钱包付款金额
     */
    //@NotBlank(message = "钱包付款金额不能为空")
    @JSONField(ordinal = 12,name="walletPayMoney")
    public String walletPayMoney;

    /**
     * 免费原因
     */
    @JSONField(ordinal = 13,name="freeReason")
    public String freeReason;

}
