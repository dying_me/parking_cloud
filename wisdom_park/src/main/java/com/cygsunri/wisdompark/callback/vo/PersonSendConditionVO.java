package com.cygsunri.wisdompark.callback.vo;

import com.cygsunri.wisdompark.bussiniss.vo.BaseCondition;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author hanyuanLiu
 * @site 上传新增人事信息VO
 * @create 2021-06-07 11:30
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class PersonSendConditionVO extends BaseCondition {

    /**
     *上传新增人事信息
     */
    private PersonSendInfo personSendInfo;

}
