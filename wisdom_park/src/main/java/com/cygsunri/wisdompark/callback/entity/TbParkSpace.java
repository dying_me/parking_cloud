package com.cygsunri.wisdompark.callback.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 车场车位数信息表
 * </p>
 *
 * @author hanyuan
 * @since 2021-08-30
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class TbParkSpace implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * ID
     */
    @TableId(value = "id", type = IdType.INPUT)
    private String id;

    /**
     * appID
     */
    private String appid;

    /**
     * 版本号
     */
    private String version;

    /**
     * 接口方法
     */
    private String method;

    /**
     * 随机值
     */
    private String rand;

    /**
     * 签名
     */
    private String sign;

    /**
     * 车场唯一编号
     */
    private String parkKey;

    /**
     * 空闲车位数
     */
    private String remainderSpaces;

    /**
     * 总车位数
     */
    private String totalSpaces;


}
