package com.cygsunri.wisdompark.callback.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * <p>
 * 	上传交接班记录data
 * </p>
 *
 * @author cygsunri
 * @since 2021-06-04
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class PutWorkHandoverInfo {

    /**
     * 停车场key
     */
    private String	Parking_Key;

    /**
     * 交班人姓名
     */
    private String	GoPersonName;

    /**
     * 接班人姓名
     */
    private String	ToPersonName;

    /**
     * 上班时间
     */
    private String	WorkStartTime;

    /**
     * 下班时间
     */
    private String	WorkEndTime;

    /**
     * 停车应收费
     */
    private String	PayableMoney;

    /**
     * 停车实收费
     */
    private String	ReceivedMoney;

    /**
     * 入场总车次
     */
    private String	InAllCount;

    /**
     * 出场总车次
     */
    private String	OutAllCount;

    /**
     * 入场临时车次
     */
    private String	InTempCarCount;

    /**
     * 出场临时车次
     */
    private String	OutTempCarCount;

    /**
     * 储值车扣款金额
     */
    private String	StoreCarDeductMoney;

    /**
     * A类车收费金额
     */
    private String	ACarReceiptMoney;

    /**
     * B类车收费金额
     */
    private String	BCarReceiptMoney;

    /**
     * C类车收费金额
     */
    private String	CCarReceiptMoney;

    /**
     * D类车收费金额
     */
    private String	DCarReceiptMoney;

    /**
     * A类车出场次数
     */
    private String	OutACarCount;

    /**
     * B类车出场次数
     */
    private String	OutBCarCount;

    /**
     * C类车出场次数
     */
    private String	OutCCarCount;

    /**
     * D类车出场次数
     */
    private String	OutDCarCount;

    /**
     * 人工开闸次数(软件开闸)
     */
    private String	SoftOpenGateCount;

    /**
     * 手工开闸次数(遥控器)
     */
    private String	HandOpenGateCount;

    /**
     * 收费车辆数
     */
    private String	ChargeCarCount;

    /**
     * 免费车辆数
     */
    private String	FreeCarCount;

    /**
     * 发生免费车辆数
     */
    private String	IntoFreeCarCount;

    /**
     * 上报时间
     */
    private String	PutDateTime;


}
