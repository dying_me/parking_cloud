package com.cygsunri.wisdompark.callback.vo;

import com.cygsunri.wisdompark.bussiniss.vo.BaseCondition;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.Valid;


/**
 * <p>
 * 回调类车辆入出场图片信息Bean
 * </p>
 *
 * @author cygsunri
 * @since 2021-06-04
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class EnterOutImgConditionVO extends BaseCondition {

    /**
     * 车辆入出场图片信息
     */
    @Valid
    private EnterOutImgInfo data;

}
