package com.cygsunri.wisdompark.callback.entity;

import lombok.Data;

/**
 * <p>
 * 封装开门记录信息
 * </p>
 *
 * @author cygsunri
 * @since 2021/6/30
 */
@Data
public class OpenRecords {

    /**
     * 所属公司
     */
    private String uninto;
    /**
     * 用户名
     */
    private String userid;
    /**
     * 截图
     */
    private String url;
    /**
     * 开门类型
     */
    private String typeopen;
    /**
     * 设备序列号
     */
    private String desn;
}
