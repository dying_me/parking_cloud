package com.cygsunri.wisdompark.callback.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 */
@Data
@Accessors(chain = true)
@TableName("tb_devicetemplatedata")
public class DeviceTemplateData implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键,UUID
     */
    @TableId(value = "ID", type = IdType.ASSIGN_UUID)
    private String id;

    /**
     *
     */
    @TableField("array")
    private int array;

    /**
     *
     */
    @TableField("calculate")
    private Boolean calculate;

    /**
     *
     */
    @TableField("comments")
    private String comments;

    /**
     *
     */
    @TableField("endureCalculate")
    private Boolean endureCalculate;

    /**
     *
     */
    @TableField("expression")
    private String expression;

    /**
     *
     */
    @TableField("indexNumber")
    private int indexNumber;

    /**
     *
     */
    @TableField("name")
    private String name;

    /**
     * type=1：遥信 2：遥测
     */
    @TableField("type")
    private int type;

    /**
     *
     */
    @TableField("unit")
    private String unit;

    /**
     *
     */
    @TableField("deviceTemplate_id")
    private String deviceTemplate_id;

    /**
     * 寄存器地址
     */
    @TableField("registerAddress")
    private String registerAddress;

    /**
     * 占用位数
     */
    @TableField("dataSize")
    private int dataSize;

    /**
     * 数据类型
     */
    @TableField("dataType")
    private String dataType;

    /**
     * 是否存储到redis
     */
    @TableField("saveRedis")
    private Boolean saveRedis;

}
