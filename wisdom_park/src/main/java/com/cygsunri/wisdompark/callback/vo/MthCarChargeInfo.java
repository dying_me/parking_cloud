package com.cygsunri.wisdompark.callback.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * <p>
 * 月租车充值延期Data Bean
 * </p>
 *
 * @author cygsunri
 * @since 2021-06-04
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class MthCarChargeInfo {

    /**
     * 停车场key
     */
    public String key;

    /**
     * 车牌号
     */
    public String carNo;

    /**
     * 充值开始时间
     */
    public String beginTime;

    /**
     * 充值结束时间
     */
    public String endTime;

    /**
     * 充值时间
     */
    public String chargeTime;

    /**
     * 充值金额
     */
    public String chargeMoney;

    /**
     * 操作员名称
     */
    public String operatorName;

}
