package com.cygsunri.wisdompark.callback.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * <p>
 * Bean
 * </p>
 *
 * @author cygsunri
 * @since 2021-06-04
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class ReserveSendDataInfo {

    /**
     * 停车场key
     */
    public String key;

    /**
     * 车牌号
     */
    public String carNo;

    /**
     * 停车订单号
     */
    public String orderNo;


}
