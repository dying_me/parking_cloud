package com.cygsunri.wisdompark.callback.vo;

import com.cygsunri.wisdompark.bussiniss.vo.BaseCondition;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author hanyuanLiu
 * @site 创建临停车支付订单vo
 * @create 2021-06-07 13:50
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class OrderPayCreateConditionVO extends BaseCondition {

    /**
     * 车场唯一编号
     */
    private String parkKey;

    /**
     * 停车场订单号
     */
    private String orderNo;

    /**
     * 订单金额单位:元
     */
    private String orderAmount;

    /**
     * 支付场景：1中央缴费，2出口缴费 （为null时，为中央缴费）
     */
    private String payScene;
}
