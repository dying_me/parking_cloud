package com.cygsunri.wisdompark.callback.vo;

import com.cygsunri.wisdompark.bussiniss.vo.BaseCondition;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;


/**
 * <p>
 * 上传停车场设备状态data
 * </p>
 *
 * @author cygsunri
 * @since 2021-06-04
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ToString(callSuper = true)
public class AboutCarReq extends BaseCondition {

    /**
     * data
     */
    public AboutCarDataInfo data;


}
