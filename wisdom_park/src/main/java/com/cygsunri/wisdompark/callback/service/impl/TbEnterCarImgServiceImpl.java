package com.cygsunri.wisdompark.callback.service.impl;

import com.cygsunri.wisdompark.callback.entity.TbEnterCarImg;
import com.cygsunri.wisdompark.callback.mapper.TbEnterCarImgMapper;
import com.cygsunri.wisdompark.callback.service.TbEnterCarImgService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 上传车辆入场图片信息表 服务实现类
 * </p>
 *
 * @author hanyuan
 * @since 2021-07-30
 */
@Service
public class TbEnterCarImgServiceImpl extends ServiceImpl<TbEnterCarImgMapper, TbEnterCarImg> implements TbEnterCarImgService {

    @Override
    public void deleteBatch(String[] ids) {
        baseMapper.deleteBatch(ids);
    }
}
