package com.cygsunri.wisdompark.callback.service;

import com.cygsunri.wisdompark.callback.entity.*;

import java.util.List;
import java.util.Map;

public interface AttrecordService {

    UserBaseInfo checkUser(Map<String, String> query);

    List<PeopleInfo> faceWhitelist(Map<String, String> query);

    List<Departree> departmentList(Map<String, String> query);

    boolean openThegate(Map<String, String> query);

    List<OpenRecords> openRecordPage(Map<String, String> query);

    String getQRcode(Map<String, String> query);

    List<VisitorInfo> visitorList(Map<String, String> query);
}
