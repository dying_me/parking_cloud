package com.cygsunri.wisdompark.callback.vo.attendance;

import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * <p>
 * 回调类抓拍数据封装
 * </p>
 *
 * @author cygsunri
 * @since 2021/8/10
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class PhotographDataDTO  {

    /**
     * 推送方协议版本
     */
    private String version;
    /**
     * 命令
     */
    private String cmd;
    /**
     * 序号
     */
    private Integer sequence_no;
    /**
     * 设备（相机）编号
     */
    private String device_no;
    /**
     * 设备（相机）序列号
     */
    private String device_sn;
    /**
     *  点位编号
     */
    private String addr_no;
    /**
     * 点位名称
     */
    private String addr_name;
    /**
     * 抓拍时间
     */
    private String cap_time;
    /**
     * 实时抓拍标志
     */
    private Integer is_realtime;
    /**
     * 不通行原因
     * 限制人员：-2,人员过期：-3,不在调度时间：-4,节假日：-5,温度过高：-6,口罩：-7,未带安全帽：-8,
     * 卡号未注册：-9,人证不匹配：-10,未授权：-11
     */
    private Integer match_failed_reson;
    /**
     * 比对结果（0：未比对。-1：比对失败。大于0的取值：比对成功时的确信度分数（100分制）。）
     */
    private Integer match_result;
    /**
     * 匹配人员信息
     */
    private Match match;
    /**
     * 全景图像标志（是否包含全景图像）
     *  1（true）， 0（false）
     */
    private Boolean overall_pic_flag;
    /**
     * 全景图像（全景图像标志为true）
     */
    private Overall_pic overall_pic;
    /**
     * 特写图像标志（是否包含特写图像）
     * 1（true）， 0（false）
     */
    private Boolean closeup_pic_flag;
    /**
     * 特写图像（特写图像标志为true）
     */
    private Closeup_pic closeup_pic;
    /**
     * 视频标志（是否包含视频）
     * 1（true）， 0（false）
     */
    private Boolean video_flag;
    /**
     * 视频（视频标志为true）
     */
    private Video video;
    /**
     * 人员属性信息
     */
    private Person person;
    /**
     * 人脸特征数据
     */
    //private List<Integer> feature_data;
    /**
     * 身份证信息
     */
    private Id_card id_card;
    /**
     * 二维码数据，检测到二维码时有此字段，否则无此字段
     */
    //private Qr_code qr_code;
    /**
     * 位置信息,只有外接了gps设备才有这个信息
     */
    //private Gps gps;

}
