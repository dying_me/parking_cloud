package com.cygsunri.wisdompark.callback.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * <p>
 * 上传违约预约车辆请求信息data
 * </p>
 *
 * @author cygsunri
 * @since 2021-06-04
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class AboutCarDataInfo {

    /**
     * 停车场key
     */
    public String key;

    /**
     * 车牌号
     */
    public String carNo;

    /**
     * 停车订单号
     */
    public String orderNo;

    /**
     * 出场时间
     */
    public String outTime;

    /**
     * 车辆类型编码
     */
    public String carType;

    /**
     * 出口车道名称
     */
    public String gateName;

    /**
     * 出口操作员名称
     */
    public String operatorName;

    /**
     * 出口车辆图片
     */
    public String imgUrl;

    /**
     * 订单总金额
     */
    public String totalAmount;


}
