package com.cygsunri.wisdompark.callback.vo;

import com.cygsunri.wisdompark.bussiniss.vo.BaseCondition;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;


/**
 * <p>
 * 	上传交接班记录
 * </p>
 *
 * @author cygsunri
 * @since 2021-06-04
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ToString(callSuper = true)
public class PutWorkHandoverReq extends BaseCondition {

    /**
     * data
     */
    public PutWorkHandoverInfo data;


}
