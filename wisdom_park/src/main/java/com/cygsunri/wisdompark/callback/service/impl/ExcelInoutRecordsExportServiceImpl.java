package com.cygsunri.wisdompark.callback.service.impl;

import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.cygsunri.common.EnumConstant;
import com.cygsunri.wisdompark.callback.mapper.TbEnterCarMapper;
import com.cygsunri.wisdompark.callback.service.ExcelInoutRecordsExportService;
import com.cygsunri.wisdompark.callback.vo.EnterOutCarQueryDTO;
import com.cygsunri.wisdompark.callback.vo.excel.ExportInoutRecordsModel;
import com.cygsunri.wisdompark.util.CommonUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *
 * </p>
 *
 * @author cygsunri
 * @since 2021/8/16
 */
@Service
public class ExcelInoutRecordsExportServiceImpl implements ExcelInoutRecordsExportService {
    @Autowired
    private TbEnterCarMapper tbEnterCarMapper;
    @Override
    public List<Object> selectListForExcelExport(Object o, int i) {
        List<Object> resultList  = new ArrayList<>();
        ObjectMapper objectMapper = new ObjectMapper();
        EnterOutCarQueryDTO enterOutCarQueryDTO = objectMapper.convertValue(o, EnterOutCarQueryDTO.class);
        enterOutCarQueryDTO.setPageNumber((i - 1) * 10000);
        enterOutCarQueryDTO.setPageSize(i * 10000);
        List<ExportInoutRecordsModel> list = tbEnterCarMapper.getExportInoutRecords(enterOutCarQueryDTO);
        list.forEach((item) ->{
            item.setCarTypeStatus(EnumConstant.StatusEnum.getDescFromCode(item.getCarType()));
        });
        if(CollectionUtils.isNotEmpty(list)){
            Integer num = 1;
            for (ExportInoutRecordsModel exportModel : list) {
                exportModel.setId(num++);
                String enterTime = exportModel.getEnterTime();
                String outTime = exportModel.getOutTime();
                if(StringUtils.isNotBlank(enterTime)&&StringUtils.isNotBlank(outTime)){
                    String dateDistance = CommonUtil.getDateDistance(enterTime, outTime);
                    exportModel.setDateDistance(dateDistance);
                }
                resultList.add(exportModel);
            }
        }
        return resultList;
    }
}
