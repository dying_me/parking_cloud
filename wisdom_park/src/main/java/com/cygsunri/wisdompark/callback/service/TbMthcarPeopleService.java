package com.cygsunri.wisdompark.callback.service;

import com.cygsunri.wisdompark.callback.entity.TbMthcarPeople;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 月租车用户信息表 服务类
 * </p>
 *
 * @author hanyuan
 * @since 2021-08-18
 */
public interface TbMthcarPeopleService extends IService<TbMthcarPeople> {

}
