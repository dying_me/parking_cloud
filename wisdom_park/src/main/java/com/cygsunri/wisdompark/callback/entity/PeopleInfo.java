package com.cygsunri.wisdompark.callback.entity;

import lombok.Data;

/**
 * <p>
 * 封装人员信息
 * </p>
 *
 * @author cygsunri
 * @since 2021/6/29
 */
@Data
public class PeopleInfo {

    /**
     * 姓名
     */
    private String personname;
    /**
     * 身份证号
     */
    private String personidcardno;
    /**
     * 电话号码
     */
    private String personphoneno;
    /**
     * 头像
     */
    private String personphoto;
    /**
     * 组织机构
     */
    private String unitno;
    /**
     * 公司部门
     */
    private Depart depart;
}
