package com.cygsunri.wisdompark.callback.mapper;

import com.cygsunri.wisdompark.callback.entity.TbOutCarImg;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 上传车辆出场图片信息表 Mapper 接口
 * </p>
 *
 * @author hanyuan
 * @since 2021-07-30
 */
public interface TbOutCarImgMapper extends BaseMapper<TbOutCarImg> {

    void deleteBatch(String[] ids);
}
