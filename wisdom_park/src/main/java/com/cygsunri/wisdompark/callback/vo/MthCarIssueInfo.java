package com.cygsunri.wisdompark.callback.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * <p>
 * 月租车登记信息Data Bean
 * </p>
 *
 * @author cygsunri
 * @since 2021-06-04
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class MthCarIssueInfo {

    /**
     * 停车场key
     */
    public String key;

    /**
     * 车牌号
     */
    public String carNo;

    /**
     * 用户编号
     */
    public String carSpaceNo;

    /**
     * 有效期开始时间
     */
    public String beginTime;

    /**
     * 有效期结束时间
     */
    public String endTime;

    /**
     * 登记时间
     */
    public String iusseTime;

    /**
     * 车辆个数
     */
    public String carSpalcesNum;

    /**
     * 车主姓名
     */
    public String userName;

    /**
     * 手机号码
     */
    public String mobNumber;

    /**
     * 家庭住址
     */
    public String homeAddress;

}
