package com.cygsunri.wisdompark.callback.mapper;

import com.cygsunri.wisdompark.callback.entity.TbOutCar;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 上传车辆出场信息表 Mapper 接口
 * </p>
 *
 * @author hanyuan
 * @since 2021-07-30
 */
public interface TbOutCarMapper extends BaseMapper<TbOutCar> {

    void deleteBatch(String[] ids);
}
