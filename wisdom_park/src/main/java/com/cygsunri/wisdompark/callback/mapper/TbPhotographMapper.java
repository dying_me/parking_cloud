package com.cygsunri.wisdompark.callback.mapper;

import com.cygsunri.wisdompark.callback.entity.TbPhotograph;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 考勤抓拍信息表 Mapper 接口
 * </p>
 *
 * @author hanyuan
 * @since 2021-08-23
 */
public interface TbPhotographMapper extends BaseMapper<TbPhotograph> {

}
