package com.cygsunri.wisdompark.callback.controller;

import com.cygsunri.response.ResultWrapper;
import com.cygsunri.wisdompark.bussiniss.vo.BaseDataResult;
import com.cygsunri.wisdompark.callback.service.NoticeService;
import com.cygsunri.wisdompark.callback.vo.notice.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * <p>
 * 智慧停车开放平台通知类接口对接
 * </p>
 *
 * @author cygsunri
 * @since 2021/8/10
 */
@Slf4j
@RestController
@RequestMapping("/v2/notice")
public class NoticeController {

    @Autowired
    private NoticeService noticeService;



    /**
     * 新增月租车用户
     * @param mthCarPeopleDTO
     * @return
     */
    @PostMapping("/addMthCarPerson")
    public ResultWrapper<Object> addMthCarPerson(@Validated @RequestBody MthCarPeopleDTO mthCarPeopleDTO) {
        BaseDataResult data = noticeService.addMthCarPerson(mthCarPeopleDTO);
        return ResultWrapper.ok("新增月租车用户成功",data);
    }


    /**
     * 新增月租车
     * @param mthCarDTO
     * @return
     */
    @PostMapping("/addMthCar")
    public ResultWrapper<Object> addMthCar(@Validated @RequestBody MthCarDTO mthCarDTO) {
        BaseDataResult data = noticeService.addMthCar(mthCarDTO);
        return ResultWrapper.ok("新增月租车成功",data);
    }

    /**
     * 注销月租车
     * @param failMonthCarDTO
     * @return
     */
    @PostMapping("/failMonthCar")
    public ResultWrapper<Object> failMonthCar(@Validated @RequestBody FailMonthCarDTO failMonthCarDTO) {
        BaseDataResult data = noticeService.failMonthCar(failMonthCarDTO);
        return ResultWrapper.ok("注销月租车成功",data);
    }

    /**
     * 批量注销月租车
     * @param failMonthCars
     * @return
     */
    @PostMapping("/failMonthCars")
    public ResultWrapper<Object> failMonthCars(@Validated @RequestBody List<FailMonthCarDTO> failMonthCars) {
        noticeService.failMonthCars(failMonthCars);
        return ResultWrapper.ok("注销月租车成功");
    }



    /**
     * 远程开闸
     * @param openGateDTO
     * @return
     */
    @PostMapping("/openGate")
    public ResultWrapper<Object> openGate(@Validated @RequestBody OpenGateDTO openGateDTO) {
        BaseDataResult data = noticeService.openGate(openGateDTO);
        return ResultWrapper.ok("远程开闸成功",data);
    }


    /**
     * 远程关闸
     * @param closeGateDTO
     * @return
     */
    @PostMapping("/closeGate")
    public ResultWrapper<Object> closeGate(@Validated @RequestBody CloseGateDTO closeGateDTO) {
        BaseDataResult data = noticeService.closeGate(closeGateDTO);
        return ResultWrapper.ok("远程关闸成功",data);
    }


    /**
     * 访客车辆新增
     * @param addVisitorCarDTO
     * @return
     */
    @PostMapping("/addVisitorCar")
    public ResultWrapper<Object> addVisitorCar(@Validated @RequestBody AddVisitorCarDTO addVisitorCarDTO) {
        BaseDataResult data = noticeService.addVisitorCar(addVisitorCarDTO);
        return ResultWrapper.ok("访客车辆新增成功",data);
    }


    /**
     * 访客车辆删除
     * @param deleteVisitorCarDTO
     * @return
     */
    @PostMapping("/deleteVisitorCar")
    public ResultWrapper<Object> deleteVisitorCar(@Validated @RequestBody DeleteVisitorCarDTO deleteVisitorCarDTO) {
        BaseDataResult data = noticeService.deleteVisitorCar(deleteVisitorCarDTO);
        return ResultWrapper.ok("访客车辆删除成功",data);
    }


    /**
     * 车辆锁定与解锁
     * @param setCarLockStatusDTO
     * @return
     */
    @PostMapping("/setCarLockStatus")
    public ResultWrapper<Object> setCarLockStatus(@Validated @RequestBody SetCarLockStatusDTO setCarLockStatusDTO) {
        BaseDataResult data = noticeService.setCarLockStatus(setCarLockStatusDTO);
        return ResultWrapper.ok("车辆锁定与解锁成功",data);
    }


}
