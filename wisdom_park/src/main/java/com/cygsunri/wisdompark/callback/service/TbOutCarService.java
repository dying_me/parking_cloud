package com.cygsunri.wisdompark.callback.service;

import com.cygsunri.wisdompark.callback.entity.TbOutCar;
import com.baomidou.mybatisplus.extension.service.IService;
/**
 * <p>
 * 上传车辆出场信息表 服务类
 * </p>
 *
 * @author hanyuan
 * @since 2021-07-30
 */
public interface TbOutCarService extends IService<TbOutCar> {


    void deleteBatch(String[] ids);
}
