package com.cygsunri.wisdompark.callback.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;


/**
 * <p>
 * 上传岗亭车道信息data
 * </p>
 *
 * @author cygsunri
 * @since 2021-06-04
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class SentryBoxSendDataInfo {

    /**
     * 停车场key
     */
    public String key;

    /**
     * 岗亭编号
     */
    public String sentrybox_No;

    /**
     * 修改时间
     */
    public String eidtTime;


    /**
     * 车道信息
     */
    public List<VehichleVO> vehichleList;


}
