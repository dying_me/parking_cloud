package com.cygsunri.wisdompark.callback.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cygsunri.wisdompark.callback.entity.DeviceInfo;
import com.cygsunri.wisdompark.callback.entity.DeviceTemplateData;
import com.cygsunri.wisdompark.callback.mapper.DeviceTemplateDatasMapper;
import com.cygsunri.wisdompark.callback.service.DeviceTemplateDatasService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 寄存器信息 服务类
 * </p>
 */
@Service
public class DeviceTemplateDatasServiceImpl extends ServiceImpl<DeviceTemplateDatasMapper, DeviceTemplateData> implements DeviceTemplateDatasService {

    @Autowired
    private DeviceTemplateDatasMapper deviceTemplateDatasMapper;
    /**
     * 通过设备ID查找模板信息
     *
     * @param psrID  设备ID
     * @param
     * @return DeviceTemplateData
     */
    @Override
    public List<DeviceTemplateData> findDataByPsrID(String psrID) {
        return deviceTemplateDatasMapper.findDataByPsrID(psrID);
    }

    /**
     * 通过设备code查找设备信息
     *
     * @param devCode  设备ID
     * @param
     * @return DeviceTemplateData
     */
    @Override
    public DeviceInfo findDeviceByDevCode(String devCode) {
        return deviceTemplateDatasMapper.findDeviceByDevCode(devCode);
    }
}
