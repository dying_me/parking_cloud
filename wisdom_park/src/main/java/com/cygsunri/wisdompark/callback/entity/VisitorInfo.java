package com.cygsunri.wisdompark.callback.entity;

import lombok.Data;

/**
 * <p>
 * 访客信息封装类
 * </p>
 *
 * @author cygsunri
 * @since 2021/6/30
 */
@Data
public class VisitorInfo {

    /**
     * 主键
     */
    private String id;
    /**
     * 姓名
     */
    private String personname;
    /**
     * 头像
     */
    private String personphoto;

    /**
     * 所属单元
     */
    private String unitno;
}
