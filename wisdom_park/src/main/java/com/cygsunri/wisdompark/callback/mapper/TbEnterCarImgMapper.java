package com.cygsunri.wisdompark.callback.mapper;

import com.cygsunri.wisdompark.callback.entity.TbEnterCarImg;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 上传车辆入场图片信息表 Mapper 接口
 * </p>
 *
 * @author hanyuan
 * @since 2021-07-30
 */
public interface TbEnterCarImgMapper extends BaseMapper<TbEnterCarImg> {

    void deleteBatch(String[] ids);
}
