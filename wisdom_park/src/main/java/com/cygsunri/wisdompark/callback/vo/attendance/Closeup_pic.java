package com.cygsunri.wisdompark.callback.vo.attendance;

import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * <p>
 * 特写图像（特写图像标志为true）
 * </p>
 *
 * @author cygsunri
 * @since 2021/8/23
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class Closeup_pic {

    /**
     * 图像格式
     */
    private String format;
    /**
     *  图像数据（二进制数据对应base64）
     */
    private String data;
    /**
     * 人脸位置位于特写图X坐标
     */
    private Integer face_x;
    /**
     * 人脸位置位于特写图Y坐标
     */
    private Integer face_y;
    /**
     * 人脸在特写图中宽度
     */
    private Integer face_width;
    /**
     * 人脸在特写图中高度
     */
    private Integer face_height;
}
