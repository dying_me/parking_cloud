package com.cygsunri.wisdompark.callback.vo;

import com.cygsunri.wisdompark.bussiniss.vo.BaseCondition;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * <p>
 * 回调类月租车登记信息Bean
 * </p>
 *
 * @author cygsunri
 * @since 2021-06-04
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class MthCarIssueConditionVO extends BaseCondition {

    /**
     * 月租车登记信息
     */
    private MthCarIssueInfo data;

}
