package com.cygsunri.wisdompark.callback.service;

import com.cygsunri.wisdompark.bussiniss.vo.PhotographBackBaseResult;
import com.cygsunri.wisdompark.callback.entity.TbPhotograph;
import com.baomidou.mybatisplus.extension.service.IService;
import com.cygsunri.wisdompark.callback.vo.attendance.PhotographDataDTO;

/**
 * <p>
 * 考勤抓拍信息表 服务类
 * </p>
 *
 * @author hanyuan
 * @since 2021-08-23
 */
public interface TbPhotographService extends IService<TbPhotograph> {

    PhotographBackBaseResult getPhotographData(PhotographDataDTO photographDataDTO);
}
