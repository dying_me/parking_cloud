package com.cygsunri.wisdompark.callback.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cygsunri.wisdompark.callback.entity.DayData;
import com.cygsunri.wisdompark.callback.vo.EnterOutCarQueryDTO;
import com.cygsunri.wisdompark.callback.vo.OutEnterCarVO;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hanyuan
 * @since 2021-07-30
 */
public interface DayDataService extends IService<DayData> {

    /**
     * 通过时间和名称和相差天数查找值
     *
     * @param time  时间
     * @param name 名称
     * @param diff 相差天数
     * @return DeviceTemplateData
     */
    List<DayData> finDiffDayDataByTimeAndName(String time,String name,int diff);

    /**
     * 通过时间和名称查找值
     *
     * @param time  时间
     * @param name 名称
     * @return DeviceTemplateData
     */
    List<DayData> finDayDataByTimeAndName(String time,String name);

}
