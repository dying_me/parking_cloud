package com.cygsunri.wisdompark.callback.service.impl;

import com.alibaba.fastjson.JSON;
import com.cygsunri.exception.BusinessException;
import com.cygsunri.response.ResultCodeEnum;
import com.cygsunri.wisdompark.bussiniss.vo.PhotographBackBaseResult;
import com.cygsunri.wisdompark.callback.entity.TbPhotograph;
import com.cygsunri.wisdompark.callback.mapper.TbPhotographMapper;
import com.cygsunri.wisdompark.callback.service.TbPhotographService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cygsunri.wisdompark.callback.vo.attendance.*;
import com.cygsunri.wisdompark.util.id.SnowFlakeIdWorker;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * <p>
 * 考勤抓拍信息表 服务实现类
 * </p>
 *
 * @author hanyuan
 * @since 2021-08-23
 */
@Slf4j
@Service
public class TbPhotographServiceImpl extends ServiceImpl<TbPhotographMapper, TbPhotograph> implements TbPhotographService {

    @Override
    @Transactional(rollbackFor = Exception.class)
    public PhotographBackBaseResult getPhotographData(PhotographDataDTO photographDataDTO) {
        log.info("TbPhotographServiceImpl PhotographBackBaseResult ==>> "+ JSON.toJSONString(photographDataDTO));
        PhotographBackBaseResult result = new PhotographBackBaseResult();
        BeanUtils.copyProperties(photographDataDTO,result);
        TbPhotograph photograph = new TbPhotograph();
        photograph.setId(SnowFlakeIdWorker.generateNextIdStr());
        BeanUtils.copyProperties(photographDataDTO,photograph);
        Id_card id_card = photographDataDTO.getId_card();
        if(id_card!=null){
            String c_sex = id_card.getSex();
            BeanUtils.copyProperties(id_card,photograph);
            photograph.setC_sex(c_sex);
        }
        Person person = photographDataDTO.getPerson();
        if(person!=null){
            BeanUtils.copyProperties(person,photograph);
        }
        Match match = photographDataDTO.getMatch();
        if(match!=null){
            BeanUtils.copyProperties(match,photograph);
        }
        Overall_pic overall_pic = photographDataDTO.getOverall_pic();
        if(overall_pic!=null){
            photograph.setO_format(overall_pic.getFormat());
            photograph.setO_data(overall_pic.getData());
            photograph.setO_face_x(overall_pic.getFace_x());
            photograph.setO_face_y(overall_pic.getFace_y());
            photograph.setO_face_width(overall_pic.getFace_width());
            photograph.setO_face_height(overall_pic.getFace_height());
        }
        Closeup_pic closeup_pic = photographDataDTO.getCloseup_pic();
        if(closeup_pic!=null){
            photograph.setC_format(closeup_pic.getFormat());
            photograph.setC_data(closeup_pic.getData());
            photograph.setC_face_x(closeup_pic.getFace_x());
            photograph.setC_face_y(closeup_pic.getFace_y());
            photograph.setC_face_width(closeup_pic.getFace_width());
            photograph.setC_face_height(closeup_pic.getFace_height());
        }
        Video video = photographDataDTO.getVideo();
        if(video!=null){
            photograph.setV_start_time(video.getStart_time());
            photograph.setV_end_time(video.getEnd_time());
            photograph.setV_format(video.getFormat());
            photograph.setV_data(video.getData());
        }
        int i = baseMapper.insert(photograph);
        if(i==0){
            result.setReply("失败");
            result.setCode(1);
            throw new BusinessException(ResultCodeEnum.SYS_ERROR.getCode(),"插入考勤抓拍信息表失败,请核实!");
        }
        result.setReply("成功");
        result.setCode(0);
        return result;
    }
}
