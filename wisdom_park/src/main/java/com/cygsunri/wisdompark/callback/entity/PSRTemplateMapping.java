package com.cygsunri.wisdompark.callback.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 */
@Data
@Accessors(chain = true)
@TableName("tb_psrtemplatemapping")
public class PSRTemplateMapping implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键,UUID
     */
    @TableId(value = "ID", type = IdType.ASSIGN_UUID)
    private String id;

    /**
     *
     */
    @TableField("PsrID")
    private String PsrID;

    /**
     *
     */
    @TableField("belong")
    private String belong;

    /**
     *
     */
    @TableField("parent")
    private String parent;

    /**
     *
     */
    @TableField("deviceTemplate_id")
    private String deviceTemplate_id;

}
