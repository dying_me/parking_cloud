package com.cygsunri.wisdompark.callback.vo;

import com.cygsunri.request.BasePageQuery;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 *
 * </p>
 *
 * @author cygsunri
 * @since 2021/8/2
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class MthcarPeopleQueryDTO extends BasePageQuery {

    /**
     * 车主姓名
     */
    private String userName;
    /**
     * 联系方式
     */
    private String mobNumber;
}
