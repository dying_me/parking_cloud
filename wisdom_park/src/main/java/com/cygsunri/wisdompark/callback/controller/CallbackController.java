package com.cygsunri.wisdompark.callback.controller;


import com.cygsunri.common.EnumConstant;
import com.cygsunri.wisdompark.bussiniss.vo.BaseDataResult;
import com.cygsunri.wisdompark.bussiniss.vo.BaseResult;
import com.cygsunri.wisdompark.callback.service.CallbackService;
import com.cygsunri.wisdompark.callback.vo.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * 智慧停车开放平台回调类 前端控制器
 * </p>
 *
 * @author cygsunri
 * @since 2021-06-04
 */
@Slf4j
@Controller
@RequestMapping("/callback/")
public class CallbackController {

    @Autowired
    private CallbackService callbackService;

    /**
     * 外部接口调用：上传车辆入场信息
     * @param enterCarConditionVO
     * @return ResponseEntity
     */
    @RequestMapping(value = "/EnterCar", method = RequestMethod.POST)
    public ResponseEntity<BaseResult> carEnterCar(@Validated @RequestBody EnterCarConditionVO enterCarConditionVO)  {
        BaseResult data = callbackService.carEnterCar(enterCarConditionVO);
        data.setCode(EnumConstant.CallBackResult.SUCCESS.getValue());
        return new ResponseEntity<>(data, HttpStatus.OK);
    }

    /**
     * 外部接口调用：上传车辆出场记录
     * @param outCarConditionVO
     * @return ResponseEntity
     */
    @RequestMapping(value = "/OutCar", method = RequestMethod.POST)
    public ResponseEntity<BaseResult> carOutCar(@Validated @RequestBody OutCarConditionVO outCarConditionVO) {
        BaseResult data = callbackService.carOutCar(outCarConditionVO);
        data.setCode(EnumConstant.CallBackResult.SUCCESS.getValue());
        return new ResponseEntity<>(data, HttpStatus.OK);
    }

    /**
     * 外部接口调用：上传车辆入场图片
     * @param enterOutImgConditionVO
     * @return ResponseEntity
     */
    @RequestMapping(value = "/EnterCarImg", method = RequestMethod.POST)
    public ResponseEntity<BaseResult> carEnterCarImg(@Validated @RequestBody EnterOutImgConditionVO enterOutImgConditionVO) {
        BaseResult data =  callbackService.carEnterCarImg(enterOutImgConditionVO);
        data.setCode(EnumConstant.CallBackResult.SUCCESS.getValue());
        return new ResponseEntity<>(data, HttpStatus.OK);
    }

    /**
     * 外部接口调用：上传车辆出场图片
     * @param enterOutImgConditionVO
     * @return ResponseEntity
     */
    @RequestMapping(value = "/OutCarImg", method = RequestMethod.POST)
    public ResponseEntity<BaseResult> carOutCarImg(@Validated @RequestBody EnterOutImgConditionVO enterOutImgConditionVO) {
        BaseResult data =  callbackService.carOutCarImg(enterOutImgConditionVO);
        data.setCode(EnumConstant.CallBackResult.SUCCESS.getValue());
        return new ResponseEntity<>(data, HttpStatus.OK);
    }

    /**
     * 外部接口调用：上传月租车登记
     * @param mthCarIssueConditionVO
     * @return ResponseEntity
     */
    @RequestMapping(value = "/MthCarIssueSend", method = RequestMethod.POST)
    public ResponseEntity<BaseResult> carMthCarIssueSend(@RequestBody MthCarIssueConditionVO mthCarIssueConditionVO) {
        BaseResult data = new BaseResult();
        data.setCode(EnumConstant.CallBackResult.SUCCESS.getValue());
        return new ResponseEntity<>(data, HttpStatus.OK);
    }

    /**
     * 外部接口调用：上传月租车注销
     * @param mthCarFailConditionVO
     * @return ResponseEntity
     */
    @RequestMapping(value = "/MthCarFailSend", method = RequestMethod.POST)
    public ResponseEntity<BaseResult> carMthCarFailSend(@RequestBody MthCarFailConditionVO mthCarFailConditionVO) {
        BaseResult data = new BaseResult();
        data.setCode(EnumConstant.CallBackResult.SUCCESS.getValue());
        return new ResponseEntity<>(data, HttpStatus.OK);
    }

    /**
     * 外部接口调用：上传月租车充值延期
     * @param mthCarChargeConditionVO
     * @return ResponseEntity
     */
    @RequestMapping(value = "/MthCarChargeSend", method = RequestMethod.POST)
    public ResponseEntity<BaseResult> carMthCarChargeSend(@RequestBody MthCarChargeConditionVO mthCarChargeConditionVO) {
        BaseResult data = new BaseResult();
        data.setCode(EnumConstant.CallBackResult.SUCCESS.getValue());
        return new ResponseEntity<>(data, HttpStatus.OK);
    }

    /**
     * 外部接口调用：上传进场车辆车牌修正
     * @param updateCarConditionVO
     * @return ResponseEntity
     */
    @RequestMapping(value = "/UpdateCar", method = RequestMethod.POST)
    public ResponseEntity<BaseResult> carUpdateCar(@RequestBody UpdateCarConditionVO updateCarConditionVO) {
        BaseResult data = new BaseResult();
        data.setCode(EnumConstant.CallBackResult.SUCCESS.getValue());
        return new ResponseEntity<>(data, HttpStatus.OK);
    }

    /**
     * 外部接口调用：上传新增人事信息
     * @param personSendConditionVO
     * @return ResponseEntity
     */
    @RequestMapping(value = "/PersonSend", method = RequestMethod.POST)
    public ResponseEntity<BaseResult> personSend(@RequestBody PersonSendConditionVO personSendConditionVO) {
        BaseResult data = new BaseResult();
        return new ResponseEntity<>(data, HttpStatus.OK);
    }

    /**
     * 外部接口调用：上传车辆黑名单
     * @param carblackConditionVO
     * @return ResponseEntity
     */
    @RequestMapping(value = "/CarBlackList", method = RequestMethod.POST)
    public ResponseEntity<BaseResult> carBlackList(@RequestBody CarblackConditionVO carblackConditionVO) {
        BaseResult data = new BaseResult();
        return new ResponseEntity<>(data, HttpStatus.OK);
    }

    /**
     * 外部接口调用：创建临停车支付订单
     * @param orderPayCreateConditionVO
     * @return ResponseEntity
     */
    @RequestMapping(value = "/Inform/OrderPayCreate", method = RequestMethod.POST)
    public ResponseEntity<BaseResult> orderPayCreate(@RequestBody OrderPayCreateConditionVO orderPayCreateConditionVO) {
        BaseDataResult data = new BaseDataResult();
        Map<String,Object> resultMap = new HashMap<>();
        resultMap.put("parkKey","");//车场唯一编号
        resultMap.put("orderNo","");//停车订单号
        resultMap.put("payOrderNo","");//支付订单号
        data.setData(resultMap);
        return new ResponseEntity<>(data, HttpStatus.OK);
    }

    /**
     * 外部接口调用：订单支付回调
     * @param payNotifyConditionVO
     * @return ResponseEntity
     */
    @RequestMapping(value = "/Inform/PayNotify", method = RequestMethod.POST)
    public ResponseEntity<BaseResult> payNotify(@RequestBody PayNotifyConditionVO payNotifyConditionVO) {
        BaseDataResult data = new BaseDataResult();
        Map<String,Object> resultMap = new HashMap<>();
        resultMap.put("parkKey","");//车场唯一编号
        resultMap.put("payOrderNo","");//支付订单号
        data.setData(resultMap);
        return new ResponseEntity<>(data, HttpStatus.OK);
    }

    /**
     * 外部接口调用：创建无牌车入场订单
     * @param unVehicleEnterConditionVO
     * @return ResponseEntity
     */
    @RequestMapping(value = "/Inform/UnVehicleEnter", method = RequestMethod.POST)
    public ResponseEntity<BaseResult> unVehicleEnter(@RequestBody UnVehicleEnterConditionVO unVehicleEnterConditionVO) {
        BaseDataResult data = new BaseDataResult();
        Map<String,Object> resultMap = new HashMap<>();
        resultMap.put("parkKey","");//车场唯一编号
        resultMap.put("carNo","");//车牌号
        resultMap.put("orderNo","");//停车订单号
        data.setData(resultMap);
        return new ResponseEntity<>(data, HttpStatus.OK);
    }

    /**
     * 外部接口调用：出口缴费开闸
     * @param unVehicleOutConditionVO
     * @return ResponseEntity
     */
    @RequestMapping(value = "/Inform/UnVehicleOut", method = RequestMethod.POST)
    public ResponseEntity<BaseResult> unVehicleOut(@RequestBody UnVehicleOutConditionVO unVehicleOutConditionVO) {
        BaseDataResult data = new BaseDataResult();
        Map<String,Object> resultMap = new HashMap<>();
        resultMap.put("parkKey","");//车场唯一编号
        resultMap.put("carNo","");//车牌号
        resultMap.put("orderNo","");//停车订单号
        resultMap.put("outTime","");//出场时间
        data.setData(resultMap);
        return new ResponseEntity<>(data, HttpStatus.OK);
    }

    /**
     * 上传违约预约车辆
     * @param reserveSendReq
     * @return ResponseEntity
     */
    @RequestMapping(value = "/ReserveSend", method = RequestMethod.POST)
    public ResponseEntity<BaseResult> reserveSend(@RequestBody ReserveSendReq reserveSendReq) {
        BaseResult data = new BaseResult();
        data.setCode("1");
        data.setMsg("ok");
        data.setRand("1535358995455");
        data.setSign("1F7A2F24353C3912CC1E46540B239306");
        return new ResponseEntity<>(data, HttpStatus.OK);
    }


    /**
     * 上传岗亭车道信息
     * @param sentryBoxSendReq
     * @return ResponseEntity
     */
    @RequestMapping(value = "/SentryBoxSend", method = RequestMethod.POST)
    public ResponseEntity<BaseResult> sentryBoxSend(@RequestBody SentryBoxSendReq sentryBoxSendReq) {
        BaseResult data = new BaseResult();
        data.setCode("1");
        data.setMsg("ok");
        data.setRand("1535358995455");
        data.setSign("1F7A2F24353C3912CC1E46540B239306");
        return new ResponseEntity<>(data, HttpStatus.OK);
    }


    /**
     * 上传交接班记录
     * @param putWorkHandoverReq
     * @return ResponseEntity
     */
    @RequestMapping(value = "/PutWorkHandover", method = RequestMethod.POST)
    public ResponseEntity<BaseResult> putWorkHandover(@RequestBody PutWorkHandoverReq putWorkHandoverReq) {
        BaseResult data = new BaseResult();
        data.setCode("1");
        data.setMsg("ok");
        data.setRand("1535358995455");
        data.setSign("1F7A2F24353C3912CC1E46540B239306");
        return new ResponseEntity<>(data, HttpStatus.OK);
    }


    /**
     * 上传车场车位数
     * @param sendParkSpaceReq
     * @return ResponseEntity
     */
    @RequestMapping(value = "/SendParkSpace", method = RequestMethod.POST)
    public ResponseEntity<BaseResult> sendParkSpace(@Validated @RequestBody SendParkSpaceReq sendParkSpaceReq) {
        BaseResult data = callbackService.sendParkSpace(sendParkSpaceReq);
        data.setCode(EnumConstant.CallBackResult.SUCCESS.getValue());
        return new ResponseEntity<>(data, HttpStatus.OK);
    }


    /**
     * 上传停车场设备状态
     * @param addDeviceMonitorReq
     * @return ResponseEntity
     */
    @RequestMapping(value = "/AddDeviceMonitor", method = RequestMethod.POST)
    public ResponseEntity<BaseResult> addDeviceMonitor(@RequestBody AddDeviceMonitorReq addDeviceMonitorReq) {
        BaseResult data = new BaseResult();
        data.setCode("1");
        data.setMsg("ok");
        data.setRand("1535358995455");
        data.setSign("1F7A2F24353C3912CC1E46540B239306");
        return new ResponseEntity<>(data, HttpStatus.OK);
    }


    /**
     * 上传临停车缴费记录
     * @param payOrderReq
     * @return ResponseEntity
     */
    @RequestMapping(value = "/PayOrder", method = RequestMethod.POST)
    public ResponseEntity<BaseResult> payOrder(@RequestBody PayOrderReq payOrderReq) {
        BaseResult data = new BaseResult();
        data.setCode("1");
        data.setMsg("ok");
        data.setRand("1535358995455");
        data.setSign("1F7A2F24353C3912CC1E46540B239306");
        return new ResponseEntity<>(data, HttpStatus.OK);
    }

    /**
     * 上传异常订单记录
     * @param aboutCarReq
     * @return ResponseEntity
     */
    @RequestMapping(value = "/AboutCar", method = RequestMethod.POST)
    public ResponseEntity<BaseResult> aboutCar(@RequestBody AboutCarReq aboutCarReq) {
        BaseResult data = new BaseResult();
        data.setCode("1");
        data.setMsg("ok");
        data.setRand("1535358995455");
        data.setSign("1F7A2F24353C3912CC1E46540B239306");
        return new ResponseEntity<>(data, HttpStatus.OK);
    }



    /**
     * 测试物联网HTTP消息推送
     * @param string
     * @return ResponseEntity
     */
    @RequestMapping(value = "/test", method = RequestMethod.POST)
    public ResponseEntity<BaseResult> test(@RequestBody String string) {
        BaseResult data = new BaseResult();
        data.setCode("1");
        data.setMsg("ok");
        data.setRand("1535358995455");
        data.setSign("1F7A2F24353C3912CC1E46540B239306");
        return new ResponseEntity<>(data, HttpStatus.OK);
    }


//    public void testApiWithSignature() throws Exception {
//        AepDeviceCommandClient client = AepDeviceCommandClient.newClient().appKey("Your app key here").appSecret("Your app secret here").build();
//        {
//            QueryCommandListRequest request = new QueryCommandListRequest();
//            // request.setParam..        // set your request params here
//            request.setParamMasterKey("MasterKey");
//            request.setParamProductId("ProductId");
//            request.setParamDeviceId("DeviceId");
//            System.out.println(client.QueryCommandList(request));
//        }
//        client.shutdown();
//    }

}
