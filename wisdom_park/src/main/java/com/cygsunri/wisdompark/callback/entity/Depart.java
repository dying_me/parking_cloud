package com.cygsunri.wisdompark.callback.entity;

import lombok.Data;

/**
 * <p>
 * 公司部门实体类
 * </p>
 *
 * @author cygsunri
 * @since 2021/6/29
 */
@Data
public class Depart {
    /**
     * id
     */
    private String id;
    /**
     * uuid
     */
    private String uuid;
    /**
     * 部门名称
     */
    private String departname;
    /**
     * 部门联系电话
     */
    private String mobile;
    /**
     * 部门描述
     */
    private String description;
    /**
     * 经度
     */
    private String lng;
    /**
     * 纬度
     */
    private String lat;
    /**
     * 地址
     */
    private String address;

    private String orgCode;
    private String sysOrgCode;

    private String updateName;
    private String createName;

    private String createBy;
    private String updateBy;

    private String sysCompanyCode;
    private String departOrder;
    private String orgType;
    private String fax;


}
