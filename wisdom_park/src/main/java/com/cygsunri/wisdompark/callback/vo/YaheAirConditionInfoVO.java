package com.cygsunri.wisdompark.callback.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * <p>
 * 空调列表数据信息
 * </p>
 *
 * @author cygsunri
 * @since 2021-08-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class YaheAirConditionInfoVO {

    private String id;

    private String name;

}
