package com.cygsunri.wisdompark.callback.vo;

import com.cygsunri.wisdompark.bussiniss.vo.BaseCondition;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.validation.Valid;


/**
 * <p>
 * 上传车场车位数
 * </p>
 *
 * @author cygsunri
 * @since 2021-06-04
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ToString(callSuper = true)
public class SendParkSpaceReq extends BaseCondition {

    /**
     * data
     */
    @Valid
    public SendParkSpaceDataInfo data;


}
