package com.cygsunri.wisdompark.callback.vo.notice;

import com.cygsunri.wisdompark.bussiniss.vo.BaseQueryNoticeCondition;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;

/**
 * <p>
 * 车辆锁定与解锁封装类
 * </p>
 *
 * @author cygsunri
 * @since 2021/8/10
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class SetCarLockStatusDTO extends BaseQueryNoticeCondition {

    /**
     * 车场唯一编号
     */
    @NotBlank(message = "车场唯一编号不能为空")
    private String parkKey;

    /**
     * 停车订单号 同一车场下唯一
     */
    @NotBlank(message = "商家编码不能为空")
    private String orderNo;

    /**
     * 车辆状态（0解锁，1锁定）
     */
    @NotBlank(message = "车牌号码不能为空")
    private String status;


}
