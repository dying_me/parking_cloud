package com.cygsunri.wisdompark.callback.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cygsunri.wisdompark.callback.entity.ModbusData;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author
 * @since 2020-12-21
 */
public interface ModbusDataMapper extends BaseMapper<ModbusData> {


}
