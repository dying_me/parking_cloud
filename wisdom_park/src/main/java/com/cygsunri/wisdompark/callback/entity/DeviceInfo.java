package com.cygsunri.wisdompark.callback.entity;

import lombok.Data;

/**
 * <p>
 *  scd设备参数表
 * </p>
 *
 * @author cygsunri
 * @since 2021/6/29
 */
@Data
public class DeviceInfo {

    /**
     * 代码
     */
    private String psrId;
    /**
     * devCode
     */
    private String devCode;
    /**
     * 描述
     */
    private String comments;

}
