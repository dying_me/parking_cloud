package com.cygsunri.wisdompark.callback.vo.attendance;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 身份证信息
 * </p>
 *
 * @author cygsunri
 * @since 2021/8/23
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class Id_card {
    /**
     * 编号
     */
    private String number;
    /**
     * 姓名
     */
    private String name;
    /**
     * 生日
     */
    private String birthday;
    /**
     * 性别（male：男 female：女）
     */
    private String sex;
    /**
     * 民族
     */
    private String national;
    /**
     * 地址
     */
    private String residence_address;
    /**
     * 签发机关
     */
    private String organ_issue;
    /**
     * 有效期起始时间
     */
    private String valid_date_start;
    /**
     * 有效期截止时间
     */
    private String valid_date_end;
}
