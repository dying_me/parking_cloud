package com.cygsunri.wisdompark.callback.controller;

import com.cygsunri.exception.BusinessException;
import com.cygsunri.response.ResultCodeEnum;
import com.cygsunri.response.ResultWrapper;
import com.cygsunri.wisdompark.callback.entity.TbMthcarPeople;
import com.cygsunri.wisdompark.callback.service.CallbackService;
import com.cygsunri.wisdompark.callback.service.TbMthcarPeopleService;
import com.cygsunri.wisdompark.callback.vo.EnterOutCarQueryDTO;
import com.cygsunri.wisdompark.callback.vo.MthcarPeopleQueryDTO;
import com.cygsunri.wisdompark.callback.vo.OutEnterCarVO;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;


/**
 * <p>
 *
 * </p>
 *
 * @author cygsunri
 * @since 2021/8/2
 */
@Slf4j
@RestController
@RequestMapping("/v2/callbackLocal")
public class CallbackLocalController {

    @Autowired
    private CallbackService callbackService;

    @Autowired
    private TbMthcarPeopleService tbMthcarPeopleService;

    /**
     * 查询车辆进出场记录
     * @param enterOutCarQueryDTO
     * @return
     */
    @PostMapping("/inOutRecordList")
    public ResultWrapper<Object> InoutRecordList(@RequestBody EnterOutCarQueryDTO enterOutCarQueryDTO){
        PageInfo<OutEnterCarVO> pageInfo = callbackService.pageInOutRecordListQuey(enterOutCarQueryDTO);
        return ResultWrapper.ok("查询车辆进出场记录信息成功", pageInfo);
    }

    /**
     * 根据id删除车辆进出场记录信息
     * @param id
     * @return
     */
    @GetMapping("deleteById/{id}")
    public ResultWrapper<Object> deleteById(@PathVariable String id){
        if(StringUtils.isBlank(id)){
            throw new BusinessException(ResultCodeEnum.PARAM_VALIDATE_ERROR.getCode(),
                    "ID不能为空,请核实!");
        }
        callbackService.deleteById(id);
        return ResultWrapper.ok("删除车辆进出场记录信息成功");
    }

    /**
     * 批量删除
     * @param ids
     * @return
     */
    @PostMapping("/deleteBatchById")
    public ResultWrapper<Object> deleteBatchByIds(@RequestBody String [] ids){
        callbackService.deleteBatchByIds(ids);
        return ResultWrapper.ok("批量删除车辆进出场记录信息成功");
    }

    /**
     * 车辆进出场记录大数据导出
     * @param enterOutCarQueryDTO
     * @param response
     * @param request
     * @return
     */
    @PostMapping("/exportedInoutRecordList")
    public ResultWrapper<Object> exportedInoutRecordList(@RequestBody EnterOutCarQueryDTO enterOutCarQueryDTO,HttpServletResponse response, HttpServletRequest request){
        ModelMap modelMap = new ModelMap();
        callbackService.exportedInoutRecordList(enterOutCarQueryDTO,request,response,modelMap);
        return ResultWrapper.ok("车辆进出场记录导出成功");
    }


    /**
     * 获取月租车用户信息
     * @return
     */
    @GetMapping("/getMthCarPersons")
    public ResultWrapper<Object> getMthCarPersons() {
        List<TbMthcarPeople> list = tbMthcarPeopleService.list();
        return ResultWrapper.ok("获取月租车用户信息成功",list);
    }

    /**
     * 分页查询月租车用户信息列表
     * @param
     * @return
     */
    @PostMapping("/getPageMthCarPersons")
    public ResultWrapper<Object> getPageMthCarPersons(@RequestBody MthcarPeopleQueryDTO mthcarPeopleQueryDTO){
        PageInfo<TbMthcarPeople> pageInfo = callbackService.getPageMthCarPersons(mthcarPeopleQueryDTO);
        return ResultWrapper.ok("分页查询月租车用户信息列表成功", pageInfo);
    }

    /**
     * 月租车用户--批量删除
     * @param ids
     * @return
     */
    @PostMapping("/deleteMthCarPerBatch")
    public ResultWrapper<Object> deleteMthCarPerBatch(@RequestBody String [] ids){
        callbackService.deleteMthCarPerBatch(ids);
        return ResultWrapper.ok("月租车用户批量删除成功");
    }
}
