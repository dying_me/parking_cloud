package com.cygsunri.wisdompark.callback.vo;

import com.cygsunri.wisdompark.bussiniss.vo.BaseCondition;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author hanyuanLiu
 * @site 上传车辆黑名单vo
 * @create 2021-06-07 11:34
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class CarblackConditionVO extends BaseCondition {

    /**
     * 上传车辆黑名单信息
     */
    private CarBlackInfo carBlackInfo;
}
