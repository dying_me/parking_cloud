package com.cygsunri.wisdompark.task;

import com.alibaba.fastjson.JSON;
import com.cygsunri.common.ConfigConstant;
import com.cygsunri.common.YaHeConstant;
import com.cygsunri.template.entity.psr.PSRTemplateMapping;
import com.cygsunri.wisdompark.bussiniss.vo.YaheBaseDataResult;
import com.cygsunri.wisdompark.bussiniss.vo.YaheEquipmentInfo;
import com.cygsunri.wisdompark.callback.entity.DayData;
import com.cygsunri.wisdompark.callback.entity.DeviceTemplateData;
import com.cygsunri.wisdompark.callback.service.DayDataService;
import com.cygsunri.wisdompark.callback.service.InfluxDBService;
import com.cygsunri.wisdompark.callback.service.ProcessingDataService;
import com.cygsunri.wisdompark.callback.service.TbOutCarService;
import com.cygsunri.wisdompark.util.DateUtil;
import com.cygsunri.wisdompark.util.WebUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.pulsar.shade.org.eclipse.util.StringUtil;
import org.influxdb.dto.Point;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.ZoneId;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.stream.Collectors;


@Component
@EnableConfigurationProperties
@EnableScheduling
@PropertySource("classpath:configConstant.properties")
public class StatisticDayDataTask {

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @Autowired
    private ProcessingDataService processingDataService;
    @Autowired
    private ConfigConstant configConstant;
    @Autowired
    private DayDataService dayDataService;

    /**
     * 统计设备每数据
     */
    @Scheduled(cron = "0 0 1 * * ?")
    public void StatisticDayData() {
        List<DayData> list = new ArrayList<>();
        //电表 todo
//        Map<String,Double> ankerui = processingDataService.getDataByDeviceTemplateID(configConstant.getAnkerui(),null);
//        Map<String,Double> yadaM1 = processingDataService.getDataByDeviceTemplateID(configConstant.getYadaM1(),null);
//        Map<String,Double> yadaM23 = processingDataService.getDataByDeviceTemplateID(configConstant.getYadaM23(),null);
        //水表
        Map<String, Double> yadaWater = processingDataService.getDataByDeviceTemplateID(configConstant.getYadawater(), "CumulativeFlow");
        getDayData(list, yadaWater, "CumulativeFlow");
        //柜式空调
        Map<String, Double> yaheac360 = processingDataService.getDataByDeviceTemplateID(configConstant.getYaheac360(), "AccumulateKwh");
        getDayData(list, yaheac360, "AccumulateKwh");

        if (CollectionUtils.isNotEmpty(list)) {
            dayDataService.saveOrUpdateBatch(list);
        }

    }

    private void getDayData(List<DayData> list, Map<String, Double> map, String name) {
        Map<String, DayData> psrIdMap = new HashMap<>();
        //查找数据库已有数据
        List<DayData> dayDataList = dayDataService.finDayDataByTimeAndName(DateUtil.today(), name);
        if (CollectionUtils.isNotEmpty(dayDataList)) {
            psrIdMap = dayDataList.stream().collect(Collectors.toMap(DayData::getPsrID, Function.identity()));
        }
        for (String k : map.keySet()) {
            DayData temp;
            if (map.get(k) != null) {
                if (psrIdMap.containsKey(k)) {
                    temp = new DayData(psrIdMap.get(k).getId(), k, map.get(k), name);
                } else {
                    temp = new DayData(k, map.get(k), name);
                }
                list.add(temp);
            }
        }
    }
}

