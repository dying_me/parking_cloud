package com.cygsunri.wisdompark.task;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.cygsunri.common.YaHeConstant;
import com.cygsunri.wisdompark.bussiniss.vo.YaheBaseDataResult;
import com.cygsunri.wisdompark.callback.entity.DeviceInfo;
import com.cygsunri.wisdompark.callback.service.PSRTemplateService;
import com.cygsunri.wisdompark.callback.service.ProcessingDataService;
import com.cygsunri.wisdompark.util.DateUtil;
import com.cygsunri.wisdompark.util.WebUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Component
@EnableConfigurationProperties
@EnableScheduling
@PropertySource("classpath:configConstant.properties")
public class StatisticYaheEquipmentTask {

    @Autowired
    private YaHeConstant yaHeConstant;

    @Autowired
    private PSRTemplateService psrTemplateService;

    @Autowired
    private ProcessingDataService processingDataService;

    /**
     * 统计亚禾设备信息。
     */
    @Scheduled(cron="${yahe.taskStaticsticEquipmentInfo}")
    public void staticsticEquipmentInfo () throws Exception {
        //获取设备id
        List<DeviceInfo> deviceInfoList = psrTemplateService.getAirIdList("", "yahe_ac360");
        String sq = "";
        for (int i = 0; i < deviceInfoList.size(); i++) {
            sq = String.valueOf(System.currentTimeMillis());
            Map paramMap = new HashMap();
            paramMap.put("sq",sq);
            paramMap.put("airid",deviceInfoList.get(i).getDevCode());//设备id
            paramMap.put("model","AC360");
            String result = WebUtils.get(yaHeConstant.getUrl()+yaHeConstant.getApiDeviceInfo1(),paramMap);
            YaheBaseDataResult yaheBaseDataResult = JSON.parseObject(result, YaheBaseDataResult.class);
            if (yaheBaseDataResult != null) {
                Map<String, Object> mapData = new HashMap<>();
                if ("OK".equals(yaheBaseDataResult.getResult())) {
                    JSONArray jsonArray = JSONArray.parseArray(yaheBaseDataResult.getData());
                    if (!jsonArray.isEmpty()) {
                        JSONObject jsonObject = (JSONObject) jsonArray.get(0);
                        if ("开".equals(jsonObject.get("conoff").toString())) {
                            mapData.put("RunStatus", "0");
                        }
                        if ("关".equals(jsonObject.get("conoff").toString())) {
                            mapData.put("RunStatus", "1");
                        }
                        mapData.put("Temperature",jsonObject.get("intemp").toString());
                        mapData.put("Humidity",jsonObject.get("humidity").toString());
                        mapData.put("Illuminance",jsonObject.get("photositive").toString());
                        mapData.put("AvailableKwh",jsonObject.get("usablele").toString());
                        mapData.put("Voltage",jsonObject.get("avol").toString());
                        mapData.put("Current",jsonObject.get("aele").toString());
                        mapData.put("Power",jsonObject.get("totalpow").toString());
                        mapData.put("AccumulateKwh",jsonObject.get("usedele").toString());
                        mapData.put("ElectricityTime",jsonObject.get("usedtime").toString());
                        mapData.put("time", DateUtil.nowMilliSeconds());
                        //保存AC360空调数据
                        processingDataService.saveData(deviceInfoList.get(i).getDevCode(), mapData, null);
                    }
                }
            }
        }
    }

}
