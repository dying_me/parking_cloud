package com.cygsunri.information.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 设备信息类
 * </p>
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@TableName("tb_information")
public class Information implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.ASSIGN_UUID)
    private String id;

    /**
     * 设备代码
     */
    @TableField("substationID")
    private String substationID;

    /**
     * 简易描述
     */
    @TableField("name")
    private String name;

    /**
     * 详细描述
     */
    @TableField("fullName")
    private String fullName;

    /**
     * 地址
     */
    @TableField("address")
    private String address;

    /**
     * 容量
     */
    @TableField("capacity")
    private Double capacity;

    /**
     * 负荷容量
     */
    @TableField("loadCapacity")
    private Double loadCapacity;


    /**
     * 纬度
     */
    @TableField("latitude")
    private Double latitude;

    /**
     * 经度
     */
    @TableField("longitude")
    private Double longitude;

    /**
     * 储能站SOC上限
     */
    @TableField("upBound")
    private Double upBound;

    /**
     * 储能站SOC下限
     */
    @TableField("lowBound")
    private Double lowBound;

    /**
     * 运行时间yyyy-MM-DD
     */
    @TableField("connectTime")
    private String connectTime;

    /**
     * 面积
     */
    @TableField("acreage")
    private Double acreage;

    /**
     * 人数
     */
    @TableField("people")
    private Double people;


    /**
     * 子站类型
     */
    @TableField("type")
    private String type;

    /**
     * 组件路径
     */
    @TableField("component")
    private String component;

    /**
     * 是否显示一次接线模块
     */
    @TableField("showSvg")
    private Boolean showSvg;

    /**
     * 是否显示所有设备模块
     */
    @TableField("showDevices")
    private Boolean showDevices;

}
