package com.cygsunri.information.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cygsunri.information.entity.Information;
import com.cygsunri.information.mapper.InformationMapper;
import com.cygsunri.information.service.InformationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 */
@Service
public class InformationServiceImpl extends ServiceImpl<InformationMapper, Information> implements InformationService {

    @Autowired
    private InformationMapper informationMapper;

    @Override
    @Transactional
    public void saveInformation(Information information) {
        informationMapper.insert(information);
    }

    @Override
    public Information getInformationByPsrID(String psrID) {
        return informationMapper.getInformationByPsrID(psrID);
    }

    @Override
    public List<Information> getInformationByType(String deviceType) {
        return informationMapper.getInformationByType(deviceType);
    }
}
