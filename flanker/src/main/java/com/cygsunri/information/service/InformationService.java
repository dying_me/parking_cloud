package com.cygsunri.information.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cygsunri.information.entity.Information;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 */
public interface InformationService extends IService<Information> {

    void saveInformation(Information information);

    /**
     * 根据psrID获取信息
     */
    Information getInformationByPsrID(String psrID);



    List<Information> getInformationByType(String deviceType);
}
