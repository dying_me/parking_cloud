package com.cygsunri.information.api;

import com.cygsunri.information.entity.Information;
import com.cygsunri.information.service.InformationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * 站点信息api
 * */
@RestController
@RequestMapping("/api/information")
public class InformationController {

    @Autowired
    private InformationService informationService;

    /**
     * 获取信息
     * @param id 站点PsrID
     */
    @RequestMapping(value = "/substations/{id}", method = RequestMethod.GET)
    public ResponseEntity<Information> getInformationByPsrID(@PathVariable("id") String id) {
        return new ResponseEntity<>(informationService.getInformationByPsrID(id), HttpStatus.OK);
    }

    /**
     * 信息保存
     */
    @RequestMapping(value = "/saveInformation", method = RequestMethod.POST)
    public ResponseEntity saveInformation(@RequestBody Information information) {
        informationService.saveInformation(information);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
