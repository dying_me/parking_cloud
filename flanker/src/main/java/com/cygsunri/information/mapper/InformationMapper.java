package com.cygsunri.information.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cygsunri.information.entity.Information;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 */
public interface InformationMapper extends BaseMapper<Information> {

    Information getInformationByPsrID(@Param("substationID") String substationID);

    List<Information> getInformationByType(@Param("type") String type);
}
