package com.cygsunri.util;

import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

import java.util.ArrayList;
import java.util.List;

public class JSONMappingJackson2HttpMessageConverter extends MappingJackson2HttpMessageConverter {

    public JSONMappingJackson2HttpMessageConverter() {
        List<MediaType> mediaTypes = new ArrayList<>();
        mediaTypes.add(new MediaType("application", "json")); //加入text/html类型的支持
        mediaTypes.add(new MediaType("application", "*+json"));
        mediaTypes.add(new MediaType("text", "html"));  //加入text/html类型的支持
        setSupportedMediaTypes(mediaTypes);
    }
}