package com.cygsunri.airCondition.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cygsunri.airCondition.entity.AirCache;
import com.cygsunri.scada.base.BasePSR;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 */
public interface AirCacheService extends IService<AirCache> {

    /**
     * 保存缓存
     */
    void saveAirCache(AirCache airCache);

    /**
     * 获取单个缓存
     */
    Double getAirCache(String psrID, String name, String date);

    /**
     * 获取一段时间缓存合计
     */

    Double getAirCacheSum(String psrID, String name, String from, String to);

    /**
     * 统计某一天的缓存
     */
    void updateCache(String date);

    /**
     * 计算缓存
     */
    void calcWorkTime(String date, BasePSR sub, List<BasePSR> air_insides);

}
