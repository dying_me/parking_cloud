package com.cygsunri.airCondition.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cygsunri.airCondition.entity.Devicecode;
import com.cygsunri.airCondition.mapper.PSRTemplateMapper;
import com.cygsunri.airCondition.service.PSRTemplateService;
import com.cygsunri.airCondition.vo.YaheCommonDataVO;
import com.cygsunri.template.entity.psr.PSRTemplateMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PSRTemplateServiceImpl extends ServiceImpl<PSRTemplateMapper, PSRTemplateMapping> implements PSRTemplateService {

    @Autowired
    PSRTemplateMapper psrTemplateMappingMapper;

    /**
     * 查询设备唯一标识
     * @param belong
     * @param deviceTemplate_id
     * @return List<String>
     */
    public List<String> getAirIdList(String belong, String deviceTemplate_id) {
        List<String> airIdList = psrTemplateMappingMapper.getAirIdList(belong, deviceTemplate_id);
        return airIdList;
    }
    /**
     * 查询设备名称
     * @param belong
     * @param deviceTemplate_id
     * @return String
     */
    public String getDeviceName(String belong, String deviceTemplate_id) {
        String deviceName = psrTemplateMappingMapper.getDeviceName(belong, deviceTemplate_id);
        return deviceName;
    }
    /**
     * 查询楼层
     * @param belong
     * @param deviceTemplate_id
     * @return String
     */
    public List<YaheCommonDataVO> getFloorInfo(String belong, String deviceTemplate_id) {
        List<YaheCommonDataVO> floorInfo = psrTemplateMappingMapper.getFloorInfo(belong, deviceTemplate_id);
        return floorInfo;
    }
    /**
     * 查询区域
     * @param belong
     * @param deviceTemplate_id
     * @return String
     */
    public List<YaheCommonDataVO> getAreaInfo(String belong, String deviceTemplate_id) {
        List<YaheCommonDataVO> floorInfo = psrTemplateMappingMapper.getAreaInfo(belong, deviceTemplate_id);
        return floorInfo;
    }

    /**
     * 条件查询空调设备id列表
     * @param areaList
     * @param deviceTemplate_id
     * @return List<Devicecode>
     */
    public List<Devicecode> getAirConditionIdList(List<String> areaList, String deviceTemplate_id) {
        List<Devicecode> airConIdList = psrTemplateMappingMapper.getAirConditionIdList(areaList, deviceTemplate_id);
        return airConIdList;
    }
}
