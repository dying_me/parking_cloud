package com.cygsunri.airCondition.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cygsunri.airCondition.entity.Devicecode;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author cygsunri
 * @since 2021-08-24
 */
public interface DevicecodeService extends IService<Devicecode> {

}
