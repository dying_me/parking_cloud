package com.cygsunri.airCondition.service;

import com.cygsunri.common.service.CommonDataService;
import com.cygsunri.measurement.entity.MeasurementValue;
import com.cygsunri.template.entity.psr.DeviceTemplate;
import com.cygsunri.template.entity.psr.DeviceTemplateData;
import com.cygsunri.template.service.DeviceTemplateDataService;
import org.assertj.core.util.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class AirConditionService {

    @Autowired
    private CommonDataService commonDataService;

    @Autowired
    private DeviceTemplateDataService deviceTemplateDataService;

    /**
     * 获取空调数据Map
     */
    public List<Map<String, Object>> getDeviceInfoExtra(String psrID, DeviceTemplate deviceTemplate) {
        List<Map<String, Object>> mapList = Lists.newArrayList();
        List<DeviceTemplateData> deviceTemplateDatas = deviceTemplateDataService.getDeviceTemplateDataListByDeviceTemplateId(deviceTemplate.getId());
        for (DeviceTemplateData data : deviceTemplate.getDeviceTemplateByType(deviceTemplateDatas, 2)) {
            MeasurementValue m = commonDataService.getValue(psrID, data.getName());
            mapList.add(new HashMap<String, Object>() {{
                put("name", data.getComments());
                put("value", m.isInValid() ? "-" : m.getData());
            }});
        }
        return mapList;
    }
}
