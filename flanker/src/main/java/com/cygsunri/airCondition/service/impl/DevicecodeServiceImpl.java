package com.cygsunri.airCondition.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cygsunri.airCondition.entity.Devicecode;
import com.cygsunri.airCondition.mapper.DevicecodeMapper;
import com.cygsunri.airCondition.service.DevicecodeService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author cygsunri
 * @since 2021-08-24
 */
@Service
public class DevicecodeServiceImpl extends ServiceImpl<DevicecodeMapper, Devicecode> implements DevicecodeService {

}
