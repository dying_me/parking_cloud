package com.cygsunri.airCondition.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author cygsunri
 * @since 2021-08-24
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("tb_devicecode")
public class Devicecode implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * scd设备表代码
     */
    @TableId(value = "id", type = IdType.ASSIGN_UUID)
    private String id;

    /**
     * 厂家关联id
     */
    @TableField("devCode")
    private String devCode;


}
