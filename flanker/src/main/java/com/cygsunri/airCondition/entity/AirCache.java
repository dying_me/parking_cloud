package com.cygsunri.airCondition.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 空调缓存
 * </p>
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@TableName("tb_airCache")
public class AirCache implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.ASSIGN_UUID)
    private String id;

    @TableField("`data`")
    private Double data;

    @TableField("`date`")
    private String date; //日期,非空,2016-01-01

    @TableField("`name`")
    private String name;

    @TableField("psrID")
    private String psrID;

    @TableField("substation")
    private String substation;

    @TableField("`time`")
    private String time; //时间,可空,16:00


}
