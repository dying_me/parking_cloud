package com.cygsunri.airCondition.entity;

import lombok.Getter;

import java.util.Arrays;
import java.util.Optional;

/**
 * 空调缓存类别
 */
@Getter
public enum AirCacheType {

    DAY(0, "DAY", "日"),
    MONTH(1, "MONTH", "月"),
    YEAR(2, "YEAR", "年");

    private int key;

    private String name;

    private String desc;

    AirCacheType(int key, String name, String desc) {
        this.key = key;
        this.name = name;
        this.desc = desc;
    }

    public static AirCacheType find(final int key) {
        Optional optional = Arrays.stream(AirCacheType.values())
                .filter(e -> e.key == key)
                .findFirst();
        return optional.isPresent() ? (AirCacheType) optional.get() : null;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
