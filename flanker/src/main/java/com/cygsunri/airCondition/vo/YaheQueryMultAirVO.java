package com.cygsunri.airCondition.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;


/**
 * <p>
 * 查询空调条件Bean
 * </p>
 *
 * @author cygsunri
 * @since 2021-08-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class YaheQueryMultAirVO {

    private List<String> deviceIds;

}
