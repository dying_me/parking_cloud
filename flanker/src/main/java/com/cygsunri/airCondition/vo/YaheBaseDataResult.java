package com.cygsunri.airCondition.vo;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author gaoang
 * @site 回调类响应结果基础Bean
 * @create 2021-08-02 16:45
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class YaheBaseDataResult extends YaheBaseResult {

    private String data;
}
