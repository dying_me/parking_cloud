package com.cygsunri.airCondition.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * <p>
 * 控制单台AC360的设备数据信息
 * </p>
 *
 * @author cygsunri
 * @since 2021-08-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class YaheCommandDataVO {

    private int conoff;

    private int mode;

    private int temperature;

    private int windSpeed;

    private int windDirection;

}
