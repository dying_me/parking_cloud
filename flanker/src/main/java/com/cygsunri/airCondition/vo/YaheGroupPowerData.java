package com.cygsunri.airCondition.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * <p>
 * 亚禾设备用电时长和用电量
 * </p>
 *
 * @author cygsunri
 * @since 2021-08-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class YaheGroupPowerData {

    /**
     * 总用电量
     */
    private String usedelecoun;

    /**
     * 总用电时长
     */
    private String usedtimecoun;
    /**
     * 分组名称
     */
    private String group_name;

}
