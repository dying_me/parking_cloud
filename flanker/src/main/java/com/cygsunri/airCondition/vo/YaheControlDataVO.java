package com.cygsunri.airCondition.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * <p>
 * 控制单台AC360的设备数据信息
 * </p>
 *
 * @author cygsunri
 * @since 2021-08-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class YaheControlDataVO {

    private String id;

    private String name;

    private YaheCommandDataVO commandSet;

}
