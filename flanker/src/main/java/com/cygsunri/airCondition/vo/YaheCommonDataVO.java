package com.cygsunri.airCondition.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * <p>
 * 亚禾公用数据信息
 * </p>
 *
 * @author cygsunri
 * @since 2021-08-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class YaheCommonDataVO {

    private String name;

    private String descInfo;

}
