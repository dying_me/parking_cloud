package com.cygsunri.airCondition.api;

import com.cygsunri.airCondition.service.AirCacheService;
import com.cygsunri.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 手动触发空调使用时间缓存API
 */
@RestController
@RequestMapping("/api/airCache")
public class AirCacheController {

    @Autowired
    private AirCacheService airCacheService;

    /**
     * 统计某一天
     * @param date yyyy-mm-dd
     */
    @RequestMapping(value = "/cache/{date}", method = RequestMethod.GET)
    public ResponseEntity makeCache(@PathVariable("date") String date) {
        airCacheService.updateCache(date);
        return new ResponseEntity(HttpStatus.OK);
    }

    /**
     * 统计一段时间
     * @param start 开始时间 yyyy-mm-dd
     * @param end 结束时间 yyyy-mm-dd
     */
    @RequestMapping(value = "/cache/days/{start}/{end}", method = RequestMethod.GET)
    public ResponseEntity makeCache(@PathVariable("start") String start, @PathVariable("end") String end) {
        List<String> days = DateUtil.getDays(start, end);
        for (String day : days) {
            airCacheService.updateCache(day);
        }
        return new ResponseEntity(HttpStatus.OK);
    }
}
