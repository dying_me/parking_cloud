package com.cygsunri.airCondition.api;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.cygsunri.airCondition.entity.Devicecode;
import com.cygsunri.airCondition.service.DevicecodeService;
import com.cygsunri.common.entity.YaHeConstant;
import com.cygsunri.template.entity.psr.PSRTemplateMapping;
import com.cygsunri.util.*;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.cygsunri.airCondition.service.PSRTemplateService;
import com.cygsunri.airCondition.vo.*;
import com.cygsunri.util.WebUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.DecimalFormat;
import java.util.*;

/**
 * 亚禾AC360空调控制
 */
@RestController
@RequestMapping("/api/v2/airCondition/")
public class YaheAirConditionController {

    @Autowired
    private YaHeConstant yaHeConstant;

    @Autowired
    private PSRTemplateService psrTemplateService;

    @Autowired
    private DevicecodeService devicecodeService;


    /**
     * 根据智慧园区substationId获取所有AC360统计数据
     *
     * @param
     * @return
     */
    @RequestMapping(value = "get/all/statistical/data", method = RequestMethod.GET)
    public ResponseEntity<Map<String, Object>> statistical(@RequestParam("substationId") String substationId) {
        Map<String, Object> data = new HashMap<>();
        //通过substationId获取所有AC360的设备(tb_psrtemplatemapping的belong字段站点+deviceTemplate_id类型)
        List<String> airIdList = psrTemplateService.getAirIdList(substationId, "yahe_ac360");
        YaheStatisticInfoVO yaheStatisticInfoVO = new YaheStatisticInfoVO();
        int totalNumber = 0;
        int successNumber = 0;
        int failureNumber = 0;
        int runningNumber = 0;
        int shutdownNumber = 0;
        int errorNumber = 0;
        try {
            String sq = "";
            for (int i = 0; i < airIdList.size(); i++) {
                sq = String.valueOf(System.currentTimeMillis());
                Map paramMap = new HashMap();
                paramMap.put("sq",sq);
                paramMap.put("airid",airIdList.get(i));
                paramMap.put("model","AC360");
                String result = WebUtils.get(yaHeConstant.getUrl()+yaHeConstant.getApiDeviceInfo1(),paramMap);
                YaheBaseDataResult yaheBaseDataResult = JSON.parseObject(result, YaheBaseDataResult.class);
                if (yaheBaseDataResult != null) {
                    totalNumber = totalNumber + 1;
                    if ("OK".equals(yaheBaseDataResult.getResult())) {
                        JSONArray jsonArray = JSONArray.parseArray(yaheBaseDataResult.getData());
                        if (!jsonArray.isEmpty()) {
                            JSONObject jsonObject = (JSONObject) jsonArray.get(0);
                            successNumber = successNumber + 1;
                            if ("开".equals(jsonObject.get("conoff").toString())) {
                                runningNumber = runningNumber + 1;
                            }
                            if ("关".equals(jsonObject.get("conoff").toString())) {
                                shutdownNumber = shutdownNumber + 1;
                            }
                        }

                    }
                    if ("ERR".equals(yaheBaseDataResult.getResult())) {
                        failureNumber = failureNumber + 1;
                    }
                }
            }
            yaheStatisticInfoVO.setTotalNumber(totalNumber);
            yaheStatisticInfoVO.setCommSuccessNumber(successNumber);
            yaheStatisticInfoVO.setCommFailureNumber(failureNumber);
            yaheStatisticInfoVO.setRunningNumber(runningNumber);
            yaheStatisticInfoVO.setShutdownNumber(shutdownNumber);
            yaheStatisticInfoVO.setErrorNumber(errorNumber);
            data.put("result", "OK");
            data.put("data" , yaheStatisticInfoVO);
            return new ResponseEntity<>(data, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            data.put("success", "ERR");
            data.put("data" , e.getMessage());
            return new ResponseEntity<>(data, HttpStatus.OK);
        }
    }

    /**
     * 根据空调设备deviceId获取单台AC360实时数据
     *
     * @param
     * @return
     */
    @RequestMapping(value = "get/single/ac/measurements", method = RequestMethod.GET)
    public ResponseEntity<Map<String, Object>> measurements(@RequestParam("deviceId") String deviceId) {
        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("id", deviceId);
        List<YaheRealTimeDataVO> yaheRealTimeDataVOList = new ArrayList<>();
        try {
            //空调控制器id转换成亚禾设备id
            LambdaQueryWrapper<Devicecode> devicecodeLambdaQueryWrapper = new LambdaQueryWrapper<>();
            devicecodeLambdaQueryWrapper.eq(Devicecode::getId, deviceId);
            Devicecode devicecode = devicecodeService.getOne(devicecodeLambdaQueryWrapper);
            if (devicecode != null) {
                //获取楼层区域信息
                LambdaQueryWrapper<PSRTemplateMapping> areaLambdaQueryWrapper = new LambdaQueryWrapper<>();
                areaLambdaQueryWrapper.eq(PSRTemplateMapping::getPsrID, deviceId);
                PSRTemplateMapping areaPSR = psrTemplateService.getOne(areaLambdaQueryWrapper);
                if (areaPSR != null) {
                    resultMap.put("areaId", areaPSR.getParent());
                    String areaName = psrTemplateService.getDeviceName(deviceId, "");
                    resultMap.put("areaName", areaName);
                    LambdaQueryWrapper<PSRTemplateMapping> floorLambdaQueryWrapper = new LambdaQueryWrapper<>();
                    floorLambdaQueryWrapper.eq(PSRTemplateMapping::getPsrID, areaPSR.getParent());
                    PSRTemplateMapping floorPSR = psrTemplateService.getOne(floorLambdaQueryWrapper);
                    if (floorPSR != null) {
                        resultMap.put("floorId",floorPSR.getParent());
                        String floorName = psrTemplateService.getDeviceName(floorPSR.getParent(), "");
                        resultMap.put("floorName", floorName);
                    }
                } else {
                    resultMap.put("areaId", "");
                    resultMap.put("areaName", "");
                    resultMap.put("floorId", "");
                    resultMap.put("floorName", "");
                }
                //获取设备信息
                String sq = String.valueOf(System.currentTimeMillis());
                Map paramMap = new HashMap();
                paramMap.put("sq",sq);
                paramMap.put("airid",devicecode.getDevCode());
                paramMap.put("model","AC360");
                String result = WebUtils.get(yaHeConstant.getUrl()+yaHeConstant.getApiDeviceInfo1(),paramMap);
                YaheBaseDataResult yaheBaseDataResult = JSON.parseObject(result, YaheBaseDataResult.class);
                if (yaheBaseDataResult != null) {
                    if ("OK".equals(yaheBaseDataResult.getResult())) {
                        JSONArray jsonArray = JSONArray.parseArray(yaheBaseDataResult.getData());
                        if (!jsonArray.isEmpty()) {
                            JSONObject jsonObject = (JSONObject) jsonArray.get(0);
                            resultMap.put("name", jsonObject.get("name"));
                            resultMap.put("time",jsonObject.get("update_date"));
                            int runStatus = 2;
                            if ("开".equals(jsonObject.get("conoff"))) {
                                runStatus = 0;
                            } else if ("关".equals(jsonObject.get("conoff"))) {
                                runStatus = 1;
                            }
                            resultMap.put("runStatus",runStatus);
                            yaheRealTimeDataVOList = getRealTimeData(jsonObject);
                            resultMap.put("data", yaheRealTimeDataVOList);
                            resultMap.put("result", "OK");
                        }
                    } else {
                        resultMap.putAll(creatErrEmptyResMap());
                    }
                } else {
                    resultMap.putAll(creatErrEmptyResMap());
                }
            } else {
                resultMap.putAll(creatAllEmptyResMap());
            }

            return new ResponseEntity<>(resultMap, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            resultMap.putAll(creatAllEmptyResMap());
            return new ResponseEntity<>(resultMap, HttpStatus.OK);
        }
    }

    private Map<String, Object> creatAllEmptyResMap() {
        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("result", "ERR");
        resultMap.put("name", "");
        resultMap.put("time", "");
        resultMap.put("areaId", "");
        resultMap.put("areaName", "");
        resultMap.put("floorId", "");
        resultMap.put("floorName", "");
        resultMap.put("runStatus", 2);
        resultMap.put("data", new ArrayList<>());
        return resultMap;
    }

    private Map<String, Object> creatErrEmptyResMap() {
        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("result", "ERR");
        resultMap.put("name", "");
        resultMap.put("time", "");
        resultMap.put("runStatus", 2);
        resultMap.put("data", new ArrayList<>());
        return resultMap;
    }

    /**
     * 发送遥控命令控制单台空调
     *
     * @param
     * @return
     */
    @RequestMapping(value = "single/execute", method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> sendCommand(@RequestBody YaheControlDataVO yaheControlDataVO) {
        Map<String, Object> data = new HashMap<>();
        //空调控制器id转换成亚禾设备id
        LambdaQueryWrapper<Devicecode> devicecodeLambdaQueryWrapper = new LambdaQueryWrapper<>();
        devicecodeLambdaQueryWrapper.eq(Devicecode::getId, yaheControlDataVO.getId());
        Devicecode devicecode = devicecodeService.getOne(devicecodeLambdaQueryWrapper);

        if (devicecode != null) {
            data.put("id", yaheControlDataVO.getId());
            data.put("name", yaheControlDataVO.getName());
            String sq = String.valueOf(System.currentTimeMillis());
            data.put("time", DateUtil.stampToDate(sq));
            try {
                Map paramMap = new HashMap();
                paramMap.put("sq",sq);
                paramMap.put("airid",devicecode.getDevCode());
                paramMap.put("conoff",yaheControlDataVO.getCommandSet().getConoff());
                paramMap.put("cmode",yaheControlDataVO.getCommandSet().getMode());
                paramMap.put("ctemp",yaheControlDataVO.getCommandSet().getTemperature());
                paramMap.put("cwind",yaheControlDataVO.getCommandSet().getWindSpeed());
                paramMap.put("cwinddir",yaheControlDataVO.getCommandSet().getWindDirection());
                String result = WebUtils.get(yaHeConstant.getUrl()+yaHeConstant.getInfrared(),paramMap);
                YaheBaseDataResult yaheBaseDataResult = JSON.parseObject(result, YaheBaseDataResult.class);
                if (yaheBaseDataResult != null) {
                    data.put("result", yaheBaseDataResult.getResult());
                    data.put("data" , yaheBaseDataResult.getData());
                } else {
                    data.put("result", "ERR");
                    data.put("data" , "通讯失败");
                }
                return new ResponseEntity<>(data, HttpStatus.OK);
            } catch (Exception e) {
                e.printStackTrace();
                data.put("result", "ERR");
                data.put("data" , e.getMessage());
                return new ResponseEntity<>(data, HttpStatus.OK);
            }
        } else {
            data.put("result", "ERR");
            data.put("data" , "通讯失败");
        }
        return new ResponseEntity<>(data, HttpStatus.OK);
    }

    /**
     * 根据智慧园区电站id获取有AC360的楼层
     *
     * @param
     * @return
     */
    @RequestMapping(value = "get/floors", method = RequestMethod.GET)
    public ResponseEntity<Map<String, Object>> getFloors(@RequestParam("substationId") String substationId) {
        Map<String, Object> data = new HashMap<>();
        try {
            List<YaheCommonDataVO> yaheFloorData = psrTemplateService.getFloorInfo(substationId, "yahe_ac360");
            if (yaheFloorData != null) {
                data.put("result", "OK");
                data.put("data" , yaheFloorData);
            } else {
                data.put("result", "ERR");
                data.put("data" , "查询失败");
            }
            return new ResponseEntity<>(data, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            data.put("result", "ERR");
            data.put("data" , e.getMessage());
            return new ResponseEntity<>(data, HttpStatus.OK);
        }
    }

    /**
     * 根据楼层id获取所有AC360区域
     *
     * @param
     * @return
     */
    @RequestMapping(value = "get/areas", method = RequestMethod.GET)
    public ResponseEntity<Map<String, Object>> getAreas(@RequestParam("floorId") String floorId) {
        Map<String, Object> data = new HashMap<>();
        try {
            List<YaheCommonDataVO> yaheAreaData = psrTemplateService.getAreaInfo(floorId, "yahe_ac360");
            if (yaheAreaData != null) {
                data.put("result", "OK");
                data.put("data" , yaheAreaData);
            } else {
                data.put("result", "ERR");
                data.put("data" , "查询失败");
            }
            return new ResponseEntity<>(data, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            data.put("result", "ERR");
            data.put("data" , e.getMessage());
            return new ResponseEntity<>(data, HttpStatus.OK);
        }
    }

    /**
     * 根据区域、空调状态、名称获取空调List
     *
     * @param
     * @return
     */
    @RequestMapping(value = "get/airConditions/by/query/conditions", method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> getAirConditionList(@RequestBody YaheQueryAirConditionVO yaheQueryAirConditionVO) {
        Map<String, Object> data = new HashMap<>();
        try {
            //查询所选区域的设备唯一码
            List<Devicecode> airConIdList = psrTemplateService.getAirConditionIdList(yaheQueryAirConditionVO.getArea(),  "yahe_ac360");
            if (airConIdList != null && airConIdList.size() > 0) {
                String sq = "";
                List<YaheAirConditionInfoVO> yaheAirConditionInfoVOList = new ArrayList<>();
                for (int i = 0; i < airConIdList.size(); i++) {
                    sq = String.valueOf(System.currentTimeMillis());
                    Map paramMap = new HashMap();
                    paramMap.put("sq",sq);
                    paramMap.put("airid",airConIdList.get(i).getDevCode());
                    paramMap.put("model","AC360");
                    String result = WebUtils.get(yaHeConstant.getUrl()+yaHeConstant.getApiDeviceInfo1(),paramMap);
                    YaheBaseDataResult yaheBaseDataResult = JSON.parseObject(result, YaheBaseDataResult.class);
                    if (yaheBaseDataResult != null && "OK".equals(yaheBaseDataResult.getResult())) {
                        JSONArray jsonArray = JSONArray.parseArray(yaheBaseDataResult.getData());
                        if (!jsonArray.isEmpty()) {
                            JSONObject jsonObject = (JSONObject) jsonArray.get(0);
                            //判断运行状态，以及机器名称模糊查询
                            if ("all".equals(yaheQueryAirConditionVO.getStatus())) {
                                if (jsonObject != null) {
                                    if (StringUtils.isNotBlank(yaheQueryAirConditionVO.getAirConditionName())
                                            && jsonObject.get("name").toString().indexOf(yaheQueryAirConditionVO.getAirConditionName()) > 0){
                                        yaheAirConditionInfoVOList.add(creatYaheAirCondition(airConIdList.get(i).getId(), jsonObject.get("name").toString()));
                                    } else if (StringUtils.isBlank(yaheQueryAirConditionVO.getAirConditionName())) {
                                        yaheAirConditionInfoVOList.add(creatYaheAirCondition(airConIdList.get(i).getId(), jsonObject.get("name").toString()));
                                    }
                                }
                            } else if ("open".equals(yaheQueryAirConditionVO.getStatus())) {
                                if (jsonObject != null && "开".equals(jsonObject.get("conoff").toString())) {
                                    if (StringUtils.isNotBlank(yaheQueryAirConditionVO.getAirConditionName())
                                            && jsonObject.get("name").toString().indexOf(yaheQueryAirConditionVO.getAirConditionName()) > 0){
                                        yaheAirConditionInfoVOList.add(creatYaheAirCondition(airConIdList.get(i).getId(), jsonObject.get("name").toString()));
                                    } else if (StringUtils.isBlank(yaheQueryAirConditionVO.getAirConditionName())) {
                                        yaheAirConditionInfoVOList.add(creatYaheAirCondition(airConIdList.get(i).getId(), jsonObject.get("name").toString()));
                                    }
                                }
                            } else if ("close".equals(yaheQueryAirConditionVO.getStatus())) {
                                if (jsonObject != null && "关".equals(jsonObject.get("conoff").toString())) {
                                    if (StringUtils.isNotBlank(yaheQueryAirConditionVO.getAirConditionName())
                                            && jsonObject.get("name").toString().indexOf(yaheQueryAirConditionVO.getAirConditionName()) > 0){
                                        yaheAirConditionInfoVOList.add(creatYaheAirCondition(airConIdList.get(i).getId(), jsonObject.get("name").toString()));
                                    } else if (StringUtils.isBlank(yaheQueryAirConditionVO.getAirConditionName())) {
                                        yaheAirConditionInfoVOList.add(creatYaheAirCondition(airConIdList.get(i).getId(), jsonObject.get("name").toString()));
                                    }
                                }
                            } else if ("error".equals(yaheQueryAirConditionVO.getStatus())) {
                                if (jsonObject != null && "-1".equals(jsonObject.get("conoff").toString())) {
                                    yaheAirConditionInfoVOList.add(creatYaheAirCondition(airConIdList.get(i).getId(), jsonObject.get("name").toString()));
                                }
                            }
                        }
                    }
                }
                data.put("result", "OK");
                data.put("data", sortByAirConName(yaheAirConditionInfoVOList));
            } else {
                data.put("result", "ERR");
                data.put("data", "查询失败。");
            }
            return new ResponseEntity<>(data, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            data.put("result", "ERR");
            data.put("data" , e.getMessage());
            return new ResponseEntity<>(data, HttpStatus.OK);
        }
    }

    /**
     * 根据空调设备deviceId列表获取多台AC360实时数据
     *
     * @param
     * @return
     */
    @RequestMapping(value = "get/multiple/ac/measurements", method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> multMeasurements(@RequestBody YaheQueryMultAirVO yaheQueryMultAirVO) {
        Map<String, Object> deviceResMap = new HashMap<>();
        List<Map<String, Object>> listData = new ArrayList<>();
        try {
            String sq = "";
            for (int i = 0; i < yaheQueryMultAirVO.getDeviceIds().size(); i++) {
                Map<String, Object> resultMap = new HashMap<>();
                List<YaheRealTimeDataVO> yaheRealTimeDataVOList = new ArrayList<>();
                resultMap.put("id", yaheQueryMultAirVO.getDeviceIds().get(i));
                //空调控制器id转换成亚禾设备id
                LambdaQueryWrapper<Devicecode> devicecodeLambdaQueryWrapper = new LambdaQueryWrapper<>();
                devicecodeLambdaQueryWrapper.eq(Devicecode::getId, yaheQueryMultAirVO.getDeviceIds().get(i));
                Devicecode devicecode = devicecodeService.getOne(devicecodeLambdaQueryWrapper);
                if (devicecode != null) {
                    //获取楼层区域信息
                    LambdaQueryWrapper<PSRTemplateMapping> areaLambdaQueryWrapper = new LambdaQueryWrapper<>();
                    areaLambdaQueryWrapper.eq(PSRTemplateMapping::getPsrID, yaheQueryMultAirVO.getDeviceIds().get(i));
                    PSRTemplateMapping areaPSR = psrTemplateService.getOne(areaLambdaQueryWrapper);
                    if (areaPSR != null) {
                        resultMap.put("areaId", areaPSR.getParent());
                        String areaName = psrTemplateService.getDeviceName(yaheQueryMultAirVO.getDeviceIds().get(i), "");
                        resultMap.put("areaName", areaName);
                        LambdaQueryWrapper<PSRTemplateMapping> floorLambdaQueryWrapper = new LambdaQueryWrapper<>();
                        floorLambdaQueryWrapper.eq(PSRTemplateMapping::getPsrID, areaPSR.getParent());
                        PSRTemplateMapping floorPSR = psrTemplateService.getOne(floorLambdaQueryWrapper);
                        if (floorPSR != null) {
                            resultMap.put("floorId",floorPSR.getParent());
                            String floorName = psrTemplateService.getDeviceName(floorPSR.getParent(), "");
                            resultMap.put("floorName", floorName);
                        }
                    } else {
                        resultMap.put("areaId", "");
                        resultMap.put("areaName", "");
                        resultMap.put("floorId", "");
                        resultMap.put("floorName", "");
                    }
                    //获取设备信息
                    sq = String.valueOf(System.currentTimeMillis());
                    Map paramMap = new HashMap();
                    paramMap.put("sq",sq);
                    paramMap.put("airid",devicecode.getDevCode());
                    paramMap.put("model","AC360");
                    String result = WebUtils.get(yaHeConstant.getUrl()+yaHeConstant.getApiDeviceInfo1(),paramMap);
                    YaheBaseDataResult yaheBaseDataResult = JSON.parseObject(result, YaheBaseDataResult.class);
                    if (yaheBaseDataResult != null) {
                        if ("OK".equals(yaheBaseDataResult.getResult())) {
                            JSONArray jsonArray = JSONArray.parseArray(yaheBaseDataResult.getData());
                            if (!jsonArray.isEmpty()) {
                                JSONObject jsonObject = (JSONObject) jsonArray.get(0);
                                resultMap.put("name", jsonObject.get("name"));
                                resultMap.put("time",jsonObject.get("update_date"));
                                int runStatus = 2;
                                if ("开".equals(jsonObject.get("conoff"))) {
                                    runStatus = 0;
                                } else if ("关".equals(jsonObject.get("conoff"))) {
                                    runStatus = 1;
                                }
                                resultMap.put("runStatus",runStatus);
                                yaheRealTimeDataVOList = getRealTimeData(jsonObject);
                                resultMap.put("data", yaheRealTimeDataVOList);
                                resultMap.put("result", "OK");
                            }
                        } else {
                            resultMap.putAll(creatErrEmptyResMap());
                        }
                    } else {
                        resultMap.putAll(creatErrEmptyResMap());
                    }
                } else {
                    resultMap.putAll(creatAllEmptyResMap());
                }
                listData.add(resultMap);
            }
            deviceResMap.put("result","OK");
            deviceResMap.put("data",sortByAirConNameForMult(listData));
            return new ResponseEntity<>(deviceResMap, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            deviceResMap.put("result", "ERR");
            deviceResMap.put("data" , listData);
            return new ResponseEntity<>(deviceResMap, HttpStatus.OK);
        }
    }


    private YaheAirConditionInfoVO creatYaheAirCondition(String id, String name) {
        YaheAirConditionInfoVO yaheAirConditionInfoVO = new YaheAirConditionInfoVO();
        yaheAirConditionInfoVO.setId(id);
        yaheAirConditionInfoVO.setName(name);
        return yaheAirConditionInfoVO;
    };

    private List<YaheRealTimeDataVO> getRealTimeData(JSONObject yaheSingleAC360InfoVO) {
        List<YaheRealTimeDataVO> yaheRealTimeDataVOList = new ArrayList<>();
        DecimalFormat df = new DecimalFormat("0.00");
        YaheRealTimeDataVO tempData = new YaheRealTimeDataVO();
        tempData.setName("temperature");
        tempData.setDescInfo("温度");
        tempData.setValue(Float.parseFloat(yaheSingleAC360InfoVO.get("intemp").toString()));
        tempData.setUnit("℃");
        yaheRealTimeDataVOList.add(tempData);
        YaheRealTimeDataVO humData = new YaheRealTimeDataVO();
        humData.setName("humidity");
        humData.setDescInfo("湿度");
        humData.setValue(Float.parseFloat(yaheSingleAC360InfoVO.get("humidity").toString()));
        humData.setUnit("%");
        yaheRealTimeDataVOList.add(humData);
        YaheRealTimeDataVO illumData = new YaheRealTimeDataVO();
        illumData.setName("Illuminance");
        illumData.setDescInfo("光照度");
        illumData.setValue(Float.parseFloat(yaheSingleAC360InfoVO.get("photositive").toString()));
        illumData.setUnit("%");
        yaheRealTimeDataVOList.add(illumData);
        YaheRealTimeDataVO avaData = new YaheRealTimeDataVO();
        avaData.setName("availableKwh");
        avaData.setDescInfo("可用电量");
        avaData.setValue(Float.parseFloat(yaheSingleAC360InfoVO.get("usablele").toString()));
        avaData.setUnit("度");
        yaheRealTimeDataVOList.add(avaData);
        YaheRealTimeDataVO volData = new YaheRealTimeDataVO();
        volData.setName("voltage");
        volData.setDescInfo("电压");
        volData.setValue(Float.parseFloat(yaheSingleAC360InfoVO.get("avol").toString()));
        volData.setUnit("V");
        yaheRealTimeDataVOList.add(volData);
        YaheRealTimeDataVO aeleData = new YaheRealTimeDataVO();
        aeleData.setName("voltage");
        aeleData.setDescInfo("电流");
        aeleData.setValue(Float.parseFloat(yaheSingleAC360InfoVO.get("aele").toString()));
        aeleData.setUnit("mA");
        yaheRealTimeDataVOList.add(aeleData);
        YaheRealTimeDataVO powerData = new YaheRealTimeDataVO();
        powerData.setName("power");
        powerData.setDescInfo("功率");
        powerData.setValue(Float.parseFloat(yaheSingleAC360InfoVO.get("totalpow").toString()));
        powerData.setUnit("W");
        yaheRealTimeDataVOList.add(powerData);
        YaheRealTimeDataVO accData = new YaheRealTimeDataVO();
        accData.setName("accumulateKwh");
        accData.setDescInfo("累计用电");
        accData.setValue(Float.parseFloat(yaheSingleAC360InfoVO.get("usedele").toString()));
        accData.setUnit("kWh");
        yaheRealTimeDataVOList.add(accData);
        YaheRealTimeDataVO eleData = new YaheRealTimeDataVO();
        eleData.setName("accumulateKwh");
        eleData.setDescInfo("用电时长");
        eleData.setValue(Float.parseFloat(yaheSingleAC360InfoVO.get("usedtime").toString()));
        yaheRealTimeDataVOList.add(eleData);

        return yaheRealTimeDataVOList;
    }

    private List<YaheAirConditionInfoVO> sortByAirConName (List<YaheAirConditionInfoVO> airConList) {
        if (airConList != null && airConList.size() > 0) {
            Collections.sort(airConList, new Comparator<YaheAirConditionInfoVO>() {
                @Override
                public int compare(YaheAirConditionInfoVO o1, YaheAirConditionInfoVO o2) {
                    if(o1.getName().compareTo(o2.getName()) > 0){
                        return 1;
                    }
                    if(o1.getName().compareTo(o2.getName()) == 0){
                        return 0;
                    }
                    return -1;
                }
            });
        }
        return airConList;
    }

    private List<Map<String, Object>> sortByAirConNameForMult (List<Map<String, Object>> airConList) {
        if (airConList != null && airConList.size() > 0) {
            Collections.sort(airConList, new Comparator<Map<String, Object>>() {
                @Override
                public int compare(Map<String, Object> o1, Map<String, Object> o2) {
                    if (o1.get("name").toString().compareTo(o2.get("name").toString()) > 0) {
                        return 1;
                    }
                    if (o1.get("name").toString().compareTo(o2.get("name").toString()) == 0) {
                        return 0;
                    }
                    return -1;
                }
            });
        }
        return airConList;
    }

}
