package com.cygsunri.airCondition.api;

import com.cygsunri.airCondition.service.AirConditionService;
import com.cygsunri.common.constant.DeviceType;
import com.cygsunri.common.entity.DeviceInfo;
import com.cygsunri.scada.base.BasePSR;
import com.cygsunri.scada.service.ScadaPSRService;
import com.cygsunri.template.entity.psr.DeviceTemplate;
import com.cygsunri.template.service.PSRTemplateMappingService;
import org.assertj.core.util.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 空调概览API
 */
@RestController
@RequestMapping("/api/airCondition")
public class AirConditionController {

    @Autowired
    private ScadaPSRService scadaPSRService;

    @Autowired
    private PSRTemplateMappingService psrTemplateMappingService;

    @Autowired
    private AirConditionService airConditionService;

    /**
     * 空调概览API
     */
    @RequestMapping(value = "/measurement/information", method = RequestMethod.GET)
    public ResponseEntity<List<DeviceInfo>> getAirConditionInfo() {
        List<DeviceInfo> deviceInfoList = Lists.newArrayList();
        List<BasePSR> air_outsides = scadaPSRService.getDevicesByType(DeviceType.AIR_OUTSIDE.getKey());
        for (BasePSR air_outside : air_outsides) {
            DeviceInfo deviceInfo = new DeviceInfo()
                    .setPsrID(air_outside.getId())
                    .setName(air_outside.getName())
                    .setType(air_outside.getPsrType().getName());
            DeviceTemplate air_outside_template = psrTemplateMappingService.getDeviceTemplateByMapping(air_outside.getId());
            if (air_outside_template != null) {
                deviceInfo.setExtra(airConditionService.getDeviceInfoExtra(air_outside.getId(), air_outside_template));
            }
            List<DeviceInfo> children = Lists.newArrayList();
            List<BasePSR> air_insides = scadaPSRService.getChildrenDevice(air_outside.getId());
            for (BasePSR air_inside : air_insides) {
                DeviceInfo d = new DeviceInfo()
                        .setPsrID(air_inside.getId())
                        .setName(air_inside.getName())
                        .setType(air_inside.getPsrType().getName());
                DeviceTemplate air_inside_template = psrTemplateMappingService.getDeviceTemplateByMapping(air_inside.getId());
                if (air_inside_template != null) {
                    d.setExtra(airConditionService.getDeviceInfoExtra(air_inside.getId(), air_inside_template));
                }
                children.add(d);
            }
            deviceInfo.setChildren(children);
            deviceInfoList.add(deviceInfo);
        }
        return new ResponseEntity<>(deviceInfoList, HttpStatus.OK);
    }
}
