package com.cygsunri.airCondition.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cygsunri.airCondition.entity.AirCache;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * Mapper 接口
 * </p>
 */
public interface AirCacheMapper extends BaseMapper<AirCache> {

    Double getAirCache(@Param("psrID") String psrID, @Param("name") String name, @Param("date") String date);

    Double getAirCacheSum(@Param("psrID") String psrID, @Param("name") String name, @Param("from") String from, @Param("to") String to);
}
