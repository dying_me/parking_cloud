package com.cygsunri.airCondition.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cygsunri.airCondition.entity.Devicecode;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author cygsunri
 * @since 2021-08-24
 */
public interface DevicecodeMapper extends BaseMapper<Devicecode> {

}
