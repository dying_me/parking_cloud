package com.cygsunri.rbac.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;
import java.util.List;

/**
 * Table Name : role
 */
@Data
@Accessors(chain = true)
@TableName("tb_role")
public class Role implements java.io.Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.ASSIGN_UUID)
    private String id;

    @TableField("roleName")
    private String roleName;

    @TableField("roleDesc")
    private String roleDesc;

    @TableField("creatorId")
    private String creatorId;

    @TableField("createTime")
    private Date createTime;

    @TableField("modifierId")
    private String modifierId;

    @TableField("modifyTime")
    private Date modifyTime;

    @TableField(exist=false)
    private List<RoleMenu> roleMenus;
}