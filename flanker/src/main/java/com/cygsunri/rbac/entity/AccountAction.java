package com.cygsunri.rbac.entity;

import lombok.Data;

@Data
public class AccountAction {

    private String username;

    private String realname;

    private Integer area;

    private String password;

    private String passwordRepeat;

    private String id;

    private String roleId;
}
