package com.cygsunri.rbac.entity;

import lombok.Data;

@Data
public class LoginAction {
    private String username;
    private String password;
    private String type;
}