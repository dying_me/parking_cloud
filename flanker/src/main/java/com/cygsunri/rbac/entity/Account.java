package com.cygsunri.rbac.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * Table Name : account
 */
@Data
@Accessors(chain = true)
@TableName("tb_account")
public class Account implements java.io.Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.ASSIGN_UUID)
    private String id;

    @TableField("username")
    private String username;

    @TableField("password")
    private String password;

    @TableField("realName")
    private String realName;

    @TableField("salt")
    private String salt;

    @TableField("creatorId")
    private String creatorId;

    @TableField("createTime")
    private Date createTime;

    @TableField("modifierId")
    private String modifierId;

    @TableField("modifyTime")
    private Date modifyTime;

    @TableField("area")
    private Integer area;

    @TableField("PsrID")
    private String PsrID;

    @TableField("roleId")
    private String roleId;

    @TableField(exist=false)
    private Role role;
}