package com.cygsunri.rbac.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;
import java.util.List;

/**
 * Table Name : menu
 */
@Data
@Accessors(chain = true)
@TableName("tb_menu")
public class Menu implements java.io.Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.ASSIGN_UUID)
    private String id;

    @TableField("parentId")
    private String parentId;

    @TableField("name")
    private String name;

    @TableField("path")
    private String path;

    @TableField("needCheck")
    private Integer needCheck;

    @TableField("isShow")
    private Integer isShow;

    @TableField("creatorId")
    private String creatorId;

    @TableField("createTime")
    private Date createTime;

    @TableField("modifierId")
    private String modifierId;

    @TableField("modifyTime")
    private Date modifyTime;

    @TableField("orderSeq")
    private Integer orderSeq;

    @TableField("icon")
    private String icon;

    @TableField("component")
    private String component;

    @TableField("redirect")
    private String redirect;

    @TableField(exist=false)
    private List<RoleMenu> roleMenus;

    @TableField(exist = false)
    private List<Menu> routes;
}