package com.cygsunri.rbac.entity;

import lombok.Data;

@Data
public class LoginResponse {
    private String status;
    private String type;
    private String userName;
    private String userID;
    private String currentAuthority;

    public LoginResponse() {

    }

    public LoginResponse(String status, String type, String userName, String userID, String currentAuthority) {
        this.status = status;
        this.type = type;
        this.userName = userName;
        this.userID = userID;
        this.currentAuthority = currentAuthority;
    }
}
