package com.cygsunri.rbac.entity;

import lombok.Data;

@Data
public class PasswordAction {

    private String userID;
    private String oldPassword;
    private String newPassword;
}
