package com.cygsunri.rbac.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * Table Name : roleMenu
 */
@Data
@Accessors(chain = true)
@TableName("tb_roleMenu")
public class RoleMenu implements java.io.Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.ASSIGN_UUID)
    private String id;

    @TableField("creatorId")
    private String creatorId;

    @TableField("createTime")
    private Date createTime;

    @TableField("modifierId")
    private String modifierId;

    @TableField("modifyTime")
    private Date modifyTime;

    @TableField("roleId")
    private String roleId;

    @TableField("menuId")
    private String menuId;

    @TableField(exist=false)
    private Role role;

    @TableField(exist=false)
    private Menu menu;

}