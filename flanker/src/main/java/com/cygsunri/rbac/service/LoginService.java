package com.cygsunri.rbac.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cygsunri.rbac.entity.Account;
import org.springframework.stereotype.Service;


@Service
public interface LoginService extends IService<Account> {

    public Boolean checkPassword(Account account, String password);

    public Boolean checkPassword(String username, String password);
}
