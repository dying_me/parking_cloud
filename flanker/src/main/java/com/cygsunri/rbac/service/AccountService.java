package com.cygsunri.rbac.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cygsunri.rbac.entity.Account;

import java.util.List;

public interface AccountService extends IService<Account> {

    public Account getAccountByName(String name);

    public Account getAccountByID(String id);

    public List<Account> getAccountList();

}
