package com.cygsunri.rbac.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cygsunri.rbac.entity.Menu;
import com.cygsunri.rbac.entity.RoleMenu;
import com.cygsunri.rbac.mapper.MenuMapper;
import com.cygsunri.rbac.mapper.RoleMenuMapper;
import com.cygsunri.rbac.service.MenuService;
import com.cygsunri.rbac.util.MenuHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MenuServiceImpl extends ServiceImpl<MenuMapper, Menu> implements MenuService {

    @Autowired
    private MenuMapper menuDao;

    @Autowired
    private RoleMenuMapper roleMenuDao;

    public Menu getMenuByName(String name) {
        return menuDao.findByName(name);
    }

//    public Menu getMenuByID(String id) {
//        return menuDao.selectByPrimaryKey(id);
//    }
//
//    public List<Menu> getMenuList() {
//        return menuDao.selectAll();
//    }

    public List<JSONObject> getMenuListByRole(String roleID) {
        List<Menu> menus = new ArrayList<>();

        List<RoleMenu> roleMenus = roleMenuDao.findMenusByRoleID(roleID);
        for (RoleMenu roleMenu :roleMenus){
            Menu menu = roleMenu.getMenu();
//            menu.setRoleMenus(null);
            menus.add(menu);
        }

        List<Menu> menuTreeList = MenuHelper.bulid(menus);

        List<JSONObject> result = MenuHelper.bulidJson(menuTreeList);

        return result;

//
//        List<Permission> selectPermissionList = null;
//        if(this.isSysAdmin(userId)) {
//            //如果是超级管理员，获取所有菜单
//            selectPermissionList = baseMapper.selectList(null);
//        } else {
//            selectPermissionList = baseMapper.selectPermissionByUserId(userId);
//        }
//
//        List<Permission> permissionList = PermissionHelper.bulid(selectPermissionList);
//        List<JSONObject> result = MemuHelper.bulid(permissionList);

    }

//    @Transactional
//    public int deleteMenuByID(String id) {
//        return menuDao.deleteByPrimaryKey(id);
//    }
//
//    @Transactional
//    public int saveMenu(Menu role) {
//        return menuDao.insert(role);
//    }

}
