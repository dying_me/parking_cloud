package com.cygsunri.rbac.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cygsunri.rbac.entity.Role;

public interface RoleService extends IService<Role> {


    public Role getRoleByName(String name);

}
