package com.cygsunri.rbac.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cygsunri.rbac.entity.Account;
import com.cygsunri.rbac.mapper.AccountMapper;
import com.cygsunri.rbac.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("accountService")
public class AccountServiceImpl extends ServiceImpl<AccountMapper,Account> implements AccountService {

    @Autowired
    private AccountMapper accountDao;

    public Account getAccountByName(String name) {
        return accountDao.findByName(name);
    }

    @Override
    public Account getAccountByID(String id) {
        return accountDao.selectById(id);
    }

    @Override
    public List<Account> getAccountList() {
        return null;
    }

}
