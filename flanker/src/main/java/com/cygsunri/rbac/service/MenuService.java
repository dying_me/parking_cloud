package com.cygsunri.rbac.service;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.service.IService;
import com.cygsunri.rbac.entity.Menu;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface MenuService extends IService<Menu> {

    public Menu getMenuByName(String name);

    public List<JSONObject> getMenuListByRole(String roleID);
}
