package com.cygsunri.rbac.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cygsunri.rbac.entity.Role;
import com.cygsunri.rbac.mapper.RoleMapper;
import com.cygsunri.rbac.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("roleService")
public class RoleServiceImpl extends ServiceImpl<RoleMapper,Role> implements RoleService {

    @Autowired
    private RoleMapper roleDao;

    public Role getRoleByName(String name) {
        return roleDao.findByName(name);
    }
}
