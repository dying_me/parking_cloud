package com.cygsunri.rbac.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cygsunri.rbac.entity.Account;
import org.springframework.stereotype.Component;


@Component
public interface AccountMapper extends BaseMapper<Account> {

    public Account findByName(String name);

}
