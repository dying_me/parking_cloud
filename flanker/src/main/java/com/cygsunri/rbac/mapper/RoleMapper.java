package com.cygsunri.rbac.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cygsunri.rbac.entity.Role;
import org.springframework.stereotype.Component;

@Component
public interface RoleMapper extends BaseMapper<Role> {

    public Role findByName(String name);

//    public Role findById(String id);
//
//    public List<Role> findAll();
//
//    public int deleteById(String id);
//
//    public int save(Role role);
//
//    public int update(Role role);
}
