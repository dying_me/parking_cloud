package com.cygsunri.rbac.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cygsunri.rbac.entity.RoleMenu;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface RoleMenuMapper extends BaseMapper<RoleMenu> {

    public List<RoleMenu> findMenusByRoleID(String roleID);

}
