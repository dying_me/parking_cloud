package com.cygsunri.rbac.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cygsunri.rbac.entity.Menu;
import org.springframework.stereotype.Component;

@Component
public interface MenuMapper extends BaseMapper<Menu> {

    public Menu findByName(String name);

//    public Menu findById(String id);
//
//    public List<Menu> findAll();
//
//    public int deleteById(String id);
//
//    public int save(Menu menu);
}
