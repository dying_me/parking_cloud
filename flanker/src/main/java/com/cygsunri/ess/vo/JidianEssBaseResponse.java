package com.cygsunri.ess.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.LinkedHashMap;

@Data
@Accessors(chain = true)
public class JidianEssBaseResponse implements Serializable {

    private static final long serialVersionUID = 1L;

    private String code;

    private String message;

    private LinkedHashMap data;

}