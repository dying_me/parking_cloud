package com.cygsunri.ess.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;


@Data
@Accessors(chain = true)
public class JidianEssChart implements Serializable {

    private static final long serialVersionUID = 1L;

    private String Pgrid;
    private String Pload;
    private String Pdc;
    private String date_time;
}
