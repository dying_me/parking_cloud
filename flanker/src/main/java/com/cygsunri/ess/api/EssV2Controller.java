package com.cygsunri.ess.api;

import com.alibaba.fastjson.JSON;
import com.cygsunri.common.constant.DataName;
import com.cygsunri.common.service.CommonDataService;
import com.cygsunri.ess.dto.KwhParams;
import com.cygsunri.ess.dto.TrendParams;
import com.cygsunri.measurement.entity.Flag;
import com.cygsunri.measurement.entity.MeasurementValue;
import com.cygsunri.multienergy.api.dto.HisData;
import com.cygsunri.multienergy.api.dto.HisDataList;
import com.cygsunri.scada.base.BasePSR;
import com.cygsunri.scada.service.ScadaPSRService;
import com.cygsunri.util.DateUtil;
import com.cygsunri.util.TimeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

/**
 * 储能页api新版本
 */

@RestController
@RequestMapping("/api/v2/ess")
public class EssV2Controller {


    @Autowired
    private ScadaPSRService scadaPSRService;

    @Autowired
    private CommonDataService commonDataService;


    /**
     * 储能每日充放电量
     *
     * @param id   储能PSRID
     * @param from 开始时间
     * @param to   结束时间
     */
    @PostMapping(value = "/charts/statistics/kwh")
    public ResponseEntity<String> statisticsKwh(@RequestBody KwhParams kwhParams) {

        HisDataList retDataList = new HisDataList();

        BasePSR substation = scadaPSRService.getDeviceByPsrId(kwhParams.getSubstationID());
        if (substation == null) {
            return new ResponseEntity<>(JSON.toJSONString(retDataList), HttpStatus.NOT_FOUND);
        }

        List<String> dates = DateUtil.getDays(kwhParams.getFrom(), kwhParams.getTo());

        HisData chargeData = new HisData();
        HisData disChargeData = new HisData();

        for (String date : dates) {
            //categoryAxis.data(date);
            retDataList.getTimeRange().add(date);

            MeasurementValue dayCharge = commonDataService.getValue(kwhParams.getSubstationID(), DataName.DAY_CHARGE, TimeUtil.toMilliSeconds(date), Flag.DAY_MAX);
            chargeData.setName(dayCharge.getComments());
            chargeData.setUnit(dayCharge.getUnit());
            chargeData.getData().add(dayCharge.isInValid() ? "-" : dayCharge.getData().toString());
            //charge.data(dayCharge.isInValid() ? "-" : dayCharge.getData());

            MeasurementValue dayDischarge = commonDataService.getValue(kwhParams.getSubstationID(), DataName.DAY_DISCHARGE, TimeUtil.toMilliSeconds(date), Flag.DAY_MAX);
            disChargeData.setName(dayDischarge.getComments());
            disChargeData.setUnit(dayDischarge.getUnit());
            disChargeData.getData().add(dayDischarge.isInValid() ? "-" : dayDischarge.getData().toString());
            //discharge.data(dayDischarge.isInValid() ? "-" : dayDischarge.getData());
        }
        retDataList.getRecords().add(chargeData);
        retDataList.getRecords().add(disChargeData);

        return new ResponseEntity<>(JSON.toJSONString(retDataList), HttpStatus.OK);
    }

    /**
     * 趋势曲线
     *
     *
     * @param trendParams 趋势请求参数
     */
    @PostMapping(value = "/charts/day/trend")
    public ResponseEntity<String> trend(@RequestBody TrendParams trendParams) {

        HisDataList retDataList = new HisDataList();

        List<MeasurementValue> todayValues = commonDataService.getHistoryValue(trendParams.getSubstationID(), trendParams.getDataName(),
                trendParams.getDate().trim() + " 00:00", trendParams.getDate().trim() + " 24:00", 5, 2, false);

        DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate today = LocalDate.parse(trendParams.getDate().trim(),df);
        LocalDate yesterday = today.plusDays(-1);

        List<MeasurementValue> yesterdayValues = commonDataService.getHistoryValue(trendParams.getSubstationID(), trendParams.getDataName(),
                yesterday.toString() + " 00:00", yesterday.toString() + " 24:00", 5, 2, false);

        if (!todayValues.isEmpty()) {

            HisData todayData = new HisData();
            todayData.setName("今日");

            for (MeasurementValue m : todayValues) {

                retDataList.getTimeRange().add(m.getTime());

                todayData.getData().add(m.isInValid() ? "-" : m.getData().toString());
            }
            retDataList.getRecords().add(todayData);
            HisData yesterdayData = new HisData();
            yesterdayData.setName("昨日");
            for (MeasurementValue m : yesterdayValues) {

                //retDataList.getTimeRange().add(m.getTime());

                yesterdayData.getData().add(m.isInValid() ? "-" : m.getData().toString());
            }
            retDataList.getRecords().add(yesterdayData);

        }

        return new ResponseEntity<>(JSON.toJSONString(retDataList), HttpStatus.OK);
    }
}
