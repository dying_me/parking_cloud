package com.cygsunri.ess.dto;

import lombok.Data;

@Data
public class TrendParams {

    String substationID;
    String dataName;
    String date;

}
