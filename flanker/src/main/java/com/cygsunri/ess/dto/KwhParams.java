package com.cygsunri.ess.dto;

import lombok.Data;

@Data
public class KwhParams {

    String substationID;
    String from;
    String to;
}
