package com.cygsunri.event.api;

import com.cygsunri.event.websocket.service.WebSocketServer;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/webSocket")
public class DemoController {

    @RequestMapping(value = "/demo", method = RequestMethod.POST)
    public ResponseEntity<String> pushToWeb(String message) {
        WebSocketServer.broadcast(message);
        return ResponseEntity.ok("MSG SEND SUCCESS");
    }
}
