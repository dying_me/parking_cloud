package com.cygsunri.event.api;

import com.cygsunri.event.entity.EventInfo;
import com.cygsunri.event.entity.EventParams;
import com.cygsunri.event.service.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 历史事项API
 */
@RestController
@RequestMapping("/api/event")
public class EventController {

    @Autowired
    private EventService eventService;

    /**
     * 查询历史事项
     */
    @RequestMapping(value = "getHistoryEvent", method = RequestMethod.POST)
    public ResponseEntity<List<EventInfo>> getHistoryEvent(@RequestBody EventParams eventParams) {
        return new ResponseEntity<>(eventService.getEventInfoList(eventParams), HttpStatus.OK);
    }

    /**
     * 设置已读历史事项
     */
    @RequestMapping(value = "setEventRead", method = RequestMethod.POST)
    public ResponseEntity<String> setEventRead(@RequestBody List<EventInfo> eventInfoList) {
        eventService.insertEventList(eventInfoList);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
