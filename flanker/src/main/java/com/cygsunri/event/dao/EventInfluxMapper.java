package com.cygsunri.event.dao;

import com.cygsunri.event.entity.EventInfo;
import com.google.common.collect.Lists;
import org.influxdb.dto.QueryResult;
import org.influxdb.impl.InfluxDBResultMapper;

import java.util.List;
import java.util.Objects;

/**
 * InfluxDB历史事项mapper
 */
public class EventInfluxMapper extends InfluxDBResultMapper {

    public List<EventInfo> toEventInfo(QueryResult queryResult) {
        Objects.requireNonNull(queryResult, "queryResult");
        List<EventInfo> result = Lists.newLinkedList();
        queryResult.getResults().stream().filter((internalResult) -> Objects.nonNull(internalResult) && Objects.nonNull(internalResult.getSeries())).forEach((internalResult) -> internalResult.getSeries().forEach((series) -> series.getValues().forEach((value) -> {
            EventInfo eventInfo = new EventInfo()
                    .transformTimestampFromUTC((String) value.get(0))
                    .setId((String) value.get(10))
                    .setKey((String) value.get(10))
                    .setIsConfirm((Boolean) value.get(1))
                    .setPsrDesc((String) value.get(2))
                    .setPsrId((String) value.get(3))
                    .setMeasurementDesc((String) value.get(4))
                    .setMeasurementID((String) value.get(5))
                    .setSubstationDesc((String) value.get(6))
                    .setSubstationID((String) value.get(7))
                    .setTxtInfo((String) value.get(8))
                    .setType(((Double) value.get(9)).intValue());
            result.add(eventInfo);
        })));

        return result;
    }
}
