package com.cygsunri.event.service;

import com.cygsunri.event.dao.EventDao;
import com.cygsunri.event.entity.EventInfo;
import com.cygsunri.event.entity.EventParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EventService {

    @Autowired
    private EventDao eventDao;

    /**
     * 通过代码拿到遥信信息
     */
    @Cacheable(value = "YxInfoMapping", key = "#code")
    public EventInfo getYxInfoMapping(String code) {
        // log.info("未找到的缓存：code:{}", code);
        return null;
    }

    /**
     * 遥信参数表信息映射为EventInfo类
     */
    public List<EventInfo> getYxInfo() {
        return eventDao.getYxInfo();
    }

    /**
     * 存储事件List到influxDB
     */
    public void insertEventList(List<EventInfo> eventInfoList) {
        eventDao.insertEventList(eventInfoList);
    }

    /**
     * 前台确认事件List到influxDB
     */
    public void confirmEventList(List<EventInfo> eventInfoList) {
        eventDao.confirmEventList(eventInfoList);
    }

    /**
     * 按条件读取事件
     */
    public List<EventInfo> getEventInfoList(EventParams eventParams) {
        return eventDao.getEventInfoList(eventParams);
    }
}
