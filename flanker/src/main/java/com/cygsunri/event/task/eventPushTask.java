package com.cygsunri.event.task;

import com.cygsunri.event.service.EventPushService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

/**
 * 事项推送任务
 */
@Component
@Slf4j
@ConditionalOnProperty(prefix = "task.event", name = "push", havingValue = "true")
public class eventPushTask implements ApplicationRunner {

    @Autowired
    private EventPushService eventPushService;

    @Override
    public void run(ApplicationArguments args) {
        eventPushService.start();
    }
}
