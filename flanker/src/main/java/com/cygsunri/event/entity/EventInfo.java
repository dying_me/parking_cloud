package com.cygsunri.event.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * 历史事项通用类
 */
@Data
@Accessors(chain = true)
public class EventInfo {
    private Long timeStamp;
    private String id;
    private String key;
    private String measurementID;
    private String measurementDesc;
    private String psrId;
    private String psrDesc;
    private String substationID;
    private String substationDesc;
    private String txtInfo;
    private Integer type; //1:soe,2:变位,0:总招
    private Boolean isConfirm;

    public EventInfo() {
    }

    public EventInfo(String measurementID, String measurementDesc, String psrId, String psrDesc, String substationID, String substationDesc) {
        this.measurementID = measurementID;
        this.measurementDesc = measurementDesc;
        this.psrId = psrId;
        this.psrDesc = psrDesc;
        this.substationID = substationID;
        this.substationDesc = substationDesc;
    }

    /**
     * 从InfluxDB的时间戳转换为系统可用的时间戳
     *
     * @param utc UTC时间
     * @return EventInfo
     */
    public EventInfo transformTimestampFromUTC(final String utc) {
        LocalDateTime localDateTime = LocalDateTime.parse(utc, DateTimeFormatter.ISO_OFFSET_DATE_TIME);
        Instant instant = localDateTime.atZone(ZoneId.systemDefault()).toInstant();
        this.setTimeStamp(Date.from(instant).getTime());
        return this;
    }
}
