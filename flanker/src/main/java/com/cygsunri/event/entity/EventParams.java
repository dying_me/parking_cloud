package com.cygsunri.event.entity;

import lombok.Data;

/**
 * 查询历史事项请求参数
 */
@Data
public class EventParams {
    private String startDate;
    private String endDate;
    private String confirm;
    private String eventType;
    private String searchName;
}
