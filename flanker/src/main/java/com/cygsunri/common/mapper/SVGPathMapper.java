package com.cygsunri.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cygsunri.common.entity.SVGPath;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * Mapper 接口
 * </p>
 */
public interface SVGPathMapper extends BaseMapper<SVGPath> {

    String getSVGPath(@Param("account") String account, @Param("menu") String menu);
}
