package com.cygsunri.common.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 实时、历史查询的设备模板
 */
@Data
@Accessors(chain = true)
public class QueryTemplate {
    private String title;
    private String value;
    private String key;
    private String psrId;
    private String devName;
    private Integer index;
    private String unit;
    private List<QueryTemplate> children;
}
