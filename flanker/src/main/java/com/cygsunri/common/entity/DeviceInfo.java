package com.cygsunri.common.entity;

import lombok.Data;
import lombok.experimental.Accessors;
import org.assertj.core.util.Lists;

import java.util.List;

/**
 * 设备信息状态类
 */
@Accessors(chain = true)
@Data
public class DeviceInfo {
    private String PsrID;
    private String name;
    private String status;
    private String type;
    private List<DeviceInfo> children = Lists.newArrayList();
    private Object extra;
}
