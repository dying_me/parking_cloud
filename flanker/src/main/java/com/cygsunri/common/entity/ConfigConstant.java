package com.cygsunri.common.entity;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * 配置常量类
 */
@Data
@Accessors(chain = true)
@Configuration
@ConfigurationProperties(prefix = "constant")
@PropertySource(name = "configConstant.properties", value = {"classpath:configConstant.properties"}, encoding = "UTF-8")
public class ConfigConstant {

    //储能站access_key_id
    private String essAccess_key_id;

    //储能站access_key_secret
    private String essAccess_key_secret;

    //储能站ID
    private String essSN;

    //储能站获取电站数据接口地址
    private String essGetDeviceDataUrl;

    //储能站获取电站数据曲线接口地址
    private String essGetDeviceChartUrl;

    //储能站获取电站数据曲线（⾃定义时间）接口地址
    private String essGetDeviceChartByTimeUrl;

    //空气传感器
    private String airSensor;

    //安科瑞电表
    private String ankerui;

    //网关
    private String tuyagateway;

    //涂鸦-人体感应传感器
    private String tuyahumanbodydetection;

    //门窗传感器
    private String tuyamcs;

    //4位情景开关
    private String tuyasceneswitch4;

    //6位情景开关
    private String tuyasceneswitch6;

    //智能插座
    private String tuyasmartsocket;

    //温湿度传感器
    private String tuyaTHTB;

    //涂鸦-通断器-zigbee
    private String tuyazigbee;

    //雅达M1电表
    private String yadaM1;

    //雅达M23电表
    private String yadaM23;

    //雅达水表
    private String yadawater;

    //亚禾AC360空调控制器
    private String yaheac360;

    //舒适度计算公式
    private String comfortJexlExp;

    //1L
    private String Obb1;

    //2L
    private String Obb2;

    //3L
    private String Obb3;

    //4L
    private String Obb4;

    //5L
    private String Obb5;

    //6L
    private String Obb6;
}
