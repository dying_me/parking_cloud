package com.cygsunri.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 *  SVG文件类
 * </p>
 */
@Data
@Accessors(chain = true)
@TableName("tb_SVGPath")
public class SVGPath implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.ASSIGN_UUID)
    private String id;

    @TableField("menu")
    private String menu;

    @TableField("path")
    private String path;

    @TableField("account_Id")
    private String accountId;


}
