package com.cygsunri.common.api;

import com.alibaba.fastjson.JSONObject;
import com.cygsunri.common.constant.DeviceType;
import com.cygsunri.common.entity.QueryTemplate;
import com.cygsunri.common.service.CommonService;
import com.cygsunri.common.service.SVGPathService;
import com.cygsunri.scada.base.BasePSR;
import com.cygsunri.scada.service.ScadaPSRService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * 常规的API
 */
@RestController
@RequestMapping("/api")
public class CommonController {

    @Autowired
    private ScadaPSRService scadaPSRService;

    @Autowired
    private CommonService commonService;

    @Autowired
    private SVGPathService svgPathService;

    /**
     * 获得所有类型子站
     */
    @RequestMapping(value = "/substations/users/{user}", method = RequestMethod.GET)
    public ResponseEntity<List<BasePSR>> getSubstations(@PathVariable("user") String user) {
        List<BasePSR> substations = new ArrayList<>();
        List<BasePSR> solar = scadaPSRService.getDevicesByType(DeviceType.SOLAR_SUB.getKey());
        List<BasePSR> ess = scadaPSRService.getDevicesByType(DeviceType.WIND_SUB.getKey());
        List<BasePSR> charge = scadaPSRService.getDevicesByType(DeviceType.CHARGE_SUB.getKey());
        List<BasePSR> consumption = scadaPSRService.getDevicesByType(DeviceType.CONSUMPTION_SUB.getKey());
        List<BasePSR> air = scadaPSRService.getDevicesByType(DeviceType.AIR_SUB.getKey());
        substations.addAll(solar);
        substations.addAll(ess);
        substations.addAll(charge);
        substations.addAll(consumption);
        substations.addAll(air);
        return new ResponseEntity<>(substations, HttpStatus.OK);
    }

    /**
     * 获取一个设备子设备
     */
    @RequestMapping(value = "/devices/children/devices/{psrID}", method = RequestMethod.GET)
    public ResponseEntity getChildrenDevices(@PathVariable("psrID") String psrID) {
        List<BasePSR> basePSRS = scadaPSRService.getChildrenDevice(psrID);
        for (BasePSR basePSR : basePSRS) {
            List<BasePSR> list = scadaPSRService.getChildrenDevice(basePSR.getId());
            basePSR.setExtra(list.isEmpty());//回给前台判断子设备是否有叶子节点
        }
        return new ResponseEntity<>(basePSRS, HttpStatus.OK);
    }

    /**
     * 获取实时、历史查询的设备模板数据
     */
    @RequestMapping(value = "/psr/template/devices/{psrID}", method = RequestMethod.GET)
    public ResponseEntity<List<QueryTemplate>> getHistoryCurveTemplate(@PathVariable("psrID") String psrID) {
        return new ResponseEntity<>(commonService.getHistoryTemplate(psrID), HttpStatus.OK);
    }

    /**
     * 获取svg文件路径
     */
    @RequestMapping(value = "/svg/path/users/{user}/menus/{menu}", method = RequestMethod.GET)
    public ResponseEntity<String> getSVGPath(@PathVariable("user") String user, @PathVariable("menu") String menu) {
        String path = svgPathService.getSVGPath(user, menu);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("path", path);
        return new ResponseEntity<>(jsonObject.toJSONString(), HttpStatus.OK);
    }
}
