package com.cygsunri.common.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cygsunri.common.entity.SVGPath;

/**
 * <p>
 *  服务类
 * </p>
 */
public interface SVGPathService extends IService<SVGPath> {
    /**
     * 获取svg文件路径
     */
    String getSVGPath(String account, String menu);
}
