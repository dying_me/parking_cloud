package com.cygsunri.common.service;

import com.cygsunri.common.entity.QueryTemplate;
import com.cygsunri.measurement.entity.MeasurementInfo;
import com.cygsunri.scada.base.BasePSR;
import com.cygsunri.scada.service.ScadaMeasurementService;
import com.cygsunri.scada.service.ScadaPSRService;
import com.cygsunri.template.entity.psr.DeviceTemplate;
import com.cygsunri.template.entity.psr.DeviceTemplateData;
import com.cygsunri.template.service.DeviceTemplateDataService;
import com.cygsunri.template.service.PSRTemplateMappingService;
import org.assertj.core.util.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 常规服务
 */
@Service
public class CommonService {

    @Autowired
    private ScadaPSRService scadaPSRService;

    @Autowired
    private ScadaMeasurementService scadaMeasurementService;

    @Autowired
    private PSRTemplateMappingService psrTemplateMappingService;

    @Autowired
    private DeviceTemplateDataService deviceTemplateDataService;

    /**
     * 获取实时、历史查询的设备模板数据
     */
    public List<QueryTemplate> getHistoryTemplate(String psrID) {
        List<QueryTemplate> templateList = Lists.newArrayList();
        BasePSR basePSR = scadaPSRService.getDeviceByPsrId(psrID);
        DeviceTemplate deviceTemplate = psrTemplateMappingService.getDeviceTemplateByMapping(psrID);
        List<DeviceTemplateData> deviceTemplateDatas = deviceTemplateDataService.getDeviceTemplateDataListByDeviceTemplateId(deviceTemplate.getId());
        List<DeviceTemplateData> yc = deviceTemplate.getDeviceTemplateByType(deviceTemplateDatas, 2);
        List<DeviceTemplateData> ym = deviceTemplate.getDeviceTemplateByType(deviceTemplateDatas, 3);

        List<QueryTemplate> ycTemplate = getHistoryTemplate(basePSR, yc);
        List<QueryTemplate> ymTemplate = getHistoryTemplate(basePSR, ym);
        templateList.addAll(ycTemplate);
        templateList.addAll(ymTemplate);
        return templateList;
    }

    private List<QueryTemplate> getHistoryTemplate(BasePSR basePSR, List<DeviceTemplateData> list) {
        List<QueryTemplate> templateList = Lists.newArrayList();
        for (DeviceTemplateData data : list) {
            MeasurementInfo info = scadaMeasurementService.getMeasurementInfo(basePSR.getId(), data.getName());
            if (info == null) {
                continue;
            }

            QueryTemplate queryTemplate = new QueryTemplate()
                    .setTitle(data.getComments())
                    .setPsrId(basePSR.getId())
                    .setDevName(basePSR.getName())
                    .setIndex(data.getIndexNumber())
                    .setUnit(data.getUnit());
            if (info.isArray()) {
                List<QueryTemplate> children = Lists.newArrayList();
                int num = 1;
                for (String measurementID : info.getArrayMeasurementID()) {
                    QueryTemplate c = new QueryTemplate().setTitle(data.getComments() + String.format("%03d", num))
                            .setValue(measurementID)
                            .setKey(measurementID);
                    children.add(c);
                    num++;
                }
                queryTemplate.setChildren(children);
            } else {
                queryTemplate.setKey(info.getMeasurementID()).setValue(info.getMeasurementID());
            }
            templateList.add(queryTemplate);
        }
        return templateList;
    }
}
