package com.cygsunri.common.service;

import com.cygsunri.consumption.entity.Branch;
import com.cygsunri.history.service.GeneralDataService;
import com.cygsunri.measurement.entity.Flag;
import com.cygsunri.measurement.entity.MeasurementValue;
import com.cygsunri.scada.base.BasePSR;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 常规数据接口
 */
@Service
public class CommonDataService extends GeneralDataService {

    /**
     * 根据PsrID和name获取实时数据
     */
    public MeasurementValue getValue(String PsrID, String name) {
        BasePSR basePSR = new BasePSR().setId(PsrID);
        MeasurementValue m = new MeasurementValue().setO(basePSR).setName(name).setRdb(true);
        return super.getValue(m);
    }

    /**
     * 根据PsrID和name获取实时数据，并填充显示模板
     */
    public MeasurementValue getValueFillDisPlay(String PsrID, String name) {
        BasePSR basePSR = new BasePSR().setId(PsrID);
        MeasurementValue m = new MeasurementValue().setO(basePSR).setName(name).setRdb(true);
        return super.getValueFillDisPlay(m);
    }

    /**
     * 根据测点代码获取实时数据
     */
    public MeasurementValue getValue(String key) {
        MeasurementValue m = new MeasurementValue().setKey(key).setRdb(true);
        return super.getValue(m);
    }

    /**
     * 根据PsrID、name、flag获取统计数据
     */
    public MeasurementValue getValue(String PsrID, String name, Long time, Integer flag) {
        BasePSR basePSR = new BasePSR().setId(PsrID);
        MeasurementValue m = new MeasurementValue().setO(basePSR).setName(name).setRdb(false).setFlag(Flag.getFlag(flag)).setTimestamp(time);
        return super.getValue(m);
    }

    /**
     * 根据测点代码、flag获取统计数据
     */
    public MeasurementValue getValue(String key, Long time, Integer flag) {
        MeasurementValue m = new MeasurementValue().setKey(key).setRdb(false).setFlag(Flag.getFlag(flag)).setTimestamp(time);
        return super.getValue(m);
    }

    /**
     * 根据PsrID、name获取历史数据
     */
    public List<Double> getHistoryValueToDouble(String PsrID, String name, final String from, final String to, final int step, final int flag) {
        List<Double> values = new ArrayList<>();
        List<MeasurementValue> measurementValues = super.getHistoryValue(PsrID, name, from, to, step, flag, false);
        for (MeasurementValue measurementValue : measurementValues) {
            values.add(measurementValue.isInValid() ? Double.NaN : measurementValue.getData());
        }
        return values;
    }

    /**
     * 根据测点代码获取历史数据
     */
    public List<Double> getHistoryValueToDouble(final String id, final String from, final String to, final int step, final int flag) {
        List<Double> values = new ArrayList<>();
        List<MeasurementValue> measurementValues = super.getHistoryValue(id, from, to, step, flag, false);
        for (MeasurementValue measurementValue : measurementValues) {
            values.add(measurementValue.isInValid() ? Double.NaN : measurementValue.getData());
        }
        return values;
    }

    /**
     * 获取支路合计数据
     */
    public Double getSumOfBranches(List<Branch> branches, String dataName) {
        Double lightingV = 0d;
        for (Branch branch : branches) {
            MeasurementValue m = getValue(branch.getPsrID(), dataName);
            if (!m.isInValid()) {
                lightingV += m.getData();
            }
        }
        return lightingV;
    }
}
