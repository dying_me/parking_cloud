package com.cygsunri.common.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cygsunri.common.entity.SVGPath;
import com.cygsunri.common.mapper.SVGPathMapper;
import com.cygsunri.common.service.SVGPathService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 */
@Service
public class SVGPathServiceImpl extends ServiceImpl<SVGPathMapper, SVGPath> implements SVGPathService {

    @Autowired
    private SVGPathMapper svgPathMapper;

    @Override
    public String getSVGPath(String account, String menu) {
        return svgPathMapper.getSVGPath(account, menu);
    }
}
