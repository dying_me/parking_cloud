package com.cygsunri.common.constant;

/**
 * 公共常量类（非业务关联）
 */
public class CommonConstant {
    public final static String DATEFORMAT_24HOURS = "yyyy-MM-dd HH:mm:ss";
    public final static String DATEFORMAT_ONLYDATE = "yyyy-MM-dd";
    public static final String USER_KEY_CACHE_PREFIX = "user:key:";
    public static final String APP_SECRET = "&f0d8cb7dbdcc40efa5a5bdbe5c8b4c21";
}
