package com.cygsunri.common.constant;

import lombok.Getter;

import java.util.Arrays;

/**
 * 设备类型枚举
 */
@Getter
public enum DeviceType {

    BREAK(1, "BREAK", "开关"),
    INVERTER(27, "INVERTER", "逆变器"),
    WEATHER_INC(29, "WEATHER_INC", "环境检测仪"),
    ESS_AIR_METER(33, "ESS_AIR_METER", "储能仓空调单相电表"),
    ESS_METER(34, "ESS_METER", "储能仓三相电表"),
    GATEWAY(35, "GATEWAY", "并网关口表"),
    AIR_OUTSIDE(36, "AIR_OUTSIDE", "空调外机"),
    AIR_INSIDE(37, "AIR_INSIDE", "空调内机"),
    AC_CHARGING(40, "AC_CHARGING", "交流充电桩"),
    DC_CHARGING(41, "DC_CHARGING", "直流充电桩"),
    SOLAR_SUB(52, "SOLAR_SUB", "光伏子站"),
    ESS_SUB(53, "ESS_SUB", "储能子站"),
    CHARGE_SUB(54, "CHARGE_SUB", "充电桩子站"),
    CONSUMPTION_SUB(55, "CONSUMPTION_SUB", "能耗子站"),
    AIR_SUB(56, "AIR_SUB", "空调子站"),
    WIND_SUB(57, "WIND_SUB", "风电子站"),
    CONSUMPTION_ELECTRICITY(71, "CONSUMPTION_ELECTRICITY", "能耗电表"),
    CONSUMPTION_WATER(72, "CONSUMPTION_WATER", "能耗水表"),
    CONSUMPTION_PRESSURE(73, "CONSUMPTION_PRESSURE", "能耗压力表"),
    CONSUMPTION_AIR(74, "CONSUMPTION_AIR", "能耗空调"),
    BATTERY(103, "BATTERY", "电池组"),
    BATTERY_CLUSTER(104, "BATTERY_CLUSTER", "电池簇"),
    AIR_CONDITION(105, "AIR_CONDITION", "空调"),
    ENTRANCE_GUARD(106, "ENTRANCE_GUARD", "门禁"),
    FIRE_CONTROL(107, "FIRE_CONTROL", "消防"),
    PCS(108, "PCS", "PCS"),
    LIQUID_LEAKAGE(109, "LIQUID_LEAKAGE", "水浸"),

    UN_KNOW(10001, "UN_KNOW", "未知");

    private int key;

    private String name;

    private String desc;

    DeviceType(int key, String name, String desc) {
        this.key = key;
        this.name = name;
        this.desc = desc;
    }

    public static DeviceType find(final int key) {
        return Arrays.stream(DeviceType.values())
                .filter(e -> e.key == key)
                .findFirst()
                .orElse(UN_KNOW);
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
