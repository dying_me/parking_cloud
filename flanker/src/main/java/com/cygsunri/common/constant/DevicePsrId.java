package com.cygsunri.common.constant;

/**
 * 招商局用固定设备代码
 */
public class DevicePsrId {
    /**
     * 风电子站
     */
    public static final String WIND = "Swind";
    /**
     * 光伏子站
     */
    public static final String SOLAR = "Ssolar";
    /**
     * 储能子站
     */
    public static final String ESS = "Sess";
    /**
     * 空调子站
     */
    public static final String AIR = "Sair";
    /**
     * 充电桩子站
     */
    public static final String CHARGE = "Scharge";
    /**
     * 能耗子站
     */
    public static final String CONSUMPTION = "Sconsumption";
}
