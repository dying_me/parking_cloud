package com.cygsunri.common.constant;

/**
 * 数据名称常量
 */
public class DataName {

    public static final String OUT_POWER = "OutPower";//逆变器、PCS ->输出有功功率
    public static final String DAY_GENERATED = "DayGenerated";//光伏子站、逆变器、风电子站  ->日发电量
    public static final String MONTH_GENERATED = "MonthGenerated";//光伏子站、逆变器、风电子站  ->月发电量
    public static final String YEAR_GENERATED = "YearGenerated";//光伏子站、逆变器、风电子站  ->年发电量
    public static final String TOTAL_GENERATED = "TotalGenerated";//光伏子站、逆变器、风电子站  ->总发电量
    public static final String TOTAL_ACTIVE_POWER = "TotalActivePower";//光伏子站、储能子站、风电子站 ->总有功功率
    public static final String ECONOMIC_BENEFIT = "EconomicBenefit";//光伏子站、储能子站、风电子站 ->经济效益
    public static final String INVERTER_STATUS = "InverterStatus";//逆变器 ->运行状态
    public static final String RUNNING = "Running";//逆变器 ->运行中
    public static final String STANDBY = "Standby";//逆变器 ->待机
    public static final String STOP = "Stop";//逆变器 ->停机
    public static final String FAULT = "Fault";//逆变器 ->故障

    public static final String SOC = "SOC";//储能子站、电池组、电池簇 ->SOC
    public static final String SOH = "SOH";//储能子站、电池组、电池簇 ->SOH
    public static final String DAY_CHARGE = "DayCharge";//储能子站、PCS、充电桩子站 ->日充电量
    public static final String DAY_DISCHARGE = "DayDischarge";//储能子站、PCS ->日放电量
    public static final String MONTH_CHARGE = "MonthCharge";//充电桩子站 ->月充电量
    public static final String YEAR_CHARGE = "YearCharge";//储能子站、充电桩子站 ->年充电量
    public static final String YEAR_DISCHARGE = "YearDischarge";//储能子站、充电桩子站 ->年放电量
    public static final String TOTAL_CHARGE = "TotalCharge";//储能子站、PCS、充电桩子站 ->总充电量
    public static final String TOTAL_DISCHARGE = "TotalDischarge";//储能子站、PCS ->总放电量
    public static final String EFFICIENCY = "Efficiency";//光伏、储能子站、PCS、风电 ->效率
    public static final String STATUS = "Status";//设备状态

    public static final String GUN1_OUT_POWER = "Gun1OutPower";//充电桩枪1 ->有功功率
    public static final String GUN2_OUT_POWER = "Gun2OutPower";//充电桩枪2 ->有功功率
    public static final String GUN1_DAY_CHARGE = "Gun1DayCharge";//充电桩枪1 ->日充电量
    public static final String GUN2_DAY_CHARGE = "Gun2DayCharge";//充电桩枪2 ->日充电量
    public static final String GUN1_TOTAL_CHARGE = "Gun1TotalCharge";//充电桩枪1 ->总充电量
    public static final String GUN2_TOTAL_CHARGE = "Gun2TotalCharge";//充电桩枪2 ->总充电量
    public static final String GUN1_MONEY = "Gun1Money";//充电桩枪1 ->充电金额
    public static final String GUN2_MONEY = "Gun2Money";//充电桩枪2 ->充电金额
    public static final String GUN1_HOUR = "Gun1Hour";//充电桩枪1 ->已充小时
    public static final String GUN2_HOUR = "Gun2Hour";//充电桩枪2 ->已充小时
    public static final String GUN1_MINUTE = "Gun1Minute";//充电桩枪1 ->已充小时
    public static final String GUN2_MINUTE = "Gun2Minute";//充电桩枪2 ->已充小时
    public static final String GUN1_CHARGE = "Gun1Charge";//充电桩枪1 ->已充电量
    public static final String GUN2_CHARGE = "Gun2Charge";//充电桩枪2 ->已充电量
    public static final String GUN1_STATUS = "Gun1Status";//充电桩枪1使用状态
    public static final String GUN2_STATUS = "Gun2Status";//充电桩枪2使用状态
    public static final String UTILIZATION = "Utilization";//充电桩整体使用率

    //环境检测仪
    public static final String TOTAL_RADIATION = "TotalRadiation"; //总辐射瞬时值
    public static final String ACCUMULATE_RADIATION = "AccumulateRadiation";//总辐射日累计值

    //能耗
    public static final String ELECTRICITY = "Electricity"; //用电量
    public static final String YEAR_ELECTRICITY = "YearElectricity"; //年用电量
    public static final String REVERSE_ELECTRICITY = "ReverseElectricity"; //反向用电量
    public static final String WATER = "Water";//用水量
    public static final String YEAR_WATER = "YearWater"; //年用水量
    public static final String TEMPERATURE = "Temperature";//温度

    public static final String COMMUNICATION_FAULT = "CommunicationFault";//通讯故障

}
