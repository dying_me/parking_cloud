package com.cygsunri.common.exception;

/**
 * <p>
 * 业务异常
 * </p>
 *
 * @author cygsunri
 * @since 2021/7/01
 */
public class BusinessException extends RuntimeException {
    private static final long serialVersionUID = 2631058374866368737L;

    private int code;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public BusinessException() {
    }

    public BusinessException(String message) {
        super(message);
    }

    public BusinessException(String message, Throwable cause) {
        super(message, cause);
    }

    public BusinessException(Throwable cause) {
        super(cause);
    }

    /**
     * 业务异常
     *
     * @param code    异常码
     * @param message 异常信息
     */
    public BusinessException(int code, String message) {
        super(message);
        this.code = code;
    }

}
