package com.cygsunri;

import com.cygsunri.event.websocket.service.WebSocketServer;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableDiscoveryClient
@EnableScheduling
@EnableCaching
@MapperScan({"com.cygsunri.*.mapper","com.cygsunri.*.*.mapper"})
public class FlankerApplication {

	public static void main(String[] args) {
		ConfigurableApplicationContext configurableApplicationContext = SpringApplication.run(FlankerApplication.class, args);
		//解决WebSocket不能注入的问题
		WebSocketServer.setApplicationContext(configurableApplicationContext);
	}

}
