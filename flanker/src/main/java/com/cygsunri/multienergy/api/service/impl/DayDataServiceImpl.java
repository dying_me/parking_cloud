package com.cygsunri.multienergy.api.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cygsunri.multienergy.api.entity.DayData;
import com.cygsunri.multienergy.api.mapper.DayDataMapper;
import com.cygsunri.multienergy.api.service.DayDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hanyuan
 * @since 2021-07-30
 */
@Service
public class DayDataServiceImpl extends ServiceImpl<DayDataMapper, DayData> implements DayDataService {

    @Autowired
    private DayDataMapper dayDataMapper;

    /**
     * 通过时间和名称和相差天数查找值
     * @param begintime 开始时间
     * @param endtime  结束时间
     * @param name 名称
     * @return DeviceTemplateData
     */
    @Override
    public List<DayData> finDiffDayDataByTimeAndName(String begintime, String endtime, String name) {
        return dayDataMapper.finDiffDayDataByTimeAndName(begintime,endtime,name);
    }
    /**
     * 通过时间和名称查找值
     *
     * @param time  时间
     * @param name 名称
     * @return DeviceTemplateData
     */
    @Override
    public List<DayData> finDayDataByTimeAndName(String time, String name) {
        LambdaQueryWrapper<DayData> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(DayData::getTime,time);
        wrapper.eq(DayData::getName,name);
        return dayDataMapper.selectList(wrapper);
    }
}
