package com.cygsunri.multienergy.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.cygsunri.util.DateUtil;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * modbus寄存器信息表
 * </p>
 */
@Data
@Accessors(chain = true)
@TableName("tb_daydata")
public class DayData implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键,UUID
     */
    @TableId(value = "ID", type = IdType.ASSIGN_UUID)
    private String id;

    /**
     * 设备id
     */
    @TableField("psrID")
    private String psrID;

    /**
     * 点位
     */
    @TableField("name")
    private String name;

    /**
     * 值
     */
    @TableField("data")
    private Double data;

    /**
     * 日期
     */
    @TableField("time")
    private String time;

    public DayData(){

    }

    public DayData(String psrID,double data,String name){
        this.psrID = psrID;
        this.data = data;
        this.name = name;
        this.time = DateUtil.today();
    }
}
