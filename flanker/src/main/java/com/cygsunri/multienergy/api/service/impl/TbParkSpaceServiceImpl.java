package com.cygsunri.multienergy.api.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cygsunri.multienergy.api.entity.TbParkSpace;
import com.cygsunri.multienergy.api.mapper.TbParkSpaceMapper;
import com.cygsunri.multienergy.api.service.TbParkSpaceService;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 车场车位数信息表 服务实现类
 * </p>
 *
 * @author hanyuan
 * @since 2021-08-30
 */
@Service
public class TbParkSpaceServiceImpl extends ServiceImpl<TbParkSpaceMapper, TbParkSpace> implements TbParkSpaceService {

    /**
     * 根据车场唯一标识查询车场车位数信息
     * @param park_key 车场唯一标识
     * @return TbParkSpace
     */
    @Override
    public TbParkSpace findTbParkSpaceByParkKey(String park_key) {
        LambdaQueryWrapper<TbParkSpace> queryWrapper = new LambdaQueryWrapper<>();
        if(StringUtils.isNotEmpty(park_key)){
            queryWrapper.eq(TbParkSpace::getParkKey,park_key);
        }
        return this.getOne(queryWrapper);
    }
}
