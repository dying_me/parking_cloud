package com.cygsunri.multienergy.api;

import com.cygsunri.information.entity.Information;
import com.cygsunri.information.service.InformationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * 站点信息apiV2
 * */
@RestController
@RequestMapping("/api/v2/information")
public class InformationV2Controller {

    @Autowired
    private InformationService informationService;

    @GetMapping(value = "/get/substations")
    public ResponseEntity<List<Information>> getInformationByPsrID(@RequestParam("user") String user, @RequestParam("type") String type) {

        List<Information> substations = new ArrayList<>();

        if (type.equals("all") || type.equals("solar")) {
            List<Information> solar = informationService.getInformationByType("solar");
            substations.addAll(solar);
        }

        if (type.equals("all") || type.equals("ess")) {
            List<Information> ess = informationService.getInformationByType("ess");
            substations.addAll(ess);
        }

        if (type.equals("all") || type.equals("charge")) {
            List<Information> charge = informationService.getInformationByType("charge");
            substations.addAll(charge);
        }

        if (type.equals("all") || type.equals("consumption")) {
            List<Information> consumption = informationService.getInformationByType("consumption");
            substations.addAll(consumption);
        }

        if (type.equals("all") || type.equals("air")) {
            List<Information> air = informationService.getInformationByType("air");
            substations.addAll(air);
        }

        if (type.equals("all") || type.equals("wind")) {
            List<Information> wind = informationService.getInformationByType("wind");
            substations.addAll(wind);
        }

        return new ResponseEntity<>(substations, HttpStatus.OK);
    }


}
