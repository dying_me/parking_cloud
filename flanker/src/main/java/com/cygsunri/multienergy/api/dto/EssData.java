package com.cygsunri.multienergy.api.dto;

import lombok.Data;

@Data
public class EssData {

    private Double power; //当前功率
    private Double ratedCapacity; //额定容量
    private Double dailyCharge; //日充电量
    private Double dailyDischarge; //日放电量
    private Double soc; //soc

}
