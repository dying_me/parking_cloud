package com.cygsunri.multienergy.api.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cygsunri.multienergy.api.entity.TbParkSpace;

/**
 * <p>
 * 车场车位数信息表 Mapper 接口
 * </p>
 *
 * @author hanyuan
 * @since 2021-08-30
 */
public interface TbParkSpaceMapper extends BaseMapper<TbParkSpace> {

}
