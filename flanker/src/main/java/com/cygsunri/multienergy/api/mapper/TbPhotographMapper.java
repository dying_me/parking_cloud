package com.cygsunri.multienergy.api.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cygsunri.multienergy.api.entity.TbPhotograph;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 考勤抓拍信息表 Mapper 接口
 * </p>
 *
 * @author hanyuan
 * @since 2021-08-23
 */
public interface TbPhotographMapper extends BaseMapper<TbPhotograph> {
    /**
     * 根据时间和类型查询进场人数
     * @param time
     * @param person_attr
     * @return DeviceTemplateData
     */
    Integer findCountByTime(@Param("time")String time, @Param("person_attr")String person_attr);
}
