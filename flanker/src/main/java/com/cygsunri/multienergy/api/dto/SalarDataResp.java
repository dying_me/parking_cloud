package com.cygsunri.multienergy.api.dto;

import lombok.Data;

@Data
public class SalarDataResp {

    private SalarData infoData;
    private HisDataList option;

    public SalarDataResp () {
        infoData = new SalarData();
        option = new HisDataList();
    }
}
