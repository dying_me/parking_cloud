package com.cygsunri.multienergy.api.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cygsunri.multienergy.api.entity.DayData;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hanyuan
 * @since 2021-07-30
 */
public interface DayDataService extends IService<DayData> {

    /**
     * 通过时间和名称和相差天数查找值
     * @param begintime 开始时间
     * @param endtime  结束时间
     * @param name 名称
     * @return DeviceTemplateData
     */
    List<DayData> finDiffDayDataByTimeAndName(String begintime, String endtime, String name);

    /**
     * 通过时间和名称查找值
     *
     * @param time  时间
     * @param name 名称
     * @return DeviceTemplateData
     */
    List<DayData> finDayDataByTimeAndName(String time, String name);

}
