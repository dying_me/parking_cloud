package com.cygsunri.multienergy.api.dto;

import lombok.Data;

@Data
public class SalarData {

    private Double power; //当前发电功率
    private Double installedCapacity; //装机容量
    private Double dailyPowerGeneration; //日发电量
    private Double totalRadiation; //总辐射
    private Double totalPowerGeneration; //总发电量

}
