package com.cygsunri.multienergy.api.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cygsunri.multienergy.api.entity.TbPhotograph;

/**
 * <p>
 * 考勤抓拍信息表 服务类
 * </p>
 *
 * @author hanyuan
 * @since 2021-08-23
 */
public interface TbPhotographService extends IService<TbPhotograph> {

    /**
     * 根据时间和类型查询进场人数
     * @param time
     * @param person_attr
     * @return DeviceTemplateData
     */
    Integer findCountByTime(String time,String person_attr);
}
