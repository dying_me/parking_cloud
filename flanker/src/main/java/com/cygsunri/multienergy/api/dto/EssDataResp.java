package com.cygsunri.multienergy.api.dto;

import lombok.Data;

@Data
public class EssDataResp {

    private EssData infoData;
    private HisDataList option;

    public EssDataResp () {
        infoData = new EssData();
        option = new HisDataList();
    }
}
