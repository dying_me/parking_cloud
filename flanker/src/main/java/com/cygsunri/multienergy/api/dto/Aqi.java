package com.cygsunri.multienergy.api.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName: Aqi
 * @Description: TODO
 * @author jiangya
 * @date 2019年8月14日 下午4:37:25
 *
 */
public class Aqi implements Serializable {
    private static final long serialVersionUID = 1L;
    private String name;

    private Double aqi;

    public Aqi(String name, Double aqi) {
        super();
        this.name = name;
        this.aqi = aqi;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getAqi() {
        return aqi;
    }

    public void setAqi(Double aqi) {
        this.aqi = aqi;
    }

}
