package com.cygsunri.multienergy.api.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class HisDataList {

    List<HisData> records;
    List<String> timeRange;

    public HisDataList () {
        records = new ArrayList<>();
        timeRange = new ArrayList<>();
    }
}
