package com.cygsunri.multienergy.api.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cygsunri.multienergy.api.entity.TbPhotograph;
import com.cygsunri.multienergy.api.mapper.DayDataMapper;
import com.cygsunri.multienergy.api.mapper.TbPhotographMapper;
import com.cygsunri.multienergy.api.service.TbPhotographService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * <p>
 * 考勤抓拍信息表 服务实现类
 * </p>
 *
 * @author hanyuan
 * @since 2021-08-23
 */
@Slf4j
@Service
public class TbPhotographServiceImpl extends ServiceImpl<TbPhotographMapper, TbPhotograph> implements TbPhotographService {

    @Autowired
    private TbPhotographMapper tbPhotographMapper;

    /**
     * 根据时间和类型查询进场人数
     * @param time
     * @param person_attr
     * @return DeviceTemplateData
     */
    @Override
    public Integer findCountByTime(String time, String person_attr) {
        return tbPhotographMapper.findCountByTime(time,person_attr);
    }
}
