package com.cygsunri.multienergy.api.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cygsunri.multienergy.api.entity.TbParkSpace;

/**
 * <p>
 * 车场车位数信息表 服务类
 * </p>
 *
 * @author hanyuan
 * @since 2021-08-30
 */
public interface TbParkSpaceService extends IService<TbParkSpace> {

    /**
     * 根据车场唯一标识查询车场车位数信息
     * @param park_key 车场唯一标识
     * @return TbParkSpace
     */
    TbParkSpace findTbParkSpaceByParkKey(String park_key);

}
