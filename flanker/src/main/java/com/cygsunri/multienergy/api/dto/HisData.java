package com.cygsunri.multienergy.api.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class HisData {

    String name;
    String unit;
    List<String> data;

    public HisData() {
        data = new ArrayList<>();
    }
}
