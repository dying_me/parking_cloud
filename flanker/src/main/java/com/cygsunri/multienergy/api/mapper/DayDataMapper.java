package com.cygsunri.multienergy.api.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cygsunri.multienergy.api.entity.DayData;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author
 * @since 2020-12-21
 */
public interface DayDataMapper extends BaseMapper<DayData> {

    /**
     * 通过时间和名称和相差天数查找值
     *
     * @param endtime  结束时间
     * @param name 名称
     * @param begintime 开始时间
     * @return DeviceTemplateData
     */
    List<DayData> finDiffDayDataByTimeAndName(@Param("begintime")String begintime,@Param("endtime")String endtime, @Param("name")String name );


}
