package com.cygsunri.income.api;

import com.cygsunri.income.service.IncomeService;
import com.cygsunri.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 手动触发收益缓存api
 */
@RestController
@RequestMapping("/api/income")
public class IncomeController {

    @Autowired
    private IncomeService incomeService;

    /**
     * 统计某一天
     * @param date yyyy-mm-dd
     */
    @RequestMapping(value = "/cache/{date}", method = RequestMethod.GET)
    public ResponseEntity makeCache(@PathVariable("date") String date) {
        incomeService.updateCache(date);
        return new ResponseEntity(HttpStatus.OK);
    }

    /**
     * 统计一段时间
     * @param start 开始时间 yyyy-mm-dd
     * @param end 结束时间 yyyy-mm-dd
     */
    @RequestMapping(value = "/cache/days/{start}/{end}", method = RequestMethod.GET)
    public ResponseEntity makeCacheDays(@PathVariable("start") String start, @PathVariable("end") String end) {
        List<String> days = DateUtil.getDays(start, end);
        for (String day : days) {
            incomeService.updateCache(day);
        }
        return new ResponseEntity(HttpStatus.OK);
    }

    /**
     * 统计指定策略
     * @param date yyyy-mm-dd
     * @param id 策略id
     */
    @RequestMapping(value = "/cache/mapping/{date}/{id}", method = RequestMethod.GET)
    public ResponseEntity makeCache(@PathVariable("date") String date, @PathVariable("id") String id) {
        incomeService.updateCache(date, id);
        return new ResponseEntity(HttpStatus.OK);
    }
}
