package com.cygsunri.income.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cygsunri.income.entity.IncomePlanMapping;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 */
public interface IncomePlanMappingMapper extends BaseMapper<IncomePlanMapping> {

    List<IncomePlanMapping> getIncomePlanMappingList(@Param("date") String date);
}
