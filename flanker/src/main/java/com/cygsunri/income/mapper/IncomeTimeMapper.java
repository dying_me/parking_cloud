package com.cygsunri.income.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cygsunri.income.entity.IncomeTime;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 */
public interface IncomeTimeMapper extends BaseMapper<IncomeTime> {

    List<IncomeTime> getIncomeTimeListByIncomePlanId(@Param("incomePlanId") String incomePlanId);
}
