package com.cygsunri.income.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cygsunri.income.entity.IncomePlan;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * Mapper 接口
 * </p>
 */
public interface IncomePlanMapper extends BaseMapper<IncomePlan> {

    IncomePlan getIncomePlan(@Param("name") String name);
}
