package com.cygsunri.income.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cygsunri.income.entity.IncomeCache;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * Mapper 接口
 * </p>
 */
public interface IncomeCacheMapper extends BaseMapper<IncomeCache> {

    Double getDaySum(@Param("psrID") String psrID, @Param("name") String name,
                     @Param("date") String date, @Param("type") Integer type, @Param("planName") String planName);

    Double getMonthSum(@Param("psrID") String psrID, @Param("name") String name,
                       @Param("from") String from, @Param("to") String to,
                       @Param("type") Integer type, @Param("planName") String planName);

    IncomeCache getIntervalIncomeCache(@Param("psrID") String psrID, @Param("name") String name,
                                       @Param("date") String date, @Param("time") String time,
                                       @Param("type") Integer type, @Param("planName") String planName);

    IncomeCache getDayIncomeCache(@Param("psrID") String psrID, @Param("name") String name,
                                  @Param("date") String date, @Param("type") Integer type,
                                  @Param("planName") String planName);

    IncomeCache getMonthIncomeCache(@Param("psrID") String psrID, @Param("name") String name,
                                    @Param("from") String from, @Param("to") String to,
                                    @Param("type") Integer type, @Param("planName") String planName);

    Double getIncomeSum(@Param("psrID") String psrID, @Param("name") String name,
                        @Param("type") Integer type, @Param("planName") String planName);
}
