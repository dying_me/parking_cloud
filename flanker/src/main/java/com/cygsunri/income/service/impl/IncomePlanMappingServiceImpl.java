package com.cygsunri.income.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cygsunri.income.entity.IncomePlanMapping;
import com.cygsunri.income.mapper.IncomePlanMappingMapper;
import com.cygsunri.income.service.IncomePlanMappingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 */
@Service
public class IncomePlanMappingServiceImpl extends ServiceImpl<IncomePlanMappingMapper, IncomePlanMapping> implements IncomePlanMappingService {

    @Autowired
    private IncomePlanMappingMapper incomePlanMappingMapper;

    @Override
    public List<IncomePlanMapping> getIncomePlanMappingList() {
        QueryWrapper<IncomePlanMapping> queryWrapper = new QueryWrapper<>();
        return incomePlanMappingMapper.selectList(queryWrapper);
    }

    @Override
    public List<IncomePlanMapping> getIncomePlanMappingList(String date) {
        return incomePlanMappingMapper.getIncomePlanMappingList(date);
    }

    @Override
    public IncomePlanMapping getIncomePlanMapping(String id) {
        QueryWrapper<IncomePlanMapping> queryWrapper = new QueryWrapper<IncomePlanMapping>()
                .eq("id", id);
        return incomePlanMappingMapper.selectOne(queryWrapper);
    }

    @Override
    public List<IncomePlanMapping> getIncomePlanMappingListByIncomePlanId(String incomePlanId) {
        QueryWrapper<IncomePlanMapping> queryWrapper = new QueryWrapper<IncomePlanMapping>()
                .eq("incomePlan_id", incomePlanId);
        return incomePlanMappingMapper.selectList(queryWrapper);
    }
}
