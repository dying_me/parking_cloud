package com.cygsunri.income.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cygsunri.income.entity.IncomePlanMapping;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 */
public interface IncomePlanMappingService extends IService<IncomePlanMapping> {

    /**
     * 获取所有策略映射
     */
    List<IncomePlanMapping> getIncomePlanMappingList();

    /**
     * 获取符合时间范围的策略映射
     */
    List<IncomePlanMapping> getIncomePlanMappingList(String date);

    /**
     * 获取指定id的策略映射
     */
    IncomePlanMapping getIncomePlanMapping(String id);

    /**
     * 获取所有策略映射
     */
    List<IncomePlanMapping> getIncomePlanMappingListByIncomePlanId(String incomePlanId);
}
