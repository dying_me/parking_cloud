package com.cygsunri.income.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cygsunri.income.entity.IncomePlan;
import com.cygsunri.income.mapper.IncomePlanMapper;
import com.cygsunri.income.service.IncomePlanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 */
@Service
public class IncomePlanServiceImpl extends ServiceImpl<IncomePlanMapper, IncomePlan> implements IncomePlanService {

    @Autowired
    private IncomePlanMapper incomePlanMapper;

    @Override
    public IncomePlan getIncomePlan(String name) {
        return incomePlanMapper.getIncomePlan(name);
    }

    @Override
    public IncomePlan getIncomePlanByID(String id) {
        QueryWrapper<IncomePlan> queryWrapper = new QueryWrapper<IncomePlan>()
                .eq("id", id);
        return incomePlanMapper.selectOne(queryWrapper);
    }
}
