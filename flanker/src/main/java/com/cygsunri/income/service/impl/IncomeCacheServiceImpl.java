package com.cygsunri.income.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cygsunri.income.entity.IncomeCache;
import com.cygsunri.income.mapper.IncomeCacheMapper;
import com.cygsunri.income.service.IncomeCacheService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 *  服务实现类
 * </p>
 */
@Service
public class IncomeCacheServiceImpl extends ServiceImpl<IncomeCacheMapper, IncomeCache> implements IncomeCacheService {

    @Autowired
    private IncomeCacheMapper incomeCacheMapper;

    @Override
    @Transactional
    public void saveIncomeCache(IncomeCache incomeCache) {
        incomeCacheMapper.insert(incomeCache);
    }

    @Override
    public Double getDaySum(String psrID, String name, String date, Integer type, String planName) {
        return incomeCacheMapper.getDaySum(psrID, name, date, type, planName);
    }

    @Override
    public Double getMonthSum(String psrID, String name, String from, String to, Integer type, String planName) {
        return incomeCacheMapper.getMonthSum(psrID, name, from, to, type, planName);
    }

    @Override
    public IncomeCache getIntervalIncomeCache(String psrID, String name, String date, String time, Integer type, String planName) {
        return incomeCacheMapper.getIntervalIncomeCache(psrID, name, date, time, type, planName);
    }

    @Override
    public IncomeCache getDayIncomeCache(String psrID, String name, String date, Integer type, String planName) {
        return incomeCacheMapper.getDayIncomeCache(psrID, name, date, type, planName);
    }

    @Override
    public IncomeCache getMonthIncomeCache(String psrID, String name, String from, String to, Integer type, String planName) {
        return incomeCacheMapper.getMonthIncomeCache(psrID, name, from, to, type, planName);
    }

    @Override
    public Double getIncomeSum(String psrID, String name, Integer type, String planName) {
        return incomeCacheMapper.getIncomeSum(psrID, name, type, planName);
    }
}
