package com.cygsunri.income.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cygsunri.income.entity.IncomeTime;
import com.cygsunri.income.mapper.IncomeTimeMapper;
import com.cygsunri.income.service.IncomeTimeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 */
@Service
public class IncomeTimeServiceImpl extends ServiceImpl<IncomeTimeMapper, IncomeTime> implements IncomeTimeService {

    @Autowired
    private IncomeTimeMapper incomeTimeMapper;

    @Override
    public List<IncomeTime> getIncomeTimeListByIncomePlanId(String incomePlanId) {
        return incomeTimeMapper.getIncomeTimeListByIncomePlanId(incomePlanId);
    }
}
