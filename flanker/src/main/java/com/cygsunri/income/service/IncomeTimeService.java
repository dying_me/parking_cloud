package com.cygsunri.income.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cygsunri.income.entity.IncomeTime;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 */
public interface IncomeTimeService extends IService<IncomeTime> {

    List<IncomeTime> getIncomeTimeListByIncomePlanId(String incomePlanId);
}
