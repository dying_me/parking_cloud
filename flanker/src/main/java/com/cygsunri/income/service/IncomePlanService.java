package com.cygsunri.income.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cygsunri.income.entity.IncomePlan;

/**
 * <p>
 *  服务类
 * </p>
 */
public interface IncomePlanService extends IService<IncomePlan> {

    /**
     * 获取指定name的策略
     */
    IncomePlan getIncomePlan(String name);

    /**
     * 获取指定id的策略
     */
    IncomePlan getIncomePlanByID(String id);
}
