package com.cygsunri.income.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cygsunri.income.entity.IncomeCache;

/**
 * <p>
 * 服务类
 * </p>
 */
public interface IncomeCacheService extends IService<IncomeCache> {

    /**
     * 保存缓存
     */
    void saveIncomeCache(IncomeCache incomeCache);

    /**
     * 获取日缓存合计
     */
    Double getDaySum(String psrID, String name, String date, Integer type, String planName);

    /**
     * 获取月缓存合计
     */
    Double getMonthSum(String psrID, String name, String from, String to, Integer type, String planName);

    /**
     * 获取分时缓存
     */
    IncomeCache getIntervalIncomeCache(String psrID, String name, String date, String time, Integer type, String planName);

    /**
     * 获取日缓存
     */
    IncomeCache getDayIncomeCache(String psrID, String name, String date, Integer type, String planName);

    /**
     * 获取月缓存
     */
    IncomeCache getMonthIncomeCache(String psrID, String name, String from, String to, Integer type, String planName);

    /**
     * 获取所有缓存合计
     */
    Double getIncomeSum(String psrID, String name, Integer type, String planName);
}
