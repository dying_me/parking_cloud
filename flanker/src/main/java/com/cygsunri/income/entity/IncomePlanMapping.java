package com.cygsunri.income.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 收益策略与设备映射
 * </p>
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@TableName("tb_incomePlanMapping")
public class IncomePlanMapping implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.ASSIGN_UUID)
    private String id;

    @TableField("PsrID")
    private String psrID; //设备代码

    @TableField("dataName")
    private String dataName; //测点名

    @TableField("dateEnd")
    private String dateEnd; //yyyy-MM-dd

    @TableField("dateFrom")
    private String dateFrom; //yyyy-MM-dd

    @TableField("type")
    private String type;

    @TableField("incomePlan_id")
    private String incomePlan;
}
