package com.cygsunri.income.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 收益结果缓存
 * </p>
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@TableName("tb_incomeCache")
public class IncomeCache implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.ASSIGN_UUID)
    private String id;

    @TableField("date")
    private String date; //日期,非空,2016-01-01

    @TableField("time")
    private String time; //时间,可空,16:00

    @TableField("psrID")
    private String psrID;

    @TableField("name")
    private String name; // 缓存数据名

    @TableField("data")
    private Double data;

    @TableField("cacheType")
    private Integer cacheType; //IncomeCacheType.key

    @TableField("planName")
    private String planName; //IncomePlan.name
}
