package com.cygsunri.income.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 收益时段和单价
 * </p>
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@TableName("tb_incomeTime")
public class IncomeTime implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.ASSIGN_UUID)
    private String id;

    @TableField("beginTime")
    private String beginTime;

    @TableField("comments")
    private String comments; //时段描述

    @TableField("endTime")
    private String endTime;

    @TableField("indexNumber")
    private Integer indexNumber;

    @TableField("level1_Price")
    private Double level1_Price;

    @TableField("level2_Price")
    private Double level2_Price;

    @TableField("level3_Price")
    private Double level3_Price;

    @TableField("name")
    private String name; //时段名称

    @TableField("incomePlan_id")
    private String incomePlan;


}
