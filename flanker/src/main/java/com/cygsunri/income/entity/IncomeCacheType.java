package com.cygsunri.income.entity;

import lombok.Getter;

import java.util.Arrays;
import java.util.Optional;

/**
 * 收益缓存类别
 */
@Getter
public enum IncomeCacheType {

    INTERVAL_CHARGE(0, "INTERVAL_CHARGE", "分段充电电量"),
    INTERVAL_DISCHARGE(1, "INTERVAL_DISCHARGE", "分段放电电量"),

    INTERVAL_CHARGE_BILLS(2, "INTERVAL_CHARGE_BILLS", "分段充电电费"),
    INTERVAL_DISCHARGE_BILL(3, "INTERVAL_CHARGE_BILLS", "分段放电电费"),

    DAY_CHARGE(4, "DAY_CHARGE", "日充电电量"),
    DAY_DISCHARGE(5, "DAY_DISCHARGE", "日放电电量"),

    DAY_CHARGE_BILLS(6, "DAY_CHARGE_BILLS", "日充电电费"),
    DAY_DISCHARGE_BILLS(7, "DAY_DISCHARGE_BILLS", "日放电电费"),

    MONTH_CHARGE(8, "MONTH_CHARGE", "月充电电量"),
    MONTH_DISCHARGE(9, "MONTH_DISCHARGE", "月放电电量"),

    MONTH_CHARGE_BILLS(10, "MONTH_CHARGE_BILLS", "月充电电费"),
    MONTH_DISCHARGE_BILLS(11, "MONTH_DISCHARGE_BILLS", "月放电电费");

    private int key;

    private String name;

    private String desc;

    IncomeCacheType(int key, String name, String desc) {
        this.key = key;
        this.name = name;
        this.desc = desc;
    }

    public static IncomeCacheType find(final int key) {
        Optional optional = Arrays.stream(IncomeCacheType.values())
                .filter(e -> e.key == key)
                .findFirst();
        return optional.isPresent() ? (IncomeCacheType) optional.get() : null;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
