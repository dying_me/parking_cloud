package com.cygsunri.income.task;

import com.cygsunri.income.service.IncomeService;
import com.cygsunri.util.DateUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@ConditionalOnProperty(prefix = "task.income", name = "open", havingValue = "true")
@PropertySource(name = "application.yml", value = {"classpath:application.yml"}, ignoreResourceNotFound = false, encoding = "UTF-8")
public class IncomeTask {

    @Autowired
    private IncomeService incomeService;

    /**
     * 统计收益缓存任务
     */
    @Scheduled(cron = "${task.income.cron}")
    private void incomeCalc() {
        long a = System.currentTimeMillis();

        String date = DateUtil.beforeDay(DateUtil.today());
        incomeService.updateCache(date);
        log.info("收益缓存共耗时：{} ms", System.currentTimeMillis() - a);
    }

}
