package com.cygsunri.consumption.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cygsunri.consumption.entity.Building;
import com.cygsunri.consumption.entity.BuildingAccountMapping;
import com.cygsunri.consumption.entity.BuildingSort;
import com.cygsunri.consumption.entity.enumerate.EnergySort;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 */
public interface BuildingService extends IService<Building> {

    /**
     * 获取所有建筑
     */
    List<Building> getAllBuilding();

    /**
     * 获取所有建筑
     */
    Building getBuildingById(String id);

    /**
     * 能源分类
     */
    @Transactional
    void saveBuilding(Building building);


    /**
     * 根据用户获取建筑
     *
     * @param accountID 用户ID
     */
    List<Building> getBuildingByAccount(String accountID);

    /**
     * 根据用户获取建筑映射
     *
     * @param accountName 用户名
     */
    List<BuildingAccountMapping> getBuildingMappingByAccount(String accountName);

    /**
     * 根据用户获取用能类型
     *
     * @param accountID 用户ID
     */
    List<EnergySort> getAccountEnergySort(String accountID);


    /**
     * 建筑按照分类排布
     */
    List<BuildingSort> getBuildingSorts(List<BuildingAccountMapping> buildingAccountMappings);


    /**
     * 根据建筑获得能源分类
     */
    List<EnergySort> getBuildingEnergySort(String buildingID);
}
