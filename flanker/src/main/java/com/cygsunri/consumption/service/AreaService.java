package com.cygsunri.consumption.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cygsunri.consumption.entity.Area;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 */
public interface AreaService extends IService<Area> {

    /**
     * 根据建筑和能源类型获得区域
     */
    List<Area> getAreasByBuildingAndEnergySort(String buildingID, String energySort);

    /**
     * 区域父子排序
     */
     List<Area> sortAreas(List<Area> areas);

}
