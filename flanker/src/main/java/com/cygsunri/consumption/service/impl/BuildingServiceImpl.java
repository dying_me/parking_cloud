package com.cygsunri.consumption.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cygsunri.consumption.entity.Building;
import com.cygsunri.consumption.entity.BuildingAccountMapping;
import com.cygsunri.consumption.entity.BuildingSort;
import com.cygsunri.consumption.entity.enumerate.EnergySort;
import com.cygsunri.consumption.mapper.BuildingMapper;
import com.cygsunri.consumption.service.BuildingAccountMappingService;
import com.cygsunri.consumption.service.BuildingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

/**
 * <p>
 * 服务实现类
 * </p>
 */
@Service
public class BuildingServiceImpl extends ServiceImpl<BuildingMapper, Building> implements BuildingService {

    @Autowired
    private BuildingMapper buildingMapper;

    @Autowired
    private BuildingAccountMappingService buildingAccountMappingService;

    @Override
    public List<Building> getAllBuilding() {
        QueryWrapper<Building> queryWrapper = new QueryWrapper<>();
        return buildingMapper.selectList(queryWrapper);
    }

    @Override
    public Building getBuildingById(String id) {
        QueryWrapper<Building> queryWrapper = new QueryWrapper<Building>()
                .eq("id", id);
        return buildingMapper.selectOne(queryWrapper);
    }

    @Override
    public void saveBuilding(Building building) {
        buildingMapper.insert(building);
    }

    @Override
    public List<Building> getBuildingByAccount(String accountID) {
        return buildingMapper.getBuildingByAccount(accountID);
    }

    @Override
    public List<BuildingAccountMapping> getBuildingMappingByAccount(String accountName) {
        return buildingAccountMappingService.getBuildingMappingByAccount(accountName);
    }

    @Override
    public List<EnergySort> getAccountEnergySort(String accountID) {
        return buildingAccountMappingService.getAccountEnergySort(accountID);
    }

    @Override
    public List<BuildingSort> getBuildingSorts(List<BuildingAccountMapping> buildingAccountMappings) {
        List<BuildingSort> buildingSorts = BuildingSort.fillBuildingSort();

        ListIterator<BuildingSort> it = buildingSorts.listIterator();
        while (it.hasNext()) {
            BuildingSort buildingSort = it.next();
            List<Building> buildings = new ArrayList<>();
            Boolean has = false;
            for (BuildingAccountMapping buildingAccountMapping : buildingAccountMappings) {
                Building building = getBuildingById(buildingAccountMapping.getBuildingId());
                if (building.getBuildingType().getName().equals(buildingSort.getName())) {
                    buildings.add(building);
                    has = true;
                }
            }
            if (!has) {
                it.remove();
            } else {
                buildingSort.setBuildings(buildings);
            }
        }
        return buildingSorts;
    }

    @Override
    public List<EnergySort> getBuildingEnergySort(String buildingID) {
        return buildingMapper.getBuildingEnergySort(buildingID);
    }
}
