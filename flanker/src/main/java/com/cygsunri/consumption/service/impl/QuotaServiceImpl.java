package com.cygsunri.consumption.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cygsunri.consumption.entity.Quota;
import com.cygsunri.consumption.mapper.QuotaMapper;
import com.cygsunri.consumption.service.QuotaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 */
@Service
public class QuotaServiceImpl extends ServiceImpl<QuotaMapper, Quota> implements QuotaService {

    @Autowired
    private QuotaMapper quotaMapper;

    @Override
    public Quota getQuotaByEnergySortAndType(String date, String energySort, String type) {
        return quotaMapper.getQuotaByEnergySortAndType(date, energySort, type);
    }
}
