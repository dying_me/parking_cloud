package com.cygsunri.consumption.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cygsunri.consumption.entity.BuildingAccountMapping;
import com.cygsunri.consumption.entity.enumerate.EnergySort;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 */
public interface BuildingAccountMappingService extends IService<BuildingAccountMapping> {

    List<BuildingAccountMapping> getBuildingMappingByAccount(String accountName);

    List<EnergySort> getAccountEnergySort(String accountID);
}
