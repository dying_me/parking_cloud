package com.cygsunri.consumption.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cygsunri.consumption.entity.AreaEnergyMapping;
import com.cygsunri.consumption.mapper.AreaEnergyMappingMapper;
import com.cygsunri.consumption.service.AreaEnergyMappingService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 */
@Service
public class AreaEnergyMappingServiceImpl extends ServiceImpl<AreaEnergyMappingMapper, AreaEnergyMapping> implements AreaEnergyMappingService {

}
