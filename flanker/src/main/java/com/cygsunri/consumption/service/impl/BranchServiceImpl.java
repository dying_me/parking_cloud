package com.cygsunri.consumption.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cygsunri.consumption.entity.Branch;
import com.cygsunri.consumption.mapper.BranchMapper;
import com.cygsunri.consumption.service.BranchService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * <p>
 * 服务实现类
 * </p>
 */
@Service
public class BranchServiceImpl extends ServiceImpl<BranchMapper, Branch> implements BranchService {

    @Autowired
    private BranchMapper branchMapper;

    @Override
    public List<Branch> getAllBranch() {
        QueryWrapper<Branch> queryWrapper = new QueryWrapper<>();
        return branchMapper.selectList(queryWrapper);
    }

    @Override
    public Branch getBranchById(String id) {
        QueryWrapper<Branch> queryWrapper = new QueryWrapper<Branch>()
                .eq("id", id);
        return branchMapper.selectOne(queryWrapper);
    }

    @Override
    public List<Branch> getBranchesByBuildingAndEnergySort(String buildingId, String energySort) {
        return branchMapper.getBranchesByBuildingAndEnergySort(buildingId, energySort);
    }

    @Override
    public List<Branch> getBranchesByEnergySortWithOutBuilding(String energySort) {
        return branchMapper.getBranchesByEnergySortWithOutBuilding(energySort);
    }

    @Override
    public List<Branch> getBranchesByEnergySortWithBuilding(String energySort) {
        return branchMapper.getBranchesByEnergySortWithBuilding(energySort);
    }
    @Override
    public List<Branch> sortBranches(List<Branch> branches) {
        List<Branch> resultList = new ArrayList<>();
        for (Branch branch : branches) {
            if (StringUtils.isBlank(branch.getParent())) {
                resultList.add(branch);
                findChildren(branch, branches);
            }
        }
        return resultList;
    }

    @Override
    public List<Branch> getBranchesHasPsrIDByBuildingAndEnergySort(String buildingId, String energySort) {
        List<Branch> resultList = new ArrayList<>();
        List<Branch> parents = branchMapper.getParentBranchesByBuildingAndEnergySort(buildingId, energySort);
        for (Branch parent : parents) {
            //父支路有关联设备直接增加，没有的搜索出关联了设备的子支路
            if (StringUtils.isBlank(parent.getPsrID())) {
                resultList.addAll(getBranchesByParentAndEnergySort(parent.getId(), energySort));
            } else {
                resultList.add(parent);
            }
        }

        return resultList;
    }

    /**
     * 递归查询下级支路
     */
    private void findChildren(Branch parent, List<Branch> branches) {
        List<Branch> childrenList = new ArrayList<>();

        for (Branch b : branches) {
            if (Objects.equals(parent.getId(), b.getParent())) {
                childrenList.add(b);
                findChildren(b, branches);
            }
        }

        if (childrenList.isEmpty()) {
            return;
        }
        parent.setChildren(childrenList);
    }

    @Override
    public List<Branch> getBranchesByParentAndEnergySort(String parentID, String energySort) {
        return branchMapper.getBranchesByParentAndEnergySort(parentID, energySort);
    }

    @Override
    public List<Branch> getBranchesByBranchAndEnergySort(String branchID, String energySort) {
        List<Branch> branches = new ArrayList<>();
        Branch branch = getBranchById(branchID);

        if (branch != null) {
            //如果支路有设备映射直接返回
            if (StringUtils.isNotBlank(branch.getPsrID())) {
                branches.add(branch);
            } else {
                //无设备映射可能为父支路，找出子支路
                branches = getBranchesByParentAndEnergySort(branch.getId(), energySort);
            }
        }

        //都不是返回空数组
        return branches;
    }

    @Override
    public List<Branch> getBranchesByDepartmentAndEnergySort(String departmentID, String energySort) {
        List<Branch> branches = branchMapper.getBranchesByDepartmentAndEnergySort(departmentID, energySort);
        //如果为空，可能为父部门且没有独立分支，搜索子部门所有支路
        if (branches.isEmpty()) {
            branches = branchMapper.getBranchesByParentDepartmentAndEnergySort(departmentID, energySort);
        }

        return branches;
    }

    @Override
    public List<Branch> getBranchesByEnergyItem(String itemType) {
        return branchMapper.getBranchesByEnergyItem(itemType);
    }

    @Override
    public List<Branch> getBranchesByBuildingAndEnergyItem(String buildingID, String energyItemID) {
        return branchMapper.getBranchesByBuildingAndEnergyItem(buildingID, energyItemID);
    }

    @Override
    public List<Branch> getBranchesByAccountAndEnergyItem(String accountID, String energyItemID) {
        return branchMapper.getBranchesByAccountAndEnergyItem(accountID, energyItemID);
    }

    @Override
    public List<Branch> getBranchesByEnergySort(String energySort) {
        return branchMapper.getBranchesByEnergySort(energySort);
    }

}
