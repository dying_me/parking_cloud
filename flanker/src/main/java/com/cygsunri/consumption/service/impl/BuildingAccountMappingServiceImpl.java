package com.cygsunri.consumption.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cygsunri.consumption.entity.BuildingAccountMapping;
import com.cygsunri.consumption.entity.enumerate.EnergySort;
import com.cygsunri.consumption.mapper.BuildingAccountMappingMapper;
import com.cygsunri.consumption.service.BuildingAccountMappingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 */
@Service
public class BuildingAccountMappingServiceImpl extends ServiceImpl<BuildingAccountMappingMapper, BuildingAccountMapping> implements BuildingAccountMappingService {

    @Autowired
    private BuildingAccountMappingMapper buildingAccountMappingMapper;

    @Override
    public List<BuildingAccountMapping> getBuildingMappingByAccount(String accountName) {
        return buildingAccountMappingMapper.getBuildingMappingByAccount(accountName);
    }

    @Override
    public List<EnergySort> getAccountEnergySort(String accountID) {
        return buildingAccountMappingMapper.getAccountEnergySort(accountID);
    }
}
