package com.cygsunri.consumption.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cygsunri.consumption.entity.Advanced;
import com.cygsunri.consumption.mapper.AdvancedMapper;
import com.cygsunri.consumption.service.AdvancedService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 */
@Service
public class AdvancedServiceImpl extends ServiceImpl<AdvancedMapper, Advanced> implements AdvancedService {

}
