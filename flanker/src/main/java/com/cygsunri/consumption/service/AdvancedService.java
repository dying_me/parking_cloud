package com.cygsunri.consumption.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cygsunri.consumption.entity.Advanced;

/**
 * <p>
 *  服务类
 * </p>
 */
public interface AdvancedService extends IService<Advanced> {

}
