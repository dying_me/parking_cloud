package com.cygsunri.consumption.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cygsunri.consumption.entity.Department;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 */
public interface DepartmentService extends IService<Department> {

    /**
     * 根据建筑和能源类型获得部门
     */
    List<Department> getDepartmentsByBuildingAndEnergySort(String buildingID, String energySort);

    /**
     * 部门父子排序
     */
    List<Department> sortDepartments(List<Department> departments);
}
