package com.cygsunri.consumption.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cygsunri.consumption.entity.Area;
import com.cygsunri.consumption.mapper.AreaMapper;
import com.cygsunri.consumption.service.AreaService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * <p>
 *  服务实现类
 * </p>
 */
@Service
public class AreaServiceImpl extends ServiceImpl<AreaMapper, Area> implements AreaService {

    @Autowired
    private AreaMapper areaMapper;

    @Override
    public List<Area> getAreasByBuildingAndEnergySort(String buildingID, String energySort) {
        return areaMapper.getAreasByBuildingAndEnergySort(buildingID, energySort);
    }

    @Override
    public List<Area> sortAreas(List<Area> areas) {
        List<Area> resultList = new ArrayList<>();
        for (Area area : areas) {
            if (StringUtils.isBlank(area.getParent())) {
                resultList.add(area);
                findChildren(area, areas);
            }
        }
        return resultList;
    }

    /**
     * 递归查询下级区域
     */
    private void findChildren(Area parent, List<Area> departments) {
        List<Area> childrenList = new ArrayList<>();

        for (Area d : departments) {
            if (Objects.equals(parent.getId(), d.getParent())) {
                childrenList.add(d);
                findChildren(d, departments);
            }
        }

        if (childrenList.isEmpty()) {
            return;
        }
        parent.setChildren(childrenList);
    }
}
