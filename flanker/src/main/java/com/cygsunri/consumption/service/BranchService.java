package com.cygsunri.consumption.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cygsunri.consumption.entity.Branch;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 */
public interface BranchService extends IService<Branch> {

    /**
     * 获取所有支路
     */
    List<Branch> getAllBranch();

    /**
     * 通过id获取支路
     *
     * @param id 主键
     */
    Branch getBranchById(String id);

    /**
     * 根据建筑id和能源类型获得支路
     */
    List<Branch> getBranchesByBuildingAndEnergySort(String buildingId, String energySort);


    /**
     * 获取没有关联建筑的支路
     */
    List<Branch> getBranchesByEnergySortWithOutBuilding(String energySort);

    /**
     * 获取关联建筑的支路
     */
    List<Branch> getBranchesByEnergySortWithBuilding(String energySort);

    /**
     * 支路父子排序
     */
    List<Branch> sortBranches(List<Branch> branches);


    /**
     * 根据建筑和类型，获得所有含有设备的支路
     */
    List<Branch> getBranchesHasPsrIDByBuildingAndEnergySort(String buildingId, String energySort);


    /**
     * 通过父级支路找到所有指定类型的子支路
     */
    List<Branch> getBranchesByParentAndEnergySort(String parentID, String energySort);

    /**
     * 根据支路id和能源类型获得所属支路
     */
    List<Branch> getBranchesByBranchAndEnergySort(String branchID, String energySort);

    /**
     * 根据部门和能源类型获得所属支路
     */
    List<Branch> getBranchesByDepartmentAndEnergySort(String departmentID, String energySort);

    /**
     * 通过分项类型获得支路
     */
    List<Branch> getBranchesByEnergyItem(String type);

    /**
     * 通过建筑和分项类型获得支路
     */
    List<Branch> getBranchesByBuildingAndEnergyItem(String building, String energyItem);


    /**
     * 通过用户和分项类型获得支路
     */
    List<Branch> getBranchesByAccountAndEnergyItem(String accountID, String energyItem);


    /**
     * 通过能源类型获得支路
     */
    List<Branch> getBranchesByEnergySort(String energySort);

}
