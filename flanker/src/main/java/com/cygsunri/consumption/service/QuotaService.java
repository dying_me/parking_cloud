package com.cygsunri.consumption.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cygsunri.consumption.entity.Quota;

/**
 * <p>
 * 服务类
 * </p>
 */
public interface QuotaService extends IService<Quota> {

    /**
     * 根据日期、能耗类型、时间类型获取定额
     */
    Quota getQuotaByEnergySortAndType(String date, String energySort, String type);
}
