package com.cygsunri.consumption.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cygsunri.consumption.entity.DepartmentEnergyMapping;
import com.cygsunri.consumption.mapper.DepartmentEnergyMappingMapper;
import com.cygsunri.consumption.service.DepartmentEnergyMappingService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 */
@Service
public class DepartmentEnergyMappingServiceImpl extends ServiceImpl<DepartmentEnergyMappingMapper, DepartmentEnergyMapping> implements DepartmentEnergyMappingService {

}
