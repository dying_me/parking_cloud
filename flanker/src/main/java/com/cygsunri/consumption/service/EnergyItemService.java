package com.cygsunri.consumption.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cygsunri.consumption.entity.EnergyItem;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 */
public interface EnergyItemService extends IService<EnergyItem> {

    /**
     * 根据id获取分项
     */
    EnergyItem getEnergyItemById(String id);

    /**
     * 根据建筑和类型获取分项
     */
    List<EnergyItem> getEnergyItemByBuildingAndEnergySort(String buildingId, String energySort);

    /**
     * 根据用户和类型获取分项
     */
    List<EnergyItem> getEnergyItemByAccountAndEnergySort(String accountID, String energySort);

    /**
     * 分类父子排序
     */
    List<EnergyItem> sortEnergyItems(List<EnergyItem> energyItems);
}
