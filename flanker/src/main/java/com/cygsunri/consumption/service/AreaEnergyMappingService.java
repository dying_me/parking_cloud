package com.cygsunri.consumption.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cygsunri.consumption.entity.AreaEnergyMapping;

/**
 * <p>
 *  服务类
 * </p>
 */
public interface AreaEnergyMappingService extends IService<AreaEnergyMapping> {

}
