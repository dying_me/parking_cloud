package com.cygsunri.consumption.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cygsunri.consumption.entity.DepartmentEnergyMapping;

/**
 * <p>
 *  Mapper 接口
 * </p>
 */
public interface DepartmentEnergyMappingMapper extends BaseMapper<DepartmentEnergyMapping> {

}
