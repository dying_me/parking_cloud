package com.cygsunri.consumption.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cygsunri.consumption.entity.Quota;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * Mapper 接口
 * </p>
 */
public interface QuotaMapper extends BaseMapper<Quota> {

    Quota getQuotaByEnergySortAndType(@Param("date") String date, @Param("energySort") String energySort, @Param("type") String type);
}
