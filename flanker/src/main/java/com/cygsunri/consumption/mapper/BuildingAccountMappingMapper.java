package com.cygsunri.consumption.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cygsunri.consumption.entity.BuildingAccountMapping;
import com.cygsunri.consumption.entity.enumerate.EnergySort;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 */
public interface BuildingAccountMappingMapper extends BaseMapper<BuildingAccountMapping> {

    List<BuildingAccountMapping> getBuildingMappingByAccount(@Param("accountName") String accountName);

    List<EnergySort> getAccountEnergySort(@Param("accountID") String accountID);
}
