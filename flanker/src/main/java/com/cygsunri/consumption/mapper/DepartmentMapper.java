package com.cygsunri.consumption.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cygsunri.consumption.entity.Department;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 */
public interface DepartmentMapper extends BaseMapper<Department> {

    List<Department> getDepartmentsByBuildingAndEnergySort(@Param("buildingID") String buildingID, @Param("energySort") String energySort);
}
