package com.cygsunri.consumption.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cygsunri.consumption.entity.Area;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 */
public interface AreaMapper extends BaseMapper<Area> {

    List<Area> getAreasByBuildingAndEnergySort(@Param("buildingId") String buildingId, @Param("energySort") String energySort);
}
