package com.cygsunri.consumption.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cygsunri.consumption.entity.Building;
import com.cygsunri.consumption.entity.enumerate.EnergySort;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 */
public interface BuildingMapper extends BaseMapper<Building> {

    List<Building> getBuildingByAccount(@Param("accountID") String accountID);

    List<EnergySort> getBuildingEnergySort(@Param("buildingID") String buildingID);
}
