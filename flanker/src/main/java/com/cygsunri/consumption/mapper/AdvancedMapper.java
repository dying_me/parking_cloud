package com.cygsunri.consumption.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cygsunri.consumption.entity.Advanced;

/**
 * <p>
 *  Mapper 接口
 * </p>
 */
public interface AdvancedMapper extends BaseMapper<Advanced> {

}
