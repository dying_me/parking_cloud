package com.cygsunri.consumption.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cygsunri.consumption.entity.Branch;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 */
public interface BranchMapper extends BaseMapper<Branch> {

    /**
     * 根据建筑和能源类型获得支路
     */
    List<Branch> getBranchesByBuildingAndEnergySort(@Param("buildingId") String buildingId, @Param("energySort") String energySort);

    /**
     * 获取没有关联建筑的支路
     */

    List<Branch> getBranchesByEnergySortWithOutBuilding(@Param("energySort") String energySort);

    /**
     * 获取关联建筑的支路
     */

    List<Branch> getBranchesByEnergySortWithBuilding(@Param("energySort") String energySort);

    /**
     * 根据建筑和能源类型获得父支路
     */
    List<Branch> getParentBranchesByBuildingAndEnergySort(@Param("buildingId") String buildingId, @Param("energySort") String energySort);

    /**
     * 通过父支路找到所有对应类型并含有设备的子支路
     */
    List<Branch> getBranchesByParentAndEnergySort(@Param("parentID") String parentID, @Param("energySort") String energySort);

    /**
     * 通过部门和能源类型获得支路
     */
    List<Branch> getBranchesByDepartmentAndEnergySort(@Param("departmentID") String departmentID, @Param("energySort") String energySort);

    /**
     * 通过父部门和能源类型获得支路
     */
    List<Branch> getBranchesByParentDepartmentAndEnergySort(@Param("parentID") String parentID, @Param("energySort") String energySort);

    /**
     * 通过分项类型获得支路
     */
    List<Branch> getBranchesByEnergyItem(@Param("itemType") String itemType);

    /**
     * 通过建筑和分项类型获得支路
     */
    List<Branch> getBranchesByBuildingAndEnergyItem(@Param("buildingID") String buildingID, @Param("energyItemID") String energyItemID);

    /**
     * 通过用户和分项类型获得支路
     */
    List<Branch> getBranchesByAccountAndEnergyItem(@Param("accountID") String accountID, @Param("energyItemID") String energyItemID);

    /**
     * 通过能源类型获得支路
     */
    List<Branch> getBranchesByEnergySort(@Param("energySort") String energySort);

}
