package com.cygsunri.consumption.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cygsunri.consumption.entity.EnergyItem;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 */
public interface EnergyItemMapper extends BaseMapper<EnergyItem> {

    List<EnergyItem> getEnergyItemByBuildingAndEnergySort(@Param("buildingId") String buildingId, @Param("energySort") String energySort);

    List<EnergyItem> getEnergyItemByAccountAndEnergySort(@Param("accountID") String accountID, @Param("energySort") String energySort);
}
