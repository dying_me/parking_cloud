package com.cygsunri.consumption.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cygsunri.consumption.entity.AreaEnergyMapping;

/**
 * <p>
 *  Mapper 接口
 * </p>
 */
public interface AreaEnergyMappingMapper extends BaseMapper<AreaEnergyMapping> {

}
