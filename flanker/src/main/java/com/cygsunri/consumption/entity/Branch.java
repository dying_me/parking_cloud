package com.cygsunri.consumption.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.cygsunri.consumption.entity.enumerate.EnergySort;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 *  用能支路
 * </p>
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@TableName("tb_consumptionBranch")
public class Branch implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.ASSIGN_UUID)
    private String id;

    /**
     * 对应scd设备参数表中设备
     */
    @TableField("PsrID")
    private String psrID;

    /**
     * 折算成标准煤系数
     */
    @TableField("coefficient")
    private Double coefficient;

    /**
     * 能源分类
     */
    @TableField("energySort")
    private EnergySort energySort;

    /**
     * 支路名称
     */
    @TableField("name")
    private String name;

    /**
     * 上级支路，为空表示是主回路
     */
    @TableField("parent")
    private String parent;

    /**
     * 单价
     */
    @TableField("unitPrice")
    private Double unitPrice;

    /**
     * 所属区域
     */
    @TableField("area_id")
    private String areaId;

    /**
     * 所属建筑
     */
    @TableField("building_id")
    private String buildingId;

    /**
     * 所属部门
     */
    @TableField("department_id")
    private String departmentId;

    /**
     * 所属能源分项
     */
    @TableField("energyItem_id")
    private String energyItemId;

    @TableField(exist=false)
    private List<Branch> children;

}
