package com.cygsunri.consumption.entity.response;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 汉光数据返回类
 */
@Accessors(chain = true)
@Data
public class HGResponse {
    private String result;
    private String message;
    private List<Object> jbody;
}
