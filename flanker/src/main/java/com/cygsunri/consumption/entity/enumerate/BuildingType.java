package com.cygsunri.consumption.entity.enumerate;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Getter;

import java.util.Arrays;

/**
 * 建筑类别
 */
@Getter
public enum BuildingType {
    Office(0, "Office", "办公"),
    Market(1, "Market", "商场"),
    Hotel(2, "Hotel", "宾馆饭店"),
    School(3, "School", "学校"),
    Medical(4, "Medical", "医疗卫生"),
    Sport(5, "Sport", "体育"),
    Traffic(6, "Traffic", "交通"),
    Comprehensive(7, "Comprehensive", "综合"),
    UnKnown(10001, "UnKnown", "未知");

    private int key;

    @EnumValue
    @JsonValue    //标记响应json值
    private String name;

    private String desc;

    BuildingType(int key, String name, String desc) {
        this.key = key;
        this.name = name;
        this.desc = desc;
    }

    public static BuildingType find(final int key) {
        return Arrays.stream(BuildingType.values())
                .filter(e -> e.key == key)
                .findFirst()
                .orElse(UnKnown);
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
