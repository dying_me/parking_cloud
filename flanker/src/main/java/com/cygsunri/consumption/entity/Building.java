package com.cygsunri.consumption.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.cygsunri.consumption.entity.enumerate.BuildingType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 *  用能建筑
 * </p>
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@TableName("tb_consumptionBuilding")
public class Building implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.ASSIGN_UUID)
    private String id;

    /**
     * 建筑名称
     */
    @TableField("name")
    private String name;

    /**
     * 对应scd设备参数表中设备
     */
    @TableField("PsrID")
    private String psrID;

    /**
     * 经度
     */
    @TableField("longitude")
    private Double longitude;

    /**
     * 纬度
     */
    @TableField("latitude")
    private Double latitude;

    /**
     * 地址
     */
    @TableField("address")
    private String address;

    /**
     * 空调面积
     */
    @TableField("airSpace")
    private Double airSpace;

    /**
     * 面积
     */
    @TableField("space")
    private String space;

    /**
     * 类别
     */
    @TableField("buildingType")
    private BuildingType buildingType;

    /**
     * 平时段
     */
    @TableField("flatTimes")
    private String flatTimes;


    /**
     * 夜间时段
     */
    @TableField("nightTimes")
    private String nightTimes;

    /**
     * 峰时段
     */
    @TableField("peakTimes")
    private String peakTimes;

    /**
     * 尖时段
     */
    @TableField("tipTimes")
    private String tipTimes;

    /**
     * 谷时段
     */
    @TableField("valleyTimes")
    private String valleyTimes;

}
