package com.cygsunri.consumption.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 *  能耗先进值
 * </p>
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@TableName("tb_consumptionadvanced")
public class Advanced implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.ASSIGN_UUID)
    private String id;

    /**
     * 先进值单位
     */
    @TableField("advanced_unit")
    private String advancedUnit;

    /**
     * 先进值
     */
    @TableField("advanced_value")
    private Double advancedValue;

    /**
     * 2020-03（month）、2020（year）
     */
    @TableField("date")
    private String date;

    /**
     * month（按月定额）、year（按年定额）
     */
    @TableField("type")
    private String type;

    /**
     * 用能单位
     */
    @TableField("unitID")
    private String unitID;

}
