package com.cygsunri.consumption.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.cygsunri.consumption.entity.enumerate.EnergySort;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 *  能源分项
 * </p>
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("tb_consumptionEnergyItem")
public class EnergyItem implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.ASSIGN_UUID)
    private String id;

    /**
     * 能源分类
     */
    @TableField("energySort")
    private EnergySort energySort;

    /**
     * 分项名称
     */
    @TableField("name")
    private String name;

    /**
     * 上级分项
     */
    @TableField("parent")
    private String parent;

    /**
     * 分项类型，暂时不用枚举
     */
    @TableField("itemType")
    private String itemType;

    @TableField(exist=false)
    private List<EnergyItem> children;
}
