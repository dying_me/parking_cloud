package com.cygsunri.consumption.entity.request;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 同比分析查询参数
 */
@Data
public class NewAnalysisRequest {
    /**
     * 建筑id
     */
    @NotNull
    private String buildingId;
    /**
     * 查询类型
     */
    @NotNull
    private String category;
    /**
     * 同比yoy/环比qoq
     */
    private String comparisonType;
    /**
     * 时间类型
     */
    @NotNull
    private String dateType;
    /**
     * 时间
     */
    @NotNull
    private String dateValue;
    /**
     * 能源类型
     */
    @NotNull
    private String energySort;
    /**
     * 查询对象id
     */
    private List<String> checkedKeys;
}
