package com.cygsunri.consumption.entity.request;

import lombok.Data;

/**
 * 同比分析查询参数（旧）
 */
@Data
public class AnalysisRequest {

    private String pageType;
    private String id;
    private String buildingId;
    private String energySort;
    private String year;
}
