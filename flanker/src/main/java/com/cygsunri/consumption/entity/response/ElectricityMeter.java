package com.cygsunri.consumption.entity.response;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 电表
 */
@Accessors(chain = true)
@Data
public class ElectricityMeter {
    private String VirtualDevID;
    private String VirtualDevName;
    private String BuildingID;
    private String EnergyType;
    private String BranchID;
    private String UpdateTime;
    private Double Ia;
    private Double Ib;
    private Double Ic;
    private Double Ua;
    private Double Ub;
    private Double Uc;
    private Double Pa;
    private Double Pb;
    private Double Pc;
    private Double Eall;
    private Double P;
    private Double Q;
    private Double PF;
    private Double Eq;
}
