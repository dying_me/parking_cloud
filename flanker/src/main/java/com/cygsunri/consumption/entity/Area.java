package com.cygsunri.consumption.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 *  用能区域
 * </p>
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@TableName("tb_consumptionArea")
public class Area implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.ASSIGN_UUID)
    private String id;

    /**
     * 区域名称
     */
    @TableField("name")
    private String name;

    /**
     * 上级区域
     */
    @TableField("parent")
    private String parent;

    /**
     * 所属建筑
     */
    @TableField("building_id")
    private String buildingId;


    @TableField(exist=false)
    private List<Area> children;
}
