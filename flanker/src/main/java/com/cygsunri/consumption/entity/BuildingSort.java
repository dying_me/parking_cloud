package com.cygsunri.consumption.entity;

import com.cygsunri.consumption.entity.enumerate.BuildingType;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.List;

/**
 * 建筑类型返回类
 */
@Data
@Accessors(chain = true)
public class BuildingSort {

    private String name;
    private String desc;
    private List<Building> buildings = new ArrayList<>();

    public static List<BuildingSort> fillBuildingSort() {
        List<BuildingSort> buildingSorts = new ArrayList<>();
        for (BuildingType buildingType : BuildingType.values()) {
            BuildingSort buildingSort = new BuildingSort().setName(buildingType.getName()).setDesc(buildingType.getDesc());
            buildingSorts.add(buildingSort);
        }
        return buildingSorts;
    }

}
