package com.cygsunri.consumption.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.cygsunri.consumption.entity.enumerate.EnergySort;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 能耗定额
 * </p>
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@TableName("tb_consumptionQuota")
public class Quota implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.ASSIGN_UUID)
    private String id;

    /**
     * 用能单位
     */
    @TableField("unitID")
    private String unitID;

    /**
     * month（按月定额）、year（按年定额）
     */
    @TableField("type")
    private String type;

    /**
     * 2020-03（month）、2020（year）
     */
    @TableField("dateFrom")
    private String dateFrom;

    /**
     * 2020-03（month）、2020（year）
     */
    @TableField("dateEnd")
    private String dateEnd;

    /**
     * 能源分类
     */
    @TableField("energySort")
    private EnergySort energySort;

    /**
     * 定额值单位
     */
    @TableField("quota_unit")
    private String quota_unit;

    /**
     * 定额值
     */
    @TableField("quota_value")
    private Double quota_value;

    /**
     * 报警阈值
     */
    @TableField("alarm_threshold")
    private Double alarm_threshold;

}
