package com.cygsunri.consumption.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 首页数据返回类
 */
@Accessors(chain = true)
@Data
public class EnergyResponse {
    private String energyCode;
    private String energyDesc;
    private String energyUnit;
    private List<Double> today;  //今日或实时值
    private List<Double> lastDay;
    private List<Double> thisMonth;
    private List<Double> lastMonth;

}
