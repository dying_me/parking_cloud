package com.cygsunri.consumption.entity.response;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 水表、压力表
 */
@Accessors(chain = true)
@Data
public class WaterMete {
    private String VirtualDevID;
    private String VirtualDevName;
    private String BuildingID;
    private String EnergyType;
    private String BranchID;
    private String UpdateTime;
    private Double Value;
}
