package com.cygsunri.consumption.entity.enumerate;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Getter;

import java.util.Arrays;

/**
 * 能源分类
 */
@Getter
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum EnergySort implements java.io.Serializable{
    Electricity(0, "Electricity", "电"),
    Water(1, "Water", "水"),
    Natural_gas(2, "Natural_gas", "天然气"),
    Liquefied_gas(3, "Liquefied_gas", "液化石油气"),
    Manufactured_gas(4, "Manufactured_gas", "人工煤气"),
    Central_heating(5, "Central_heating", "集中供热量"),
    Central_cooling(6, "Central_cooling", "集中供冷量"),
    Coal(7, "Coal", "煤"),
    Gasoline(8, "Gasoline", "汽油"),
    Kerosene(9, "Kerosene", "煤油"),
    Diesel(10, "Diesel", "柴油"),
    Renewable(11, "Renewable", "可再生能源"),
    Pressure(12, "Pressure", "压力"),

    UnKnown(10001, "UnKnown", "未知");

    private int key;

    @EnumValue
    @JsonValue    //标记响应json值
    private String name;

    private String desc;

    EnergySort(int key, String name, String desc) {
        this.key = key;
        this.name = name;
        this.desc = desc;
    }

    public static EnergySort find(final int key) {
        return Arrays.stream(EnergySort.values())
                .filter(e -> e.key == key)
                .findFirst()
                .orElse(UnKnown);
    }

    @Override
    public String toString() {
        return super.toString();
    }

}
