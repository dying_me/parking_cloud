package com.cygsunri.consumption.transport;

import lombok.extern.slf4j.Slf4j;
import org.apache.axis.client.Call;
import org.apache.axis.client.Service;
import org.apache.axis.encoding.XMLType;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.xml.namespace.QName;
import javax.xml.rpc.ParameterMode;

/**
 *
 * 汉光webservice.asmx数据接口，详见接口文档 docs/HKST数据接口.docx
 */
@Slf4j
@Component
public class AsmxTrans {

    @Value("${task.consumption.meter.url}")
    private String url;

    @Value("${task.consumption.meter.soapAction}")
    private String soapAction;

    /**
     * 获取建筑
     */
    public String getBuildings(Object[] params) {
        String operationName = "GetBuildings";// 调用方法名
        Service service = new Service();
        String ret = null;
        try {
            Call call = (Call) service.createCall();
            call.setTargetEndpointAddress(url);
            call.setOperationName(new QName(soapAction, operationName)); // 设置要调用哪个方法
            call.addParameter(new QName(soapAction, "Uid"), XMLType.XSD_STRING, ParameterMode.IN);// 设置要传递的参数1
            call.setReturnType(XMLType.XSD_STRING);// （标准的类型）
            call.setUseSOAPAction(true);
            call.setTimeout(10000);
            call.setSOAPActionURI(soapAction + operationName);
            ret = (String) call.invoke(params);// 调用方法并传递参数

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return ret;
    }

    /**
     * 获取建筑仪表列表
     */
    public String getBuildingMeters(Object[] params) {
        String operationName = "GetBuildingMeters";// 调用方法名
        Service service = new Service();
        String ret = null;
        try {
            Call call = (Call) service.createCall();
            call.setTargetEndpointAddress(url);
            call.setOperationName(new QName(soapAction, operationName)); // 设置要调用哪个方法
            call.addParameter(new QName(soapAction, "Uid"), XMLType.XSD_STRING, ParameterMode.IN);// 设置要传递的参数1
            call.addParameter(new QName(soapAction, "BuildingID"), XMLType.XSD_STRING, ParameterMode.IN);// 设置要传递的参数2
            call.addParameter(new QName(soapAction, "EnergyType"), XMLType.XSD_STRING, ParameterMode.IN);
            call.setReturnType(XMLType.XSD_STRING);// （标准的类型）
            call.setUseSOAPAction(true);
            call.setTimeout(10000);
            call.setSOAPActionURI(soapAction + operationName);
            ret = (String) call.invoke(params);// 调用方法并传递参数

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return ret;
    }

    /**
     * 获取仪表实时数据
     */
    public String getBuildingMeterCurrentData(Object[] params) {
        String operationName = "GetBuildingMeterCurrentData";// 调用方法名
        Service service = new Service();
        String ret = null;
        try {
            Call call = (Call) service.createCall();
            call.setTargetEndpointAddress(url);
            call.setOperationName(new QName(soapAction, operationName)); // 设置要调用哪个方法
            call.addParameter(new QName(soapAction, "Uid"), XMLType.XSD_STRING, ParameterMode.IN);// 设置要传递的参数1
            call.addParameter(new QName(soapAction, "BuildingID"), XMLType.XSD_STRING, ParameterMode.IN);// 设置要传递的参数2
            call.addParameter(new QName(soapAction, "EnergyType"), XMLType.XSD_STRING, ParameterMode.IN);
            call.addParameter(new QName(soapAction, "PageIndex"), XMLType.XSD_STRING, ParameterMode.IN);
            call.addParameter(new QName(soapAction, "PageSize"), XMLType.XSD_STRING, ParameterMode.IN);
            call.setReturnType(XMLType.XSD_STRING);// （标准的类型）
            call.setUseSOAPAction(true);
            call.setTimeout(10000);
            call.setSOAPActionURI(soapAction + operationName);
            ret = (String) call.invoke(params);// 调用方法并传递参数

        } catch (Exception ex) {
            log.warn("汉光通讯失败，{}", ex.toString());
            ex.printStackTrace();
        }
        return ret;
    }
}
