package com.cygsunri.consumption.api;

import com.cygsunri.consumption.transport.AsmxTrans;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 获取汉光接口数据（测试用）
 */
@RestController
@RequestMapping("/api/consumption/hg")
public class ConsumptionHGController {

    @Autowired
    private AsmxTrans asmxTrans;

    /**
     * 获取建筑
     */
    @RequestMapping(value = "/getBuildings", method = RequestMethod.GET)
    public ResponseEntity<String> getBuildings() {
        //sessionID为空，详见汉光接口文档
        Object[] p = {""};
        String buildings = asmxTrans.getBuildings(p);
        return new ResponseEntity<>(buildings, HttpStatus.OK);
    }

    /**
     * 获取建筑仪表列表
     * @param building： 12
     * @param type： 002001 电、002002 水、002006 压力表
     */
    @RequestMapping(value = "/getBuildingMeters/{building}/{type}", method = RequestMethod.GET)
    public ResponseEntity<String> getBuildingMeters(@PathVariable("building") String building, @PathVariable("type") String type) {
        Object[] p = {"", building, type};
        String meters = asmxTrans.getBuildingMeters(p);
        return new ResponseEntity<>(meters, HttpStatus.OK);
    }

    /**
     * 获取仪表实时数据
     * @param building： 12
     * @param type： 002001 电、002002 水、002006 压力表
     */
    @RequestMapping(value = "/getBuildingMeterCurrentData/{building}/{type}", method = RequestMethod.GET)
    public ResponseEntity<String> getBuildingMeterCurrentData(@PathVariable("building") String building, @PathVariable("type") String type) {
        Object[] p = {"", building, type, "1", "20"};
        String data = asmxTrans.getBuildingMeterCurrentData(p);
        return new ResponseEntity<>(data, HttpStatus.OK);
    }
}
