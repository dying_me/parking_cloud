package com.cygsunri.solar.dto;

import lombok.Data;

@Data
public class SolarParams {

    String deviceId;
    String date;
}
