package com.cygsunri.solar.config;

import lombok.Data;
import org.springframework.stereotype.Component;

@Data
@Component
public class CurveConfig {

    private String beginTime = "05:00";

    private String endTime = "20:00";

    public String buildBeginTime(String date) {
        return date.trim() + " " + beginTime.trim();
    }

    public String buildEndTime(String date) {
        return date.trim() + " " + endTime.trim();
    }
}
