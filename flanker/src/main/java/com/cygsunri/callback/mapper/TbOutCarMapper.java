package com.cygsunri.callback.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cygsunri.callback.entity.TbOutCar;

/**
 * <p>
 * 上传车辆出场信息表 Mapper 接口
 * </p>
 *
 * @author hanyuan
 * @since 2021-07-30
 */
public interface TbOutCarMapper extends BaseMapper<TbOutCar> {

    void deleteBatch(String[] ids);
    /**
     * 查找某天出场车辆数
     *
     */
    Integer findCountByTime(String time);
}
