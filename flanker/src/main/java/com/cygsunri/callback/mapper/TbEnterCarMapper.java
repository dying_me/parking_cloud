package com.cygsunri.callback.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cygsunri.callback.entity.TbEnterCar;
import com.cygsunri.callback.vo.EnterOutCarQueryDTO;
import com.cygsunri.callback.vo.OutEnterCarVO;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * <p>
 * 上传车辆入场信息表 Mapper 接口
 * </p>
 *
 * @author hanyuan
 * @since 2021-07-30
 */
@Repository
public interface TbEnterCarMapper extends BaseMapper<TbEnterCar> {

    List<OutEnterCarVO> getInEnterCarPageList(EnterOutCarQueryDTO enterOutCarQueryDTO);
    /**
     * 查找某天进场车辆数
     *
     */
    Integer findCountByTime(String time);
}
