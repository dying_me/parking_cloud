package com.cygsunri.callback.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cygsunri.callback.entity.TbOutCarImg;

/**
 * <p>
 * 上传车辆出场图片信息表 Mapper 接口
 * </p>
 *
 * @author hanyuan
 * @since 2021-07-30
 */
public interface TbOutCarImgMapper extends BaseMapper<TbOutCarImg> {

    void deleteBatch(String[] ids);
}
