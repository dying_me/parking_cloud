package com.cygsunri.callback.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cygsunri.callback.entity.TbEnterCarImg;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 上传车辆入场图片信息表 Mapper 接口
 * </p>
 *
 * @author hanyuan
 * @since 2021-07-30
 */
public interface TbEnterCarImgMapper extends BaseMapper<TbEnterCarImg> {

    void deleteBatch(String[] ids);
}
