package com.cygsunri.callback.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 上传车辆入场信息表
 * </p>
 *
 * @author hanyuan
 * @since 2021-07-30
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class TbEnterCar implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * 车辆入场ID
     */
    @TableId(value = "id", type = IdType.INPUT)
    private String id;

    /**
     * 版本号
     */
    private String version;

    /**
     * 应用ID
     */
    private String appid;

    /**
     * 接口方法
     */
    private String method;

    /**
     * 随机字符串
     */
    private String rand;

    /**
     * 签名字符串
     */
    private String sign;

    /**
     * 停车场key
     */
    private String parkKey;

    /**
     * 车牌号
     */
    private String carNo;

    /**
     * 停车订单号
     */
    private String orderNo;

    /**
     * 入场时间
     */
    private Date enterTime;

    /**
     * 车辆类型编码
     */
    private String carType;

    /**
     * 入口车道名称
     */
    private String gateName;

    /**
     * 入口操作员名称
     */
    private String operatorName;

    /**
     * 预约车位订单号
     */
    private String reserveOrderNo;

    /**
     * 入口车辆图片
     */
    private String imgUrl;


}
