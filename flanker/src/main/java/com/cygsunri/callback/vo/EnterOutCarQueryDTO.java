package com.cygsunri.callback.vo;

import com.cygsunri.common.request.BasePageQuery;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 *
 * </p>
 *
 * @author cygsunri
 * @since 2021/8/2
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class EnterOutCarQueryDTO extends BasePageQuery {

    /**
     * 停车场编号
     */
    private String parkingLotId;
    /**
     * 车牌号
     */
    private String carNumber;
    /**
     * 开始时间
     */
    private String beginTime;
    /**
     * 结束时间
     */
    private String endTime;

    /**
     * 是否出场 0 为出场 1 已出场
     */
    private Integer flag;
}
