package com.cygsunri.callback.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 上传车辆入场信息表
 * </p>
 *
 * @author hanyuan
 * @since 2021-07-30
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class OutEnterCarVO implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * 车辆入场ID
     */
    private String id;

    /**
     * 版本号
     */
    private String version;

    /**
     * 应用ID
     */
    private String appid;

    /**
     * 停车场key
     */
    private String parkKey;

    /**
     * 车牌号
     */
    private String carNo;

    /**
     * 停车订单号
     */
    private String orderNo;

    /**
     * 入场时间
     */
    private String enterTime;

    /**
     * 车辆类型编码
     */
    private String carType;

    /**
     * 车辆类型编码字典值释义
     */
    private String carTypeStatus;

    /**
     * 入口车道名称
     */
    private String gateName;

    /**
     * 入口操作员名称
     */
    private String operatorName;

    /**
     * 预约车位订单号
     */
    private String reserveOrderNo;

    /**
     * 入口车辆图片
     */
    private String imgEnterUrl;

    /**
     * 出场数据
     * ======================================================
     */
    /**
     * 出场时间
     */
    private String outTime;

    /**
     * 车辆类型编码
     */
    private String carOutType;


    /**
     * 入口车道名称
     */
    private String gateOutName;

    /**
     * 入口操作员名称
     */
    private String operatorOutName;



    /**
     * 订单总金额
     */
    private BigDecimal totalAmount;

    /**
     * 优惠券GUID
     */
    private String couponKey;

    /**
     * 出口车辆图片
     */
    private String imgOutUrl;

    /**
     * 优惠券金额
     */
    private BigDecimal couponMoney;

    /**
     * 钱包付款金额
     */
    private BigDecimal walletPayMoney;

    /**
     * 免费原因
     */
    private String freeReason;


}
