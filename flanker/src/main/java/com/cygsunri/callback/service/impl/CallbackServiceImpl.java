package com.cygsunri.callback.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.cygsunri.callback.entity.TbEnterCar;
import com.cygsunri.callback.entity.TbEnterCarImg;
import com.cygsunri.callback.entity.TbOutCar;
import com.cygsunri.callback.entity.TbOutCarImg;
import com.cygsunri.callback.mapper.TbEnterCarMapper;
import com.cygsunri.callback.service.*;
import com.cygsunri.callback.vo.EnterOutCarQueryDTO;
import com.cygsunri.callback.vo.OutEnterCarVO;
import com.cygsunri.common.enume.EnumConstant;
import com.cygsunri.common.exception.BusinessException;
import com.cygsunri.common.response.ResultCodeEnum;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;


/**
 * <p>
 * 智慧停车开放平台回调类接口Service
 * </p>
 *
 * @author cygsunri
 * @since 2021/7/29
 */
@Slf4j
@Service
public class CallbackServiceImpl implements CallbackService {


    @Autowired
    private TbEnterCarService tbEnterCarService;

    @Autowired
    private TbOutCarService tbOutCarService;

    @Autowired
    private TbEnterCarImgService tbEnterCarImgService;

    @Autowired
    private TbOutCarImgService tbOutCarImgService;

    @Autowired
    private TbEnterCarMapper tbEnterCarMapper;




    @Override
    public PageInfo<OutEnterCarVO> pageInOutRecordListQuey(EnterOutCarQueryDTO enterOutCarQueryDTO) {
        PageHelper.startPage(enterOutCarQueryDTO.getPageNumber(),enterOutCarQueryDTO.getPageSize());
        List<OutEnterCarVO> list = tbEnterCarMapper.getInEnterCarPageList(enterOutCarQueryDTO);
        list.forEach((item) ->{
            item.setCarTypeStatus(EnumConstant.StatusEnum.getDescFromCode(item.getCarType()));
        });
        return new PageInfo<>(list);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteById(String id) {
        TbEnterCar enterCar = tbEnterCarService.getById(id);
        if(enterCar!=null){
            String parkKey = enterCar.getParkKey();
            String carNo = enterCar.getCarNo();
            String orderNo = enterCar.getOrderNo();
            boolean temp = tbEnterCarService.removeById(enterCar.getId());
            if(!temp){
                throw new BusinessException(ResultCodeEnum.SYS_ERROR.getCode(),"删除车辆入场信息失败,请核实!");
            }
            QueryWrapper<TbEnterCarImg> enterImgWarpper = new QueryWrapper<>();
            if(StringUtils.isNotBlank(parkKey)){
                enterImgWarpper.eq("park_key",parkKey);
            }
            if(StringUtils.isNotBlank(carNo)){
                enterImgWarpper.eq("car_no",carNo);
            }
            if(StringUtils.isNotBlank(orderNo)){
                enterImgWarpper.eq("order_no",orderNo);
            }
            TbEnterCarImg tbEnterCarImg = tbEnterCarImgService.getOne(enterImgWarpper);
            if(tbEnterCarImg!=null){
                boolean flag = tbEnterCarImgService.removeById(tbEnterCarImg.getId());
                if(!flag){
                    throw new BusinessException(ResultCodeEnum.SYS_ERROR.getCode(),"删除车辆入场照片信息失败,请核实!");
                }
            }
            QueryWrapper<TbOutCar> outWarpper = new QueryWrapper<>();
            if(StringUtils.isNotBlank(parkKey)){
                outWarpper.eq("park_key",parkKey);
            }
            if(StringUtils.isNotBlank(carNo)){
                outWarpper.eq("car_no",carNo);
            }
            if(StringUtils.isNotBlank(orderNo)){
                outWarpper.eq("order_no",orderNo);
            }
            TbOutCar tbOutCar = tbOutCarService.getOne(outWarpper);
            if(tbOutCar!=null){
                boolean flag = tbOutCarService.removeById(tbOutCar.getId());
                if(!flag){
                    throw new BusinessException(ResultCodeEnum.SYS_ERROR.getCode(),"删除车辆出场信息失败,请核实!");
                }
            }
            QueryWrapper<TbOutCarImg> outImgWarpper = new QueryWrapper<>();
            if(StringUtils.isNotBlank(parkKey)){
                outImgWarpper.eq("park_key",parkKey);
            }
            if(StringUtils.isNotBlank(carNo)){
                outImgWarpper.eq("car_no",carNo);
            }
            if(StringUtils.isNotBlank(orderNo)){
                outImgWarpper.eq("order_no",orderNo);
            }
            TbOutCarImg tbOutCarImg = tbOutCarImgService.getOne(outImgWarpper);
            if(tbOutCarImg!=null){
                boolean flag = tbOutCarImgService.removeById(tbOutCarImg.getId());
                if(!flag){
                    throw new BusinessException(ResultCodeEnum.SYS_ERROR.getCode(),"删除车辆出场照片信息失败,请核实!");
                }
            }
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteBatchByIds(String[] ids) {
        //删除车辆入场照片信息表
        tbEnterCarImgService.deleteBatch(ids);
        //删除车辆出场信息表
        tbOutCarService.deleteBatch(ids);
        //删除车辆出场照片信息表
        tbOutCarImgService.deleteBatch(ids);
        //删除车辆入场信息表
        tbEnterCarService.removeByIds(Arrays.asList(ids));
    }
}
