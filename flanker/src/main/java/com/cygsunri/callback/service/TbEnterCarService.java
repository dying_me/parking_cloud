package com.cygsunri.callback.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.cygsunri.callback.entity.TbEnterCar;

/**
 * <p>
 * 上传车辆入场信息表 服务类
 * </p>
 *
 * @author hanyuan
 * @since 2021-07-30
 */
public interface TbEnterCarService extends IService<TbEnterCar> {

    /**
     * 查找某天进场车辆数
     *
     */
    Integer findCountByTime(String time);
}
