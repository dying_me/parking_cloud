package com.cygsunri.callback.service;


import com.cygsunri.callback.vo.EnterOutCarQueryDTO;
import com.cygsunri.callback.vo.OutEnterCarVO;
import com.github.pagehelper.PageInfo;


public interface CallbackService {




    PageInfo<OutEnterCarVO> pageInOutRecordListQuey(EnterOutCarQueryDTO enterOutCarQueryDTO);

    void deleteById(String id);

    void deleteBatchByIds(String[] ids);

}
