package com.cygsunri.callback.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.cygsunri.callback.entity.TbOutCar;


/**
 * <p>
 * 上传车辆出场信息表 服务类
 * </p>
 *
 * @author hanyuan
 * @since 2021-07-30
 */
public interface TbOutCarService extends IService<TbOutCar> {

    void deleteBatch(String[] ids);

    /**
     * 查找某天出场车辆数
     *
     */
    Integer findCountByTime(String time);
}
