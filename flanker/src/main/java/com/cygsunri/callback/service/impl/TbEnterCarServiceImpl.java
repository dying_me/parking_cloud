package com.cygsunri.callback.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cygsunri.callback.entity.TbEnterCar;
import com.cygsunri.callback.mapper.TbEnterCarMapper;
import com.cygsunri.callback.service.TbEnterCarService;
import com.cygsunri.multienergy.api.mapper.DayDataMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 上传车辆入场信息表 服务实现类
 * </p>
 *
 * @author hanyuan
 * @since 2021-07-30
 */
@Service
public class TbEnterCarServiceImpl extends ServiceImpl<TbEnterCarMapper, TbEnterCar> implements TbEnterCarService {

    @Autowired
    private TbEnterCarMapper tbEnterCarMapper;

    /**
     * 查找某天进场车辆数
     *
     */
    @Override
    public Integer findCountByTime(String time) {
        return tbEnterCarMapper.findCountByTime(time);
    }
}
