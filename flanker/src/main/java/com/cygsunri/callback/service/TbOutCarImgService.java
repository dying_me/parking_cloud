package com.cygsunri.callback.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.cygsunri.callback.entity.TbOutCarImg;

/**
 * <p>
 * 上传车辆出场图片信息表 服务类
 * </p>
 *
 * @author hanyuan
 * @since 2021-07-30
 */
public interface TbOutCarImgService extends IService<TbOutCarImg> {

    void deleteBatch(String[] ids);
}
