package com.cygsunri.callback.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cygsunri.callback.entity.TbEnterCarImg;
import com.cygsunri.callback.mapper.TbEnterCarImgMapper;
import com.cygsunri.callback.service.TbEnterCarImgService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 上传车辆入场图片信息表 服务实现类
 * </p>
 *
 * @author hanyuan
 * @since 2021-07-30
 */
@Service
public class TbEnterCarImgServiceImpl extends ServiceImpl<TbEnterCarImgMapper, TbEnterCarImg> implements TbEnterCarImgService {

    @Override
    public void deleteBatch(String[] ids) {
        baseMapper.deleteBatch(ids);
    }
}
