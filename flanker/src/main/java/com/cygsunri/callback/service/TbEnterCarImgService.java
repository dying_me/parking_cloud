package com.cygsunri.callback.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.cygsunri.callback.entity.TbEnterCarImg;

/**
 * <p>
 * 上传车辆入场图片信息表 服务类
 * </p>
 *
 * @author hanyuan
 * @since 2021-07-30
 */
public interface TbEnterCarImgService extends IService<TbEnterCarImg> {

    void deleteBatch(String[] ids);
}
