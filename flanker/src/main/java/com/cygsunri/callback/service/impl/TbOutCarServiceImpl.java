package com.cygsunri.callback.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cygsunri.callback.entity.TbOutCar;
import com.cygsunri.callback.mapper.TbEnterCarMapper;
import com.cygsunri.callback.mapper.TbOutCarMapper;
import com.cygsunri.callback.service.TbOutCarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * <p>
 * 上传车辆出场信息表 服务实现类
 * </p>
 *
 * @author hanyuan
 * @since 2021-07-30
 */
@Service
public class TbOutCarServiceImpl extends ServiceImpl<TbOutCarMapper, TbOutCar> implements TbOutCarService {

    @Autowired
    private TbOutCarMapper tbOutCarMapper;


    @Override
    public void deleteBatch(String[] ids) {
        baseMapper.deleteBatch(ids);
    }

    /**
     * 查找某天出场车辆数
     *
     */
    @Override
    public Integer findCountByTime(String time) {
        return tbOutCarMapper.findCountByTime(time);
    }
}
