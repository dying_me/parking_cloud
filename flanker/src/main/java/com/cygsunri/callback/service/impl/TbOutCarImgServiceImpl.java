package com.cygsunri.callback.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cygsunri.callback.entity.TbOutCarImg;
import com.cygsunri.callback.mapper.TbOutCarImgMapper;
import com.cygsunri.callback.service.TbOutCarImgService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 上传车辆出场图片信息表 服务实现类
 * </p>
 *
 * @author hanyuan
 * @since 2021-07-30
 */
@Service
public class TbOutCarImgServiceImpl extends ServiceImpl<TbOutCarImgMapper, TbOutCarImg> implements TbOutCarImgService {

    @Override
    public void deleteBatch(String[] ids) {
        baseMapper.deleteBatch(ids);
    }
}
