package com.cygsunri.callback.controller;

import com.cygsunri.callback.service.CallbackService;
import com.cygsunri.callback.vo.EnterOutCarQueryDTO;
import com.cygsunri.callback.vo.OutEnterCarVO;
import com.cygsunri.common.exception.BusinessException;
import com.cygsunri.common.response.ResultCodeEnum;
import com.cygsunri.common.response.ResultWrapper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
 * <p>
 *
 * </p>
 *
 * @author cygsunri
 * @since 2021/8/2
 */
@Slf4j
@RestController
@RequestMapping("/api/v2/callbackLocal")
public class CallbackLocalController {

    @Autowired
    private CallbackService callbackService;

    /**
     * 查询车辆进出场记录
     * @param enterOutCarQueryDTO
     * @return
     */
    @PostMapping("/inOutRecordList")
    public ResultWrapper<Object> InoutRecordList(@RequestBody EnterOutCarQueryDTO enterOutCarQueryDTO){
        PageInfo<OutEnterCarVO> pageInfo = callbackService.pageInOutRecordListQuey(enterOutCarQueryDTO);
        return ResultWrapper.ok("查询车辆进出场记录信息成功", pageInfo);
    }

    /**
     * 根据id删除车辆进出场记录信息
     * @param id
     * @return
     */
    @GetMapping("deleteById/{id}")
    public ResultWrapper<Object> deleteById(@PathVariable String id){
        if(StringUtils.isBlank(id)){
            throw new BusinessException(ResultCodeEnum.PARAM_VALIDATE_ERROR.getCode(),
                    "ID不能为空,请核实!");
        }
        callbackService.deleteById(id);
        return ResultWrapper.ok("删除车辆进出场记录信息成功");
    }

    /**
     * 批量删除
     * @param ids
     * @return
     */
    @PostMapping("/deleteBatchById")
    public ResultWrapper<Object> deleteBatchByIds(@RequestBody String [] ids){
        callbackService.deleteBatchByIds(ids);
        return ResultWrapper.ok("批量删除车辆进出场记录信息成功");
    }
}
