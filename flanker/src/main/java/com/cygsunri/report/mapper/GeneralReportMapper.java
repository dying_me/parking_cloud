package com.cygsunri.report.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cygsunri.report.entity.GeneralReport;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 */
public interface GeneralReportMapper extends BaseMapper<GeneralReport> {

    List<GeneralReport> getGeneralReportByReportTime(@Param("reportTime") String reportTime);

    List<GeneralReport> getGeneralReportByReportTimeAndIsTimedPrinting(@Param("reportTime") String reportTime, @Param("isTimedPrinting") Boolean isTimedPrinting);
}
