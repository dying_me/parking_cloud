package com.cygsunri.report.api;

import com.alibaba.fastjson.JSONObject;
import com.cygsunri.report.entity.GeneralReport;
import com.cygsunri.report.entity.GeneralReportCatalog;
import com.cygsunri.report.entity.ReportTime;
import com.cygsunri.report.excel2pdf.Excel2Pdf;
import com.cygsunri.report.excel2pdf.ExcelObject;
import com.cygsunri.report.service.GeneralReportService;
import com.cygsunri.util.DateUtil;
import com.cygsunri.util.ExcelConvertToHtml;
import com.cygsunri.util.JxlUtil;
import com.itextpdf.text.DocumentException;
import org.assertj.core.util.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * 报表API
 */
@RestController
@RequestMapping("/api/reports")
public class GeneralReportController {

    @Autowired
    private GeneralReportService generalReportService;

    /**
     * 获取报表列表
     */
    @RequestMapping(value = "/list/users/{users}", method = RequestMethod.GET)
    public ResponseEntity<List<GeneralReportCatalog>> ReportList(@PathVariable("users") String users) {
        List<GeneralReportCatalog> list = Lists.newArrayList();
        for (ReportTime reportTime : ReportTime.values()) {
            List<GeneralReport> generalReports = generalReportService.getGeneralReportByReportTime(reportTime.getName());
            if (generalReports.isEmpty()) continue;
            GeneralReportCatalog generalReportCatalog = new GeneralReportCatalog()
                    .setId(reportTime.getKey())
                    .setName(reportTime.getDesc())
                    .setTimeType(reportTime)
                    .setReports(generalReports);
            list.add(generalReportCatalog);
        }
        return new ResponseEntity<>(list, HttpStatus.OK);
    }


    /**
     * 显示报表
     *
     * @return 报表显示的HTML
     */
    @RequestMapping(value = "/showReport", method = RequestMethod.POST)
    public ResponseEntity<String> showReport(@RequestBody GeneralReport generalReport) throws IOException {
        GeneralReport report = generalReportService.getReport(generalReport.getId());

        if (report == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

//        if(report.getTimeType() == ReportTime.DAY) {
//            Integer month = DateUtil.parse(generalReport.getDate()).getMonth().getValue();
//            if (month >= 7 && month <= 9) {
//                report.setFileName(report.getFileName().replace(".xls", "1.xls"));
//            }
//        }

        report.setDate(generalReport.getDate());
        report.setEndDate(generalReport.getEndDate());
        report.setSubstation(generalReport.getSubstation());

        int result = generalReportService.generateReport(report);

        if (result != 0) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        //打开这个文件,转换成HTML,传递给前台显示
        String excelFileName = System.getProperty("java.io.tmpdir") + File.separator + report.getFileName();
        StringBuffer content = null;
        try {
            content = ExcelConvertToHtml.read(excelFileName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        JSONObject object = new JSONObject();
        object.put("file", report.getFileName());
        object.put("contexts", content);
        return new ResponseEntity<>(JSONObject.toJSONString(object), HttpStatus.OK);
    }

    /**
     * 下载报表
     */
    @RequestMapping(value = "multiReport", method = RequestMethod.GET)
    public void getMultiReport(HttpServletRequest request, HttpServletResponse response) {
        String fileName = request.getParameter("fileName");
        String realName = request.getParameter("realName");
        String path = System.getProperty("java.io.tmpdir") + File.separator + fileName;
        JxlUtil.DownLoadExcel(request, response, path, realName);
    }

    /**
     * 下载报表
     */
    @RequestMapping(value = "multiReport/pdf", method = RequestMethod.GET)
    public void getMultiReportOfPdf(HttpServletRequest request, HttpServletResponse response) {
        try {
            String fileName = request.getParameter("fileName");
            String realName = request.getParameter("realName");

            String date = request.getParameter("date");
            String pathOfXls = System.getProperty("java.io.tmpdir") + File.separator + fileName;
            int index = fileName.indexOf(".");
            String pdfName = fileName.substring(0, index) + "-" + date + ".pdf";
            String pathOfPdf = System.getProperty("java.io.tmpdir") + File.separator + pdfName;

            FileInputStream fis = new FileInputStream(pathOfXls);
            List<ExcelObject> objects = new ArrayList<>();
            objects.add(new ExcelObject("导航", fis));
            FileOutputStream fos = new FileOutputStream(pathOfPdf);
            Excel2Pdf pdf = new Excel2Pdf(objects, fos);
            pdf.convert();

            JxlUtil.DownLoadExcel(request, response, pathOfPdf, pdfName);
        } catch (DocumentException | IOException e) {
            e.printStackTrace();
        }
    }

}
