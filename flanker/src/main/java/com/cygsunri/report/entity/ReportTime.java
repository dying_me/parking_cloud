package com.cygsunri.report.entity;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Getter;

/**
 * 报表时间类型,日、月、年、周
 */
@Getter
public enum ReportTime {
    DAY(0, "DAY", "日报表"), //日
    WEEK(1, "WEEK", "周报表"), //周
    MONTH(2, "MONTH", "月报表"), //月
    YEAR(3, "YEAR", "年报表"); //年

    private int key;

    @EnumValue
    @JsonValue    //标记响应json值
    private String name;

    private String desc;

    ReportTime(int key, String name, String desc) {
        this.key = key;
        this.name = name;
        this.desc = desc;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
