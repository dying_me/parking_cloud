package com.cygsunri.report.entity;

import lombok.Data;

/**
 * 报表的元素
 */
@Data
public class ReportElement {
    //private Integer area; // 区域
    private String substation; //子站ID
    private Integer deviceType; //设备类型,与子站ID配合,进行设备的批量填充
    private boolean showDeviceName; //批量模式时,是否生成一行显示设备名称
    private boolean trimSubstation; //是否从设备名称中去掉厂站名称
    private boolean timeSequence; //时间序列,如每日的小时,每月的日,每年的月
    private boolean portrait; //纵向表格,即横向是设备,纵向是时间

    private String psrID; //设备ID
    private String name; //数据点名称
    private Integer type; //Flag类型

    private String date; //时间
    private String time; //日期
    private ReportTime dateType; //日期类型,日月周年
    private Double slope = 1d; //系数

    private boolean sequenceFilled; //表示序列填充完毕,配置表格时,不填充这个选项,只在序列填充时使用
    private boolean end; //

    private Integer elementType = 0; //缓存标志 0:常规统计数据，1：收益缓存，2：空调缓存
    private Integer cacheType; //对应收益或空调的缓存类
    private String planName; //策略名

    //指定时间的统计量使用
    private String startDate; //开始日期
    private String endDate; //结束日期
    private String startTime; //开始时间
    private String endTime; //结束时间

}
