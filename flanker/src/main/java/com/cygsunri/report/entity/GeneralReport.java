package com.cygsunri.report.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Objects;

/**
 * <p>
 *
 * </p>
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@TableName("tb_generalReport")
public class GeneralReport implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.ASSIGN_UUID)
    private String id;

    private String name; //报表名称,非空,不需要唯一

    @TableField("timeType")
    private ReportTime timeType; //非空,报表类型、日、年、周

    @TableField("fileName")
    private String fileName; //xls模板文件名,非空

    @TableField("hideInTree")
    private Boolean hideInTree; //是否在报表查询的树形菜单中不可见

    @TableField("needSubstation")
    private Boolean needSubstation; //是否需要输入子站ID

    @TableField("indexNumber")
    private Integer indexNumber;

    //不映射数据库
    @TableField(exist = false)
    private String substation;
    @TableField(exist = false)
    private String date;
    @TableField(exist = false)
    private String time;
    @TableField(exist = false)
    private String absoluteFile;
    @TableField(exist = false)
    private String endDate;


    public GeneralReport() {
    }

    public GeneralReport(String name) {
        this.name = name;
    }

    public Boolean needSubstation() {
        return Objects.equals(needSubstation, Boolean.TRUE);
    }

}
