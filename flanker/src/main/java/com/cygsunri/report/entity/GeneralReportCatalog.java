package com.cygsunri.report.entity;

import com.google.common.collect.Lists;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
public class GeneralReportCatalog {
    private int id; //集合名称,非空唯一
    private String name; //集合名称,非空唯一
    private ReportTime timeType; //集合的时间单位,非空
    private List<GeneralReport> reports = Lists.newArrayList();

}
