package com.cygsunri.report.service.picker;

import com.cygsunri.airCondition.entity.AirCacheType;
import com.cygsunri.report.entity.ReportElement;
import com.cygsunri.util.DateUtil;
import org.apache.poi.ss.usermodel.Cell;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;

/**
 * 空调缓存数据接口获取
 */
@Component("AirCachePicker")
public class AirCachePicker extends AbstractDataPicker {

    @Override
    public boolean fillData(ReportElement element, Cell cell) {
        Integer cacheType = element.getCacheType();
        AirCacheType airCacheType = AirCacheType.find(cacheType);
        Double value = null;
        switch (Objects.requireNonNull(airCacheType)) {
            case DAY:
                value = airCacheService.getAirCache(element.getPsrID(), element.getName(), element.getDate());
                break;
            case MONTH:
                List<String> days = DateUtil.getDays(element.getDate());
                value = airCacheService.getAirCacheSum(element.getPsrID(), element.getName(), days.get(0), days.get(days.size() - 1));
                break;
            case YEAR:
                value = airCacheService.getAirCacheSum(element.getPsrID(), element.getName(), element.getDate() + "-01-01", element.getDate() + "-12-31");
                break;
            default:
                return false;
        }

        if (value == null) return false;
        setCellValue(cell, value * element.getSlope());
        return true;
    }
}
