package com.cygsunri.report.service.mediator;

import com.cygsunri.report.entity.GeneralReport;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * 中继器初始化
 */
@Component
public class OverAllMediator extends AbstractReportMediator implements InitializingBean {
    private Map<String, AbstractReportMediator> m;

    @Autowired
    public void setMagicMap(Map<String, AbstractReportMediator> m) {
        this.m = m;
    }

    @Override
    public int mediateInner(GeneralReport report, Workbook workbook) {
        return 0;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        AbstractReportMediator saveMediator = m.get("saveMediator");
        AbstractReportMediator deviceMediator = m.get("deviceMediator");
        AbstractReportMediator dateTimeMediator = m.get("dateTimeMediator");
        AbstractReportMediator constStringMediator = m.get("constStringMediator");
        AbstractReportMediator dataMediator = m.get("dataMediator");

        this.subMediator(deviceMediator)
                .subMediator(dateTimeMediator)
                .subMediator(constStringMediator)
                .subMediator(dataMediator)
                .subMediator(saveMediator);
    }
}
