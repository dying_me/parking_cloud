package com.cygsunri.report.service.mediator;

import com.cygsunri.report.entity.GeneralReport;
import com.cygsunri.report.entity.ReportElement;
import com.cygsunri.scada.base.BasePSR;
import com.cygsunri.scada.service.ScadaPSRService;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 设备中介器
 */
@Component
public class DeviceMediator extends AbstractReportMediator {
    @Autowired
    private ScadaPSRService scadaPSRService;

    @Override
    protected int mediateInner(GeneralReport report, Workbook workbook) {
        boolean found;

        // 每次填充完一行或者一列后,要重新开始迭代。因为纵向填充列时,会使得迭代器失效
        do {
            found = false;
            for (Sheet sheet : workbook) {
                for (Row row : sheet) {
                    for (Cell cell : row) {
                        logger.info("单元格类型: {}", cell.getCellType());

                        if (cell.getCellType() == HSSFCell.CELL_TYPE_STRING) {
                            String context = cell.getStringCellValue();
                            logger.info("单元格内容: {}", context);

                            ReportElement element = makeElement(context);

                            if (element == null || element.getDeviceType() == null) {
                                continue;
                            }
                            if (element.getSubstation() == null && !report.getNeedSubstation()) {
                                continue;
                            }
                            fillDevices(report, sheet, element, cell);
                            found = true;
                        }
                    }

                    if (found) {
                        break;
                    }
                }
            }
        } while (found);

        return 0;
    }

    private void fillDevices(GeneralReport report, Sheet sheet, ReportElement element, Cell cell) {


        String substationID = element.getSubstation();
        BasePSR substation = scadaPSRService.getDeviceByPsrId(substationID);
        if (substationID == null && report.needSubstation()) {
            substationID = report.getSubstation();
            substation = scadaPSRService.getDeviceByPsrId(substationID);
        }

        List<BasePSR> devices = scadaPSRService.getDeviceOfSubstationByType(substationID, element.getDeviceType());

        element.setSubstation(null);
        element.setDeviceType(null);

        boolean portrait = element.isPortrait();

        int row = cell.getRowIndex();
        int col = cell.getColumnIndex();

        for (BasePSR device : devices) {
            if (element.isTrimSubstation() && (report.needSubstation() || substationID != null)) {
                device.setName(device.getName().replace(substation.getName(), ""));
            }

            if (element.isShowDeviceName()) {
                int targetRow, targetCol;

                if (portrait) {
                    targetRow = row;
                    targetCol = cell.getColumnIndex() - 1;
                } else {
                    targetRow = cell.getRowIndex() - 1;
                    targetCol = col;
                }
                setCell(sheet, targetRow, targetCol, device.getName());
            }

            element.setPsrID(device.getId());
            setCell(sheet, row, col, makeString(element));

            if (portrait) {
                row++;
            } else {
                col++;
            }
        }
    }
}
