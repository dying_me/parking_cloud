package com.cygsunri.report.service.picker;

import com.cygsunri.measurement.entity.MeasurementValue;
import com.cygsunri.report.entity.ReportElement;
import org.apache.poi.ss.usermodel.Cell;
import org.springframework.stereotype.Component;

/**
 * 实时数据接口获取
 */
@Component("RealTimePicker")
public class RealTimePicker extends AbstractDataPicker {

    @Override
    public boolean fillData(ReportElement element, Cell cell) {
        MeasurementValue m = commonDataService.getHistoryValue(element.getPsrID(), element.getName(), element.getDate() + " " + element.getTime());
        if (m.isInValid()) {
            return false;
        }
        setCellValue(cell, m.getData() * element.getSlope());
        return true;
    }
}
