package com.cygsunri.report.service.picker;

import com.cygsunri.measurement.entity.Flag;
import com.cygsunri.measurement.entity.MeasurementValue;
import com.cygsunri.report.entity.ReportElement;
import com.cygsunri.util.TimeUtil;
import org.apache.poi.ss.usermodel.Cell;
import org.springframework.stereotype.Component;

/**
 * 统计数据接口获取
 */
@Component("StatisticPicker")
public class StatisticPicker extends AbstractDataPicker {

    @Override
    public boolean fillData(ReportElement element, Cell cell) {

        long time;
        if (element.getType() == Flag.HOUR_DIFF) {
            time = TimeUtil.toMilliSeconds(element.getDate() + " " + element.getTime());
        } else {
            time = TimeUtil.toMilliSeconds(element.getDate());
        }

        MeasurementValue m = commonDataService.getValue(element.getPsrID(), element.getName(), time, element.getType());
        if (m.isInValid()) {
            return false;
        }
        setCellValue(cell, m.getData() * element.getSlope());
        return true;
    }
}
