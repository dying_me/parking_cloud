package com.cygsunri.report.service.impl;

import com.cygsunri.report.service.EmailService;
import com.cygsunri.util.ExcelConvertToHtml;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

@Service
public class EmailServiceImpl implements EmailService {

    @Autowired
    private JavaMailSender javaMailSender;

    @Value("${spring.mail.username}")
    private String from;


    @Override
    public void sendMail(String to, String subject, String text) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(from);
        message.setTo(to);
        message.setSubject(subject);
        message.setText(text);
        javaMailSender.send(message);
    }

    @Override
    public void sendMailWithAttachment(String to, String subject, String text, File attachment) {
        try {
            MimeMessage mimeMessage = javaMailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
            helper.setFrom(from);
            helper.setTo(to);
            helper.setSubject(subject);
            helper.setText(text);
            helper.addAttachment(attachment.getName(), attachment);
            javaMailSender.send(mimeMessage);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void sendRichMail(String to, String subject, String text, File attachment) {
        try {
            MimeMessage mimeMessage = javaMailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);

            StringBuffer content = ExcelConvertToHtml.read(attachment.getPath());
            int index = attachment.getName().indexOf(".");
            String htmlFileName = System.getProperty("java.io.tmpdir") + File.separator + attachment.getName().substring(0, index) + ".html";
            File file = new File(htmlFileName);
            if (!file.exists()) {
                file.createNewFile();
                byte[] data = content.toString().getBytes();
                BufferedOutputStream write = new BufferedOutputStream(new FileOutputStream(file));
                write.write(data);
                write.flush();
                write.close();
            }

            helper.setFrom(from);
            helper.setTo(to);
            helper.setSubject(subject);
            helper.setText(text, true);
            helper.addInline("qr", new FileSystemResource(htmlFileName));
            helper.addAttachment(attachment.getName(), attachment);
            javaMailSender.send(mimeMessage);

        } catch (MessagingException | IOException e) {
            e.printStackTrace();
        }
    }
}
