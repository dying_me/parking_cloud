package com.cygsunri.report.service.mediator;

import com.cygsunri.report.entity.GeneralReport;
import com.cygsunri.report.entity.ReportElement;
import com.cygsunri.report.service.picker.OverAllDataPicker;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 填充数据中介器
 */
@Component
public class DataMediator extends AbstractReportMediator {
    @Autowired
    private OverAllDataPicker overAllDataPicker;

    @Override
    protected int mediateInner(GeneralReport report, Workbook workbook) {
        for (Sheet sheet : workbook) {
            for (Row row : sheet) {
                for (Cell cell : row) {
                    logger.info("单元格类型: {}", cell.getCellType());

                    if (cell.getCellType() == HSSFCell.CELL_TYPE_STRING) {
                        String context = cell.getStringCellValue();
                        logger.info("单元格内容: {}", context);

                        ReportElement element = makeElement(context);

                        if (element == null) {
                            continue;
                        }

                        overAllDataPicker.fillData(element, cell);
                    }
                }
            }
        }
        return 0;
    }
}
