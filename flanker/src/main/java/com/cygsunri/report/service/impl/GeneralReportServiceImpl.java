package com.cygsunri.report.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cygsunri.report.entity.GeneralReport;
import com.cygsunri.report.mapper.GeneralReportMapper;
import com.cygsunri.report.service.GeneralReportService;
import com.cygsunri.report.service.mediator.OverAllMediator;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 */
@Service
public class GeneralReportServiceImpl extends ServiceImpl<GeneralReportMapper, GeneralReport> implements GeneralReportService {

    @Autowired
    private OverAllMediator overAllMediator;

    @Autowired
    private GeneralReportMapper generalReportMapper;

    @Override
    public int generateReport(GeneralReport report) throws IOException {
        //查询内置report文件位置方法
        ClassPathResource resource = new ClassPathResource("report/" + report.getFileName());
        InputStream ips = resource.getInputStream();

        //查询外置report文件位置方法
        //ApplicationHome h = new ApplicationHome(getClass());
        //File jarF = h.getSource();
        //InputStream ips = new FileInputStream(jarF.getParentFile().getPath() + "/report/" + report.getFileName());

        Workbook wb = new HSSFWorkbook(ips);
        wb.setForceFormulaRecalculation(true);
        return overAllMediator.mediate(report, wb);
    }

    @Override
    public GeneralReport getReport(String id) {
        QueryWrapper<GeneralReport> queryWrapper = new QueryWrapper<GeneralReport>()
                .eq("id", id);
        return generalReportMapper.selectOne(queryWrapper);
    }

    @Override
    public List<GeneralReport> getGeneralReportByReportTime(String reportTime) {
        return generalReportMapper.getGeneralReportByReportTime(reportTime);
    }

    @Override
    public List<GeneralReport> getGeneralReportByReportTimeAndIsTimedPrinting(String reportTime, Boolean isTimedPrinting) {
        return generalReportMapper.getGeneralReportByReportTimeAndIsTimedPrinting(reportTime, isTimedPrinting);
    }
}
