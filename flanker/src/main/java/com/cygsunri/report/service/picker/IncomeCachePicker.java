package com.cygsunri.report.service.picker;

import com.cygsunri.income.entity.IncomeCache;
import com.cygsunri.report.entity.ReportElement;
import com.cygsunri.util.DateUtil;
import org.apache.poi.ss.usermodel.Cell;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 缓存数据接口获取
 */
@Component("IncomeCachePicker")
public class IncomeCachePicker extends AbstractDataPicker {

    @Override
    public boolean fillData(ReportElement element, Cell cell) {
        Integer cacheType = element.getCacheType();
        IncomeCache incomeCache = null;
        switch (cacheType) {
            case 0:
            case 1:
            case 2:
            case 3:
                incomeCache = incomeCacheService.getIntervalIncomeCache(element.getPsrID(), element.getName(), element.getDate(), element.getTime(), element.getCacheType(), element.getPlanName());
                break;
            case 4:
            case 5:
            case 6:
            case 7:
                incomeCache = incomeCacheService.getDayIncomeCache(element.getPsrID(), element.getName(), element.getDate(), element.getCacheType(), element.getPlanName());
                break;
            case 8:
            case 9:
            case 10:
            case 11:
                List<String> days = DateUtil.getDays(element.getDate());
                incomeCache = incomeCacheService.getMonthIncomeCache(element.getPsrID(), element.getName(), days.get(0), days.get(days.size() - 1), element.getCacheType(), element.getPlanName());
                break;
            default:
                return false;
        }

        if (incomeCache == null) return false;
        setCellValue(cell, incomeCache.getData() * element.getSlope());
        return true;
    }
}
