package com.cygsunri.report.service;

import java.io.File;

public interface EmailService {
    /**
     * 发送纯文本邮件.
     *
     * @param to      目标email 地址
     * @param subject 邮件主题
     * @param text    纯文本内容
     */
    void sendMail(String to, String subject, String text);

    /**
     * 发送邮件并携带附件.
     * 请注意 from 、 to 邮件服务器是否限制邮件大小
     *
     * @param to       目标email 地址
     * @param subject  邮件主题
     * @param text     纯文本内容
     * @param attachment 附件 当然你可以改写传入文件
     */
    void sendMailWithAttachment(String to, String subject, String text, File attachment);

    /**
     * 发送html邮件.
     *
     * @param to       目标email 地址
     * @param subject  邮件主题
     * @param text     纯文本内容
     * @param attachment 附件
     */
    void sendRichMail(String to, String subject, String text, File attachment);
}
