package com.cygsunri.report.service.picker;

import com.cygsunri.measurement.entity.Flag;
import com.cygsunri.report.entity.ReportElement;
import com.google.common.base.Preconditions;
import org.apache.poi.ss.usermodel.Cell;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * 通用的接口获取
 */
@Component
public class OverAllDataPicker extends AbstractDataPicker {
    private Map<String, AbstractDataPicker> pickers;

    @Autowired
    public void setPickers(Map<String, AbstractDataPicker> pickers) {
        this.pickers = pickers;
    }

    @Override
    public boolean fillData(ReportElement element, Cell cell) {
        Preconditions.checkNotNull(element);
        Preconditions.checkNotNull(cell);
        // Preconditions.checkNotNull(element.getType());
        Preconditions.checkNotNull(element.getPsrID());
        Preconditions.checkNotNull(element.getName());
        Preconditions.checkNotNull(element.getDate());
        AbstractDataPicker picker;
        if (element.getElementType() == 0) {
            if (element.getType() == Flag.REAL_TIME) {
                picker = pickers.get("RealTimePicker");
            } else if (element.getType() == Flag.PERIOD_AVERAGE || element.getType() == Flag.PERIOD_MAX || element.getType() == Flag.PERIOD_MIN || element.getType() == Flag.PERIOD_DIFF) {
                picker = pickers.get("PeriodStatisticPicker");
            } else {
                picker = pickers.get("StatisticPicker");
            }
        } else if (element.getElementType() == 1) {
            picker = pickers.get("IncomeCachePicker");

        } else if (element.getElementType() == 2) {
            picker = pickers.get("AirCachePicker");
        } else {
            return false;
        }

        boolean success = picker.fillData(element, cell);
        if (!success) {
            cell.setCellType(Cell.CELL_TYPE_BLANK);
        }
        return true;
    }
}
