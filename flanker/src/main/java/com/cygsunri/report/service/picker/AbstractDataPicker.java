package com.cygsunri.report.service.picker;

import com.cygsunri.airCondition.service.AirCacheService;
import com.cygsunri.common.service.CommonDataService;
import com.cygsunri.income.service.IncomeCacheService;
import com.cygsunri.report.entity.ReportElement;
import org.apache.poi.ss.usermodel.Cell;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 抽象的数据读取器
 * 用通用接口获取
 */
public abstract class AbstractDataPicker {

    @Autowired
    CommonDataService commonDataService;
    @Autowired
    IncomeCacheService incomeCacheService;
    @Autowired
    AirCacheService airCacheService;

    public abstract boolean fillData(ReportElement element, Cell cell);

    void setCellValue(Cell cell, Double value) {
        cell.setCellValue(Double.valueOf(String.format("%.2f", value)));
    }
}
