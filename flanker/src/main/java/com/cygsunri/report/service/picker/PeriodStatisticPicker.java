package com.cygsunri.report.service.picker;

import com.cygsunri.measurement.entity.Flag;
import com.cygsunri.measurement.entity.MeasurementValue;
import com.cygsunri.report.entity.ReportElement;
import com.cygsunri.util.TimeUtil;
import org.apache.poi.ss.usermodel.Cell;
import org.springframework.stereotype.Component;

/**
 * 指定时间段统计数据接口获取
 */
@Component("PeriodStatisticPicker")
public class PeriodStatisticPicker extends AbstractDataPicker {

    @Override
    public boolean fillData(ReportElement element, Cell cell) {

        String start = TimeUtil.convertTime((element.getStartDate() == null ? element.getDate() : element.getStartDate()) + " " + element.getStartTime());
        String end = TimeUtil.convertTime((element.getEndDate() == null ? element.getDate() : element.getEndDate()) + " " + element.getEndTime());

        MeasurementValue m = MeasurementValue.fromNameMode(element.getPsrID(), element.getName()).setFlag(Flag.getFlag(element.getType()));
        m = commonDataService.getStatisticValue(m, start, end);
        if (m.isInValid()) {
            return false;
        }
        setCellValue(cell, m.getData() * element.getSlope());
        return true;
    }
}
