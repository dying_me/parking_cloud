package com.cygsunri.report.service.mediator;


import com.cygsunri.report.entity.GeneralReport;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileOutputStream;

/**
 * 文件获取中继器
 */
@Component
public class SaveMediator extends AbstractReportMediator {
    @Override
    protected int mediateInner(GeneralReport report, Workbook workbook) {
        try {
            FileOutputStream fOut = new FileOutputStream(System.getProperty("java.io.tmpdir") + File.separator + report.getFileName());
            workbook.write(fOut);
            fOut.close();
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }

        return 0;
    }
}
