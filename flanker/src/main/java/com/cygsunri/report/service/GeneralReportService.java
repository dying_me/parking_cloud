package com.cygsunri.report.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cygsunri.report.entity.GeneralReport;

import java.io.IOException;
import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 */
public interface GeneralReportService extends IService<GeneralReport> {

    /**
     * 通用生成报表
     */
    int generateReport(GeneralReport report) throws IOException;

    /**
     * 根据ID获取报表信息
     *
     * @param id 报表id
     */
    GeneralReport getReport(String id);

    /**
     * 根据时间类型获取报表
     *
     * @param reportTime 报表时间类型
     */
    List<GeneralReport> getGeneralReportByReportTime(String reportTime);

    /**
     * 根据时间类型及是否定时打印获取报表
     *
     * @param reportTime 报表时间类型
     * @param isTimedPrinting 是否定时打印
     */
    List<GeneralReport> getGeneralReportByReportTimeAndIsTimedPrinting(String reportTime, Boolean isTimedPrinting);
}
