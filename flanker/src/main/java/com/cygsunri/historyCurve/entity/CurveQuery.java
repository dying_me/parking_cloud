package com.cygsunri.historyCurve.entity;

import com.cygsunri.measurement.entity.MeasurementInfo;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 历史查询请求参数
 */
@Data
@Accessors(chain = true)
public class CurveQuery {
    private List<MeasurementInfo> measurementParams;
    private String startTime;
    private String endTime;
    private Integer step;
    private Boolean mutation = false;  //突变量查询标识
}
