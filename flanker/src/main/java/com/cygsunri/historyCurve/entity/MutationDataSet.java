package com.cygsunri.historyCurve.entity;

import lombok.Data;

/**
 * 突变数据返回格式
 */
@Data
public class MutationDataSet {
    private String name;
    private String unit;
    private String[] value;
}
