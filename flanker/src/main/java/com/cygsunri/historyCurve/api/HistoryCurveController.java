package com.cygsunri.historyCurve.api;

import com.alibaba.fastjson.JSON;
import com.cygsunri.common.service.CommonDataService;
import com.cygsunri.history.service.MutationDataService;
import com.cygsunri.historyCurve.entity.CurveQuery;
import com.cygsunri.historyCurve.entity.MutationDataSet;
import com.cygsunri.measurement.entity.MeasurementInfo;
import com.cygsunri.measurement.entity.MeasurementValue;
import com.github.abel533.echarts.Legend;
import com.github.abel533.echarts.Option;
import com.github.abel533.echarts.axis.CategoryAxis;
import com.github.abel533.echarts.series.Line;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 历史查询api
 */
@RestController
@RequestMapping("/api/history")
public class HistoryCurveController {

    @Autowired
    private CommonDataService commonDataService;

    @Autowired
    private MutationDataService mutationDataService;


    /**
     * 历史数据查询
     */
    @RequestMapping(value = "/curve/multi", method = RequestMethod.POST)
    public ResponseEntity<String> getHistoryCurve(@RequestBody CurveQuery query) {
        Option option = new Option();
        CategoryAxis categoryAxis = new CategoryAxis();
        option.xAxis(categoryAxis);

        Legend legend = new Legend();
        option.legend(legend);

        String startTime = query.getStartTime();
        String endTime = query.getEndTime();
        Integer step = query.getStep();
        Boolean mutation = query.getMutation();

        int first = 0, current = 0;
        for (MeasurementInfo info : query.getMeasurementParams()) {
            Line line = new Line(info.getComments());
            option.series(line);
            legend.data(info.getComments());
            if (mutation) {
                List<MeasurementValue> list = mutationDataService.getMutationValue(info.getMeasurementID(), startTime, endTime, 1);
                for (MeasurementValue m : list) {
                    MutationDataSet mutationDataSet = new MutationDataSet();
                    mutationDataSet.setName(m.getDate() + " " + m.getTime());
                    String[] array = new String[2];
                    array[0] = m.getDate() + " " + m.getTime();
                    array[1] = m.isInValid() ? "-" : m.getData().toString();
                    mutationDataSet.setValue(array);
                    line.data(mutationDataSet);
                }

            } else {
                current++;
                List<MeasurementValue> list = commonDataService.getHistoryValue(info.getMeasurementID(), startTime, endTime, step, 2, false);
                if (!list.isEmpty()) {
                    if (first == 0) {
                        first = current;
                    }
                    for (MeasurementValue m : list) {
                        if (first == current) {
                            categoryAxis.data(m.getDate() + " " + m.getTime());
                        }
                        line.data(m.isInValid() ? "-" : m.getData());
                    }
                }
            }
        }
        return new ResponseEntity<>(JSON.toJSONString(option), HttpStatus.OK);
    }
}
